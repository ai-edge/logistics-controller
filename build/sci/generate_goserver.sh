#!/bin/sh
java -jar ../../third_party/openapi-codegen/openapi-generator-cli.jar \
generate -i ../../api/base/sci-restapi.yaml -g go-server \
-o ../../pkg/sci/httpsrv -c ../../third_party/openapi-codegen/go-sci-config.json