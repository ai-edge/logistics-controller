#!/bin/sh
export CONTEXT=$GOPATH/src/equipment-sim
DOCKER_BUILDKIT=1 docker build --rm -f $CONTEXT/build/operations/Dockerfile -t registry.gitlab.com/ecs-kube/equipment-sim/operations:latest $CONTEXT