#!/bin/sh
export CONTEXT=$GOPATH/src/equipment-sim
DOCKER_BUILDKIT=1 docker build --rm -f $CONTEXT/build/gs/Dockerfile -t registry.gitlab.com/ecs-kube/equipment-sim/gs:latest $CONTEXT