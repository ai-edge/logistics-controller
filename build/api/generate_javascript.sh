#!/bin/sh
java -jar ../../third_party/openapi-codegen/openapi-generator-cli.jar \
generate -i ../../api/base/operations-restapi.yaml -g javascript \
-o ../../api/generated/javascript -c ../../third_party/openapi-codegen/models-config-javascript.json 
