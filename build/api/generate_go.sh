#!/bin/sh
	  
java -jar ../../third_party/openapi-codegen/openapi-generator-cli.jar \
generate -i ../../api/base/operations-restapi.yaml -g go \
-o ../../api/generated/models -c ../../third_party/openapi-codegen/models-config.json \
-Dmodels --ignore-file-override=openapi-generator-ignore 

java -jar ../../third_party/openapi-codegen/openapi-generator-cli.jar \
generate -i ../../api/base/tosgw-restapi.yaml -g go \
-o ../../api/generated/models -c ../../third_party/openapi-codegen/models-config.json \
-Dmodels --ignore-file-override=openapi-generator-ignore 

java -jar ../../third_party/openapi-codegen/openapi-generator-cli.jar \
generate -i ../../api/base/sci-restapi.yaml -g go \
-o ../../api/generated/models -c ../../third_party/openapi-codegen/models-config.json \
-Dmodels --ignore-file-override=openapi-generator-ignore 

java -jar ../../third_party/openapi-codegen/openapi-generator-cli.jar \
generate -i ../../api/base/scheduler-restapi.yaml -g go \
-o ../../api/generated/models -c ../../third_party/openapi-codegen/models-config.json \
-Dmodels --ignore-file-override=openapi-generator-ignore 

java -jar ../../third_party/openapi-codegen/openapi-generator-cli.jar \
generate -i ../../api/base/dispatcher-restapi.yaml -g go \
-o ../../api/generated/models -c ../../third_party/openapi-codegen/models-config.json \
-Dmodels --ignore-file-override=openapi-generator-ignore 

java -jar ../../third_party/openapi-codegen/openapi-generator-cli.jar \
generate -i ../../api/base/ht-restapi.yaml -g go \
-o ../../api/generated/models -c ../../third_party/openapi-codegen/models-config.json \
-Dmodels --ignore-file-override=openapi-generator-ignore 