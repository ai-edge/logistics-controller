#!/bin/sh
java -jar ../../third_party/openapi-codegen/openapi-generator-cli.jar \
generate -i ../../api/base/operations-restapi.yaml  -g typescript-axios \
-o ../../api/generated/axios-operations -c ../../third_party/openapi-codegen/models-config-typescript-axios-operations.json

java -jar ../../third_party/openapi-codegen/openapi-generator-cli.jar \
generate -i ../../api/base/tosgw-restapi.yaml  -g typescript-axios \
-o ../../api/generated/axios-tosgw -c ../../third_party/openapi-codegen/models-config-typescript-axios-tosgw.json