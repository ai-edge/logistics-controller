This project simulates logistics flow in container terminals and warehouses.

### Documentation

![ISA95 Levels](https://gitlab.com/ai-fusion/logistics-controller/-/blob/master/docs/ISA%2095%20Levels.png?ref_type=heads)

![Terminal Layout](https://gitlab.com/ai-fusion/logistics-controller/-/blob/master/docs/Terminal%20Layout.png?ref_type=heads)

![Sequence Diagram Sideloading Truck to Yard] (https://gitlab.com/ai-fusion/logistics-controller/-/blob/master/docs/mermaid/sequence-happyday-sideloading-truck-to-yard_cust.md?ref_type=heads)

![Documents](docs?raw=true "Documents")

### URL's
*  https://terminalsim.asuscomm.com (currently offline)
