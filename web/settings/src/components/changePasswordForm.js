import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

export default function changePasswordForm(props) {
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");

  const handleChangeOldPassword = (event) => {
    setOldPassword(event.target.value);
  };
  const handleChangeNewPassword = (event) => {
    setNewPassword(event.target.value);
  };

  return (
    <div>
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Change Password</DialogTitle>
        <DialogContent>
          <DialogContentText>Enter the old and new passwords</DialogContentText>
          <TextField
            margin="dense"
            id="oldPassword"
            label="OldPassword"
            type="password"
            onChange={handleChangeOldPassword}
            fullWidth
          />
          <TextField
            margin="dense"
            id="newPassword"
            label="NewPassword"
            type="password"
            onChange={handleChangeNewPassword}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.handleCancelChangePassword} color="primary">
            Cancel
          </Button>
          <Button
            onClick={() => props.handleChangePassword(oldPassword, newPassword)}
            color="primary"
          >
            Change Password
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
