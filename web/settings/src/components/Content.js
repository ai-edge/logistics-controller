import React, { useState, useEffect } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import RefreshIcon from "@material-ui/icons/Refresh";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import AddUserForm from "./addUserForm";
import ChangePasswordForm from "./changePasswordForm";
import CreateMessage from "./createMessage";

const useStyles = makeStyles(
  (theme) => ({
    paper: {
      maxWidth: 936,
      margin: "auto",
      overflow: "hidden",
    },
    tableUser: {
      color: "textSecondary",
      margin: 10,
      maxWidth: 700,
    },
    searchBar: {
      borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
    },
    searchInput: {
      fontSize: theme.typography.fontSize,
    },
    block: {
      display: "block",
    },
    addUser: {
      marginRight: theme.spacing(1),
    },
    contentWrapper: {
      margin: "40px 16px",
    },
    containerGrid: {
      justify: "center",
      spacing: theme.spacing(2),
    },
  }),
  { name: "MuiEcsAuthContent" }
);

function Content(props) {
  const classes = useStyles();
  const [showAddUser, setShowAddUser] = useState(false);
  const [userList, setUserList] = useState([]);
  const [currentUser, setCurrentUser] = useState(null);
  const [currentUserEmail, setCurrentUserEmail] = useState("");
  const [showChangePassword, setShowChangePassword] = useState(false);
  const [messageText, setMessageText] = useState("");
  const [messageHeader, setMessageHeader] = useState("");
  const [showMessageBox, setShowMessageBox] = useState(false);

  const getAllUsers = functions.httpsCallable("getAllUsers");

  useEffect(() => {
    var user = auth.currentUser;
    if (user != null) {
      setCurrentUser(user);
      setCurrentUserEmail(user.email);
    }
  }, []);

  useEffect(() => {
    if (props.isAdmin) {
      getAllUsers(50)
        .then((result) => {
          setUserList(result.data.users);
        })
        .catch((error) => console.log(error));
    } else {
      setUserList([]);
    }
  }, [props.isAdmin]);

  const onAddUserClicked = () => {
    setShowAddUser(true);
  };
  const handleCancelAddUser = () => {
    setShowAddUser(false);
  };
  const handleAddUser = (email, password) => {
    setShowAddUser(false);
    const createUser = functions.httpsCallable("createUser");
    createUser({
      email: email,
      password: password,
    })
      .then(
        function (userRecord) {
          // See the UserRecord reference doc for the contents of userRecord.
          setMessageHeader("Success");
          setMessageText(userRecord.data.message);
          setShowMessageBox(true);
          getAllUsers(50)
            .then((result) => {
              setUserList(result.data.users);
            })
            .catch((error) => console.log(error));
        },
        (error) => {
          setMessageHeader(error.code);
          setMessageText(error.message);
          setShowMessageBox(true);
        }
      )
      .catch(function (error) {
        console.log("Error creating new user:", error);
      });
  };

  const onChangePasswordClicked = () => {
    setShowChangePassword(true);
  };

  const handleCancelChangePassword = () => {
    setShowChangePassword(false);
  };

  const reauthenticate = (currentPassword) => {
    var cred = firebase.auth.EmailAuthProvider.credential(
      currentUser.email,
      currentPassword
    );
    return currentUser.reauthenticateWithCredential(cred);
  };

  const handleChangePassword = (oldPassword, newPassword) => {
    setShowChangePassword(false);
    reauthenticate(oldPassword)
      .then(() => {
        currentUser
          .updatePassword(newPassword)
          .then(() => {
            setMessageHeader("Success");
            setMessageText("Password is updated.");
            setShowMessageBox(true);
          })
          .catch((error) => {
            setMessageHeader(error.code);
            setMessageText(error.message);
            setShowMessageBox(true);
          });
      })
      .catch((error) => {
        setMessageHeader(error.code);
        setMessageText(error.message);
        setShowMessageBox(true);
      });
  };

  const handleMessageClose = () => {
    setShowMessageBox(false);
  };

  const onChangeAdminState = (user) => {
    // get user admin level and change the status
    const addAdminRole = functions.httpsCallable("addAdminRole");
    addAdminRole({ email: user.email }).then(
      (result) => {
        setMessageHeader("Success");
        setMessageText(result.data.message);
        setShowMessageBox(true);
      },
      (error) => {
        setMessageHeader(error.code);
        setMessageText(error.message);
        setShowMessageBox(true);
      }
    );
  };

  const onRemoveUser = (user) => {
    // remove the user
    const deleteUser = functions.httpsCallable("deleteUser");
    deleteUser(user.uid, user.email).then(
      (result) => {
        setMessageHeader("Success");
        setMessageText(result.data.message);
        setShowMessageBox(true);
        getAllUsers(50)
          .then((result) => {
            setUserList(result.data.users);
          })
          .catch((error) => console.log(error));
      },
      (error) => {
        setMessageHeader(error.code);
        setMessageText(error.message);
        setShowMessageBox(true);
      }
    );
  };

  return (
    <div>
      <TableContainer>
        <Table className={classes.tableUser} aria-label="user data">
          <TableBody>
            <TableRow key="current-user">
              <TableCell align="right">
                <b>Current User:</b>
              </TableCell>
              <TableCell align="left"> {currentUserEmail}</TableCell>
              <TableCell align="center">
                <Button
                  variant="outlined"
                  color="primary"
                  className={classes.addUser}
                  onClick={onChangePasswordClicked}
                >
                  Change Password
                </Button>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
      {props.isAdmin ? (
        <Paper className={classes.paper}>
          <AppBar
            className={classes.searchBar}
            position="static"
            color="default"
            elevation={0}
          >
            <Toolbar>
              <Grid container className={classes.containerGrid}>
                <Grid item>
                  <SearchIcon className={classes.block} color="inherit" />
                </Grid>
                <Grid item xs>
                  <TextField
                    fullWidth
                    placeholder="Search by email address, phone number, or user UID"
                    InputProps={{
                      disableUnderline: true,
                      className: classes.searchInput,
                    }}
                  />
                </Grid>
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.addUser}
                    onClick={onAddUserClicked}
                  >
                    Add user
                  </Button>
                  <Tooltip title="Reload">
                    <IconButton>
                      <RefreshIcon className={classes.block} color="inherit" />
                    </IconButton>
                  </Tooltip>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <div className={classes.contentWrapper}>
            <TableContainer>
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="left">
                      <b>Identifier</b>
                    </TableCell>
                    <TableCell align="center"></TableCell>
                    <TableCell align="center"></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {userList.map((user, index) => (
                    <TableRow key={index}>
                      <TableCell align="left">{`${user.email}`}</TableCell>
                      <TableCell align="center">
                        <Button
                          variant="outlined"
                          color="primary"
                          className={classes.addUser}
                          onClick={() => onChangeAdminState(user)}
                        >
                          Set As Admin
                        </Button>
                      </TableCell>
                      <TableCell align="center">
                        <Button
                          variant="outlined"
                          color="primary"
                          className={classes.addUser}
                          onClick={() => onRemoveUser(user)}
                        >
                          Remove User
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        </Paper>
      ) : null}
      <div>
        <AddUserForm
          open={showAddUser}
          handleCancelAddUser={handleCancelAddUser}
          handleAddUser={handleAddUser}
        />
        <ChangePasswordForm
          open={showChangePassword}
          handleCancelChangePassword={handleCancelChangePassword}
          handleChangePassword={handleChangePassword}
        />
        <CreateMessage
          open={showMessageBox}
          handleClose={handleMessageClose}
          messageText={messageText}
          messageHeader={messageHeader}
        />
      </div>
    </div>
  );
}

export default Content;
