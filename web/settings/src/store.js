import { createStore } from "redux";
import settingsReducer from "./reducers/settingsReducer";

export const storeInstance = createStore(settingsReducer);
