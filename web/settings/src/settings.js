import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { BrowserRouter, Redirect } from "react-router-dom";
import Content from "./components/Content";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";

import {
  updateHeaderText,
  updateSelectedTab,
  updatePageLoading,
} from "./actions/settingsActions";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Equipment Sim
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(
  (theme) => ({
    main: {
      flex: 1,
      padding: theme.spacing(6, 4),
      background: "#eaeff1",
    },
    footer: {
      padding: theme.spacing(2),
      background: "#eaeff1",
    },
    app: {
      flex: 1,
      display: "flex",
      flexDirection: "column",
    },
  }),
  { name: "MuiEcsAuthorization" }
);

function Settings(props) {
  const classes = useStyles();

  useEffect(() => {
    props.globalEventDistributor.dispatch(props.updateHeaderText("SETTINGS"));
    props.globalEventDistributor.dispatch(props.updateSelectedTab(3));
    // page is loaded
    props.globalEventDistributor.dispatch(props.updatePageLoading(false));
  }, []);

  const renderRedirect = () => {
    if (!props.isLoggedIn || props.isLoggedIn === "false") {
      return (
        <BrowserRouter>
          <Redirect from="/#/settings" to="/#/login" />
        </BrowserRouter>
      );
    }
  };

  return (
    <div className={classes.app}>
      {renderRedirect()}
      <main className={classes.main}>
        <Content isAdmin={props.isAdmin} />
      </main>
      <footer className={classes.footer}>
        <Copyright />
      </footer>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    isLoggedIn: state.isLoggedIn,
    isAdmin: state.isAdmin,
  };
}

const mapDispatchToProps = {
  updateHeaderText,
  updateSelectedTab,
  updatePageLoading,
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
