import { LOGIN, LOGOUT, SET_ADMIN } from "../actions/settingsActions";
import produce from "immer";

const initialState = {
  isLoggedIn: localStorage.getItem("isLoggedIn") || false,
  isAdmin: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return produce(state, (draft) => {
        localStorage.setItem("isLoggedIn", true);
        draft.isLoggedIn = true;
      });
    case LOGOUT:
      return produce(state, (draft) => {
        localStorage.setItem("isLoggedIn", false);
        draft.isLoggedIn = false;
      });
    case SET_ADMIN:
      return produce(state, (draft) => {
        draft.isAdmin = action.payload;
      });
    default:
      return state;
  }
}
