const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

exports.addAdminRole = functions.https.onCall((data, context) => {
  // get user and add custom claim (admin for this case)
  return admin
    .auth()
    .getUserByEmail(data.email)
    .then((user) => {
      return admin.auth().setCustomUserClaims(user.uid, {
        admin: true,
      });
    })
    .then(() => {
      return {
        message: `Success! ${data.email} has been made admin`,
      };
    })
    .catch((err) => {
      return err;
    });
});

exports.getAllUsers = functions.https.onCall((maxResults) => {
  return admin.auth().listUsers(maxResults);
});

exports.deleteUser = functions.https.onCall((uid, email) => {
  return admin
    .auth()
    .deleteUser(uid)
    .then(() => {
      return {
        message: `Successfully deleted user`,
      };
    })
    .catch((err) => {
      return err;
    });
});

exports.createUser = functions.https.onCall((uid, user) => {
  return admin
    .auth()
    .createUser(uid, user)
    .then((userRecord) => {
      return {
        message: `Success! ${userRecord.email} has been created`,
      };
    })
    .catch((err) => {
      return err;
    });
});

exports.updateUser = functions.https.onCall((uid, user) => {
  return admin
    .auth()
    .updateUser(uid, user)
    .then((userRecord) => {
      return {
        message: `Successfully updated user: ${userRecord.email}`,
      };
    })
    .catch((err) => {
      return err;
    });
});
