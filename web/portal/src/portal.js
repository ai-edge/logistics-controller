import "zone.js";
import * as singleSpa from "single-spa";
import { GlobalEventDistributor } from "./globalEventDistributor";
import { loadApp } from "./helper";

function hashPrefix(prefix) {
  return function (location) {
    return location.hash.startsWith(`#${prefix}`);
  };
}

function navActive() {
  return function () {
    return true;
  };
}

async function init() {
  const globalEventDistributor = new GlobalEventDistributor();
  const loadingPromises = [];

  // operations: The URL "/operations/..." is being redirected to "http://localhost:9001/..." this is done by the webpack proxy (webpack.config.js)
  loadingPromises.push(
    loadApp(
      "operations",
      hashPrefix("/operations"),
      "/operations/singleSpaEntry.js",
      "/operations/store.js",
      globalEventDistributor
    )
  );

  // planning: The URL "/planning/..." is being redirected to "http://localhost:9002/..." this is done by the webpack proxy (webpack.config.js)
  loadingPromises.push(
    loadApp(
      "planning",
      hashPrefix("/planning"),
      "/planning/singleSpaEntry.js",
      "/planning/store.js",
      globalEventDistributor
    )
  );

  // settings: The URL "/settings/..." is being redirected to "http://localhost:9003/..." this is done by the webpack proxy (webpack.config.js)
  loadingPromises.push(
    loadApp(
      "settings",
      hashPrefix("/settings"),
      "/settings/singleSpaEntry.js",
      "/settings/store.js",
      globalEventDistributor
    )
  );

  // nav: The URL "/nav/..." is being redirected to "http://localhost:9004/..." this is done by the webpack proxy (webpack.config.js)
  loadingPromises.push(
    loadApp(
      "nav",
      navActive,
      "/nav/singleSpaEntry.js",
      "nav/store.js",
      globalEventDistributor
    )
  );

  // login: The URL "/login/..." is being redirected to "http://localhost:9005/..." this is done by the webpack proxy (webpack.config.js)
  loadingPromises.push(
    loadApp(
      "login",
      hashPrefix("/login"),
      "/login/singleSpaEntry.js",
      "/login/store.js",
      globalEventDistributor
    )
  );

  // about: The URL "/about/..." is being redirected to "http://localhost:9006/..." this is done by the webpack proxy (webpack.config.js)
  loadingPromises.push(
    loadApp(
      "about",
      hashPrefix("/about"),
      "/about/singleSpaEntry.js",
      "/about/store.js",
      globalEventDistributor
    )
  );

  // wait until all stores are loaded and all apps are registered with singleSpa
  await Promise.all(loadingPromises);

  singleSpa.start();
}

init();
