const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: {
    main: "src/portal.js",
  },
  output: {
    publicPath: "",
    filename: "[name].js",
    path: path.resolve(__dirname, "release"),
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: [path.resolve(__dirname, "node_modules")],
        loader: "babel-loader",
      },
    ],
  },
  node: {
    fs: "empty",
  },
  resolve: {
    modules: [__dirname, "node_modules"],
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        { from: path.resolve(__dirname, "src/index.html") },
        { from: path.resolve(__dirname, "libs/system.js") },
      ],
    }),
    new CleanWebpackPlugin(),
  ],
  devtool: "source-map",
  externals: [],
  mode: "development",
  devServer: {
    contentBase: "./release",
    historyApiFallback: true,
    watchOptions: { aggregateTimeout: 300, poll: 1000 },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers":
        "X-Requested-With, content-type, Authorization",
    },
    // Proxy config for development purposes. In production, you would configure you webserver to do something similar.
    proxy: {
      "/operations": {
        target: "http://localhost:9001",
        pathRewrite: { "^/operations": "" },
      },
      "/planning": {
        target: "http://localhost:9002",
        pathRewrite: { "^/planning": "" },
      },
      "/settings": {
        target: "http://localhost:9003",
        pathRewrite: { "^/settings": "" },
      },
      "/nav": {
        target: "http://localhost:9004",
        pathRewrite: { "^/nav": "" },
      },
      "/login": {
        target: "http://localhost:9005",
        pathRewrite: { "^/login": "" },
      },
      "/about": {
        target: "http://localhost:9006",
        pathRewrite: { "^/about": "" },
      },
    },
  },
};
