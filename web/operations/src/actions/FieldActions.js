export const SET_STACKING_FIELDS = "SET_STACKING_FIELDS";
export const SET_TRANSFER_FIELDS = "SET_TRANSFER_FIELDS";
export const UPDATE_STACKING_FIELD_STATUS = "UPDATE_STACKING_FIELD_STATUS";
export const UPDATE_TRANSFER_FIELD_STATUS = "UPDATE_TRANSFER_FIELD_STATUS";
export const DELETE_STACKING_FIELD_STATUS = "DELETE_STACKING_FIELD_STATUS";
export const DELETE_TRANSFER_FIELD_STATUS = "DELETE_TRANSFER_FIELD_STATUS";
export const SET_STACKING_FIELD_STATUSES = "SET_STACKING_FIELD_STATUSES";
export const SET_TRANSFER_FIELD_STATUSES = "SET_TRANSFER_FIELD_STATUSES";
export const CLEAR_STACKING_FIELD_STATUSES = "CLEAR_STACKING_FIELD_STATUSES";
export const CLEAR_TRANSFER_FIELD_STATUSES = "CLEAR_TRANSFER_FIELD_STATUSES";
export const DELETE_CONTAINER = "DELETE_CONTAINER";

export const setStackingFields = (stFields) => ({
  type: SET_STACKING_FIELDS,
  payload: stFields,
});
export const setTransferFields = (trFields) => ({
  type: SET_TRANSFER_FIELDS,
  payload: trFields,
});
export const updateStackingFieldStatus = (stFieldStatus) => ({
  type: UPDATE_STACKING_FIELD_STATUS,
  payload: stFieldStatus,
});
export const updateTransferFieldStatus = (trFieldStatus) => ({
  type: UPDATE_TRANSFER_FIELD_STATUS,
  payload: trFieldStatus,
});
export const deleteStackingFieldStatus = (stFieldStatus) => ({
  type: DELETE_STACKING_FIELD_STATUS,
  payload: stFieldStatus,
});
export const deleteTransferFieldStatus = (trFieldStatus) => ({
  type: DELETE_TRANSFER_FIELD_STATUS,
  payload: trFieldStatus,
});
export const setStackingFieldStatuses = (stFieldStatuses) => ({
  type: SET_STACKING_FIELD_STATUSES,
  payload: stFieldStatuses,
});
export const setTransferFieldStatuses = (trFieldStatuses) => ({
  type: SET_TRANSFER_FIELD_STATUSES,
  payload: trFieldStatuses,
});

export const clearStackingFieldStatuses = () => ({
  type: CLEAR_STACKING_FIELD_STATUSES,
  payload: [],
});

export const clearTransferFieldStatuses = () => ({
  type: CLEAR_TRANSFER_FIELD_STATUSES,
  payload: [],
});

export const deleteContainer = (id) => ({
  type: DELETE_CONTAINER,
  payload: id,
});
