export const SET_TRUCK_STATUSES = "SET_TRUCK_STATUSES";
export const RESET_TRUCK_STATUSES = "RESET_TRUCK_STATUSES";
export const UPDATE_TRUCK_STATUS = "UPDATE_TRUCK_STATUS";

export const setTruckStatuses = (truckStatuses) => ({
  type: SET_TRUCK_STATUSES,
  payload: truckStatuses,
});
export const resetTruckStatuses = () => ({
  type: RESET_TRUCK_STATUSES,
});
export const updateTruckStatus = (truckStatus) => ({
  type: UPDATE_TRUCK_STATUS,
  payload: truckStatus,
});
