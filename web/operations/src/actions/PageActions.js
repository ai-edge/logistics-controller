export const SHOW_CREATE_DEVICE_FORM = "SHOW_CREATE_DEVICE_FORM";
export const HIDE_CREATE_DEVICE_FORM = "HIDE_CREATE_DEVICE_FORM";
export const UPDATE_HEADER_TEXT = "UPDATE_HEADER_TEXT";
export const UPDATE_SELECTED_TAB = "UPDATE_SELECTED_TAB";
export const UPDATE_PAGE_LOADING = "UPDATE_PAGE_LOADING";
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const SET_SOCKET = "SET_SOCKET";

export const showCreateDeviceForm = () => ({
  type: SHOW_CREATE_DEVICE_FORM,
});
export const hideCreateDeviceForm = () => ({
  type: HIDE_CREATE_DEVICE_FORM,
});
export const updateHeaderText = (headerText) => ({
  type: UPDATE_HEADER_TEXT,
  payload: headerText,
});
export const updateSelectedTab = (tab) => ({
  type: UPDATE_SELECTED_TAB,
  payload: tab,
});
export const updatePageLoading = (pageLoading) => ({
  type: UPDATE_PAGE_LOADING,
  payload: pageLoading,
});
export const setSocket = (socket) => ({
  type: SET_SOCKET,
  payload: socket,
});
