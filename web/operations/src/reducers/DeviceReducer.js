import {
  SET_DEVICES,
  RESET_DEVICES,
  ADD_DEVICE,
  DELETE_DEVICE,
  UPDATE_DEVICE,
  SET_DEVICE_STATUSES,
  RESET_DEVICE_STATUSES,
  UPDATE_DEVICE_STATUS,
} from "../actions/DeviceActions";
import produce from "immer";

const initialState = {
  devices: [],
  deviceStatuses: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_DEVICES:
      return produce(state, (draft) => {
        draft.devices = action.payload;
      });
    case RESET_DEVICES:
      return produce(state, (draft) => {
        draft.devices = [];
      });
    case ADD_DEVICE:
      return produce(state, (draft) => {
        draft.devices.concat(action.payload);
      });
    case DELETE_DEVICE:
      return produce(state, (draft) => {
        draft.devices.filter(
          (device) => device.stackingCraneID !== action.payload
        );
      });
    case UPDATE_DEVICE:
      let device = action.payload;
      return produce(state, (draft) => {
        draft.devices.map((currentDevice) =>
          currentDevice.resourceID === device.resourceID
            ? device
            : currentDevice
        );
      });
    case SET_DEVICE_STATUSES:
      return produce(state, (draft) => {
        draft.deviceStatuses = action.payload;
      });
    case RESET_DEVICE_STATUSES:
      return produce(state, (draft) => {
        draft.deviceStatuses = [];
      });
    case UPDATE_DEVICE_STATUS:
      let deviceStatus = action.payload;
      let deviceStatuses = state.deviceStatuses.map((currentDevice) =>
        currentDevice.stackingCraneID === deviceStatus.stackingCraneID
          ? deviceStatus
          : currentDevice
      );

      return produce(state, (draft) => {
        draft.deviceStatuses = deviceStatuses;
      });
    default:
      return state;
  }
}
