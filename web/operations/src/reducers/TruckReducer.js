import {
  SET_TRUCK_STATUSES,
  RESET_TRUCK_STATUSES,
  UPDATE_TRUCK_STATUS,
} from "../actions/TruckActions";
import produce from "immer";

const initialState = {
  truckStatuses: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_TRUCK_STATUSES:
      return produce(state, (draft) => {
        draft.truckStatuses = []
          .concat(action.payload)
          .sort((a, b) => (a.lastUpdated < b.lastUpdated ? 1 : -1))
          .slice(0, 8);
      });
    case RESET_TRUCK_STATUSES:
      return produce(state, (draft) => {
        draft.truckStatuses = [];
      });
    case UPDATE_TRUCK_STATUS:
      let truckStatus = action.payload;
      let truckInState = state.truckStatuses.find(
        (status) => status.htID === truckStatus.htID
      );
      if (truckInState === undefined) {
        return produce(state, (draft) => {
          draft.truckStatuses = tempTruckStatuses.state.truckStatuses
            .concat(action.payload)
            .sort((a, b) => (a.lastUpdated < b.lastUpdated ? 1 : -1))
            .slice(0, 8);
        });
      } else {
        let truckStatuses = state.truckStatuses
          .map((status) =>
            status.htID === truckStatus.htID ? truckStatus : status
          )
          .sort((a, b) => (a.lastUpdated < b.lastUpdated ? 1 : -1));
        return produce(state, (draft) => {
          draft.truckStatuses = truckStatuses.slice(0, 8);
        });
      }
    default:
      return state;
  }
}
