import {
  SET_STACKING_FIELDS,
  SET_TRANSFER_FIELDS,
} from "../actions/FieldActions";
import produce from "immer";

const initialState = {
  stFields: [],
  trFields: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_STACKING_FIELDS:
      return produce(state, (draft) => {
        draft.stFields = action.payload;
      });
    case SET_TRANSFER_FIELDS:
      return produce(state, (draft) => {
        draft.trFields = action.payload;
      });
    default:
      return state;
  }
}
