import {
  SET_TRANSFER_FIELD_STATUSES,
  CLEAR_TRANSFER_FIELD_STATUSES,
  UPDATE_TRANSFER_FIELD_STATUS,
  DELETE_TRANSFER_FIELD_STATUS,
} from "../actions/FieldActions";
import produce from "immer";

const initialState = {
  trFieldStatus: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_TRANSFER_FIELD_STATUSES:
      return produce(state, (draft) => {
        if (action.payload === null) {
          draft.trFieldStatus = [];
        } else {
          draft.trFieldStatus = action.payload;
        }
      });
    case CLEAR_TRANSFER_FIELD_STATUSES:
      return initialState;
    case UPDATE_TRANSFER_FIELD_STATUS:
      let fieldStatus = action.payload;
      let fieldInState = state.trFieldStatus.find(
        (status) => status.fieldNameComposite === fieldStatus.fieldNameComposite
      );
      if (fieldInState === undefined) {
        return produce(state, (draft) => {
          draft.trFieldStatus.push(action.payload);
        });
      }
      return produce(state, (draft) => {
        draft.trFieldStatus.map((status) =>
          status.fieldNameComposite === fieldStatus.fieldNameComposite
            ? fieldStatus
            : status
        );
      });
    case DELETE_TRANSFER_FIELD_STATUS:
      let fldStatus = action.payload;
      let tempFldStatuses = state.trFieldStatus.filter(
        (status) => status.fieldNameComposite !== fldStatus.fieldNameComposite
      );
      return produce(state, (draft) => {
        draft.trFieldStatus = tempFldStatuses;
      });
    default:
      return state;
  }
}
