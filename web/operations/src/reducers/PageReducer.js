import {
  SHOW_CREATE_DEVICE_FORM,
  HIDE_CREATE_DEVICE_FORM,
  LOGIN,
  LOGOUT,
  SET_SOCKET,
} from "../actions/PageActions";
import produce from "immer";

const initialState = {
  showCreateDevice: false,
  isLoggedIn: localStorage.getItem("isLoggedIn") || false,
  socket: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_CREATE_DEVICE_FORM:
      return produce(state, (draft) => {
        draft.showCreateDevice = true;
      });
    case HIDE_CREATE_DEVICE_FORM:
      return produce(state, (draft) => {
        draft.showCreateDevice = false;
      });
    case LOGIN:
      return produce(state, (draft) => {
        localStorage.setItem("isLoggedIn", true);
        draft.isLoggedIn = true;
      });
    case LOGOUT:
      return produce(state, (draft) => {
        localStorage.setItem("isLoggedIn", false);
        draft.isLoggedIn = false;
      });
    case SET_SOCKET:
      console.log("socket is updated");
      return produce(state, (draft) => {
        draft.socket = action.payload;
      });
    default:
      return state;
  }
}
