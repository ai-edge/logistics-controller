import React from "react";
import ReactDOM from "react-dom";
import singleSpaReact from "single-spa-react";
import Root from "./root.component.js";

const reactLifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: Root,
  domElementGetter,
});

export function bootstrap(props) {
  console.log("BFF URL " + process.env.REACT_APP_OP_BFF_URL);
  return reactLifecycles.bootstrap(props);
}

export function mount(props) {
  console.log("operations mounted!");
  return reactLifecycles.mount(props);
}

export function unmount(props) {
  console.log("operations unmounted!");
  return reactLifecycles.unmount(props);
}

function domElementGetter() {
  // Make sure there is a div for us to render into
  let el = document.getElementById("operations");
  if (!el) {
    el = document.createElement("div");
    el.id = "operations";
    document.body.appendChild(el);
  }

  return el;
}
