import LogicalLocation from "./LogicalLocation";
import { v4 as uuidv4 } from "uuid";

export default function Container(props) {
  var dimensions = {
    width: 0,
    length: 0,
    depth: 0,
  };
  var IDInfo = {
    ISOID: "BBBU11" + Date.now(),
    containerType: "C20G0",
  };
  this.itemType = "container";
  this.itemGUID = uuidv4();
  this.ISOID = "BBBU11" + Date.now();
  this.itemDimensions = dimensions;
  this.weight = 12800;
  this.containerIDInfo = IDInfo;
  this.doorDirection = "ignore";
  this.containerLogicalLocation = new LogicalLocation(
    props.fieldType,
    props.bay,
    props.row,
    props.tier
  );
  this.Deviation3D = {
    x: 0,
    y: 0,
    z: 0,
  };
}
