import CranePosition from "./CranePosition"

export default function DummyDevice(){
    this.resourceID = "-";
    this.assignedBlock = "-";
    this.resourceType = "-";
    this.craneMainAxesPosition = new CranePosition("-","-","-"); 
    this.resourceState = "rIdle";
}