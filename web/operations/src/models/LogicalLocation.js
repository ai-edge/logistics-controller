export default function LogicalLocation(area, bay, row, tier) {
  this.areaName = area;
  this.bay = bay;
  this.row = row;
  this.tier = tier;
}
