export default function CranePosition(gantry, trolley, hoist) {
    this.gantryPosition = gantry;
    this.trolleyPosition = trolley;
    this.hoistPosition = hoist;
}