import { combineReducers } from "redux";
import deviceReducer from "./reducers/DeviceReducer";
import pageReducer from "./reducers/PageReducer";
import fieldReducer from "./reducers/FieldReducer";
import stStatusReducer from "./reducers/StackingFieldStatusReducer";
import trStatusReducer from "./reducers/TransferFieldStatusReducer.js";
import truckStatusReducer from "./reducers/TruckReducer.js";

export default combineReducers({
  deviceData: deviceReducer,
  page: pageReducer,
  fields: fieldReducer,
  stStatus: stStatusReducer,
  trStatus: trStatusReducer,
  truckStatus: truckStatusReducer,
});
