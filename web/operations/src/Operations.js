import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { BrowserRouter, Redirect } from "react-router-dom";
import Divider from "@material-ui/core/Divider";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Grid from "@material-ui/core/Grid";

import "./operations.css";
import DeviceList from "./components/DeviceList";
import CreateDeviceForm from "./components/CreateDeviceForm";
import FieldStatus from "./components/FieldStatus";
import TruckStatus from "./components/TruckStatus";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Equipment Sim
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

import {
  showCreateDeviceForm,
  hideCreateDeviceForm,
  updateHeaderText,
  updateSelectedTab,
  updatePageLoading,
} from "./actions/PageActions";

import {
  setDevices,
  resetDevices,
  addDevice,
  deleteDevice,
  updateDevice,
  setDeviceStatuses,
  resetDeviceStatuses,
  updateDeviceStatus,
} from "./actions/DeviceActions";

import {
  setStackingFields,
  setTransferFields,
  setStackingFieldStatuses,
  updateStackingFieldStatus,
  deleteStackingFieldStatus,
  setTransferFieldStatuses,
  updateTransferFieldStatus,
  deleteTransferFieldStatus,
  clearStackingFieldStatuses,
  clearTransferFieldStatuses,
  deleteContainer,
} from "./actions/FieldActions";

import {
  setTruckStatuses,
  resetTruckStatuses,
  updateTruckStatus,
} from "./actions/TruckActions";

const useStyles = makeStyles(
  (theme) => ({
    main: {
      flex: 1,
      padding: theme.spacing(6, 4),
      background: "#eaeff1",
    },
    footer: {
      padding: theme.spacing(2),
      background: "#eaeff1",
    },
    app: {
      flex: 1,
      display: "flex",
      flexDirection: "column",
    },
    box: {
      margin: theme.spacing(3),
      alignContent: "center",
      border: "solid",
    },
    fabAddIcon: {
      color: "primary",
    },
    containerGrid: {
      justify: "center",
      spacing: theme.spacing(3),
      direction: "row",
    },
  }),
  { name: "MuiEcsOperations" }
);

function Operations(props) {
  const [deviceNames, setDeviceNames] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    if (!props.isLoggedIn || props.isLoggedIn === "false") {
      return;
    }
    props.globalEventDistributor.dispatch(props.updateHeaderText("OPERATIONS"));
    props.globalEventDistributor.dispatch(props.updateSelectedTab(0));
    props.globalEventDistributor.dispatch(props.updatePageLoading(false));
    console.log(`Operations mounted`);

    if (props.socket != null && props.socket.readyState == WebSocket.OPEN) {
      loadPageData();
    } else {
      setTimeout(heartbeat, 1000);
    }

    return function cleanup() {
      console.log(`Operations will unmount`);
    };
  }, []);

  useEffect(() => {
    if (props.socket === null) {
      return;
    }
    connectWS((msg) => {
      let data = JSON.parse(msg.data);
      let body = JSON.parse(data.body);
      switch (data.type) {
        case "setDevices":
          props.setDevices(body);
          break;
        case "setDeviceStatuses":
          props.setDeviceStatuses(body);
          break;
        case "updateDeviceStatus":
          props.updateDeviceStatus(body);
          break;
        case "setStackingFields":
          props.setStackingFields(body);
          break;
        case "setTransferFields":
          props.setTransferFields(body);
          break;
        case "setStackingFieldStatuses":
          props.setStackingFieldStatuses(body);
          break;
        case "setTransferFieldStatuses":
          props.setTransferFieldStatuses(body);
          break;
        case "updateStackingFieldStatus":
          props.updateStackingFieldStatus(body);
          break;
        case "deleteStackingFieldStatus":
          props.deleteStackingFieldStatus(body);
          break;
        case "updateTransferFieldStatus":
          props.updateTransferFieldStatus(body);
          break;
        case "deleteTransferFieldStatus":
          props.deleteTransferFieldStatus(body);
          break;
        case "setTruckStatuses":
          props.setTruckStatuses(body);
          break;
        case "updateTruckStatus":
          props.updateTruckStatus(body);
          break;
        case "createdDevice":
          console.log(`Device is created: ${body}`);
          break;
        case "errorMessage":
          setMessageHeader("An Error Occured!");
          setMessageText(body);
          setShowMessageBox(true);
          break;
        case "successMessage":
          setMessageHeader("Operation is Successful");
          setMessageText(body);
          setShowMessageBox(true);
          break;
        default:
          console.log(`unhandled message type: ${data.type}`);
          break;
      }
    });
    return function cleanup() {
      console.log(`Planning will unmount`);
    };
  }, [props.socket]);

  useEffect(() => {
    setDeviceNames(
      props.deviceStatuses.map(({ stackingCraneID }) => stackingCraneID)
    );
  }, [props.deviceStatuses]);

  const heartbeat = () => {
    console.log(`heartbeat. socket state: ${props.socket.readyState}`);
    if (props.socket.readyState === WebSocket.OPEN) {
      loadPageData();
    } else if (props.socket.readyState === WebSocket.CLOSED) {
      window.dispatchEvent(new CustomEvent("reconnectSocket"));
      setTimeout(heartbeat, 10000);
    } else {
      setTimeout(heartbeat, 1000);
    }
  };

  const connectWS = (cb) => {
    console.log("Attempting Connection...");

    props.socket.onmessage = (msg) => {
      cb(msg);
    };

    props.socket.onopen = () => {
      console.log("Successfully Connected");
      loadPageData();
    };

    props.socket.onclose = (event) => {
      console.log("Socket Closed Connection: ", event);
      window.dispatchEvent(new CustomEvent("reconnectSocket"));
      setTimeout(heartbeat, 10000);
    };

    props.socket.onerror = (error) => {
      var textMess = error.response.data
        .replace(/"/gi, "")
        .replace("message:", "")
        .replace(/}/gi, "")
        .replace(/{/gi, "");
      setMessageHeader("WebSocket Error");
      setMessageText(textMess);
      setShowMessageBox(true);
    };
  };

  const sendMsg = (msg) => {
    console.log("sending msg: ", msg);
    props.socket.send(msg);
  };

  const renderRedirect = () => {
    if (!props.isLoggedIn || props.isLoggedIn === "false") {
      return (
        <BrowserRouter>
          <Redirect from="/#/operations" to="/#/login" />
        </BrowserRouter>
      );
    }
  };

  const loadPageData = () => {
    sendMsg(`getDevices; ""`);
    sendMsg(`getDeviceStatuses; ""`);
    sendMsg(`getStackingFields; ""`);
    sendMsg(`getTransferFields; ""`);
    sendMsg(`getStackingFieldStatuses; ""`);
    sendMsg(`getTransferFieldStatuses; ""`);
    sendMsg(`getTruckStatuses; ""`);
  };

  const handleSubmit = (data) => {
    sendMsg(`createDevice;${JSON.stringify(data)}`);
    props.hideCreateDeviceForm();
  };

  const handleDelete = (name) => {
    sendMsg(`deleteDevice;${name}`);
  };

  const handleAddContainer = (data) => {
    sendMsg(`addContainer;${JSON.stringify(data)}`);
  };

  const handleDeleteContainer = (guid) => {
    sendMsg(`deleteContainer;${guid}`);
  };

  return (
    <div className={classes.app}>
      {renderRedirect()}
      <div className={classes.main}>
        <div>
          <DeviceList
            devices={props.devices}
            deviceStatuses={props.deviceStatuses}
            onDelete={(device) => handleDelete(device)}
          />
          {/* <Divider />
          <Fab
            className="fabAddIcon"
            area-label="add"
            onClick={props.showCreateDeviceForm}
          >
            <AddIcon className="plusIcon" />
          </Fab> */}
        </div>
      </div>
      <div id="fieldstatus">
        <Grid container>
          <Grid item xs={10}>
            <div className={classes.box}>
              <FieldStatus
                stFields={props.stFields}
                trFields={props.trFields}
                stFieldStatus={props.stFieldStatus}
                trFieldStatus={props.trFieldStatus}
                names={deviceNames}
                devices={props.devices}
                deviceStatuses={props.deviceStatuses}
                handleDeleteContainer={(guid) => handleDeleteContainer(guid)}
                handleAddContainer={(data) => handleAddContainer(data)}
              />
            </div>
          </Grid>
          <Grid item xs={2}>
            <div className={classes.box}>
              <TruckStatus truckStatuses={props.truckStatuses} />
            </div>
          </Grid>
        </Grid>
      </div>
      <footer className={classes.footer}>
        <Copyright />
      </footer>
      <div>
        <CreateDeviceForm
          show={props.showCreateDevice}
          handleClose={props.hideCreateDeviceForm}
          handleSubmit={handleSubmit}
        />
      </div>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    devices: state.deviceData.devices,
    deviceStatuses: state.deviceData.deviceStatuses,
    showCreateDevice: state.page.showCreateDevice,
    isLoggedIn: state.page.isLoggedIn,
    socket: state.page.socket,
    stFields: state.fields.stFields,
    trFields: state.fields.trFields,
    stFieldStatus: state.stStatus.stFieldStatus,
    trFieldStatus: state.trStatus.trFieldStatus,
  };
}

const mapDispatchToProps = {
  updateSelectedTab,
  updateHeaderText,
  updatePageLoading,
  setDevices,
  resetDevices,
  addDevice,
  deleteDevice,
  updateDevice,
  setDeviceStatuses,
  resetDeviceStatuses,
  updateDeviceStatus,
  showCreateDeviceForm,
  hideCreateDeviceForm,
  setStackingFields,
  setTransferFields,
  setStackingFieldStatuses,
  updateStackingFieldStatus,
  deleteStackingFieldStatus,
  setTransferFieldStatuses,
  updateTransferFieldStatus,
  deleteTransferFieldStatus,
  clearStackingFieldStatuses,
  clearTransferFieldStatuses,
  setTruckStatuses,
  resetTruckStatuses,
  updateTruckStatus,
};

export default connect(mapStateToProps, mapDispatchToProps)(Operations);
