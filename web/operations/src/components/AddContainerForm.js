import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import Container from "../models/Container";

const types = [
  "C20G0",
  "C20G1",
  "C20G2",
  "C20G3",
  "C22G0",
  "C22G1",
  "C22G2",
  "C22G3",
  "C22G4",
  "C22G8",
  "C22G9",
  "C25G0",
  "C26G0",
  "C2EG0",
  "C40G0",
  "C42G0",
  "C42G1",
];

function AddContainerForm(props) {
  const [selectedContainer, setSelectedContainer] = useState(
    new Container(props)
  );

  useEffect(() => {
    if (props.show) {
      setSelectedContainer(new Container(props));
    }
  }, [props.show]);

  const handleSubmit = () => {
    props.handleSubmit(selectedContainer);
  };

  return (
    <div>
      <Dialog open={props.show} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add Container</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Please fill all the information for new container
          </DialogContentText>
          <div>
            <TextField
              value={selectedContainer.itemGUID}
              type="string"
              name="itemGUID"
              id="enter-guid"
              helperText="Please enter a unique id"
              label="Container GUID"
              fullWidth={true}
            />
          </div>
          <div>
            <TextField
              value={selectedContainer.containerIDInfo.ISOID}
              type="string"
              name="containerIDInfo.ISOID"
              id="enter-isoid"
              helperText="Please enter ISOID of the container"
              label="Container ISOID"
              fullWidth={true}
            />
          </div>
          <div>
            <TextField
              name="containertype"
              id="select-type"
              helperText="Please select container type"
              select
              label="type"
              value={selectedContainer.containerIDInfo.containerType}
              type="string"
            >
              {types.map((option) => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </div>
          <div>
            <div>
              <TextField
                value={selectedContainer.weight}
                type="number"
                name="weight"
                id="enter-weight"
                helperText="Please enter weight of the container"
                label="Container Weight"
              />
            </div>
            <div>Container Logical Location</div>
            <TextField
              value={selectedContainer.containerLogicalLocation.bay}
              type="string"
              name="bay"
              id="enter-bay"
              label="Container Bay"
            />
            <TextField
              value={selectedContainer.containerLogicalLocation.row}
              type="string"
              name="row"
              id="enter-row"
              label="Container Row"
            />
            <TextField
              value={selectedContainer.containerLogicalLocation.tier}
              type="string"
              name="tier"
              id="enter-tier"
              label="Container Tier"
            />
          </div>
          <div>
            <div>Container Deviation</div>
            <TextField
              value={selectedContainer.Deviation3D.x}
              type="number"
              name="x"
              id="enter-deviationX"
              helperText="Please enter x value for deviation"
              label="X"
            />
            <TextField
              value={selectedContainer.Deviation3D.y}
              type="number"
              name="y"
              id="enter-deviationY"
              helperText="Please enter y value for deviation"
              label="Y"
            />
            <TextField
              value={selectedContainer.Deviation3D.z}
              type="number"
              name="z"
              id="enter-deviationZ"
              helperText="Please enter z value for deviation"
              label="Z"
            />
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => props.handleClose()} color="primary">
            Cancel
          </Button>
          <Button onClick={() => handleSubmit()} color="primary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default AddContainerForm;
