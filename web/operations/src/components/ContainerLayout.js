import React from "react";

class ContainerLayout extends React.PureComponent {
  componentDidMount() {
    console.log(`FieldLayout mounted`);
  }

  getTooltipDataAttrsForValue(value) {
    const { tooltipDataAttrs } = this.props;

    if (typeof tooltipDataAttrs === "function") {
      return tooltipDataAttrs(value);
    }
    return tooltipDataAttrs;
  }

  handleClick(value) {
    if (this.props.onClick) {
      this.props.onClick(value);
    }
  }

  handleMouseOver(e, value) {
    if (this.props.onMouseOver) {
      this.props.onMouseOver(e, value);
    }
  }

  handleMouseLeave(e, value) {
    if (this.props.onMouseLeave) {
      this.props.onMouseLeave(e, value);
    }
  }

  getClassNameForIndex(itemCount, fieldType) {
    return this.props.classForValue(itemCount, fieldType);
  }

  renderRectangle(value) {
    const [x, y] = [value.x * this.props.scale, value.y * this.props.scale];

    const rectWidth = value.width * this.props.scale;
    const rectLength = value.length * this.props.scale;
    const xText = x + rectWidth / 2;
    const res = value.fieldNameComposite.split("-");
    var fieldName = "";
    if (res.length > 0) {
      fieldName = res[0];
    }

    if (value.fieldStatus === "Treserved") {
      // reserved field by HT
      const rect = (
        <g>
          // eslint-disable-next-line jsx-a11y/mouse-events-have-key-events
          <rect
            //key={`${value.bay}-${value.row}-rect`}
            width={rectWidth}
            height={rectLength}
            x={x}
            y={y + 2 * this.props.gutterSize}
            className="color-reservedField"
          >
            <title>{`Reserved: ${value.reservedHT}`}</title>
          </rect>
          <text
            className="fieldlayout-reserved-text"
            x={x + rectWidth / 20}
            y={y + (2 * rectLength) / 3}
          >
            {` Reserved: ${value.reservedHT} `}
          </text>
        </g>
      );
      return rect;
    } else {
      const rect = (
        <g>
          // eslint-disable-next-line jsx-a11y/mouse-events-have-key-events
          <rect
            //key={`${value.bay}-${value.row}-rect`}
            width={rectWidth}
            height={rectLength}
            x={x}
            y={y + 2 * this.props.gutterSize}
            className={this.getClassNameForIndex(value.itemCount, fieldName)}
            onClick={() => this.handleClick(value)}
            onMouseOver={(e) => this.handleMouseOver(e, value)}
            onMouseLeave={(e) => this.handleMouseLeave(e, value)}
            {...this.getTooltipDataAttrsForValue(value)}
          >
            <title>{`Item Count: ${value.itemCount}`}</title>
          </rect>
          <text
            className="fieldlayout-item-text"
            x={xText}
            y={y + (2 * rectLength) / 3}
          >
            {` ${value.itemCount} `}
          </text>
        </g>
      );
      return rect;
    }
  }

  renderTrFieldItems() {
    // console.log(`renderFields data[${this.props.data.length}]`)
    return React.Children.toArray(
      this.props.trFieldItems.map((value, i) => this.renderRectangle(value))
    );
  }
  renderStFieldItems() {
    // console.log(`renderFields data[${this.props.data.length}]`)
    return React.Children.toArray(
      this.props.stFieldItems.map((value, i) => this.renderRectangle(value))
    );
  }

  render() {
    return (
      <svg>
        <g>{this.renderTrFieldItems()}</g>
        <g>{this.renderStFieldItems()}</g>
      </svg>
    );
  }
}

export default ContainerLayout;
