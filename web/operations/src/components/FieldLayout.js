import React from "react";

export class Label {
  constructor(labelText, x, y, width, length) {
    this.labelText = labelText;
    this.x = x;
    this.y = y;
    this.width = width;
    this.length = length;
  }
}

class FieldLayout extends React.PureComponent {
  componentDidMount() {
    console.log(`FieldLayout mounted`);
  }

  getTooltipDataAttrsForValue(value) {
    const { tooltipDataAttrs } = this.props;

    if (typeof tooltipDataAttrs === "function") {
      return tooltipDataAttrs(value);
    }
    return tooltipDataAttrs;
  }

  getRectCoordinates(value) {
    return [this.getX(value), this.getY(value)];
  }

  getX(value) {
    return Math.floor(value.x * this.props.scale);
  }

  getY(value) {
    return Math.floor(value.y * this.props.scale);
  }

  handleClick(value) {
    if (this.props.onClick) {
      this.props.onClick(value);
    }
  }

  handleMouseOver(e, value) {
    if (this.props.onMouseOver) {
      this.props.onMouseOver(e, value);
    }
  }

  handleMouseLeave(e, value) {
    if (this.props.onMouseLeave) {
      this.props.onMouseLeave(e, value);
    }
  }

  renderRectangle(value) {
    const [x, y] = this.getRectCoordinates(value);

    const rectWidth =
      value.width * this.props.scale - 2 * this.props.gutterSize;
    const rectLength =
      value.length * this.props.scale - 2 * this.props.gutterSize;

    const rect = (
      <g>
        // eslint-disable-next-line jsx-a11y/mouse-events-have-key-events
        <rect
          //key={`${value.bay}-${value.row}-rect`}
          width={rectWidth}
          height={rectLength}
          x={x + this.props.gutterSize}
          y={y + this.props.gutterSize}
          className={this.props.classForValue}
          onClick={() => this.handleClick(value)}
          onMouseOver={(e) => this.handleMouseOver(e, value)}
          onMouseLeave={(e) => this.handleMouseLeave(e, value)}
          {...this.getTooltipDataAttrsForValue(value)}
        >
          <title>{`Row: ${value.row}, Bay: ${value.bay}`}</title>
        </rect>
      </g>
    );
    return rect;
  }

  renderFields() {
    // console.log(`renderFields data[${this.props.data.length}]`)
    return React.Children.toArray(
      this.props.data.map((value, i) => this.renderRectangle(value.fieldBase))
    );
  }

  renderColumnLabels() {
    if (!this.props.showColumnLabels) {
      return null;
    }
    //console.log(`renderColumnLabels columnLabels[${this.props.columnLabels.length}]`)
    return this.props.columnLabels.map((label) => {
      const [x, y] = [
        this.getX(label),
        this.getY(label) - label.length * this.props.scale,
      ];
      const rectLength =
        label.length * this.props.scale - 2 * this.props.gutterSize;

      // console.log(`renderColumnLabels [x, y] : [${x}, ${y}]. Actual x value: ${label.x}. Height scale: ${this.props.scale}`)
      return (
        <text
          className="fieldlayout-text"
          key={`col-${x}-${y}`}
          x={x + 20}
          y={y + (2 * rectLength) / 3}
        >
          {label.labelText}
        </text>
      );
    });
  }

  renderRowLabels() {
    //console.log(`renderRowLabels rowLabels[${this.props.rowLabels.length}]`)
    return this.props.rowLabels.map((label) => {
      const [x, y] = [
        this.getX(label) - label.width * this.props.scale,
        this.getY(label),
      ];
      const rectLength =
        label.length * this.props.scale - 2 * this.props.gutterSize;

      // eslint-disable-next-line no-bitwise
      // console.log(`renderRowLabels [x, y] : [${x}, ${y}]. Actual y value: ${label.y}. length: ${label.length}. Height scale: ${this.props.scale}`)
      return (
        <text
          className="fieldlayout-text"
          key={`row-${x}-${y}`}
          x={x + 20}
          y={y + (2 * rectLength) / 3}
        >
          {label.labelText}
        </text>
      );
    });
  }

  render() {
    return (
      <svg>
        <g>{this.renderColumnLabels()}</g>
        <g>{this.renderFields()}</g>
        <g>{this.renderRowLabels()}</g>
      </svg>
    );
  }
}

export default FieldLayout;
