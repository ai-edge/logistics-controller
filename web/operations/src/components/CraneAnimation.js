import React, { useEffect, useState } from "react";
import * as d3 from "d3";

export default function CraneAnimation(props) {
  const [gantryPosition, setGantryPosition] = useState([]);
  const [trolleyPosition, setTrolleyPosition] = useState([]);
  const [gantryFill, setGantryFill] = useState("#e5c0ff");
  const [trolleyFill, setTrolleyFill] = useState("#cccccc");
  const [hoistFill, setHoistFill] = useState("#cccccc");

  useEffect(() => {
    setGantryPosition(props.position.gantryPosition);
    setGantryFill("#e5c0ff");
    setTrolleyFill("#cccccc");
    setHoistFill("#cccccc");
    setTimeout(function () {
      if (props.state == "rFault") {
        setGantryFill("red");
        setTrolleyFill("red");
        setHoistFill("red");
      } else {
        setGantryFill("green");
        setTrolleyFill("green");
        setHoistFill("green");
      }
    }, 500);
  }, [props.position.gantryPosition, props.scale]);

  useEffect(() => {
    setTrolleyPosition(props.position.trolleyPosition);
    setTrolleyFill("#cccccc");
    setHoistFill("#cccccc");
    setTimeout(function () {
      if (props.state == "rFault") {
        setTrolleyFill("red");
        setHoistFill("red");
      } else {
        setTrolleyFill("green");
        setHoistFill("green");
      }
    }, 500);
  }, [props.position.trolleyPosition, props.scale]);

  useEffect(() => {
    setHoistFill("#cccccc");
    setTimeout(function () {
      if (props.state == "rFault") {
        setHoistFill("red");
      } else {
        setHoistFill("green");
      }
    }, 500);
  }, [props.position.hoistPosition, props.scale]);

  useEffect(() => {
    if (props.state == "rFault") {
      setGantryFill("red");
      setTrolleyFill("red");
      setHoistFill("red");
    } else if (props.state == "rIdle") {
      setGantryFill("green");
      setTrolleyFill("green");
      setHoistFill("green");
    } else if (props.state == "rOff") {
      setGantryFill("#e5c0ff");
      setTrolleyFill("#cccccc");
      setHoistFill("#cccccc");
    }
  }, [props.state]);

  if (typeof props.name === "undefined") {
    return null;
  }

  function getPosition(value) {
    if (value === "-") {
      return 0;
    }

    return Math.floor(value * props.scale);
  }

  function triggerTransition(rectId, x, y) {
    d3.select(rectId)
      .transition()
      .duration(1000)
      .attr("x", `${x}`)
      .attr("y", `${y}`);
  }

  function getGantryDimension() {
    if (props.dimensions === undefined) {
      return 0;
    }

    return {
      width: props.dimensions.gantry.width * props.scale,
      length: props.dimensions.gantry.length * props.scale,
    };
  }

  function getTrolleyDimension() {
    if (props.dimensions === undefined) {
      return 0;
    }

    return {
      width: props.dimensions.trolley.width * props.scale,
      length: props.dimensions.trolley.length * props.scale,
    };
  }

  function getHoistDimension() {
    if (props.dimensions === undefined) {
      return 0;
    }

    return {
      width: props.dimensions.hoist.width * props.scale,
      length: props.dimensions.hoist.length * props.scale,
    };
  }

  function renderHoist() {
    if (props.offsets === undefined || props.dimensions === undefined) {
      return;
    }
    const [x, y] = [
      getPosition(gantryPosition) + props.offsets.x * props.scale,
      getPosition(trolleyPosition) + props.offsets.y * props.scale,
    ];

    const hoistDim = getHoistDimension();

    const rect = (
      // eslint-disable-next-line jsx-a11y/mouse-events-have-key-events
      <rect
        width={hoistDim.width}
        height={hoistDim.length}
        id={`hoistrect-${props.name}`}
        fill={hoistFill}
        stroke="#7b7b7b"
        fillOpacity="0.6"
      />
    );
    triggerTransition(
      `#hoistrect-${props.name}`,
      x - hoistDim.width / 2,
      y - hoistDim.length / 2
    );
    return rect;
  }

  function renderTrolley() {
    if (props.offsets === undefined || props.dimensions === undefined) {
      return;
    }
    const [x, y] = [
      getPosition(gantryPosition) + props.offsets.x * props.scale,
      getPosition(trolleyPosition) + props.offsets.y * props.scale,
    ];

    const trolleyDim = getTrolleyDimension();

    const rect = (
      <g>
        // eslint-disable-next-line jsx-a11y/mouse-events-have-key-events
        <rect
          //key={`${value.bay}-${value.row}-rect`}
          width={trolleyDim.width}
          height={trolleyDim.length}
          id={`trolleyrect-${props.name}`}
          fill={trolleyFill}
          stroke="#2525f8"
          fillOpacity="0.6"
        />
      </g>
    );
    triggerTransition(
      `#trolleyrect-${props.name}`,
      x - trolleyDim.width / 2,
      y - trolleyDim.length / 2
    );
    return rect;
  }

  function renderGantry() {
    if (props.offsets === undefined || props.dimensions === undefined) {
      return;
    }
    const gantryDim = getGantryDimension();

    const [x, y] = [
      getPosition(gantryPosition) + props.offsets.x * props.scale,
      props.offsets.y * props.scale + gantryDim.length / 2,
    ];

    const textWidth = 50;
    const rect = (
      <g>
        // eslint-disable-next-line jsx-a11y/mouse-events-have-key-events
        <rect
          //key={`${value.bay}-${value.row}-rect`}
          width={gantryDim.width}
          height={gantryDim.length}
          id={`gantryrect-${props.name}`}
          fill={gantryFill}
          stroke="#fe33d5"
          fillOpacity="0.6"
        />
        <text id={`gantrytext-${props.name}`} textLength={textWidth}>
          {` ${props.name} `}
        </text>
      </g>
    );

    triggerTransition(
      `#gantryrect-${props.name}`,
      x - gantryDim.width / 2,
      y - gantryDim.length / 2
    );
    triggerTransition(
      `#gantrytext-${props.name}`,
      x - textWidth / 2,
      y - (gantryDim.length / 2 + 2)
    );
    return rect;
  }

  return (
    <svg>
      <g>{renderHoist()}</g>
      <g>{renderGantry()}</g>
      <g>{renderTrolley()}</g>
    </svg>
  );
}
