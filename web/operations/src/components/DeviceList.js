import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import "../operations.css";

const useStyles = makeStyles(
  {
    table: {
      minWidth: 350,
    },
  },
  { name: "MuiEcsDeviceList" }
);

function DeviceName(props) {
  return (
    <TableRow key={props.device.stackingCraneID} hover>
      <TableCell
        key={`${props.device.stackingCraneID}-name`}
        align="center"
      >{`${props.device.resourceID}`}</TableCell>
      <TableCell
        key={`${props.device.stackingCraneID}-assigned-block`}
        align="center"
      >{`${props.device.assignedBlock}`}</TableCell>
      <TableCell
        key={`${props.device.stackingCraneID}-resource-type`}
        align="center"
      >{`${props.device.resourceType}`}</TableCell>
      <TableCell
        key={`${props.device.stackingCraneID}-gantry`}
        align="center"
      >{`${props.deviceStatus.craneMainAxesPosition.gantryPosition}`}</TableCell>
      <TableCell
        key={`${props.device.stackingCraneID}-trolley`}
        align="center"
      >{`${props.deviceStatus.craneMainAxesPosition.trolleyPosition}`}</TableCell>
      <TableCell
        key={`${props.device.stackingCraneID}-hoist`}
        align="center"
      >{`${props.deviceStatus.craneMainAxesPosition.hoistPosition}`}</TableCell>
      <TableCell key={`${props.device.stackingCraneID}-delete`} align="center">
        <IconButton
          edge="end"
          aria-label="delete"
          onClick={props.onDelete}
          key={`${props.device.stackingCraneID}-deleteIcon`}
        >
          <DeleteIcon />
        </IconButton>
      </TableCell>
    </TableRow>
  );
}

function DeviceList(props) {
  if (props.devices.length === 0 || props.deviceStatuses.length === 0) {
    return <div></div>;
  }
  const classes = useStyles();
  return (
    <TableContainer key="device-list-container">
      <Table
        className={classes.table}
        aria-label="simple table"
        key="device-list-table"
      >
        <TableHead key="device-list-table-head">
          <TableRow key="header-row">
            <TableCell key="header-name" align="center">
              Name
            </TableCell>
            <TableCell key="header-assigned-blcok" align="center">
              Assigned&nbsp;Block
            </TableCell>
            <TableCell key="header-resource-type" align="center">
              Resource&nbsp;Type
            </TableCell>
            <TableCell key="header-gantry" align="center">
              Gantry&nbsp;Position
            </TableCell>
            <TableCell key="header-trolley" align="center">
              Trolley&nbsp;Position
            </TableCell>
            <TableCell key="header-hoist" align="center">
              Hoist&nbsp;Position
            </TableCell>
            <TableCell key="header-delete" align="center"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody key="device-list-table-body">
          {props.devices.map(function (device) {
            return (
              <DeviceName
                device={device}
                deviceStatus={props.deviceStatuses.find(
                  (deviceStatus) =>
                    deviceStatus.stackingCraneID === device.resourceID
                )}
                onDelete={() => props.onDelete(device.resourceID)}
              />
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default DeviceList;
