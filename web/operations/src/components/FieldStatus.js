import React, { useEffect, useState } from "react";
import FieldLayout, { Label } from "./FieldLayout";
import ReactTooltip from "react-tooltip";
import lodash from "lodash";
import CraneAnimation from "./CraneAnimation";
import ContainerLayout from "./ContainerLayout";
import ContainerDialog from "./ContainerDialog";

function FieldStatus(props, { interval }) {
  const debounceTime = Number(interval) || 300;
  const maxWaitTime = 3000;

  const [stackRows, setStackRows] = useState([]);
  const [stackCols, setStackCols] = useState([]);
  const [transferRows, setTransferRows] = useState([]);
  const [transferCols, setTransferCols] = useState([]);
  const [columnLabels, setColumnLabels] = useState([]);
  const [stRowLabels, setStRowLabels] = useState([]);
  const [trRowLabels, setTrRowLabels] = useState([]);
  const [width, setWidth] = useState(window.innerWidth);
  const [height, setHeight] = useState(window.innerHeight);
  const [scale, setScale] = useState(0.02);
  const GUTTER_SIZE = 2;
  const [open, setOpen] = React.useState(false);
  const [containerList, setContainerList] = useState([]);
  const [selectedBay, setSelectedBay] = useState("");
  const [selectedRow, setSelectedRow] = useState("");
  const [selectedTier, setSelectedTier] = useState("");
  const [selectedFieldType, setSelectedFieldType] = useState("");

  useEffect(() => {
    console.log("FieldStatus mounted");
    const handleResize = () => {
      console.log("FieldStatus resize");
      // setWidth(window.innerWidth);
      // setHeight(window.innerHeight);
      // UpdateFields();
    };

    const handleResizeThrottled = lodash.debounce(handleResize, debounceTime, {
      leading: true,
      maxWait: maxWaitTime,
    });

    window.addEventListener("resize", handleResizeThrottled);
    return () => {
      console.log("FieldStatus will unmount");
      window.removeEventListener("resize", handleResizeThrottled);
    };
  }, []);

  useEffect(() => {
    // console.log("FieldStatus props trFields update");
    SetTransferFieldData();
  }, [props.trFields]);

  useEffect(() => {
    // console.log("FieldStatus props stFields update");
    SetStackingFieldData();
  }, [props.stFields]);

  useEffect(() => {
    // console.log("UpdateFields");
    SetStackingFieldLabels();
    SetScale();
  }, [stackRows, stackCols]);

  useEffect(() => {
    SetTransferFieldLabels();
    SetScale();
  }, [transferRows, transferCols]);

  const SetStackingFieldData = () => {
    setStackRows(props.stFields.filter((row) => row.fieldBase.bay === "001"));
    setStackCols(props.stFields.filter((col) => col.fieldBase.row === "001"));
  };

  const SetTransferFieldData = () => {
    setTransferRows(
      props.trFields.filter((row) => row.fieldBase.bay === "001")
    );
    setTransferCols(
      props.trFields.filter((col) => col.fieldBase.row === "001")
    );
  };

  const SetStackingFieldLabels = () => {
    var clabels = [];
    for (var iCol = 0; iCol < stackCols.length; iCol++) {
      var colLabel = new Label(
        `Bay ${stackCols[iCol].fieldBase.bay}`,
        stackCols[iCol].fieldBase.x,
        stackCols[iCol].fieldBase.y,
        stackCols[iCol].fieldBase.width,
        stackCols[iCol].fieldBase.length
      );
      clabels.push(colLabel);
    }
    setColumnLabels(clabels);

    var rlabels = [];
    for (var iRow = 0; iRow < stackRows.length; iRow++) {
      var stRowLabel = new Label(
        `Row ${stackRows[iRow].fieldBase.row}`,
        stackRows[iRow].fieldBase.x,
        stackRows[iRow].fieldBase.y,
        stackRows[iRow].fieldBase.width,
        stackRows[iRow].fieldBase.length
      );
      rlabels.push(stRowLabel);
    }
    setStRowLabels(rlabels);
  };

  const SetScale = () => {
    let horScale = 1;
    let verScale = 1;
    if (stackCols.length > 0) {
      const lastCol = stackCols[stackCols.length - 1].fieldBase;
      horScale = Math.min(horScale, width / (lastCol.x + lastCol.width));
    }
    if (transferCols.length > 0) {
      const lastCol = transferCols[transferCols.length - 1].fieldBase;
      horScale = Math.min(horScale, width / (lastCol.x + lastCol.width));
    }
    if (transferRows.length > 0) {
      const lastRow = transferRows[transferRows.length - 1].fieldBase;
      verScale = Math.min(verScale, height / (lastRow.y + lastRow.length));
    }
    if (stackRows.length > 0) {
      const lastRow = stackRows[stackRows.length - 1].fieldBase;
      verScale = Math.min(verScale, height / (lastRow.y + lastRow.length));
    }
    setScale(Math.min(horScale, verScale));
  };

  const SetTransferFieldLabels = () => {
    var rlabels = [];
    for (var iRow = 0; iRow < transferRows.length; iRow++) {
      var trRowLabel = new Label(
        `Lane ${transferRows[iRow].fieldBase.row}`,
        transferRows[iRow].fieldBase.x,
        transferRows[iRow].fieldBase.y,
        transferRows[iRow].fieldBase.width,
        transferRows[iRow].fieldBase.length
      );
      rlabels.push(trRowLabel);
    }
    setTrRowLabels(rlabels);
  };

  const handleClickStatusItem = (value) => {
    setContainerList(value.stackedItems);
    setSelectedTier(("0" + (value.itemCount + 1)).slice(-2));
    var res = value.fieldNameComposite.split("-");
    setSelectedBay(res[1]);
    setSelectedRow(res[2]);
    setSelectedFieldType(res[0]);

    setOpen(true);
  };

  const handleClickFieldItem = (value, fieldType) => {
    setContainerList([]);
    setSelectedTier("01");
    setSelectedBay(value.bay);
    setSelectedRow(value.row);
    setSelectedFieldType(fieldType);

    setOpen(true);
  };

  const handleClose = (newValue) => {
    setOpen(false);

    // send the container info add or delete?
  };

  const getCraneDimensions = (devStatus) => {
    var dev = props.devices.filter(
      (device) => device.resourceID === devStatus.stackingCraneID
    )[0];
    if (dev != undefined) {
      return dev.craneDimensions;
    }
  };

  const getCraneOffsets = (devStatus) => {
    var dev = props.devices.filter(
      (device) => device.resourceID === devStatus.stackingCraneID
    )[0];
    if (dev != undefined) {
      return dev.craneMainAxesOffsets;
    }
  };

  return (
    <svg className="data-grid" viewBox={`0 0 ${width} ${height}`}>
      <FieldLayout
        classForValue={"color-stackingField"}
        scale={scale}
        data={props.stFields}
        fieldType={"BA01"}
        showColumnLabels={true}
        columnLabels={columnLabels}
        rowLabels={stRowLabels}
        gutterSize={GUTTER_SIZE}
        onClick={(value) => handleClickFieldItem(value, "BA01")}
        tooltipDataAttrs={(value) => {
          return {
            "data-tip": `Row: ${value.row}, Bay: ${value.bay}`,
          };
        }}
      />

      <FieldLayout
        classForValue={"color-transferField"}
        scale={scale}
        data={props.trFields}
        fieldType={"TZ01"}
        showColumnLabels={false}
        rowLabels={trRowLabels}
        gutterSize={GUTTER_SIZE}
        onClick={(value) => handleClickFieldItem(value, "TZ01")}
        tooltipDataAttrs={(value) => {
          return {
            "data-tip": `Row: ${value.row}, Bay: ${value.bay}`,
          };
        }}
      />
      <ReactTooltip />

      <ContainerLayout
        classForValue={(value, fieldType) => {
          if (!value) {
            if (fieldType == "TZ01") {
              return "color-transferField";
            }
            return "color-stackingField";
          }
          if (fieldType == "BA01") {
            return `color-stackingField-${value}`;
          }
          return `color-transferField-${value}`;
        }}
        scale={scale}
        trFieldItems={props.trFieldStatus}
        stFieldItems={props.stFieldStatus}
        gutterSize={GUTTER_SIZE}
        onClick={(value) => handleClickStatusItem(value)}
        tooltipDataAttrs={(value) => {
          return {
            "data-tip": `Item Count: ${value.itemCount}`,
          };
        }}
      />
      <ContainerDialog
        containerList={containerList}
        bay={selectedBay}
        row={selectedRow}
        tier={selectedTier}
        fieldType={selectedFieldType}
        open={open}
        onClose={handleClose}
        handleDeleteContainer={props.handleDeleteContainer}
        handleAddContainer={props.handleAddContainer}
      />

      {props.deviceStatuses.map((devStatus) => (
        <CraneAnimation
          key={`${devStatus.stackingCraneID}`}
          name={devStatus.stackingCraneID}
          position={devStatus.craneMainAxesPosition}
          state={devStatus.resourceState}
          dimensions={getCraneDimensions(devStatus)}
          offsets={getCraneOffsets(devStatus)}
          scale={scale}
        />
      ))}
      <ReactTooltip />
    </svg>
  );
}

export default FieldStatus;
