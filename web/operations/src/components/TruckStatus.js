import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import { setTruckStatuses, resetTruckStatuses } from "../actions/TruckActions";

const useStyles = makeStyles(
  (theme) => ({
    truckRoot: {
      width: "100%",
      backgroundColor: theme.palette.background.paper,
    },
    nested: {
      paddingLeft: theme.spacing(2),
    },
  }),
  { name: "MuiEcsTruckStatus" }
);

function TruckStatus(props) {
  const classes = useStyles();

  const getStatusColor = (status) => {
    switch (status) {
      case "htArriving":
        return "#e9ac1f";
      case "htWaiting":
        return "#9ed9e7";
      case "htEntering":
        return "#7af26e";
      case "htArrived":
        return "#05bce7";
      case "htLeaving":
        return "#bfc2bf";
      case "htLeft":
        return "#f9f9f9";
      case "htFailed":
        return "#ff0000";
      default:
        return "#ff0000";
    }
  };

  return (
    <div>
      <List
        subheader={
          <ListSubheader style={{ textAlign: 'center', background: "#3f51b5", color: "#ffffff" }}>
            TRUCK STATUS
          </ListSubheader>
        }
        className={classes.truckRoot}
        key="truck-status-list"
      >
        {props.truckStatuses && props.truckStatuses.length > 1 ? 
        props.truckStatuses.map((value) => {
          const labelId = `truck-list-label-${value.htID}`;

          return (
            <div>
              <Divider />
              <ListItem key={`${value.htID}-item`} role={value.htID}>
                <ListItemText
                  id={labelId}
                  key={`${value.htID}-text`}
                  primary={
                    <Typography
                      variant="caption"
                      style={{ background: getStatusColor(value.htState) }}
                    >{`${value.htID}: ${value.htState}`}</Typography>
                  }
                />
              </ListItem>
            </div>
          );
        }): 
        <ListItemText style={{textAlign: 'center'}}> No Trucks Detected </ListItemText>}
      </List>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    truckStatuses: state.truckStatus.truckStatuses,
  };
}

const mapDispatchToProps = {
  setTruckStatuses,
  resetTruckStatuses,
};

export default connect(mapStateToProps, mapDispatchToProps)(TruckStatus);
