import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const protocols = [
    {
      value: 'OPCUA',
      label: 'OPCUA',
    },
    {
      value: 'OPC',
      label: 'OPC',
    },
    {
      value: 'S7',
      label: 'S7',
    },
    {
      value: 'MQTT',
      label: 'MQTT',
    },
  ];

  const types = [
    {
      value: 'StackingCrane',
      label: 'StackingCrane',
    },
    {
      value: 'QuayCrane',
      label: 'QuayCrane',
    },
    {
      value: 'ContainerTruck',
      label: 'ContainerTruck',
    },
    {
      value: 'ContainerAGV',
      label: 'ContainerAGV',
    },
  ];

class CreateDeviceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            type: "",
            protocol: "",
            simulation: true,
        }
    }

    handleChange = (event) => {
        this.setState({
          [event.target.name]: event.target.value
        });
    }

    handleSubmit = () => {
        this.props.handleSubmit(this.state);
    }

    render() {
        return (
            <div>
            <Dialog open={this.props.show} onClose={() => this.props.handleClose()} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Create Device</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Please fill all the information for new device
                    </DialogContentText>
                    <div>
                        <TextField value={this.state.name} onChange={this.handleChange}
                            name="name"
                            id="enter-device-name"
                            helperText="Please enter a unique device name"
                            label="Device Name"
                        />
                    </div>
                    <div>
                        <TextField
                            name="protocol"
                            id="select-protocol"
                            helperText="Please select protocol"
                            select
                            label="Protocol"
                            value={this.state.protocol}
                            onChange={this.handleChange}
                        >
                            {protocols.map(option => (
                                <MenuItem key={option.value} value={option.value}>
                                {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                    </div>
                    <div>
                        <TextField
                            name="type"
                            id="select-device-type"
                            helperText="Please select device type"
                            select
                            label="Device Type"
                            value={this.state.type}
                            onChange={this.handleChange}
                        >
                            {types.map(option => (
                                <MenuItem key={option.value} value={option.value}>
                                {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                    </div>
                    <div>
                        <FormControlLabel
                            value={this.state.simulation}
                            control={<Switch color="primary" />}
                            label="Simulation"
                            labelPlacement="start"
                        />
                    </div>
                </DialogContent>
                <DialogActions>
                <Button onClick={() => this.props.handleClose()} color="primary">
                    Cancel
                </Button>
                <Button onClick={() => this.handleSubmit()} color="primary">
                    Submit
                </Button>
                </DialogActions>
            </Dialog>
            </div>
        );
    }
}

export default CreateDeviceForm;