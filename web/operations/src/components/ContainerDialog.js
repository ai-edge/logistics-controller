import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DashboardIcon from "@material-ui/icons/Dashboard";
import AddIcon from "@material-ui/icons/Add";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import { blue } from "@material-ui/core/colors";
import AddContainerForm from "./AddContainerForm";
import DeleteContainerForm from "./DeleteContainerForm";

const useStyles = makeStyles(
  (theme) => ({
    avatar: {
      backgroundColor: blue[100],
      color: blue[600],
    },
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  }),
  { name: "MuiEcsContainerDialog" }
);

const DialogTitle = (props) => {
  const classes = useStyles();
  const { children, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
};

function ContainerDialog(props) {
  const classes = useStyles();
  const { onClose, open, handleDeleteContainer, handleAddContainer } = props;
  const [openAddContainer, setOpenAddContainer] = useState(false);
  const [openDeleteContainer, setOpenDeleteContainer] = useState(false);
  const [selectedContainer, setSelectedContainer] = useState("");

  const handleClose = () => {
    // send selected value on close
    onClose(null);
  };

  const handleCloseAddContainer = () => {
    // send selected value on close
    setOpenAddContainer(false);
  };

  const handleCloseDeleteContainer = () => {
    // send selected value on close
    setOpenDeleteContainer(false);
  };

  const handleListItemClick = (value) => {
    if (value === "addContainer") {
      setOpenAddContainer(true);
      if (props.containerList.length > 0) {
        setSelectedContainer(props.containerList[0]);
      }
    } else {
      setSelectedContainer(value);
      setOpenDeleteContainer(true);
    }
    onClose(null);
  };

  const handleAddSubmit = (data) => {
    handleAddContainer(data);
    setOpenAddContainer(false);
  };

  const handleDeleteSubmit = (guid) => {
    handleDeleteContainer(guid);
    setOpenDeleteContainer(false);
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
        className={classes.root}
      >
        <DialogTitle id="simple-dialog-title" onClose={handleClose}>
          Containers
        </DialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>Area: {props.fieldType}</Typography>
          <Typography gutterBottom>
            Container information on Bay:{props.bay}, Row: {props.row}
          </Typography>
        </DialogContent>
        <List>
          {props.containerList.map((container) => (
            <ListItem
              button
              onClick={() => handleListItemClick(container)}
              key={container.ISOID}
            >
              <ListItemAvatar>
                <Avatar className={classes.avatar}>
                  <DashboardIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={container.ISOID} />
            </ListItem>
          ))}

          <ListItem
            autoFocus
            button
            onClick={() => handleListItemClick("addContainer")}
          >
            <ListItemAvatar>
              <Avatar>
                <AddIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Add container" />
          </ListItem>
        </List>
      </Dialog>
      <AddContainerForm
        show={openAddContainer}
        bay={props.bay}
        row={props.row}
        tier={props.tier}
        fieldType={props.fieldType}
        container={selectedContainer}
        handleClose={() => handleCloseAddContainer()}
        handleSubmit={(data) => handleAddSubmit(data)}
      />
      <DeleteContainerForm
        container={selectedContainer}
        show={openDeleteContainer}
        handleClose={() => handleCloseDeleteContainer()}
        handleSubmit={(guid) => handleDeleteSubmit(guid)}
      />
    </div>
  );
}

export default ContainerDialog;
