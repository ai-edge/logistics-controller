import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import Header from "./Header";
import "./root.component.css";

export default class Root extends React.Component {
  state = {
    store: this.props.store,
    globalEventDistributor: this.props.globalEventDistributor,
  };

  componentDidCatch(error, info) {
    console.log(error, info);
  }

  render() {
    let ret = <div></div>;
    if (this.state.store && this.state.globalEventDistributor) {
      ret = (
        <Provider store={this.state.store}>
          <BrowserRouter>
            <Header
              globalEventDistributor={this.state.globalEventDistributor}
            />
          </BrowserRouter>
        </Provider>
      );
    }

    return ret;
  }
}
