import React from "react";
import ReactDOM from "react-dom";
import singleSpaReact from "single-spa-react";
import Root from "./root.component.js";

const reactLifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: Root,
  domElementGetter,
});

export function bootstrap(props) {
  console.log("header bootrstapped");
  return reactLifecycles.bootstrap(props);
}

export function mount(props) {
  console.log("header mounted");
  return reactLifecycles.mount(props);
}

export function unmount(props) {
  console.log("header unmounted");
  return reactLifecycles.unmount(props);
}

function domElementGetter() {
  return document.getElementById("navbar");
}
