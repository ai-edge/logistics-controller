import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Redirect, BrowserRouter, Route } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Toolbar from "@material-ui/core/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import LogoutForm from "./components/logoutForm";
import CreateMessage from "./components/createMessage";

import {
  login,
  logout,
  setAdmin,
  updateHeaderText,
  updateSelectedTab,
  updatePageLoading,
  setSocket,
} from "./actions/HeaderActions";

const useStyles = makeStyles(
  (theme) => ({
    secondaryBar: {
      zIndex: 0,
    },
    app: {
      flex: 1,
      display: "flex",
      flexDirection: "column",
    },
    containerGrid: {
      justify: "center",
      spacing: theme.spacing(1),
    },
    footer: {
      padding: theme.spacing(2),
      background: "#eaeff1",
    },
  }),
  { name: "MuiEcsHeader" }
);

var loc = window.location,
  new_uri;

if (loc.host.includes("localhost")) {
  new_uri = "ws://localhost:8084";
} else {
  if (loc.protocol === "https:") {
    new_uri = "wss:";
  } else {
    new_uri = "ws:";
  }
  new_uri += "//" + loc.host + "/srv/bff";
}
var socket = new WebSocket(new_uri);
console.log(`socket is created. address: ${socket.url}`);

function Header(props) {
  const classes = useStyles();
  const [showLogoutDialog, setShowLogoutDialog] = useState(false);
  const [showMessageBox, setShowMessageBox] = useState(false);
  const [messageText, setMessageText] = useState("");
  const [messageHeader, setMessageHeader] = useState("");

  auth.onAuthStateChanged((user) => {
    if (user) {
      props.globalEventDistributor.dispatch(props.login());
      user.getIdTokenResult().then((idTokenResult) => {
        props.globalEventDistributor.dispatch(
          props.setAdmin(idTokenResult.claims.admin)
        );
      });
      props.globalEventDistributor.dispatch(
        props.selectedTab(localStorage.getItem("selectedTab"))
      );
    } else {
      props.globalEventDistributor.dispatch(props.logout());
      props.globalEventDistributor.dispath(props.setAdmin(false));
    }
  });

  useEffect(() => {
    props.globalEventDistributor.dispatch(props.setSocket(socket));
    window.addEventListener("reconnectSocket", (evt) => reconnectSocket());
  }, []);

  const reconnectSocket = () => {
    console.log(`try to recreate websocket connection. address: ${socket.url}`);
    socket = new WebSocket(new_uri);
    props.globalEventDistributor.dispatch(props.setSocket(socket));
  };

  const renderRedirect = () => {
    if (!props.isLoggedIn || props.isLoggedIn === "false") {
      props.globalEventDistributor.dispatch(props.updatePageLoading(true));
      return (
        <BrowserRouter>
          <Route path="/">
            <Redirect to="/#/login" />
          </Route>
        </BrowserRouter>
      );
    } else {
      var fromHash = window.location.hash;
      switch (props.selectedTab) {
        case "0": case "-1":
          return <Redirect from={fromHash} to="/#/operations" />;
        case "1":
          return <Redirect from={fromHash} to="/#/planning" />;
        case "2":
          return <Redirect from={fromHash} to="/#/about" />;
        case "3":
          return <Redirect from={fromHash} to="/#/settings" />;
        default:
          break;
      }
    }
  };

  const handleShowLogout = () => {
    setShowLogoutDialog(true);
  };

  const handleMessageClose = () => {
    setShowMessageBox(false);
  };

  const handleCancelLogout = () => {
    setShowLogoutDialog(false);
  };

  const handleLogout = () => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        setShowLogoutDialog(false);
        props.globalEventDistributor.dispatch(props.logout());
      }),
      (error) => {
        setMessageHeader(error.code);
        setMessageText(error.message);
        setShowMessageBox(true);
      };
  };

  const handleTabChange = (event, newValue) => {
    props.globalEventDistributor.dispatch(props.updateSelectedTab(newValue));
    props.globalEventDistributor.dispatch(props.updatePageLoading(true));
    switch (newValue) {
      case 0:
        props.globalEventDistributor.dispatch(
          props.updateHeaderText("OPERATIONS")
        );
        break;
      case 1:
        props.globalEventDistributor.dispatch(
          props.updateHeaderText("PLANNING")
        );
        break;
      case 2:
        props.globalEventDistributor.dispatch(
          props.updateHeaderText("ABOUT")
        );
        break;
      case 3:
        props.globalEventDistributor.dispatch(
          props.updateHeaderText("SETTINGS")
        );
        break;
      default:
        break;
    }
  };

  return (
    <div>
      <CssBaseline />
      <div className={classes.app}>
        {renderRedirect()}
        <React.Fragment>
          <AppBar color="primary" position="sticky" elevation={0}>
            <Toolbar>
              <Grid container className={classes.containerGrid}>
                <Grid item xs />
                {props.isLoggedIn  || props.isLoggedIn === "true"? (
                  <Tooltip title="Logout">
                    <Button color="inherit" onClick={() => handleShowLogout()}>
                      Logout
                    </Button>
                  </Tooltip>
                ) : null}
              </Grid>
            </Toolbar>
          </AppBar>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            <Toolbar>
              <Grid container alignItems="center" spacing={1}>
                <Grid item xs>
                  <Typography color="inherit" variant="h5" component="h1">
                    {props.headerText}
                  </Typography>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <AppBar
            component="div"
            className={classes.secondaryBar}
            color="primary"
            position="static"
            elevation={0}
          >
            {props.isLoggedIn || props.isLoggedIn === "true"? (
              <Tabs
                value={props.selectedTab}
                onChange={handleTabChange}
                variant="fullWidth"
                textColor="inherit"
              >
                <Tab textColor="inherit" label="Operations" href="#/operations" />
                <Tab textColor="inherit" label="Planning" href="#/planning" />
                <Tab textColor="inherit" label="About" href="#/about" />
                <Tab textColor="inherit" label="Settings" href="#/settings" />
              </Tabs>
            ) : null}
          </AppBar>
        </React.Fragment>
        <div>
          <LogoutForm
            open={showLogoutDialog}
            handleCancelLogout={handleCancelLogout}
            handleLogout={handleLogout}
          />
          <CreateMessage
            open={showMessageBox}
            handleClose={handleMessageClose}
            messageText={messageText}
            messageHeader={messageHeader}
          />
        </div>
      </div>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    isLoggedIn: state.isLoggedIn,
    isAdmin: state.isAdmin,
    headerText: state.headerText,
    selectedTab: state.selectedTab,
  };
}

const mapDispatchToProps = {
  login,
  logout,
  setAdmin,
  updateHeaderText,
  updateSelectedTab,
  updatePageLoading,
  setSocket,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
