import {
  LOGIN,
  LOGOUT,
  UPDATE_HEADER_TEXT,
  UPDATE_SELECTED_TAB,
  UPDATE_PAGE_LOADING,
  SET_ADMIN,
} from "../actions/HeaderActions";
import produce from "immer";

const initialState = {
  isLoggedIn: localStorage.getItem("isLoggedIn") || false,
  isAdmin: false,
  headerText: "Equipment Sim",
  selectedTab: localStorage.getItem("selectedTab") || -1,
  pageLoading: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return produce(state, (draft) => {
        localStorage.setItem("isLoggedIn", true);
        draft.isLoggedIn = true;
      });
    case LOGOUT:
      return produce(state, (draft) => {
        localStorage.setItem("isLoggedIn", false);
        draft.isLoggedIn = false;
      });
    case UPDATE_HEADER_TEXT:
      return produce(state, (draft) => {
        draft.headerText = action.payload;
      });
    case UPDATE_SELECTED_TAB:
      return produce(state, (draft) => {
        localStorage.setItem("selectedTab", action.payload);
        draft.selectedTab = action.payload;
      });
    case UPDATE_PAGE_LOADING:
      return produce(state, (draft) => {
        draft.pageLoading = action.payload;
      });
    case SET_ADMIN:
      return produce(state, (draft) => {
        draft.isAdmin = action.payload;
      });
    default:
      return state;
  }
}
