import { createStore } from "redux";
import rootReducer from "./reducers/HeaderReducer";

export const storeInstance = createStore(rootReducer);
