import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

export default function LogoutForm(props) {
  return (
    <div>
      <Dialog open={props.open} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Logout</DialogTitle>
        <DialogContent>
          <DialogContentText>Do you want to logout?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.handleCancelLogout} color="primary">
            Cancel
          </Button>
          <Button onClick={() => props.handleLogout()} color="primary">
            Logout
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
