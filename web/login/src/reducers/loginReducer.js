import {
  UPDATE_SELECTED_TAB,
  UPDATE_HEADER_TEXT,
  LOGIN,
  LOGOUT,
} from "../actions/loginActions";
import produce from "immer";

const initialState = {
  selectedTab: localStorage.getItem("selectedTab") || -1,
  headerText: "",
  isLoggedIn: localStorage.getItem("isLoggedIn") || false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_SELECTED_TAB:
      return produce(state, (draft) => {
        localStorage.setItem("selectedTab", action.payload);
        draft.selectedTab = action.payload;
      });
    case UPDATE_HEADER_TEXT:
      return produce(state, (draft) => {
        draft.headerText = action.payload;
      });
    case LOGIN:
      return produce(state, (draft) => {
        localStorage.setItem("isLoggedIn", true);
        draft.isLoggedIn = true;
      });
    case LOGOUT:
      return produce(state, (draft) => {
        localStorage.setItem("isLoggedIn", false);
        draft.isLoggedIn = false;
      });
    default:
      return state;
  }
}
