import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import {
  Paper,
  makeStyles,
  Grid,
  TextField,
  Button,
  Typography,
  Link,
} from "@material-ui/core";
import CreateMessage from "./components/createMessage";
import {
  updateSelectedTab,
  updateHeaderText,
  updatePageLoading,
} from "./actions/loginActions";
import "../../style.css";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Equipment Sim
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(
  (theme) => ({
    margin: {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
      marginLeft: theme.spacing(20),
      marginRight: theme.spacing(20),
    },
    main: {
      flex: 1,
      padding: theme.spacing(6, 4),
      background: "#eaeff1",
      width: "70%",
      margin: "auto",
    },
    footer: {
      padding: theme.spacing(2),
      background: "#eaeff1",
    },
    container: {
      background: "#eaeff1",
    },
  }),
  { name: "MuiEcsLogin" }
);

function Login(props) {
  const classes = useStyles();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showMessageBox, setShowMessageBox] = useState(false);
  const [messageText, setMessageText] = useState("");
  const [messageHeader, setMessageHeader] = useState("");

  useEffect(() => {
    props.globalEventDistributor.dispatch(
      props.updateHeaderText("EQUIPMENT SIM")
    );
    props.globalEventDistributor.dispatch(props.updatePageLoading(false));
  }, []);

  const renderRedirect = () => {
    if (props.isLoggedIn || props.isLoggedIn === "true") {
      switch (props.selectedTab) {
        case 0:
          return <Redirect to="/#/operations" />;
        case 1:
          return <Redirect to="/#/planning" />;
        case 2:
          return <Redirect to="/#/about" />;
        case 3:
          return <Redirect to="/#/settings" />;
        default: 
          break;
      }
    }
  };

  const handleChangeEmail = (event) => {
    setEmail(event.target.value);
  };

  const handleChangePassword = (event) => {
    setPassword(event.target.value);
  };

  const handleLogin = (email, password) => {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(
        (cred) => {
          props.globalEventDistributor.dispatch(props.updateSelectedTab(0));
        },
        (error) => {
          setMessageHeader(error.code);
          setMessageText(error.message);
          setShowMessageBox(true);
        }
      );
  };

  const handleMessageClose = () => {
    setShowMessageBox(false);
  };

  return (
    <div className={classes.container}>
      {renderRedirect()}
      <header id="header">
        <div className="intro">
          <div className="overlay">
            <div className="container">
              <div className="row">
                <div className="col-md-8 col-md-offset-2 intro-text">
                  <h1>
                    {"Welcome to Equipment Sim"}
                    <span></span>
                  </h1>
                  <p>
                    {
                      "This project uses React&Redux, Golang microservices, RabbitMQ, MongoDB, MinIO, running on-premises kubernetes cluster."
                    }
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <div className={classes.main}>
        <div className={classes.margin}>
          <Grid item md={true} sm={true} xs={true}>
            <TextField
              margin="dense"
              id="name"
              label="Email Address"
              type="email"
              onChange={handleChangeEmail}
              fullWidth
            />
          </Grid>
          <Grid item md={true} sm={true} xs={true}>
            <TextField
              margin="dense"
              id="password"
              label="Password"
              type="password"
              onChange={handleChangePassword}
              fullWidth
            />
          </Grid>
          <Grid container justify="center">
            <Button
              color="primary"
              onClick={() => handleLogin(email, password)}
            >
              Login
            </Button>
          </Grid>
        </div>
      </div>
      {/* </Paper> */}
      <footer className={classes.footer}>
        <Copyright />
      </footer>

      <CreateMessage
        open={showMessageBox}
        handleClose={handleMessageClose}
        messageText={messageText}
        messageHeader={messageHeader}
      />
    </div>
  );
}

function mapStateToProps(state) {
  return {
    selectedTab: state.selectedTab,
    headerText: state.headerText,
    isLoggedIn: state.isLoggedIn,
  };
}

const mapDispatchToProps = {
  updateSelectedTab,
  updateHeaderText,
  updatePageLoading,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
