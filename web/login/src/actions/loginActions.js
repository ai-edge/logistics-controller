export const UPDATE_SELECTED_TAB = "UPDATE_SELECTED_TAB";
export const UPDATE_HEADER_TEXT = "UPDATE_HEADER_TEXT";
export const UPDATE_PAGE_LOADING = "UPDATE_PAGE_LOADING";
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

export const updateSelectedTab = (tab) => ({
  type: UPDATE_SELECTED_TAB,
  payload: tab,
});

export const updateHeaderText = (text) => ({
  type: UPDATE_HEADER_TEXT,
  payload: text,
});

export const updatePageLoading = (pageLoading) => ({
  type: UPDATE_PAGE_LOADING,
  payload: pageLoading,
});
