const path = require("path");
const TerserPlugin = require("terser-webpack-plugin");

var config = {
  entry: {
    singleSpaEntry: "./src/singleSpaEntry.js",
    store: "./src/store.js",
  },

  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "release"),
    libraryTarget: "amd",
    library: "reactApp",
  },

  module: {
    rules: [
      {
        test: /\.js/,
        use: ["babel-loader?cacheDirectory"],
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              publicPath: "/about/",
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
    ],
  },

  plugins: [],
};

module.exports = (argv) => {
  let prod = argv.mode === "production";

  if (prod) {
    config.plugins.push(
      new TerserPlugin({
        parallel: true,
        terserOptions: {
          ecma: 6,
        },
      })
    );
  }
  return config;
};
