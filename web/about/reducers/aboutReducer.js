import { LOGIN, LOGOUT, UPDATE_HEADER_TEXT } from "../actions/aboutActions";
import produce from "immer";

const initialState = {
  headerText: "",
  isLoggedIn: localStorage.getItem("isLoggedIn") || false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return produce(state, (draft) => {
        localStorage.setItem("isLoggedIn", true);
        draft.isLoggedIn = true;
      });
    case LOGOUT:
      return produce(state, (draft) => {
        localStorage.setItem("isLoggedIn", false);
        draft.isLoggedIn = false;
      });
    case UPDATE_HEADER_TEXT:
      return produce(state, (draft) => {
        draft.headerText = action.payload;
      });
    default:
      return state;
  }
}
