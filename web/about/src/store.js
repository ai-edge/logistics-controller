import { createStore } from "redux";
import aboutReducer from "../reducers/aboutReducer";

export const storeInstance = createStore(aboutReducer);
