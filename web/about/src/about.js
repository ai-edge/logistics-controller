import React, { useEffect } from "react";
import { connect } from "react-redux";
import { BrowserRouter, Redirect } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import "../../style.css";

import {
  updateSelectedTab,
  updateHeaderText,
  updatePageLoading,
} from "../actions/aboutActions";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Equipment Sim
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}
const useStyles = makeStyles(
  (theme) => ({
    main: {
      flex: 1,
      padding: theme.spacing(6, 4),
      margin: "30",
    },
    gridItem: {
      flex: 1,
      padding: theme.spacing(10),
      margin: "30",
    },
    footer: {
      padding: theme.spacing(2),
      background: "#eaeff1",
    },
    app: {
      flex: 1,
      display: "flex",
      flexDirection: "column",
      background: "#eaeff1",
    },
  }),
  { name: "MuiEcsPlanning" }
);

function About(props) {
  const classes = useStyles();

  useEffect(() => {
    if (!props.isLoggedIn || props.isLoggedIn === "false") {
      return;
    }
    props.globalEventDistributor.dispatch(props.updateHeaderText("ABOUT"));
    props.globalEventDistributor.dispatch(props.updateSelectedTab(2));
    props.globalEventDistributor.dispatch(props.updatePageLoading(false));
  }, []);

  const renderRedirect = () => {
    if (!props.isLoggedIn || props.isLoggedIn === "false") {
      return (
        <BrowserRouter>
          <Redirect from="/#/about" to="/#/login" />
        </BrowserRouter>
      );
    }
  };

  return (
    <div className={classes.app}>
      {renderRedirect()}
      <header id="header">
        <div className="intro">
          <div className="overlay">
            <div className="container">
              <div className="row">
                <div className="col-md-8 col-md-offset-2 intro-text">
                  <h1>
                    {"Welcome to Equipment Sim"}
                    <span></span>
                  </h1>
                  <p>
                    {
                      "This project uses React&Redux, Golang microservices, RabbitMQ, MongoDB, MinIO, running on-premises kubernetes cluster."
                    }
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <footer className={classes.footer}>
        <Copyright />
      </footer>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    selectedTab: state.selectedTab,
    headerText: state.headerText,
    isLoggedIn: state.isLoggedIn,
  };
}

const mapDispatchToProps = {
  updateSelectedTab,
  updateHeaderText,
  updatePageLoading,
};

export default connect(mapStateToProps, mapDispatchToProps)(About);
