import React from "react";
import ReactDOM from "react-dom";
import singleSpaReact from "single-spa-react";
import Root from "./root.component.js";

const reactLifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: Root,
  domElementGetter,
});

export function bootstrap(props) {
  console.log("about bootstrapped");
  return reactLifecycles.bootstrap(props);
}

export function mount(props) {
  console.log("about mounted");
  return reactLifecycles.mount(props);
}

export function unmount(props) {
  console.log("about unmounted");
  return reactLifecycles.unmount(props);
}

function domElementGetter() {
  // Make sure there is a div for us to render into
  let el = document.getElementById("about");
  if (!el) {
    el = document.createElement("div");
    el.id = "about";
    document.body.appendChild(el);
  }

  return el;
}
