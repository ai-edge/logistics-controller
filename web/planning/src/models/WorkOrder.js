export default function WorkOrder(resourceID, workItems) {
    this.workOrder = {
        "resourceID": resourceID,
        "workItemRequests": workItems,
    };
}