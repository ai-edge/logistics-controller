export default function WorkItem(
  startingPoint,
  finalPoint,
  containerType,
  priority
) {
  this.from = startingPoint;
  this.to = finalPoint;
  this.containerType = containerType;
  this.priority = priority;
}
