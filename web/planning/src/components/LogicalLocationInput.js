import React, { useEffect, useState } from "react";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(
  (theme) => ({
    internalBox: {
      margin: theme.spacing(2),
      alignContent: "center",
    },
  }),
  { name: "MuiEcsPlanningLLInput" }
);

export default function LogicalLocationInput(props) {
  const [bays, setBays] = useState([
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
  ]);
  const [rows, setRows] = useState([1, 2, 3, 4, 5, 6]);
  const [tiers, setTiers] = useState([1, 2, 3, 4, 5, 6]);
  const [bay, setBay] = useState(1);
  const [row, setRow] = useState(1);
  const [tier, setTier] = useState(1);

  const classes = useStyles();

  useEffect(() => {
    setBay(props.bay);
  }, [props.bay]);

  useEffect(() => {
    setRow(props.row);
  }, [props.row]);

  useEffect(() => {
    setTier(props.tier);
  }, [props.tier]);

  useEffect(() => {
    var data = [];
    for (var i = 1; i <= props.bayCount; i++) {
      data.push(i);
    }
    setBays(data);
    if (bay > props.bayCount) {
      setBay(props.bayCount);
    }
  }, [props.bayCount]);

  useEffect(() => {
    var data = [];
    for (var i = 1; i <= props.rowCount; i++) {
      data.push(i);
    }
    setRows(data);
    if (row > props.rowCount) {
      setRow(props.rowCount);
    }
  }, [props.rowCount]);

  useEffect(() => {
    var data = [];
    if (props.isTransferZone) {
      for (var i = 1; i <= props.tzTierCount; i++) {
        data.push(i);
      }
      setTiers(data);
      if (tier > props.tzTierCount) {
        setTier(props.tzTierCount);
      }
    } else {
      for (var i = 1; i <= props.saTierCount; i++) {
        data.push(i);
      }
      setTiers(data);
      if (tier > props.saTierCount) {
        setTier(props.saTierCount);
      }
    }
  }, [props.tzTierCount, props.saTierCount, props.isTransferZone]);

  return (
    <div>
      <div>
        <div className={classes.internalBox}>
          <TextField
            fullWidth={true}
            name="bay"
            id="bay"
            select
            label="bay"
            value={bay}
            onChange={props.handleBayChange}
          >
            {bays.map((option) => (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            ))}
          </TextField>
        </div>
      </div>
      <div>
        <div className={classes.internalBox}>
          <TextField
            fullWidth={true}
            name="row"
            id="row"
            select
            label="row"
            value={row}
            onChange={props.handleRowChange}
          >
            {rows.map((option) => (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            ))}
          </TextField>
        </div>
      </div>
      <div>
        <div className={classes.internalBox}>
          <TextField
            fullWidth={true}
            name="tier"
            id="tier"
            select
            label="tier"
            value={tier}
            onChange={props.handleTierChange}
          >
            {tiers.map((option) => (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            ))}
          </TextField>
        </div>
      </div>
    </div>
  );
}
