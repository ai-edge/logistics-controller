import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";

import WorkItem from "../models/WorkItem";
import WorkOrder from "../models/WorkOrder";
import LogicalLocationForm from "./LogicalLocationForm";
import WorkItemList from "./WorkItemList";
import CreateMessage from "./CreateMessage";
import { makeStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";

// TODO : these are added manually but maybe there is a better way ??
const types = [
  "C20G0",
  "C20G1",
  "C20G2",
  "C20G3",
  "C22G0",
  "C22G1",
  "C22G2",
  "C22G3",
  "C22G4",
  "C22G8",
  "C22G9",
  "C25G0",
  "C26G0",
  "C2EG0",
  "C40G0",
  "C42G0",
  "C42G1",
];

const useStyles = makeStyles(
  (theme) => ({
    internalBox: {
      margin: theme.spacing(2),
      alignContent: "center",
    },
    paddedBox: {
      margin: theme.spacing(2),
      alignContent: "center",
      paddingBottom: theme.spacing(5),
    },
    appBar: {
      position: "relative",
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1,
    },
    textField: {
      width: "25ch",
    },
  }),
  { name: "MuiEcsPlanningCWOF" }
);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function CreateWorkOrderForm(props) {
  if (props.fieldData === null) {
    return <div></div>;
  }
  const [showFromLocationForm, setShowFromLocationForm] = useState(false);
  const [showToLocationForm, setShowToLocationForm] = useState(false);
  const [disableTransferZones, setDisableTransferZones] = useState(false);
  const [resourceID, setResourceID] = useState("RTG001");
  const [priority, setPriority] = useState(6);
  const [containerType, setContainerType] = useState(types[0]);
  const [woButtonDisabled, setWoButtonDisabled] = useState(true);
  const [addWiButtonDisabled, setAddWiButtonDisabled] = useState(false);
  const [workItemList, setWorkItemList] = React.useState({
    data: [],
  });
  const [itemCount, setItemCount] = useState(0);
  const [startingPoint, setStartingPoint] = useState({});
  const [finalPoint, setFinalPoint] = useState({});
  const [showMessageBox, setShowMessageBox] = useState(false);
  const [messageText, setMessageText] = useState("");
  const [messageHeader, setMessageHeader] = useState("");
  const classes = useStyles();

  const handleSubmit = () => {
    props.handleSubmit(new WorkOrder(resourceID, workItemList.data));
    setWoButtonDisabled(true);
    setAddWiButtonDisabled(false);
    setWorkItemList({
      data: [],
    });
  };

  const addWorkItem = () => {
    if (
      startingPoint.areaName === undefined ||
      finalPoint.areaName === undefined
    ) {
      setMessageHeader("From or To address is invalid");
      setMessageText(
        `Starting Point: ${startingPoint.areaName}-${startingPoint.bay}-${startingPoint.row}-${startingPoint.tier}
         Final Point: ${finalPoint.areaName}-${finalPoint.bay}-${finalPoint.row}-${finalPoint.tier}`
      );
      setShowMessageBox(true);
      return;
    }
    if (
      props.fieldData.transferZoneNames.includes(startingPoint.areaName) &&
      props.fieldData.transferZoneNames.includes(finalPoint.areaName)
    ) {
      setMessageHeader("Invalid Order Type");
      setMessageText(
        "Work item from Transfer Zone to Transfer Zone is not accepted"
      );
      setShowMessageBox(true);
      return;
    }
    const wi = new WorkItem(startingPoint, finalPoint, containerType, priority);

    setWorkItemList((prevState) => {
      const data = [...prevState.data];
      data.push(wi);
      return { ...prevState, data };
    });
    setWoButtonDisabled(false);
  };

  useEffect(() => {
    console.log("item count is updated");
    setItemCount(workItemList.length);
  }, [workItemList]);

  const handleFromLocation = () => {
    setDisableTransferZones(false);
    if (props.fieldData.transferZoneNames.includes(finalPoint.areaName)) {
      setDisableTransferZones(true);
    }
    setShowFromLocationForm(true);
  };

  const handleToLocation = () => {
    setDisableTransferZones(false);
    if (props.fieldData.transferZoneNames.includes(startingPoint.areaName)) {
      setDisableTransferZones(true);
    }
    setShowToLocationForm(true);
  };

  const handleCloseFrom = (data) => {
    setStartingPoint(data);
    setShowFromLocationForm(false);
  };

  const handleUpdateContaier = (data) => {
    setContainerType(data);
  };

  const handleCloseTo = (data) => {
    setFinalPoint(data);
    setShowToLocationForm(false);
  };

  const handleMessageClose = () => {
    setShowMessageBox(false);
  };

  const handleResourceIDChange = (event) => {
    setResourceID(event.target.value);
  };

  const handleContainerTypeChange = (event) => {
    setContainerType(event.target.value);
  };

  const handlePriorityChange = (event) => {
    setPriority(event.target.value);
  };

  return (
    <Dialog
      fullScreen
      open={props.open}
      onClose={props.handleClose}
      TransitionComponent={Transition}
    >
      <AppBar className={classes.appBar}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={props.handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Create Work Order
          </Typography>
        </Toolbar>
      </AppBar>
      <div>
        <div className={classes.internalBox}>
          <Typography
            component={"span"}
            color="textSecondary"
            margin="5px"
            gutterBottom
          >
            Please fill all the information for new work order
          </Typography>
        </div>
        <div>
          <div className={classes.internalBox}>
            <TextField
              className={classes.textField}
              name="resourceID"
              id="resource-id"
              select
              label="ResourceID"
              value={resourceID}
              onChange={handleResourceIDChange}
            >
              {props.devices.map((option) => (
                <MenuItem key={option.resourceID} value={option.resourceID}>
                  {option.resourceID}
                </MenuItem>
              ))}
            </TextField>
          </div>
        </div>
        <div className={classes.internalBox}>
          <div margin="20px">
            <Typography component={"span"} color="textSecondary" gutterBottom>
              Add Work Item
            </Typography>
          </div>
          <div className={classes.internalBox}>
            <TextField
              className={classes.textField}
              name="containerType"
              id="container-type"
              select
              label="Container Type"
              value={containerType}
              onChange={handleContainerTypeChange}
            >
              {types.map((option) => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </div>
          <div className={classes.internalBox}>
            <TextField
              className={classes.textField}
              name="priority"
              id="priority"
              select
              label="Priority"
              value={priority}
              onChange={handlePriorityChange}
            >
              {[0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map((option) => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>
          </div>
          <div className="container outerContainer">
            <div className={classes.internalBox}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleFromLocation}
              >
                From {startingPoint.areaName}-{startingPoint.bay}-
                {startingPoint.row}-{startingPoint.tier}
              </Button>
            </div>
            <div className={classes.internalBox}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleToLocation}
              >
                To {finalPoint.areaName}-{finalPoint.bay}-{finalPoint.row}-
                {finalPoint.tier}
              </Button>
            </div>
            <div
              className={classes.paddedBox}
              alignContent="center"
              paddingBottom="20px"
            >
              <Button
                variant="contained"
                color="primary"
                onClick={addWorkItem}
                disabled={addWiButtonDisabled}
              >
                Add Work Item
              </Button>
            </div>
            <WorkItemList list={workItemList} itemCount={itemCount} />
          </div>
        </div>
        <div className={classes.internalBox}>
          <Button
            variant="contained"
            color="primary"
            onClick={handleSubmit}
            disabled={woButtonDisabled}
          >
            Create Work Order
          </Button>
        </div>
        <div>
          <LogicalLocationForm
            name="from"
            show={showFromLocationForm}
            isToForm={false}
            fieldData={props.fieldData}
            stackingFieldStatuses={props.stackingFieldStatuses}
            disableTransferZones={disableTransferZones}
            handleClose={(data) => handleCloseFrom(data)}
            handleUpdateContainer={(data) => handleUpdateContaier(data)}
          ></LogicalLocationForm>
          <LogicalLocationForm
            name="to"
            isToForm={true}
            show={showToLocationForm}
            fieldData={props.fieldData}
            disableTransferZones={disableTransferZones}
            handleClose={(data) => handleCloseTo(data)}
            handleUpdateContainer={(data) => handleUpdateContaier(data)}
          ></LogicalLocationForm>
        </div>
        <CreateMessage
          open={showMessageBox}
          handleClose={handleMessageClose}
          messageText={messageText}
          messageHeader={messageHeader}
        />
      </div>
    </Dialog>
  );
}

export default CreateWorkOrderForm;
