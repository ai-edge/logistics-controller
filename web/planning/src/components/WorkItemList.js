import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

const useStyles = makeStyles(
  {
    table: {
      minWidth: 350,
    },
  },
  { name: "MuiEcsWorkItemList" }
);

export default function WorkItemList(props) {
  const classes = useStyles();
  const [data, setData] = useState([]);

  useEffect(() => {
    setData(props.list.data);
  }, [props.list.data]);

  return (
    <TableContainer>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="center">From</TableCell>
            <TableCell align="center">To</TableCell>
            <TableCell align="center">Container&nbsp;Type</TableCell>
            <TableCell align="center">Priority</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row, index) => (
            <TableRow key={index}>
              <TableCell align="center">{`${row.from.areaName}-${row.from.bay}-${row.from.row}-${row.from.tier}`}</TableCell>
              <TableCell align="center">{`${row.to.areaName}-${row.to.bay}-${row.to.row}-${row.to.tier}`}</TableCell>
              <TableCell align="center">{row.containerType}</TableCell>
              <TableCell align="center">{row.priority}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
