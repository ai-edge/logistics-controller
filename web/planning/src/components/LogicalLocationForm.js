import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import LogicalLocation from "../models/LogicalLocation";
import LogicalLocationInput from "./LogicalLocationInput";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";
import SelectFromStackingArea from "./SelectFromStackingArea";

const useStyles = makeStyles(
  (theme) => ({
    internalBox: {
      margin: theme.spacing(2),
      alignContent: "center",
    },
    paddedBox: {
      margin: theme.spacing(2),
      alignContent: "center",
      paddingBottom: theme.spacing(5),
    },
  }),
  { name: "MuiEcsPlanningLLForm" }
);

function pad(str, size) {
  while (str.length < size) {
    str = "0" + str;
  }
  return str;
}

function LogicalLocationForm(props) {
  const [areaName, setAreaName] = useState("TZ01");
  const [areaNames, setAreaNames] = useState(["BA01, TZ01"]);
  const [bay, setBay] = useState(5);
  const [row, setRow] = useState(4);
  const [tier, setTier] = useState(1);
  const [bayCount, setBayCount] = useState(15);
  const [rowCount, setRowCount] = useState(1);
  const [saTierCount, setSaTierCount] = useState(5);
  const [tzTierCount, setTzTierCount] = useState(1);
  const [showSASelection, setShowSASelection] = useState(false);
  const [stackingAreas, setStackingAreas] = useState([]);
  const [containerMap, setContainerMap] = useState(new Map());
  const [isTransferZone, setIsTransferZone] = useState(true);
  const [location, setLocation] = useState(
    new LogicalLocation("BA01", "005", "001", "01")
  );

  const classes = useStyles();

  useEffect(() => {
    var bayStr = pad(bay.toString(), 3);
    var rowStr = pad(row.toString(), 3);
    var tierStr = pad(tier.toString(), 2);

    setLocation(new LogicalLocation(areaName, bayStr, rowStr, tierStr));
  }, [bay, row, tier]);

  useEffect(() => {
    if (areaNames.length === 0) {
      setAreaName("");
    } else {
      setAreaName(areaNames[0]);
    }
  }, [areaNames]);

  useEffect(() => {
    if (row > rowCount) {
      setRow(rowCount);
    }
  }, [rowCount]);

  useEffect(() => {
    if (bay > bayCount) {
      setBay(bayCount);
    }
  }, [bayCount]);

  useEffect(() => {
    if (props.fieldData != undefined) {
      var data = [];
      if (props.isToForm) {
        for (let field of props.fieldData.stackingAreaNames) {
          data.push(field);
        }
      }
      if (!props.disableTransferZones) {
        for (let field of props.fieldData.transferZoneNames) {
          data.push(field);
        }
      }
      setAreaNames(data);

      setSaTierCount(props.fieldData.maxSATierCount);
      setTzTierCount(props.fieldData.maxTZTierCount);

      if (props.fieldData.stackingAreaNames.includes(areaName)) {
        setBayCount(props.fieldData.maxStackingBayCount);
        setRowCount(props.fieldData.maxStackingRowCount);
      } else if (props.fieldData.transferZoneNames.includes(areaName)) {
        setBayCount(props.fieldData.maxTransferBayCount);
        setRowCount(props.fieldData.maxTransferRowCount);
      }
    }
  }, [props.fieldData, props.disableTransferZones]);

  useEffect(() => {
    if (props.fieldData != undefined) {
      if (props.fieldData.stackingAreaNames.includes(areaName)) {
        setBayCount(props.fieldData.maxStackingBayCount);
        setRowCount(props.fieldData.maxStackingRowCount);
        setIsTransferZone(false);
      } else if (props.fieldData.transferZoneNames.includes(areaName)) {
        setBayCount(props.fieldData.maxTransferBayCount);
        setRowCount(props.fieldData.maxTransferRowCount);
        setIsTransferZone(true);
      }
    }
    var bayStr = pad(bay.toString(), 3);
    var rowStr = pad(row.toString(), 3);
    var tierStr = pad(tier.toString(), 2);

    setLocation(new LogicalLocation(areaName, bayStr, rowStr, tierStr));
  }, [areaName]);

  const handleAreaNameChange = (event) => {
    setAreaName(event.target.value);
  };

  const handleBayChange = (event) => {
    setBay(parseInt(event.target.value));
  };

  const handleRowChange = (event) => {
    setRow(parseInt(event.target.value));
  };

  const handleTierChange = (event) => {
    setTier(parseInt(event.target.value));
  };

  const showSelectStackingArea = () => {
    var locations = [];
    var map = new Map();
    for (let status of props.stackingFieldStatuses) {
      const compFieldName = status.fieldNameComposite;
      if (status.itemCount > 0) {
        map.set(compFieldName, status.stackedItems[0].containerType);
      }
      var res = compFieldName.split("-");
      for (var i = 1; i <= status.stackedItems.length; i++) {
        var tier = pad(i.toString(), 2);
        locations.push(new LogicalLocation(res[0], res[1], res[2], tier));
      }
    }
    setStackingAreas(locations);
    setContainerMap(map);
    setShowSASelection(true);
  };

  const handleCloseStackingArea = (data) => {
    setShowSASelection(false);
    props.handleClose(data);
    const compFieldName = `${data.areaName}-${data.bay}-${data.row}-01`;
    const contType = containerMap.get(compFieldName);
    if (contType != undefined) {
      props.handleUpdateContainer(contType);
    }
  };

  return (
    <div>
      <Dialog open={props.show} aria-labelledby="form-dialog-title">
        <DialogContent>
          <div>
            <div className={classes.paddedBox}>
              <Button
                variant="contained"
                color="primary"
                onClick={showSelectStackingArea}
                disabled={props.isToForm}
              >
                Select from Stacking Area
              </Button>
            </div>
            <div className={classes.internalBox}>
              <TextField
                fullWidth={true}
                name="areaName"
                id="area-name"
                select
                label="areaName"
                value={areaName}
                onChange={handleAreaNameChange}
              >
                {areaNames.map((option) => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </TextField>
            </div>
          </div>
          <LogicalLocationInput
            bay={bay}
            row={row}
            tier={tier}
            bayCount={bayCount}
            rowCount={rowCount}
            saTierCount={saTierCount}
            tzTierCount={tzTierCount}
            isTransferZone={isTransferZone}
            handleBayChange={handleBayChange}
            handleRowChange={handleRowChange}
            handleTierChange={handleTierChange}
          ></LogicalLocationInput>
          <DialogActions>
            <Button onClick={() => props.handleClose(location)} color="primary">
              OK
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
      <SelectFromStackingArea
        show={showSASelection}
        data={stackingAreas}
        handleClose={(data) => handleCloseStackingArea(data)}
      />
    </div>
  );
}

export default LogicalLocationForm;
