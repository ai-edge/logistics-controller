import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";

const useStyles = makeStyles(
  {
    table: {
      minWidth: 350,
    },
  },
  { name: "MuiEcsSelectStackingArea" }
);

export default function SelectFromStackingArea(props) {
  const classes = useStyles();

  return (
    <Dialog open={props.show} aria-labelledby="form-dialog-title">
      <DialogContent>
        <TableContainer>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Area Name</TableCell>
                <TableCell align="center">Bay</TableCell>
                <TableCell align="center">Row</TableCell>
                <TableCell align="center">Tier</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props.data.map((row, index) => (
                <TableRow
                  hover
                  key={index}
                  onClick={() => props.handleClose(row)}
                >
                  <TableCell align="center">{row.areaName}</TableCell>
                  <TableCell align="center">{row.bay}</TableCell>
                  <TableCell align="center">{row.row}</TableCell>
                  <TableCell align="center">{row.tier}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </DialogContent>
    </Dialog>
  );
}
