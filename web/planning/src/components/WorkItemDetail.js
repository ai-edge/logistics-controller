import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

export default function WorkItemDetail(props) {
  const [workItemText, setWorkItemText] = useState("");
  const [tokens, setTokens] = useState([]);

  useEffect(() => {
    if (props.workItem != null) {
      const fullText = JSON.stringify(props.workItem)
        .replace(/]/gi, "")
        .replace(/\[/gi, "");
      setWorkItemText(fullText.substring(1, fullText.length - 1));
    }
  }, [props.workItem]);

  useEffect(() => {
    let workItemTokens = workItemText.split(",");
    let tempTokens = [];
    let iterationCount = 0;
    for (var i = 0; i < workItemTokens.length; i++) {
      var values = getSubTokens(workItemTokens, i, iterationCount);
      if (iterationCount >= values[2] && iterationCount > 0) {
        // update the last entry, because this chunk belongs to previous line
        var lastToken = tempTokens[tempTokens.length - 1];
        if (lastToken[0][1] != undefined) {
          for (let value of values[0]) {
            lastToken[0][1].push(value);
          }
          tempTokens[tempTokens.length - 1] = lastToken;
        }
      } else {
        tempTokens.push(values[0]);
      }
      i = values[1];
      iterationCount = values[2];
    }
    setTokens(tempTokens);
  }, [workItemText]);

  const getSubTokens = (workItemTokens, i, iterationCount) => {
    var tokenArray = [];
    var token = workItemTokens[i];
    var splitterPos = token.search(":{");
    const newTokens = token.split('":');
    if (splitterPos != -1) {
      // sub data exists
      var tokenArrayValues = [];
      var lastToken = "";
      if (newTokens.length === 3) {
        lastToken = newTokens[2].replace(/}/gi, "");
      }
      tokenArrayValues.push([
        newTokens[1].replace(/"/gi, "").replace(/{/gi, ""),
        lastToken,
      ]);
      i++;
      if (i < workItemTokens.length) {
        iterationCount++;
        var values = getSubTokens(workItemTokens, i, iterationCount);
        for (var valuesInt = 0; valuesInt < values[0].length; valuesInt++) {
          tokenArrayValues.push(values[0][valuesInt]);
        }
        i = values[1];
        iterationCount = values[2];
      }
      tokenArray.push([newTokens[0].replace(/"/gi, ""), tokenArrayValues]);
    } else if (newTokens.length === 2) {
      var innerFinalPos = newTokens[1].search("}");
      if (innerFinalPos != -1) {
        // end of inner data
        const delimiterCount = (newTokens[1].match(/}/g) || []).length;
        iterationCount -= delimiterCount;
        tokenArray.push([
          newTokens[0].replace(/"/gi, ""),
          newTokens[1].replace(/}/gi, ""),
        ]);
      } else {
        tokenArray.push([newTokens[0].replace(/"/gi, ""), newTokens[1]]);
        if (iterationCount > 0) {
          i++;
          if (i < workItemTokens.length) {
            var values = getSubTokens(workItemTokens, i, iterationCount);
            for (var valuesInt = 0; valuesInt < values[0].length; valuesInt++) {
              tokenArray.push(values[0][valuesInt]);
            }
            i = values[1];
            iterationCount = values[2];
          }
        }
      }
    }

    return [tokenArray, i, iterationCount];
  };

  const renderToken = (token, index) => {
    if (token === undefined) {
      return;
    }

    if (!Array.isArray(token[1])) {
      const formattedToken = (
        <Typography gutterBottom>{`${token[0]}: ${token[1]}`}</Typography>
      );
      return formattedToken;
    } else {
      const acordionToken = (
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography gutterBottom> {`${token[0]}`} </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              {token[1].map((value, tokenIndex) => {
                const labelId = `list-label-${index}-${tokenIndex}`;

                return (
                  <div>
                    <ListItem
                      key={`${token[0]}-${index}-${tokenIndex}`}
                      role={`${token[0]}-${index}-${tokenIndex}`}
                    >
                      {renderToken(value, tokenIndex)}
                    </ListItem>
                  </div>
                );
              })}
            </List>
          </AccordionDetails>
        </Accordion>
      );
      return acordionToken;
    }
  };

  if (props.workItem === null) {
    return <div></div>;
  }

  return (
    <div>
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{`${props.workItem.workItemName}`}</DialogTitle>
        <DialogContent dividers>
          <Typography>
            <List>
              {tokens.map((token, index) => {
                const labelId = `list-label-${token}-${index}`;

                return (
                  <ListItem
                    key={`${token}-${index}`}
                    role={`${token}-${index}`}
                  >
                    {renderToken(token[0], index)}
                  </ListItem>
                );
              })}
            </List>
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.handleClose} color="primary" autoFocus>
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
