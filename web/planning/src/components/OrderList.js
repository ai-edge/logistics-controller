import React, { useEffect, useState } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Title from "./Title";
import WorkItemDetail from "./WorkItemDetail";
import CreateMessage from "./CreateMessage";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(
  (theme) => ({
    table: {
      maxWidth: 700,
      alignContent: "center",
      border: "1px solid blue",
      borderRadius: "3px",
      overflow: "hidden",
      background: "#eaeffa",
    },
    nested: {
      paddingLeft: theme.spacing(2),
    },
  }),
  { name: "MuiPlanningOrderList" }
);

export default function OrderList(props) {
  const [orders, setOrders] = useState([]);
  const [selectedWorkItem, setSelectedWorkItem] = useState(null);
  const [showWorkItemDetail, setShowWorkItemDetail] = useState(false);
  const [showMessageBox, setShowMessageBox] = useState(false);
  const [messageText, setMessageText] = useState("");
  const [messageHeader, setMessageHeader] = useState("");
  const classes = useStyles();

  useEffect(() => {
    if (props.orders != undefined) {
      setOrders(props.orders);
    }
  }, [props.orders]);

  const workItemDetailClicked = (workItem) => {
    setSelectedWorkItem(workItem);
    setShowWorkItemDetail(true);
  };

  const handleWorkItemDetailClose = () => {
    setShowWorkItemDetail(false);
  };

  const handleClick = (data, operation) => {
    if (operation === "delete") {
      handleDeleteOrder(data);
    } else if (operation === "put") {
      handlePutOrder(data);
    } else if (operation === "deleteCreated") {
      handleDeleteCreateOrder(data);
    }
  };

  const handleMessageClose = () => {
    setShowMessageBox(false);
  };

  const handleDeleteOrder = (data) => {
    props.sendMsg(`deleteWorkOrder;${data.workOrderName}`);
  };

  const handlePutOrder = (data) => {
    props.sendMsg(`putWorkOrder;${JSON.stringify(data)}`);
  };

  const handleDeleteCreateOrder = (data) => {
    props.sendMsg(`deleteCreatedWorkOrder;${data.workOrderName}`);
  };

  return (
    <div>
      <React.Fragment>
        <div className={classes.table}>
          <Title>{props.title}</Title>
          <Table size="small">
            <TableBody>
              {orders.map((order) => (
                <TableRow key={order.workOrderSequence}>
                  <TableCell>
                    {order.workOrderName}

                    <Collapse in={true} timeout="auto" unmountOnExit>
                      <List component="div" className={classes.nested}>
                        {order.OperationsWorkOrderItems.map((wi) => {
                          const labelId = `checkbox-list-label-${wi.workItemName}`;

                          return (
                            <ListItem
                              key={`${wi.workItemName}-completed`}
                              role={wi.workItemName}
                              dense
                              button
                              onClick={() => workItemDetailClicked(wi)}
                            >
                              <ListItemText
                                id={labelId}
                                primary={`${wi.workItemName} ${
                                  props.operation == "delete"
                                    ? wi.itemStatus
                                    : ""
                                }`}
                              />
                            </ListItem>
                          );
                        })}
                      </List>
                    </Collapse>
                  </TableCell>
                  {props.operation == "put" ? (
                    <div>
                      <TableCell align="center">{order.dispatchTime}</TableCell>
                      <TableCell align="center">
                        <Tooltip title="Delete">
                          <IconButton
                            edge="end"
                            aria-label="delete"
                            onClick={() => handleClick(order, "deleteCreated")}
                          >
                            <DeleteIcon />
                          </IconButton>
                        </Tooltip>
                      </TableCell>
                      <TableCell align="right">
                        <Tooltip title={props.tooltip}>
                          <IconButton
                            edge="end"
                            aria-label="put"
                            onClick={() => handleClick(order, props.operation)}
                          >
                            {props.icon}
                          </IconButton>
                        </Tooltip>
                      </TableCell>
                    </div>
                  ) : (
                    <div>
                      <TableCell align="center">{order.dispatchTime}</TableCell>
                      <TableCell align="right">
                        <Tooltip title={props.tooltip}>
                          <IconButton
                            edge="end"
                            aria-label="put"
                            onClick={() => handleClick(order, props.operation)}
                          >
                            {props.icon}
                          </IconButton>
                        </Tooltip>
                      </TableCell>
                    </div>
                  )}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
      </React.Fragment>

      <WorkItemDetail
        open={showWorkItemDetail}
        handleClose={handleWorkItemDetailClose}
        workItem={selectedWorkItem}
      />
      <CreateMessage
        open={showMessageBox}
        handleClose={handleMessageClose}
        messageText={messageText}
        messageHeader={messageHeader}
      />
    </div>
  );
}
