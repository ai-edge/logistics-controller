import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";

function LogicalLocationForm(props) {
  const [areaName, setAreaName] = useState("BA01");
  const [areaNames, setAreaNames] = useState(["BA01, TZ01"]);

  useEffect(() => {
    setAreaNames(props.areaNames);
    if (props.areaNames.lenght > 0) {
      setAreaName(props.areaNames[0]);
    }
  }, [props.areaNames]);

  const handleAreaNameChange = (event) => {
    setAreaName(event.target.value);
  };

  return (
    <div>
      <Dialog open={props.show} aria-labelledby="form-dialog-title">
        <DialogContent>
          <div>
            <Box m={2}>
              <TextField
                fullWidth={true}
                name="areaName"
                id="area-name"
                select
                label="areaName"
                value={areaName}
                onChange={handleAreaNameChange}
              >
                {areaNames.map((option) => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </TextField>
            </Box>
          </div>
          <DialogActions>
            <Button onClick={() => props.handleClose(areaName)} color="primary">
              OK
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}

export default LogicalLocationForm;
