import {
  SET_STACKING_FIELD_STATUSES,
  CLEAR_STACKING_FIELD_STATUSES,
  UPDATE_STACKING_FIELD_STATUS,
  DELETE_STACKING_FIELD_STATUS,
} from "../actions/FieldActions";
import produce from "immer";

const initialState = {
  stFieldStatus: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_STACKING_FIELD_STATUSES:
      return produce(state, (draft) => {
        if (action.payload === null) {
          draft.stFieldStatus = [];
        } else {
          draft.stFieldStatus = action.payload;
        }
      });
    case CLEAR_STACKING_FIELD_STATUSES:
      return initialState;
    case UPDATE_STACKING_FIELD_STATUS:
      let fieldStatus = action.payload;
      let fieldInState = state.stFieldStatus.find(
        (status) => status.fieldNameComposite === fieldStatus.fieldNameComposite
      );
      if (fieldInState === undefined) {
        return produce(state, (draft) => {
          draft.stFieldStatus.push(action.payload);
        });
      }
      return produce(state, (draft) => {
        draft.stFieldStatus.map((status) =>
          status.fieldNameComposite === fieldStatus.fieldNameComposite
            ? fieldStatus
            : status
        );
      });
    case DELETE_STACKING_FIELD_STATUS:
      let fldStatus = action.payload;
      let tempFldStatuses = state.stFieldStatus.filter(
        (status) => status.fieldNameComposite !== fldStatus.fieldNameComposite
      );
      return produce(state, (draft) => {
        draft.stFieldStatus = tempFldStatuses;
      });
    default:
      return state;
  }
}
