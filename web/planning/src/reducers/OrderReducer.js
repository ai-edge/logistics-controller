import {
  SET_WORK_ORDERS,
  RESET_WORK_ORDERS,
  ADD_WORK_ORDER,
  DELETE_WORK_ORDER,
  UPDATE_WORK_ORDER,
  SET_ACTIVE_WORK_ORDERS,
  RESET_ACTIVE_WORK_ORDERS,
  SET_WAITING_WORK_ORDERS,
  RESET_WAITING_WORK_ORDERS,
  SET_COMPLETED_WORK_ORDERS,
  RESET_COMPLETED_WORK_ORDERS,
  SET_CREATED_WORK_ORDERS,
  ADD_CREATED_WORK_ORDER,
  DELETE_CREATED_WORK_ORDER,
  RESET_CREATED_WORK_ORDERS,
} from "../actions/OrderActions";
import produce from "immer";

const initialState = {
  selectedDeviceId: "",
  devices: [],
  workOrders: [],
  activeWorkOrders: [],
  waitingWorkOrders: [],
  completedWorkOrders: [],
  createdWorkOrders: [],
  isLoggedIn: false,
  fieldData: [],
  stFieldStatus: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_WORK_ORDERS:
      return produce(state, (draft) => {
        draft.workOrders = []
          .concat(action.payload)
          .sort((a, b) => (a.workOrderName > b.workOrderName ? 1 : -1));
      });
    case RESET_WORK_ORDERS:
      return produce(state, (draft) => {
        draft.workOrders = [];
      });
    case ADD_WORK_ORDER:
      return produce(state, (draft) => {
        draft.workOrders = state.workOrders.concat(action.payload);
      });
    case DELETE_WORK_ORDER:
      return produce(state, (draft) => {
        draft.workOrders = state.workOrders.filter(
          (wo) => wo.workOrderName !== action.payload
        );
      });
    case UPDATE_WORK_ORDER:
      let workOrder = action.payload;
      let workOrders = state.workOrders.map((order) =>
        order.workOrderName === workOrder.workOrderName ? workOrder : order
      );
      return produce(state, (draft) => {
        draft.workOrders = workOrders;
      });
    case SET_ACTIVE_WORK_ORDERS:
      return produce(state, (draft) => {
        draft.activeWorkOrders = []
          .concat(action.payload)
          .sort((a, b) => (a.workOrderName > b.workOrderName ? 1 : -1));
      });
    case RESET_ACTIVE_WORK_ORDERS:
      return produce(state, (draft) => {
        draft.activeWorkOrders = [];
      });
    case SET_WAITING_WORK_ORDERS:
      return produce(state, (draft) => {
        draft.waitingWorkOrders = []
          .concat(action.payload)
          .sort((a, b) => (a.workOrderName > b.workOrderName ? 1 : -1));
      });
    case RESET_WAITING_WORK_ORDERS:
      return produce(state, (draft) => {
        draft.waitingWorkOrders = [];
      });
    case SET_COMPLETED_WORK_ORDERS:
      return produce(state, (draft) => {
        draft.completedWorkOrders = []
          .concat(action.payload)
          .sort((a, b) => (a.workOrderName > b.workOrderName ? 1 : -1));
      });
    case RESET_COMPLETED_WORK_ORDERS:
      return produce(state, (draft) => {
        draft.completedWorkOrders = [];
      });
    case SET_CREATED_WORK_ORDERS:
      return produce(state, (draft) => {
        draft.createdWorkOrders = []
          .concat(action.payload)
          .sort((a, b) => (a.workOrderName > b.workOrderName ? 1 : -1));
      });
    case ADD_CREATED_WORK_ORDER:
      return produce(state, (draft) => {
        draft.createdWorkOrders = draft.createdWorkOrders.concat(
          action.payload
        );
      });
    case DELETE_CREATED_WORK_ORDER:
      return produce(state, (draft) => {
        draft.createdWorkOrders = state.createdWorkOrders.filter(
          (wo) => wo.workOrderName !== action.payload
        );
      });
    case RESET_CREATED_WORK_ORDERS:
      return produce(state, (draft) => {
        draft.createdWorkOrders = [];
      });
    default:
      return state;
  }
}
