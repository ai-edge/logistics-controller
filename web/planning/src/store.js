import { createStore } from 'redux';
import rootReducer from './RootReducer';

export const storeInstance = createStore(rootReducer);