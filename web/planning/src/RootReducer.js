import { combineReducers } from "redux";
import deviceReducer from "./reducers/DeviceReducer";
import orderReducer from "./reducers/OrderReducer";
import stStatusReducer from "./reducers/StackingFieldStatusReducer";
import pageReducer from "./reducers/PageReducer";

export default combineReducers({
  deviceData: deviceReducer,
  orderData: orderReducer,
  stStatus: stStatusReducer,
  page: pageReducer,
});
