import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { BrowserRouter, Redirect } from "react-router-dom";
import DeleteIcon from "@material-ui/icons/Delete";
import InputIcon from "@material-ui/icons/Input";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

import CreateWorkOrderForm from "./components/CreateWorkOrderForm";
import CreateMessage from "./components/CreateMessage";
import OrderList from "./components/OrderList";

import { setDevices, resetDevices } from "./actions/DeviceActions";

import {
  updateHeaderText,
  updateSelectedTab,
  updatePageLoading,
  setFieldData,
} from "./actions/PageActions";

import {
  setStackingFieldStatuses,
  updateStackingFieldStatus,
  deleteStackingFieldStatus,
} from "./actions/FieldActions";

import {
  setWorkOrders,
  resetWorkOrders,
  addWorkOrder,
  deleteWorkOrder,
  updateWorkOrder,
  setActiveWorkOrders,
  resetActiveWorkOrders,
  setWaitingWorkOrders,
  resetWaitingWorkOrders,
  setCompletedWorkOrders,
  resetCompletedWorkOrders,
  setCreatedWorkOrders,
  addCreatedWorkOrder,
  deleteCreatedWorkOrder,
  resetCreatedWorkOrders,
} from "./actions/OrderActions";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Equipment Sim
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}
const useStyles = makeStyles(
  (theme) => ({
    main: {
      flex: 1,
      padding: theme.spacing(6, 4),
      margin: "30",
    },
    gridItem: {
      flex: 1,
      padding: theme.spacing(10),
      margin: "30",
    },
    footer: {
      padding: theme.spacing(2),
      background: "#eaeff1",
    },
    app: {
      flex: 1,
      display: "flex",
      flexDirection: "column",
      background: "#eaeff1",
    },
  }),
  { name: "MuiEcsPlanning" }
);

function Planning(props) {
  const [showForm, setShowForm] = useState(false);
  const [showMessageBox, setShowMessageBox] = useState(false);
  const [messageText, setMessageText] = useState("");
  const [messageHeader, setMessageHeader] = useState("");
  const classes = useStyles();

  useEffect(() => {
    if (!props.isLoggedIn || props.isLoggedIn === "false") {
      return;
    }
    props.globalEventDistributor.dispatch(props.updateHeaderText("PLANNING"));
    props.globalEventDistributor.dispatch(props.updateSelectedTab(1));
    props.globalEventDistributor.dispatch(props.updatePageLoading(false));

    if (props.socket != null && props.socket.readyState == WebSocket.OPEN) {
      loadPageData();
    } else {
      setTimeout(heartbeat, 1000);
    }
  }, []);

  useEffect(() => {
    if (props.workOrders.length > 0) {
      updateWorkOrders();
    }
  }, [props.workOrders]);

  useEffect(() => {
    if (props.socket === null) {
      return;
    }
    connectWS((msg) => {
      let data = JSON.parse(msg.data);
      let body = JSON.parse(data.body);
      switch (data.type) {
        case "setDevices":
          props.setDevices(body);
          break;
        case "setWorkOrders":
          if (body) {
            props.setWorkOrders(body);
          } else {
            cleanWorkOrderData();
          }
          break;
        case "setCreatedWorkOrders":
          props.setCreatedWorkOrders(body);
          break;
        case "setFieldData":
          props.setFieldData(body);
          break;
        case "updateWorkOrder":
          props.updateWorkOrder(body);
          break;
        case "addedCreatedWorkOrder":
          props.addCreatedWorkOrder(body);
          break;
        case "deletedCreatedWorkOrder":
          props.deleteCreatedWorkOrder(body);
          break;
        case "addedWorkOrder":
          props.addWorkOrder(body);
          break;
        case "deletedWorkOrder":
          props.deleteWorkOrder(body);
          break;
        case "setStackingFieldStatuses":
          props.setStackingFieldStatuses(body);
          break;
        case "updateStackingFieldStatus":
          props.updateStackingFieldStatus(body);
          break;
        case "deleteStackingFieldStatus":
          props.deleteStackingFieldStatus(body);
          break;
        case "updateWorkOrder":
          props.updateWorkOrder(body);
          break;
        case "errorMessage":
          setMessageHeader("An Error Occured!");
          setMessageText(body);
          setShowMessageBox(true);
          break;
        case "successMessage":
          setMessageHeader("Operation is Successful");
          setMessageText(body);
          setShowMessageBox(true);
          break;
        default:
          console.log(`unhandled message type: ${data.type}`);
          break;
      }
    });
    return function cleanup() {
      console.log(`Planning will unmount`);
    };
  }, [props.socket]);

  const heartbeat = () => {
    console.log(`heartbeat. socket state: ${props.socket.readyState}`);
    if (props.socket.readyState === WebSocket.OPEN) {
      loadPageData();
    } else if (props.socket.readyState === WebSocket.CLOSED) {
      window.dispatchEvent(new CustomEvent("reconnectSocket"));
      setTimeout(heartbeat, 10000);
    } else {
      setTimeout(heartbeat, 1000);
    }
  };

  const connectWS = (cb) => {
    console.log("Attempting Connection...");

    props.socket.onmessage = (msg) => {
      cb(msg);
    };

    props.socket.onopen = () => {
      console.log("Successfully Connected");
      loadPageData();
    };

    props.socket.onclose = (event) => {
      console.log("Socket Closed Connection: ", event);
      window.dispatchEvent(new CustomEvent("reconnectSocket"));
      setTimeout(heartbeat, 10000);
    };

    props.socket.onerror = (error) => {
      var textMess = error.response.data
        .replace(/"/gi, "")
        .replace("message:", "")
        .replace(/}/gi, "")
        .replace(/{/gi, "");
      setMessageHeader("WebSocket Error");
      setMessageText(textMess);
      setShowMessageBox(true);
    };
  };

  const sendMsg = (msg) => {
    console.log("sending msg: ", msg);
    props.socket.send(msg);
  };

  const renderRedirect = () => {
    if (!props.isLoggedIn || props.isLoggedIn === "false") {
      return (
        <BrowserRouter>
          <Redirect from="/#/planning" to="/#/login" />
        </BrowserRouter>
      );
    }
  };

  const loadPageData = () => {
    sendMsg(`getDevices; ""`);
    sendMsg(`getWorkOrders; ""`);
    sendMsg(`getCreatedWorkOrders; ""`);
    sendMsg(`getFieldData; ""`);
    sendMsg(`getStackingFieldStatuses; ""`);
  };

  const cleanWorkOrderData = () => {
    props.resetWorkOrders();
    props.resetActiveWorkOrders();
    props.resetCompletedWorkOrders();
    props.resetWaitingWorkOrders();
  };

  const updateWorkOrders = () => {
    props.setActiveWorkOrders(
      props.workOrders.filter((wo) => hasActiveWorkItem(wo))
    );
    props.setWaitingWorkOrders(
      props.workOrders.filter((wo) => allWorkItemsWaiting(wo))
    );
    props.setCompletedWorkOrders(
      props.workOrders.filter(
        (wo) => !(hasActiveWorkItem(wo) || allWorkItemsWaiting(wo))
      )
    );
  };

  const hasActiveWorkItem = (workOrder) => {
    var waitingItemExists = false;
    var completedItemExists = false;
    for (let wi of workOrder.OperationsWorkOrderItems) {
      if (wi.itemStatus === "working") {
        return true;
      }
      if (
        wi.itemStatus != "completed" &&
        wi.itemStatus != "completed_different_target_location" &&
        wi.itemStatus != "completed_automatically" &&
        wi.itemStatus != "completed_manually" &&
        wi.itemStatus != "aborted"
      ) {
        completedItemExists = true;
      } else if (wi.itemStatus != "entered" && wi.itemStatus != "waiting") {
        waitingItemExists = true;
      }
    }
    return waitingItemExists && completedItemExists;
  };

  const allWorkItemsWaiting = (workOrder) => {
    for (let wi of workOrder.OperationsWorkOrderItems) {
      if (wi.itemStatus != "entered" && wi.itemStatus != "waiting") {
        return false;
      }
    }
    return true;
  };

  const handleOpenCreateWorkOrder = () => {
    setShowForm(true);
  };

  const handleCloseCreateWorkOrder = () => {
    setShowForm(false);
  };

  const handleMessageClose = () => {
    setShowMessageBox(false);
  };

  const handleSubmit = (data) => {
    var workOrderCommand = JSON.stringify(data);
    var n = workOrderCommand.indexOf(":");
    sendMsg(`createWorkOrder;${workOrderCommand.substring(n + 1)}`);
    setShowForm(false);
  };

  return (
    <div className={classes.app}>
      {renderRedirect()}
      <Grid container direction="row">
        <Grid item xs={3}>
          <div className={classes.main}>
            <Button
              color="primary"
              variant="contained"
              onClick={handleOpenCreateWorkOrder}
            >
              Create Work Order
            </Button>
          </div>
          <CreateWorkOrderForm
            open={showForm}
            devices={props.devices}
            fieldData={props.fieldData}
            stackingFieldStatuses={props.stackingFieldStatuses}
            handleClose={handleCloseCreateWorkOrder}
            handleSubmit={(data) => handleSubmit(data)}
          />
        </Grid>
        <Grid item xs={9}>
          <div className={classes.main}>
            <OrderList
              orders={props.createdWorkOrders}
              setCreatedWorkOrders={props.setCreatedWorkOrders}
              resetCreatedWorkOrders={props.resetCreatedWorkOrders}
              sendMsg={(msg) => sendMsg(msg)}
              tooltip="Put Order"
              title="Created Work Orders"
              icon={<InputIcon />}
              operation="put"
            />
          </div>
        </Grid>
      </Grid>
      <Grid container direction="row">
        <Grid item xs={6}>
          <div className={classes.gridItem}>
            <OrderList
              orders={props.waitingWorkOrders}
              sendMsg={(msg) => sendMsg(msg)}
              tooltip="Cancel"
              title="Waiting Work Orders"
              icon={<DeleteIcon />}
              operation="delete"
            />
          </div>
        </Grid>
        <Grid item xs={6}>
          <div className={classes.gridItem}>
            <OrderList
              orders={props.activeWorkOrders}
              sendMsg={(msg) => sendMsg(msg)}
              tooltip="Abort"
              title="Active Work Orders"
              icon={<DeleteIcon />}
              operation="delete"
            />
          </div>
        </Grid>
      </Grid>
      <Grid container direction="row" justify="center">
      <div style={{width: '700px'}}>
        <OrderList
          orders={props.completedWorkOrders}
          sendMsg={(msg) => sendMsg(msg)}
          tooltip="Delete"
          title="Finished (Not Active) Work Orders"
          icon={<DeleteIcon />}
          operation="delete"
        />
        </div>
      </Grid>

      <footer className={classes.footer}>
        <Copyright />
      </footer>
      <div>
        <CreateMessage
          open={showMessageBox}
          handleClose={handleMessageClose}
          messageText={messageText}
          messageHeader={messageHeader}
        />
      </div>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    devices: state.deviceData.devices,
    workOrders: state.orderData.workOrders,
    activeWorkOrders: state.orderData.activeWorkOrders,
    waitingWorkOrders: state.orderData.waitingWorkOrders,
    completedWorkOrders: state.orderData.completedWorkOrders,
    createdWorkOrders: state.orderData.createdWorkOrders,
    stackingFieldStatuses: state.stStatus.stFieldStatus,
    fieldData: state.page.fieldData,
    isLoggedIn: state.page.isLoggedIn,
    socket: state.page.socket,
  };
}

const mapDispatchToProps = {
  updateSelectedTab,
  updateHeaderText,
  updatePageLoading,
  setDevices,
  resetDevices,
  setWorkOrders,
  resetWorkOrders,
  addWorkOrder,
  deleteWorkOrder,
  updateWorkOrder,
  setActiveWorkOrders,
  resetActiveWorkOrders,
  setWaitingWorkOrders,
  resetWaitingWorkOrders,
  setCompletedWorkOrders,
  resetCompletedWorkOrders,
  setCreatedWorkOrders,
  addCreatedWorkOrder,
  deleteCreatedWorkOrder,
  resetCreatedWorkOrders,
  updateHeaderText,
  updateSelectedTab,
  updatePageLoading,
  setFieldData,
  setStackingFieldStatuses,
  updateStackingFieldStatus,
  deleteStackingFieldStatus,
};

export default connect(mapStateToProps, mapDispatchToProps)(Planning);
