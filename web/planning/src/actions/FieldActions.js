export const SET_STACKING_FIELDS = "SET_STACKING_FIELDS";
export const UPDATE_STACKING_FIELD_STATUS = "UPDATE_STACKING_FIELD_STATUS";
export const DELETE_STACKING_FIELD_STATUS = "DELETE_STACKING_FIELD_STATUS";
export const SET_STACKING_FIELD_STATUSES = "SET_STACKING_FIELD_STATUSES";
export const CLEAR_STACKING_FIELD_STATUSES = "CLEAR_STACKING_FIELD_STATUSES";
export const DELETE_CONTAINER = "DELETE_CONTAINER";

export const setStackingFields = (stFields) => ({
  type: SET_STACKING_FIELDS,
  payload: stFields,
});
export const updateStackingFieldStatus = (stFieldStatus) => ({
  type: UPDATE_STACKING_FIELD_STATUS,
  payload: stFieldStatus,
});
export const deleteStackingFieldStatus = (stFieldStatus) => ({
  type: DELETE_STACKING_FIELD_STATUS,
  payload: stFieldStatus,
});
export const setStackingFieldStatuses = (stFieldStatuses) => ({
  type: SET_STACKING_FIELD_STATUSES,
  payload: stFieldStatuses,
});
export const clearStackingFieldStatuses = () => ({
  type: CLEAR_STACKING_FIELD_STATUSES,
  payload: [],
});
