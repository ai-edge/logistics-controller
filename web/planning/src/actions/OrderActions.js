export const SET_WORK_ORDERS = "SET_WORK_ORDERS";
export const ADD_WORK_ORDER = "ADD_WORK_ORDER";
export const DELETE_WORK_ORDER = "DELETE_WORK_ORDER";
export const UPDATE_WORK_ORDER = "UPDATE_WORK_ORDER";
export const RESET_WORK_ORDERS = "RESET_WORK_ORDERS";
export const SET_ACTIVE_WORK_ORDERS = "SET_ACTIVE_WORK_ORDERS";
export const RESET_ACTIVE_WORK_ORDERS = "RESET_ACTIVE_WORK_ORDERS";
export const SET_WAITING_WORK_ORDERS = "SET_WAITING_WORK_ORDERS";
export const RESET_WAITING_WORK_ORDERS = "RESET_WAITING_WORK_ORDERS";
export const SET_COMPLETED_WORK_ORDERS = "SET_COMPLETED_WORK_ORDERS";
export const RESET_COMPLETED_WORK_ORDERS = "RESET_COMPLETED_WORK_ORDERS";
export const SET_CREATED_WORK_ORDERS = "SET_CREATED_WORK_ORDERS";
export const ADD_CREATED_WORK_ORDER = "ADD_CREATED_WORK_ORDER";
export const DELETE_CREATED_WORK_ORDER = "DELETE_CREATED_WORK_ORDER";
export const RESET_CREATED_WORK_ORDERS = "RESET_CREATED_WORK_ORDERS";

export const setWorkOrders = (workOrders) => ({
  type: SET_WORK_ORDERS,
  payload: workOrders,
});
export const addWorkOrder = (workOrder) => ({
  type: ADD_WORK_ORDER,
  payload: workOrder,
});
export const deleteWorkOrder = (workOrderName) => ({
  type: DELETE_WORK_ORDER,
  payload: workOrderName,
});
export const updateWorkOrder = (workOrder) => ({
  type: UPDATE_WORK_ORDER,
  payload: workOrder,
});
export const resetWorkOrders = () => ({
  type: RESET_WORK_ORDERS,
});
export const setActiveWorkOrders = (workOrders) => ({
  type: SET_ACTIVE_WORK_ORDERS,
  payload: workOrders,
});
export const resetActiveWorkOrders = () => ({
  type: RESET_ACTIVE_WORK_ORDERS,
});
export const setWaitingWorkOrders = (workOrders) => ({
  type: SET_WAITING_WORK_ORDERS,
  payload: workOrders,
});
export const resetWaitingWorkOrders = () => ({
  type: RESET_WAITING_WORK_ORDERS,
});
export const setCompletedWorkOrders = (workOrders) => ({
  type: SET_COMPLETED_WORK_ORDERS,
  payload: workOrders,
});
export const resetCompletedWorkOrders = () => ({
  type: RESET_COMPLETED_WORK_ORDERS,
});
export const setCreatedWorkOrders = (workOrders) => ({
  type: SET_CREATED_WORK_ORDERS,
  payload: workOrders,
});
export const addCreatedWorkOrder = (workOrder) => ({
  type: ADD_CREATED_WORK_ORDER,
  payload: workOrder,
});
export const deleteCreatedWorkOrder = (workOrderName) => ({
  type: DELETE_CREATED_WORK_ORDER,
  payload: workOrderName,
});
export const resetCreatedWorkOrders = () => ({
  type: RESET_CREATED_WORK_ORDERS,
});
