export const SET_SELECTED_DEVICE = "SET_SELECTED_DEVICE";
export const SET_DEVICES = "SET_DEVICES";
export const RESET_DEVICES = "RESET_DEVICES";
export const ADD_DEVICE = "ADD_DEVICE";
export const DELETE_DEVICE = "DELETE_DEVICE";
export const UPDATE_DEVICE = "UPDATE_DEVICE";
export const SET_DEVICE_STATUSES = "SET_DEVICE_STATUSES";
export const RESET_DEVICE_STATUSES = "RESET_DEVICE_STATUSES";
export const UPDATE_DEVICE_STATUS = "UPDATE_DEVICE_STATUS";

export const setSelectedDevice = (id) => ({
  type: SET_SELECTED_DEVICE,
  payload: id,
});
export const setDevices = (devices) => ({
  type: SET_DEVICES,
  payload: devices,
});
export const resetDevices = () => ({
  type: RESET_DEVICES,
});
export const addDevice = (device) => ({
  type: ADD_DEVICE,
  payload: device,
});
export const deleteDevice = (name) => ({
  type: DELETE_DEVICE,
  payload: name,
});
export const updateDevice = (device) => ({
  type: UPDATE_DEVICE,
  payload: device,
});
export const setDeviceStatuses = (deviceStatuses) => ({
  type: SET_DEVICE_STATUSES,
  payload: deviceStatuses,
});
export const resetDeviceStatuses = () => ({
  type: RESET_DEVICE_STATUSES,
});
export const updateDeviceStatus = (status) => ({
  type: UPDATE_DEVICE_STATUS,
  payload: status,
});
