ag api/base/tosgw-asyncapi.yaml @asyncapi/markdown-template -o api/generated/ecs-interfaces/out/base/tosgw-asyncapi
ag api/base/operations-asyncapi.yaml @asyncapi/markdown-template -o api/generated/ecs-interfaces/out/base/operations-asyncapi
ag api/base/scgw-asyncapi.yaml @asyncapi/markdown-template -o api/generated/ecs-interfaces/out/base/scgw-asyncapi
ag api/base/scheduler-asyncapi.yaml @asyncapi/markdown-template -o api/generated/ecs-interfaces/out/base/scheduler-asyncapi
ag api/base/dispatcher-asyncapi.yaml @asyncapi/markdown-template -o api/generated/ecs-interfaces/out/base/dispatcher-asyncapi
ag api/base/ht-asyncapi.yaml @asyncapi/markdown-template -o api/generated/ecs-interfaces/out/base/ht-asyncapi

java -jar third_party/openapi-codegen/openapi-generator-cli.jar generate -i api/base/operations-restapi.yaml -g go -o api/generated/models -c third_party/openapi-codegen/models-config.json -Dmodels --ignore-file-override=openapi-generator-ignore
java -jar third_party/openapi-codegen/openapi-generator-cli.jar generate -i api/base/tosgw-restapi.yaml -g go -o api/generated/models -c third_party/openapi-codegen/models-config.json -Dmodels --ignore-file-override=openapi-generator-ignore
java -jar third_party/openapi-codegen/openapi-generator-cli.jar generate -i api/base/sci-restapi.yaml -g go -o api/generated/models -c third_party/openapi-codegen/models-config.json -Dmodels --ignore-file-override=openapi-generator-ignore
java -jar third_party/openapi-codegen/openapi-generator-cli.jar generate -i api/base/scheduler-restapi.yaml -g go -o api/generated/models -c third_party/openapi-codegen/models-config.json -Dmodels --ignore-file-override=openapi-generator-ignore
java -jar third_party/openapi-codegen/openapi-generator-cli.jar generate -i api/base/dispatcher-restapi.yaml -g go -o api/generated/models -c third_party/openapi-codegen/models-config.json -Dmodels --ignore-file-override=openapi-generator-ignore
java -jar third_party/openapi-codegen/openapi-generator-cli.jar generate -i api/base/ht-restapi.yaml -g go -o api/generated/models -c third_party/openapi-codegen/models-config.json -Dmodels --ignore-file-override=openapi-generator-ignore
java -jar third_party/openapi-codegen/openapi-generator-cli.jar generate -i api/base/bff-restapi.yaml -g go -o api/generated/models -c third_party/openapi-codegen/models-config.json -Dmodels --ignore-file-override=openapi-generator-ignore

cd ./api/base
mkdir -p ../../api/generated/ecs-interfaces/out/base/operations-restapi/
api2html operations-restapi.yaml -o ../../api/generated/ecs-interfaces/out/base/operations-restapi/index.html -r /
mkdir -p ../../api/generated/ecs-interfaces/out/base/tosgw-restapi/
api2html tosgw-restapi.yaml  -o ../../api/generated/ecs-interfaces/out/base/tosgw-restapi/index.html -r /
mkdir -p ../../api/generated/ecs-interfaces/out/base/sci-restapi/
api2html sci-restapi.yaml  -o ../../api/generated/ecs-interfaces/out/base/sci-restapi/index.html -r /
mkdir -p ../../api/generated/ecs-interfaces/out/base/scheduler-restapi/
api2html scheduler-restapi.yaml  -o ../../api/generated/ecs-interfaces/out/base/scheduler-restapi/index.html -r /
mkdir -p ../../api/generated/ecs-interfaces/out/base/dispatcher-restapi/
api2html dispatcher-restapi.yaml  -o ../../api/generated/ecs-interfaces/out/base/dispatcher-restapi/index.html -r /
mkdir -p ../../api/generated/ecs-interfaces/out/base/ht-restapi/
api2html ht-restapi.yaml  -o ../../api/generated/ecs-interfaces/out/base/ht-restapi/index.html -r /
mkdir -p ../../api/generated/ecs-interfaces/out/base/bff-restapi/
api2html bff-restapi.yaml  -o ../../api/generated/ecs-interfaces/out/base/bff-restapi/index.html -r /
