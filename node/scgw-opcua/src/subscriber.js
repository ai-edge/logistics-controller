const connectOptions={
  clientId: process.env.SERVICE_ID + "-subscriber",
  username: process.env.RABBIT_USER,
  password: process.env.RABBIT_PASSWD,
  keepalive: 60,
  clean:true
};

var mqtt = require('mqtt');



	//start receiving jobs from topic "scgw/{craneid}/jobrequest"
	//start receiving status from topic "scgw/+/cranestatus"



module.exports = function () {
  this.client = mqtt.connect(process.env.RABBIT_URL, connectOptions);
  this.subscribe = function (topic) { 
        this.client.subscribe(topic, function (err) {
          if (!err) {
            console.log('subscribe successful');
          }
        })
  }
  this.client.on('connect', function () {
      console.log("subscriber connected");
  })
  this.client.on('message', function (topic, message) {
    // message is Buffer
    console.log(topic);
    console.log(message.toString());
  })
}
