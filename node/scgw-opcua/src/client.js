/*global require,console,setTimeout */
const opcua = require("node-opcua");
const async = require("async");
const crypto = require("node-opcua-crypto");
const Publisher = require("./publisher.js")
const Subscriber = require("./subscriber.js")

const craneStatusTopic = "scgw/" + process.env.SERVICE_ID + "/cranestatus";
const craneJobTopic = "scgw/" + process.env.SERVICE_ID + "/jobrequest";

var publisher = new Publisher();
var subscriber = new Subscriber();
subscriber.subscribe(craneJobTopic)

const opcURL = process.env.address;

var options = {
    requestedSessionTimeout: 10000,
    applicationName: process.env.SERVICE_ID,
    endpoint_must_exist: false,
    certificateFile: process.env.certificateFile,
    privateKeyFile: process.env.privateKeyFile,
    serverCertificate: crypto.readCertificate(process.env.serverCertificate)
};

const client = opcua.OPCUAClient.create(options);

client.on("backoff", (retry, delay) =>
    console.log("still trying to connect to ", endpointUrl, ": retry =", retry, "next attempt in ", delay / 1000, "seconds")
);

let the_session, the_subscription, browseNodeId;

async.series([

    // step 1 : connect to
    function (callback) {
        client.connect(opcURL, function (err) {
            if (err) {
                console.log(" cannot connect to endpoint :", endpointUrl);
            } else {
                console.log("connected !");
            }
            callback(err);
        });
    },

    // step 2 : createSession
    function (callback) {
        client.createSession(function (err, session) {
            if (err) {
                return callback(err);
            }
            the_session = session;
            callback();
        });
    },

    // step 3 : browse
    function (callback) {
        console.log('rootFolder')
        the_session.browse("RootFolder", function (err, browseResult) {
            if (!err) {
                console.log("Browsing rootfolder: ");
                for (let reference of browseResult.references) {
                    console.log('...', reference.browseName, reference.nodeId);
                }
            }
            callback(err);
        });
    },

    // step 4 : read a variable with readVariableValue
      function (callback) {
  
          the_session.readVariableValue("ns=1;s=free_memory", function (err, dataValue) {
              if (!err) {
                  console.log(" free mem % = ", dataValue.toString());
              }
              callback(err);
          });
      },
     function (callback) {
        const browsePath = opcua.makeBrowsePath(
            "ObjectsFolder",
            "/1:crane1/1:ResourceState"
          );
          
        the_session.translateBrowsePath(browsePath, (err, result) =>{
            if (err) { return callback(err); }
            browseNodeId = result.targets[0].targetId;
            console.log(" Browse nodeId = ", browseNodeId.toString());
            callback();
          });
     },

    // step 5: install a subscription and install a monitored item
    function (callback) {
        const subscriptionOptions = {
            maxNotificationsPerPublish: 1000,
            publishingEnabled: true,
            requestedLifetimeCount: 100,
            requestedMaxKeepAliveCount: 10,
            requestedPublishingInterval: 1000
        };
        the_session.createSubscription2(subscriptionOptions, (err, subscription) => {

            if (err) { return callback(err); }

            the_subscription = subscription;

            the_subscription.on("started", () => {
                console.log("subscription started- subscriptionId=", the_subscription.subscriptionId);
            }).on("keepalive", function () {
                console.log("subscription keepalive");
            }).on("terminated", function () {
                console.log("terminated");
            });
            callback();
        });
    },
    function (callback) {
        // install monitored item
        const monitoredItem = the_subscription.monitor({
            nodeId: opcua.resolveNodeId(browseNodeId),
            attributeId: opcua.AttributeIds.Value
        },
            {
                samplingInterval: 100,
                discardOldest: true,
                queueSize: 10
            },
            opcua.TimestampsToReturn.Both
        );

        monitoredItem.then(item => {
            item.on("changed", function (item) {
                console.log(browseNodeId.toString(), item.value.value);
                publisher.publish(craneStatusTopic, browseNodeId.toString() + item.value.value)
            });
            item.on("err", function (err_message) {
                console.log(item.itemToMonitor.nodeId.toString(), " ERROR".red, err_message);
            });
        })
        console.log('monitored item')

    }

],
function (err) {
    if (err) {
        console.log(" failure ", err);
    } else {
        console.log("done!");
    }
    client.disconnect(function () { });
});