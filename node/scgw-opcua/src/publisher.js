const connectOptions={
  clientId: process.env.SERVICE_ID + "-publisher",
  username: process.env.RABBIT_USER,
  password: process.env.RABBIT_PASSWD,
  keepalive: 60,
  clean:true
};

	//Publish sc status to "scgw/{craneid}/cranestatus"

const qos1={
  qos:1
};

var mqtt = require('mqtt');

module.exports = function () {
  this.client = mqtt.connect(process.env.RABBIT_URL, connectOptions);
  this.publish = function (topic, message) { 
        this.client.publish(topic, message, qos1);
    }
  this.client.on('connect', function () {
      console.log("publisher connected");
  })
}