const opcua = require("node-opcua");
const ScraneStatus = require("../../models/scraneStatus");
const ScraneCommand = require("../../models/scraneCommand");
const Speed = require("../../models/speed");

module.exports = class AddressSpace  {
    constructor(id){
        this.status = new ScraneStatus.ScraneStatus(id);
        this.command = new ScraneCommand.ScraneCommand();
        this.nominalSpeeds = new Speed.SpeedAbsolute(90,60,120);
    }

    build(server, deviceName) {
        this.addressSpace = server.engine.addressSpace;
        this.namespace = this.addressSpace.getOwnNamespace();
        this.deviceName = deviceName;

        let device = this.addObject(this.addressSpace.rootFolder.objects, this.deviceName);
        this.addReadOnlyVariable(device,"resourceState",opcua.DataType.Int32,this.status);
        this.addReadOnlyVariable(device,"stackingCraneID",opcua.DataType.String,this.status);
        this.addVariable(device,"command",opcua.DataType.Int32,this.command);
        let itemIdInfoObject = this.addObject(device,"handledItemIDInfo");
        this.addReadOnlyVariable(itemIdInfoObject,"ISOID",opcua.DataType.String,this.status.handledItemIDInfo);
        this.addReadOnlyVariable(itemIdInfoObject,"containerType",opcua.DataType.String,this.status.handledItemIDInfo);
        let devicePosition = this.addObject(device,"craneMainAxesPosition");
        this.addReadOnlyVariable(devicePosition,"gantryPosition",opcua.DataType.Int32,this.status.craneMainAxesPosition);
        this.addReadOnlyVariable(devicePosition,"trolleyPosition",opcua.DataType.Int32,this.status.craneMainAxesPosition);
        this.addReadOnlyVariable(devicePosition,"hoistPosition",opcua.DataType.Int32,this.status.craneMainAxesPosition);
        let speed = this.addObject(device,"craneSpeedsPercentageOfNominal");
        this.addReadOnlyVariable(speed,"gantrySpeedPercentage",opcua.DataType.Int32,this.status.craneSpeedsPercentageOfNominal);
        this.addReadOnlyVariable(speed,"trolleySpeedPercentage",opcua.DataType.Int32,this.status.craneSpeedsPercentageOfNominal);    
        this.addReadOnlyVariable(speed,"hoistSpeedPercentage",opcua.DataType.Int32,this.status.craneSpeedsPercentageOfNominal); 
        let destination = this.addObject(device,"destination");
        this.addVariable(destination,"bayPosition",opcua.DataType.Int32,this.command.destination);
        this.addVariable(destination,"rowPosition",opcua.DataType.Int32,this.command.destination);
        this.addVariable(destination,"tierPosition",opcua.DataType.Int32,this.command.destination); 
    }

    addObject = (organizedBy, browseName) => {
         return this.namespace.addObject({
            organizedBy: organizedBy,
            browseName: browseName
        });         
    }

    addReadOnlyVariable = (componentOf, browseName,dataType,opcObject) => {
        this.namespace.addVariable({
        componentOf: componentOf,
        browseName: browseName,
        dataType: dataType,
        value: {
            get: () => {
                return new opcua.Variant({ dataType: dataType, value: opcObject[browseName] });
                }
            }
        });
    } 

    addVariable = (componentOf, browseName,dataType,opcObject) => {
        this.namespace.addVariable({
        componentOf: componentOf,
        browseName: browseName,
        dataType: dataType,
        value: {
            get: () => {
                return new opcua.Variant({ dataType: dataType, value: opcObject[browseName] });
                },
            set: (variant) => {
                    opcObject[browseName] = variant.value;
                    return opcua.StatusCodes.Good;
                }
            }
        });
    } 
}
