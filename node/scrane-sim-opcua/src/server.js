/*global require,setInterval,console */
const opcua = require("node-opcua");
const AddressSpace = require("./addressSpace");
const program = require("../../models/program");

const PORT = 4334

// Let's create an instance of OPCUAServer
const server = new opcua.OPCUAServer({
    port: PORT, // the port of the listening socket of the server
    resourcePath: process.env.resourcePath, // this path will be added to the endpoint resource name
    certificateFile: process.env.certificateFile,
    privateKeyFile: process.env.privateKeyFile,
    allowAnonymous: true,
    buildInfo: {
        productName: "Scrane-simulator",
        buildNumber: "7658",
        buildDate: new Date(),
        manufacturerName: "Siemens"
    }
});

function construct_my_address_space(server) {
    var addressSpace = new AddressSpace(process.env.SERVICE_ID);
    addressSpace.build(server, process.env.SERVICE_ID);
    program.initialize(addressSpace);

    const intervalObj = setInterval(() => {
        program.execute(addressSpace);
    }, 1000);
}

function post_initialize() {
    console.log("initialized");

    construct_my_address_space(server);
    server.start(function () {
        console.log("Server is now listening ... ( press CTRL+C to stop)");
        console.log("port ", server.endpoints[0].port);
        const endpointUrl = server.endpoints[0].endpointDescriptions()[0].endpointUrl;
        console.log(" the primary server endpoint url is ", endpointUrl);
    });
}
server.initialize(post_initialize);