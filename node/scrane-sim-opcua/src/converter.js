const Speed = require("../../models/speed");
const Position = require("../../models/position");

const m_min_to_mm_sec = 1000 / 60;
const stackingFieldWidth = 6458;

//distance to target target position (gantry) to start moving the trolley to target pos
const midTrolleyPosition = 2480 * 3;

//helper func to convert m/min to mm/sec
function convertToMmSec(absSpeedMmin) {
	absSpeedMmSec = new Speed.SpeedAbsolute();
	absSpeedMmSec.gantrySpeedAbsolute = absSpeedMmin.gantrySpeedAbsolute * m_min_to_mm_sec;
	absSpeedMmSec.trolleySpeedAbsolute = absSpeedMmin.trolleySpeedAbsolute * m_min_to_mm_sec;
    absSpeedMmSec.hoistSpeedAbsolute = absSpeedMmin.hoistSpeedAbsolute * m_min_to_mm_sec;
	return absSpeedMmSec
}

//helper func. Assumes Block's bay01, row01 coordinates (starting point): (x,y) = (10000, 10000).
function convertToAbsoluteCoordinates(logicalLocation) {

	absolutePosition = new Position.DevicePosition();
	absolutePosition.gantryPosition = logicalLocation.bayPosition * stackingFieldWidth/2;
	absolutePosition.trolleyPosition = 10000 + logicalLocation.rowPosition * midTrolleyPosition - (midTrolleyPosition/2);
	absolutePosition.hoistPosition = logicalLocation.tierPosition;
	return absolutePosition
}

module.exports = {
    convertToMmSec: convertToMmSec,
    convertToAbsoluteCoordinates: convertToAbsoluteCoordinates
}