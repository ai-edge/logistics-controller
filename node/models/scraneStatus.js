const Position = require("./position");
const Speed = require("./speed");

const ResourceState = {
    IDLE: 1,
    MOVE: 2,
    ERROR: 99
}

function ScraneStatus(id) {
    this.resourceState = ResourceState.IDLE;
    this.stackingCraneID = id;
    this.handledItemIDInfo = new ItemIdInfo(id);
    this.craneMainAxesPosition = new Position.DevicePosition();
    this.craneSpeedsPercentageOfNominal = new Speed.SpeedPercentage();
}

function ItemIdInfo(id){
    this.ISOID = id;
    this.containerType = "tank";
}

module.exports = {
    ScraneStatus: ScraneStatus,
    ResourceState: ResourceState
}
