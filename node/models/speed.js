function SpeedPercentage(){
    this.gantrySpeedPercentage = 0;
    this.trolleySpeedPercentage = 0;
    this.hoistSpeedPercentage = 0;
}

function SpeedAbsolute(gantry, trolley, hoist){
    this.gantrySpeedAbsolute = gantry;
    this.trolleySpeedAbsolute = trolley;
    this.hoistSpeedAbsolute = hoist;
}

module.exports = {
    SpeedPercentage: SpeedPercentage,
    SpeedAbsolute: SpeedAbsolute
}