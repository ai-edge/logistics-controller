function DevicePosition(){
    this.gantryPosition  = 0;
    this.trolleyPosition  = 0;
    this.hoistPosition  = 0;
}

function LogicalPosition(){
    this.bayPosition  = 0;
    this.rowPosition  = 0;
    this.tierPosition  = 0;
}

module.exports = {
    DevicePosition: DevicePosition,
    LogicalPosition: LogicalPosition
}