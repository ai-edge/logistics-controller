const Position = require("./position");

function ScraneCommand() {
    this.command = 0; // IDLE
    this.destination = new Position.LogicalPosition();
}

module.exports = {
    ScraneCommand: ScraneCommand
}
