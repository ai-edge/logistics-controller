const Converter = require("../scrane-sim-opcua/src/converter");
const Scrane = require("./scraneStatus");

const safetyHoistHeightMm = 22500;

var lastCommand = 0;

function initialize(context){
    context.status.resourceState = Scrane.ResourceState.IDLE;
}

// TODO apply proper state pattern
function execute(context){
    switch(context.status.resourceState) {
        case Scrane.ResourceState.IDLE:
          idleState(context);
          break;
        case Scrane.ResourceState.MOVE:
          moveState(context);
          break;
        case Scrane.ResourceState.ERROR:
          errorState(context);
          break;
        default:
            console.log("resourceState: ", context.status.resourceState);
          break;
    }
    lastCommand = context.command.command;
}

function errorState(context) {
    console.log("error");
    context.status.resourceState = Scrane.ResourceState.IDLE;
}

function idleState(context) {
    command = context.command.command;
    if ((lastCommand == 0) && (command == 1)){
        context.status.resourceState = Scrane.ResourceState.MOVE;
    }
}

function moveState(context) {
    let current = context.status.craneMainAxesPosition;
    let destination = Converter.convertToAbsoluteCoordinates(context.command.destination);
    let nominalSpeedMmSec = Converter.convertToMmSec(context.nominalSpeeds);
    let status = Scrane.ResourceState.MOVE;

    if ((current.gantryPosition === destination.gantryPosition) && (current.trolleyPosition === destination.trolleyPosition)) {
        status = moveHoist(current, destination, nominalSpeedMmSec);
	} else if (safetyHoistHeightMm != current.hoistPosition){
        status = initialLift(current, nominalSpeedMmSec);
    } else {
        status = moveCrane(current, destination, nominalSpeedMmSec);
    }

    context.status.resourceState = status;
}

function moveHoist(current, destination, speed){
    if (current.hoistPosition < destination.hoistPosition) {
        console.log(" invalid hoist height. curAbsPos: ");
        return Scrane.ResourceState.ERROR;
    }

    //if hoist needs to be lowered?
    if (current.hoistPosition > destination.hoistPosition) {
        current.hoistPosition = move(current.hoistPosition, destination.hoistPosition, speed.hoistSpeedAbsolute);
    } else {
        //target position reached! complete the order without waiting
        console.log("Target Position Reached!");
        return Scrane.ResourceState.IDLE;
    }
    return Scrane.ResourceState.MOVE;
}

function initialLift(current, speed){
    current.hoistPosition = move(current.hoistPosition, safetyHoistHeightMm, speed.hoistSpeedAbsolute);
    return Scrane.ResourceState.MOVE;
}

function moveCrane(current, destination, speed){
    current.gantryPosition = move(current.gantryPosition, destination.gantryPosition, speed.gantrySpeedAbsolute);
    current.trolleyPosition = move(current.trolleyPosition, destination.trolleyPosition, speed.trolleySpeedAbsolute);
    return Scrane.ResourceState.MOVE;
}

function move(current, destination, speed){
    let distance = destination - current;
    if (Math.abs(distance) > speed) {
        return (current + Math.sign(distance) * speed);
    } else {
        return destination;
    }
}

module.exports = {
    initialize: initialize,
    execute: execute
}