components:
  schemas:
    controlType:
      type: string
      enum: ["Manual", "Automatic", "NoLanding"]
      description: movement control type
      default: "Manual"
      example: "Automatic"

    stackingCraneJobControlFlags:
      type: object
      description: base flags
      properties:
        isNewOrder:
          type: boolean
          description: Not set when canceling the order
          default: false
        isUpdateTargetData:
          type: boolean
          description: Updates the data listed in the "Destination Data" area
          default: false
        isJobCancel:
          type: boolean
          description: Cancels the order with the number given in the order number
          default: false
        isSafetyHeightAfterJob:
          type: boolean
          description: Activates the "AUTO AUTOMATISM" status in the crane control after completion of the job.
          default: false
        isReleaseOptimalLoadDistribution:
          type: boolean
          description:
            The trolley may be driven through the crane control in the middle for optimal
            load distribution during long travel distances of the crane.
          default: false

    stackingCraneJobOrder_cust:
      description: extended type of stacking crane job order
      type: object
      properties:
        scraneJobControlFlags:
          description: control flags for the job
          $ref: "#/components/schemas/stackingCraneJobControlFlags"

        requiredContainerLength:
          type: integer
          description: Required length (telescopic) of the load handler e.g. 20, 40 and 45ft
          format: ft
          minimum: 20
          maximum: 45
        maximumPermissebleContainerWeight:
          type: integer
          description: Maximum permissible container weight
          format: kg
          minimum: 0
          maximum: 75000
        transportResource:
          description: Transport Resource. E.g. truck. Can be extended
          $ref: "../base/resourcetypes.yaml#/components/schemas/htOrderRes"

        fromLogicalLocation:
          description: Crane move's beginning logical location. Optional.
          $ref: "./basictypes.yaml#/components/schemas/logicalLocation3D"
        fromAbsolutePosition:
          description: Move's beginning Absolute Position
          $ref: "../base/basictypes.yaml#/components/schemas/position3D"
        fromAngle:
          type: integer
          description: Angle of the lifting device in mgrad (0…360000)
          minimum: 0
        fromControlType:
          description: From Movement Control Type
          $ref: "#/components/schemas/controlType"

        destinationAbsolutePosition:
          description: Destionation Absolute Position
          $ref: "../base/basictypes.yaml#/components/schemas/position3D"
        destinationAngle:
          type: integer
          description: Angle of the lifting device in mgrad (0…360000)
          minimum: 0
        destinationControlType:
          description: Destination Movement Control Type
          $ref: "#/components/schemas/controlType"

        obstacles:
          description: array of obstacles
          type: array
          items:
            $ref: "../base/basictypes.yaml#/components/schemas/obstacle"

    stackingCraneJobRequest_cust:
      description: message is used when informing the Resource about new job orders
      headers:
        $ref: "../base/apitypes.yaml#/components/schemas/mqttHeader"
      payload:
        allOf:
          - $ref: "../base/apitypes.yaml#/components/schemas/messageInfo"
          - $ref: "../base/resourceordertypes.yaml#/components/schemas/stackingCraneJobOrder_base"
          - $ref: "#/components/schemas/stackingCraneJobOrder_cust"
