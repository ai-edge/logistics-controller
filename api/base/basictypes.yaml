components:
  schemas:
    abstractService:
      type: object
      description: abstract type for all services
      required:
        - serviceType
        - serviceID
      properties:
        serviceType:
          $ref: "./apitypes.yaml#/components/schemas/serviceType"
        serviceID:
          $ref: "./apitypes.yaml#/components/schemas/serviceID"

    abstractLocation:
      type: object
      description: the base type for location
      required:
        - areaName
      properties:
        areaName:
          $ref: "./apitypes.yaml#/components/schemas/areaName"

    fieldTypeEnum:
      type: string
      description: type of the field
      enum: ["TstackingField", "TtransferField", "containerBlockComposite"]
      example: "TstackingField"

    abstractField:
      description: the base type for fields
      allOf:
        - $ref: "#/components/schemas/logicalLocation3D"
        - $ref: "#/components/schemas/rectangularField2D"
        - type: object
          description: the base type for fields
          required:
            - fieldType
          properties:
            fieldType:
              $ref: "#/components/schemas/fieldTypeEnum"

    logicalLocation3D:
      description: 3D logical location representation in a field (block, transferzone etc.)
      allOf:
        - $ref: "#/components/schemas/abstractLocation"
        - type: object
          required:
            - bay
            - row
            - tier
          properties:
            bay:
              $ref: "./apitypes.yaml#/components/schemas/bayName"
            row:
              $ref: "./apitypes.yaml#/components/schemas/rowName"
            tier:
              $ref: "./apitypes.yaml#/components/schemas/tierName"

    bayLogicalLocation:
      description: Bay location in a block
      allOf:
        - $ref: "#/components/schemas/abstractLocation"
        - type: object
          required:
            - blockID
            - bay
          properties:
            blockID:
              type: integer
              description: Unique block number
              minimum: 0
              example: 01
            bay:
              $ref: "./apitypes.yaml#/components/schemas/bayName"

    rotation:
      type: object
      description: rotation information
      required:
        - rotationAngle
      properties:
        rotationAngle:
          type: integer
          description: rotate counter-clockwise
          minimum: 0
          maximum: 360
          format: degree
          example: 0

    position2D:
      type: object
      description: 2D coordinates
      required:
        - x
        - y
      properties:
        x:
          type: integer
          description: x coordinate
          minimum: 0
          format: mm
          example: 66680
        y:
          type: integer
          description: y coordinate
          minimum: 0
          format: mm
          example: 4600

    position3D:
      description: 3D position
      allOf:
        - $ref: "#/components/schemas/position2D"
        - type: object
          required:
            - z
          properties:
            z:
              type: integer
              description: z coordinate
              minimum: 0
              format: mm
              example: 12600

    dimensions2D:
      type: object
      description: |
        2D dimensions of a specific field.
      required:
        - width
        - length
      properties:
        width:
          type: integer
          description: horizontal width of a 2D object
          format: mm
          minimum: 0
          example: 6058
        length:
          type: integer
          description: vertical length of a 2D object
          format: mm
          minimum: 0
          example: 2438

    dimensions3D:
      description: |
        For objects, dimensions are based on object's stacked state.
        E.g. paperrolls when stacked vertically, width = length = roll's diameter
      allOf:
        - $ref: "#/components/schemas/dimensions2D"
        - type: object
          required:
            - depth
          properties:
            depth:
              type: integer
              description: object's height
              format: mm
              minimum: 0
              example: 2438

    rectangularField2D:
      description: 2D representation of a rectangular field on 2D space
      allOf:
        - $ref: "#/components/schemas/position2D"
          description: |
            Coordinates (x,y) represent the "start" location (top left) of the field.
        - $ref: "#/components/schemas/dimensions2D"
          description: |
            2D Dimensions of the field (x-width,y-length) 
            relative to its starting position (x,y).
        - $ref: "#/components/schemas/rotation"
          description: |
            Rotate the field around its starting poistion's axis. 0 means not rotated.

    resourceTypeEnum:
      type: string
      description: Type of the Resource
      enum:
        [
          "stackingCrane",
          "RTG",
          "RMG",
          "quayCrane",
          "containerTruck",
          "containerAGV",
          "strad",
          "person",
        ]
      example: "RTG"

    obstacle:
      type: object
      description:
      required:
        - obstacleNumber
        - height
        - center
        - width
      properties:
        obstacleNumber:
          type: integer
          minimum: 0
          default: 0
          description: Unique number of the obstacle (e.g., bay number).
            0 if the obstacle does not exist or should not be considered.
        height:
          type: integer
          description: Highest altitude belonging to the obstacle.
          minimum: 0
        center:
          type: integer
          format: mm
          minimum: 0
          description: Center Position of the obstacle in Resource travel direction
        width:
          type: integer
          format: mm
          minimum: 0
          description: Width of the obstacle

    chasisTypeEnum:
      type: string
      description: trailer type. "0" means undefined.
      enum: ["T0", "T20", "T40", "T45"]
      default: "T0"

    containerLocationOnChasisEnum:
      type: string
      description:
        container location on the chasis. "empty" means chasis is empty.
        Use "front", "middle"or "rear" when a 20 feet container is on larger chasis.
        Use "middle" otherwise.
      enum: ["empty", "front", "middle", "rear"]
      default: "empty"
      example: "empty"

    containerType:
      type: string
      description: |
        includes 4 digit container type coded accoring to ISO 6346.
        "C0000" means undefined.
      minLength: 5
      maxLength: 5
      enum:
        [
          "C0000",
          "C20G0",
          "C20G1",
          "C20G2",
          "C20G3",
          "C22G0",
          "C22G1",
          "C22G2",
          "C22G3",
          "C22G4",
          "C22G8",
          "C22G9",
          "C25G0",
          "C26G0",
          "C2EG0",
          "C40G0",
          "C42G0",
          "C42G1",
        ]
      example: "C20G0"
      default: "C0000"

    containerTrailerInformation:
      type: object
      description: information for container trailer
      required:
        - chasisType
        - containerType
        - containerLocationOnChasis
      properties:
        chasisType:
          $ref: "#/components/schemas/chasisTypeEnum"
        containerType:
          $ref: "#/components/schemas/containerType"
        containerLocationOnChasis:
          $ref: "#/components/schemas/containerLocationOnChasisEnum"

    targetChasisInformation:
      description: target chasis of a from/to order
      type: object
      properties:
        targetChasis:
          type: integer
          description: To be discharged/loaded chasis number.
            0 = no target, 1 = first chasis, 2 = second chasis after the truck...
          minimum: 0
          default: 0
        targetLocationOnChasis:
          $ref: "#/components/schemas/containerLocationOnChasisEnum"

    statusInterval:
      type: integer
      description: |
        Change this parameter to adjust simulation's speed.
        In reality, one interval step's duration equals to 1 second. 
        Status events are published in the given interval.
      format: millisecond
      minimum: 1
      example: 1000
