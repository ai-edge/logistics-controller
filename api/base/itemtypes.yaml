components:
  schemas:
    containerIdentificationInfo:
      description: ISO id and type information of a container
      type: object
      required:
        - ISOID
        - containerType
      properties:
        ISOID:
          $ref: "./apitypes.yaml#/components/schemas/containerISOID"
        containerType:
          $ref: "./basictypes.yaml#/components/schemas/containerType"

    itemTypeEnum:
      type: string
      description: Type of the Item
      enum: ["container", "paperRoll", "metalCoil"]
      example: "container"

    abstractItem:
      type: object
      description: base for handled items
      required:
        - itemType
        - itemGUID
        - itemDimensions
        - weight
      properties:
        itemType:
          $ref: "#/components/schemas/itemTypeEnum"
        itemGUID:
          $ref: "./apitypes.yaml#/components/schemas/GUID"
        itemDimensions:
          $ref: "./basictypes.yaml#/components/schemas/dimensions3D"
        weight:
          type: integer
          description: item weight
          format: kg
          minimum: 0
          example: 15000

    doorDirectionEnum:
      type: string
      description: |
        door direction of the container. normal=north. 
        Ignore means not detected/handled
      enum: ["normal", "reverse", "ignore"]
      default: "ignore"
      example: "ignore"

    container:
      description: container is derived from the abstractItem.
      allOf:
        - $ref: "#/components/schemas/abstractItem"
        - type: object
          required:
            - containerIDInfo
            - containerLogicalLocation
          properties:
            containerIDInfo:
              $ref: "#/components/schemas/containerIdentificationInfo"
            doorDirection:
              $ref: "#/components/schemas/doorDirectionEnum"
            containerLogicalLocation:
              $ref: "./basictypes.yaml#/components/schemas/logicalLocation3D"
              description: location information of the placed container
            Deviation3D:
              $ref: "./basictypes.yaml#/components/schemas/position3D"
              description: |
                Container's center deviation from nominal point. 
                Null if container is in the nominal position.
              example:
                x: 0
                y: 0
                z: 0

    containerStatus:
      type: object
      description: status of a stacking field
      required:
        - containerIDInfo
        - containerLogicalLocation
        - containerPlaced
      properties:
        containerIDInfo:
          $ref: "#/components/schemas/containerIdentificationInfo"
        containerLogicalLocation:
          $ref: "./basictypes.yaml#/components/schemas/logicalLocation3D"
        containerPlaced:
          title: containerPlacedFlag
          description: informs operations if the container is placed or removed on/from the field
          type: boolean
          default: false

    containerStatusEvent:
      description: message is used when informing operations about the container status
      payload:
        allOf:
          - $ref: "./apitypes.yaml#/components/schemas/messageInfo"
          - $ref: "#/components/schemas/containerStatus"
