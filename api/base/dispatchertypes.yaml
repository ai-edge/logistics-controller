components:
  schemas:
    abstractDispatch:
      type: object
      description: base for L3 orders to L2
      required:
        - dispatchName
        - dispatchNr
        - scheduleReference
      properties:
        dispatchName:
          $ref: "./apitypes.yaml#/components/schemas/dispatchName"
        dispatchNr:
          type: integer
          description: Unique order number. 0 = undefined/no order
          minimum: 1
          example: 1

    dispatchReqType:
      description: Dispatch request type.
      type: string
      enum: ["NewDisp", "CancelDisp", "AbortDisp", "DeleteDisp"]
      example: "NewDisp"

    dispatchTaskType:
      description: Task type
      type: string
      enum:
        [
          "tMove",
          "tPick",
          "tPlace",
          "tWaitOnTask",
          "tWaitOnSchedule",
          "tWaitOnTruckPark",
          "tWaitOnTruckLeave",
        ]
      default: "tMove"
      example: "tMove"

    dispatchTaskState:
      type: string
      description: dispatcher state
      enum: ["tsReady", "tsExecuting", "tsCompleted"]
      default: "tsReady"
      example: "tsReady"

    dispatchState:
      type: string
      description: dispatch state
      enum:
        [
          "dReady",
          "dFinished",
          "dBlocked",
          "dVoid",
          "dInitialize",
          "dEntered",
          "dRunning",
          "dCompleted",
          "dCanceled",
          "dAborted",
          "dFault",
        ]
      default: "dVoid"
      example: "dVoid"

    taskNr:
      type: integer
      description: Task number starts from 0 and defines the sequence of execution.
      minimum: 0
      example: 0

    waitedEvent:
      type: object
      description: Fields of an event. Non-empty in case the task is a wait task.
      properties:
        waitedEventName:
          type: string
          description: Name of the event e.g. schedule name
        waitedTaskNumber:
          $ref: "#/components/schemas/taskNr"

    dispatchTask:
      type: object
      properties:
        taskNr:
          $ref: "#/components/schemas/taskNr"
        taskType:
          $ref: "#/components/schemas/dispatchTaskType"
        taskState:
          $ref: "#/components/schemas/dispatchTaskState"
        waitedEvent:
          $ref: "#/components/schemas/waitedEvent"
        handledItems:
          description: handled items by the crane, incl. dimensions and door orientation
          type: array
          items:
            $ref: "./itemtypes.yaml#/components/schemas/container"
        destinationLogicalLocation:
          description: |
            Destination Logical Location of the move
          $ref: "./basictypes.yaml#/components/schemas/logicalLocation3D"
        transportResource:
          description: transport resource for the reference work item
          $ref: "./resourcetypes.yaml#/components/schemas/htOrderRes"
        chasisInformation:
          description: transport resource's chasis information.
          $ref: "./basictypes.yaml#/components/schemas/targetChasisInformation"

    dispatch:
      description: is derived from abstractDispatch
        and is used in sending dispatches to dispatcher.
        Dispatch can contain various information according to “Dispatch Type” field.
        Some fields can be empty if the actual Job type has no such information.
      allOf:
        - $ref: "#/components/schemas/abstractDispatch"
        - type: object
          description: abstract dispatch
          required:
            - dispatchReqType
            - stackingCraneID
            - dispatchPriority
            - dispatchTasks
          properties:
            dispatchReqType:
              $ref: "#/components/schemas/dispatchReqType"
            stackingCraneID:
              $ref: "./apitypes.yaml#/components/schemas/resourceID"
            dispatchPriority:
              type: integer
              description: Dispatch priority from ‘0’ to ‘9’ means highest to lowest.
              minimum: 0
              maximum: 9
              example: 5
            dispatchTasks:
              description: assigned tasks of a dispatch
              type: array
              items:
                $ref: "#/components/schemas/dispatchTask"

    dispatchStatus:
      description: is used to notify about the status of dispatch
      allOf:
        - $ref: "#/components/schemas/dispatch"
        - type: object
          description: dispatch
          required:
            - dispatchState
          properties:
            dispatchState:
              $ref: "#/components/schemas/dispatchState"
            lastGantryPosition:
              description: |
                Last known position of the gantry(offset) in mm.
              type: integer
              minimum: 0
              example: 36540

    dispatchRequest:
      description: is used when informing dispatcher about dispatch
      payload:
        allOf:
          - $ref: "./apitypes.yaml#/components/schemas/messageInfo"
          - $ref: "#/components/schemas/dispatch"

    dispatchStatusEvent:
      description: message is used when informing about the dispatch status
      payload:
        allOf:
          - $ref: "./apitypes.yaml#/components/schemas/messageInfo"
          - $ref: "#/components/schemas/dispatchStatus"
