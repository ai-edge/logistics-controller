kubectl get secret web-tls -n web -o yaml \
| sed s/"namespace: web"/"namespace: master"/\
| kubectl apply -n main -f -
