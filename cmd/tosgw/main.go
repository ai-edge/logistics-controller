package main

import (
	"context"
	"ecs/pkg/setup/repo/terminal"
	"ecs/pkg/tosgw"
	"ecs/pkg/tosgw/httpsrv"
	"ecs/tools/mongodb"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"runtime/debug"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

const port = ":8081"

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.DebugLevel)
}

func main() {
	var server http.Server
	defer os.Exit(0)
	defer func() {
		r := recover()
		if r == nil {
			return // no panic underway
		}
		log.Errorf("panicHandler invoked %v", r)
		// print debug stack
		debug.PrintStack()
		if err := server.Shutdown(context.Background()); err != nil {
			log.WithError(err).Error("HTTP server Shutdown", err)
		}
		runtime.Goexit()
	}()

	//init Repository
	log.Debugln("starting the tosgw", os.Getenv("SERVICE_ID"))
	dbClient, err := mongodb.NewMongoDBClient(
		os.Getenv("DB_URL"),
		os.Getenv("DB_AUTH"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWD"))
	if err != nil {
		log.WithError(err).Fatal("cannot create db client")
	}
	defer dbClient.DeInit()

	tr, err := terminal.NewTerminalRepository(dbClient)
	if err != nil {
		log.WithError(err).Fatal("cannot create repository")
	}
	defer tr.DeInit()

	//validate
	tosgwCfg, err := tr.GetTOSGW(os.Getenv("SERVICE_ID"))
	if err != nil {
		log.WithError(err).Fatalf("Could not find matching tosgw with given id %s", os.Getenv("SERVICE_ID"))
	}

	//init MQ connection. One connection to be shared among threads
	rabbitURL := fmt.Sprintf("amqp://%s:%s@%s", os.Getenv("RABBIT_USER"), os.Getenv("RABBIT_PASSWD"), os.Getenv("RABBIT_URL"))
	log.Debugf("connecting with url %s", rabbitURL)
	conn, err := amqp.Dial(rabbitURL)
	if err != nil {
		log.WithError(err).Fatal("Failed to connect to MQ")
	}
	defer conn.Close()

	//Create the TOSGW
	tosgwService, error := tosgw.NewTOSGW(tr, conn)
	if error != nil {
		log.WithError(error).Fatal("cannot create tosgw")
	}
	defer tosgwService.DeInit()

	//create query handler
	server.Addr = port
	go httpsrv.StartHTTPSrv(&server, tr, conn, tosgwCfg.ServiceID)

	//wait on exit
	sigint := make(chan os.Signal, 1)
	// interrupt signal sent from terminal
	signal.Notify(sigint, os.Interrupt)
	// sigterm signal sent from kubernetes
	signal.Notify(sigint, syscall.SIGTERM)
	<-sigint
	log.Debugln("exiting tosgwService", tosgwService)
	if err := server.Shutdown(context.Background()); err != nil {
		log.WithError(err).Error("HTTP server Shutdown", err)
	}
	runtime.Goexit()

}
