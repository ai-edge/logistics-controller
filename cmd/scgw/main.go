package main

import (
	"context"
	"ecs/pkg/scgw"
	"ecs/pkg/scgw/httpsrv"
	"ecs/pkg/setup/repo/terminal"
	"ecs/tools/mongodb"
	"ecs/tools/mqttwrap"
	"ecs/tools/scsim"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"runtime/debug"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
)

const port = ":8087"

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.WarnLevel)
}

func main() {
	var server http.Server
	defer os.Exit(0)
	defer func() {
		r := recover()
		if r == nil {
			return // no panic underway
		}
		log.Errorf("panicHandler invoked %v", r)
		// print debug stack
		debug.PrintStack()
		if err := server.Shutdown(context.Background()); err != nil {
			log.WithError(err).Error("HTTP server Shutdown", err)
		}
		runtime.Goexit()
	}()

	log.Debugf("starting the scgw %s", os.Getenv("SERVICE_ID"))

	dbClient, err := mongodb.NewMongoDBClient(os.Getenv("DB_URL"), os.Getenv("DB_AUTH"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWD"))
	if err != nil {
		log.WithError(err).Fatal("cannot create db client")
	}
	defer dbClient.DeInit()

	tr, err := terminal.NewTerminalRepository(dbClient)
	if err != nil {
		log.WithError(err).Fatal("cannot create repository")
	}
	defer tr.DeInit()

	serviceid := os.Getenv("SERVICE_ID")

	//validate
	gw, err := tr.GetSCGWByServiceID(serviceid)
	if err != nil {
		log.WithError(err).Fatalf("Could not find matching gateway cfg with given serviceid %s", serviceid)
	}

	scrane, err := tr.GetScraneByScraneID(gw.AssignedResourceID)
	if err != nil {
		log.WithError(err).Fatalf("cannot get resource:%s", gw.AssignedResourceID)
	}

	//initialize stacking crane simulator
	interval := time.Duration(gw.StatusInterval) * time.Millisecond
	sc, err := scsim.NeWScraneSimulator(scrane, interval) //start handler loop
	if err != nil {
		log.WithError(err).Fatalf("cannot start crane simulator %s", err)
	}
	defer sc.DeInit()

	// initialize Broker.
	mqConn, err := mqttwrap.NewMQTTConn(os.Getenv("RABBIT_URL"), serviceid, os.Getenv("RABBIT_USER"), os.Getenv("RABBIT_PASSWD"))
	if err != nil {
		log.WithError(err).Fatalf("cannot init mqtt conn:%s", os.Getenv("RABBIT_URL"))
	}
	defer mqConn.DeInit()

	// initialize scrane gateway, which is an L2 software responsible for gatewaying resource communication
	scGateway := scgw.NewSCGW(sc, interval, mqConn, dbClient) //starts publisher loop for given interval
	log.Debugln("scgw created", scGateway)
	defer scGateway.DeInit()

	//create query handler
	envPort := os.Getenv("PORT")
	if envPort == "" {
		envPort = port
	}
	server.Addr = envPort
	//http handler worker
	go httpsrv.StartHTTPSrv(&server, sc)

	//wait on exit
	sigint := make(chan os.Signal, 1)
	// interrupt signal sent from terminal
	signal.Notify(sigint, os.Interrupt)
	// sigterm signal sent from kubernetes
	signal.Notify(sigint, syscall.SIGTERM)
	<-sigint
	log.Debugln("exiting scgw", scGateway)
	if err := server.Shutdown(context.Background()); err != nil {
		log.WithError(err).Error("HTTP server Shutdown", err)
	}
	runtime.Goexit()
}
