package main

import (
	"context"
	"ecs/pkg/sci"
	"ecs/pkg/sci/httpsrv"
	"ecs/pkg/setup/repo/terminal"
	"ecs/tools/kubeclient"
	"ecs/tools/mongodb"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"runtime/debug"
	"syscall"

	log "github.com/sirupsen/logrus"
)

const port = ":8086"

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.DebugLevel)
}

func main() {
	var server http.Server
	defer os.Exit(0)
	defer func() {
		r := recover()
		if r == nil {
			return // no panic underway
		}
		log.Errorf("panicHandler invoked %v", r)
		// print debug stack
		debug.PrintStack()
		if err := server.Shutdown(context.Background()); err != nil {
			log.WithError(err).Error("HTTP server Shutdown", err)
		}
		runtime.Goexit()
	}()

	//init Repository
	log.Debugln("starting the sci...")
	dbClient, err := mongodb.NewMongoDBClient(
		os.Getenv("DB_URL"),
		os.Getenv("DB_AUTH"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWD"))
	if err != nil {
		log.WithError(err).Fatal("cannot create db client")
	}
	defer dbClient.DeInit()

	tr, err := terminal.NewTerminalRepository(dbClient)
	if err != nil {
		log.WithError(err).Fatal("cannot create repository")
	}
	defer tr.DeInit()

	var kbc *kubeclient.KubeConnection
	if os.Getenv("KUBECLIENT") == "external" {
		log.Debugln("external")
		kbc, err = kubeclient.NewExternalClient()
	}
	if os.Getenv("KUBECLIENT") == "internal" {
		log.Debugln("internal")
		kbc, err = kubeclient.NewInternalClient()
	}
	if err != nil {
		log.WithError(err).Fatal("cannot create kube client")
	}

	//Create the SCI
	sciService := sci.NewSCI(tr, kbc)

	defer sciService.DeInitSCI()

	//create query handler
	server.Addr = port
	go httpsrv.StartHTTPSrv(&server, sciService)

	//wait on exit
	sigint := make(chan os.Signal, 1)
	// interrupt signal sent from terminal
	signal.Notify(sigint, os.Interrupt)
	// sigterm signal sent from kubernetes
	signal.Notify(sigint, syscall.SIGTERM)
	<-sigint
	log.Debugln("exiting sciService", sciService)
	if err := server.Shutdown(context.Background()); err != nil {
		log.WithError(err).Error("HTTP server Shutdown", err)
	}
	runtime.Goexit()

}
