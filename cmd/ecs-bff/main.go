package main

import (
	"context"
	"ecs/pkg/ecs-bff/controller"
	"fmt"
	"os/signal"
	"runtime"
	"runtime/debug"
	"syscall"

	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

const port = ":8084"

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.DebugLevel)
}

func main() {
	var server http.Server
	defer os.Exit(0)
	defer func() {
		r := recover()
		if r == nil {
			return // no panic underway
		}
		log.Errorf("panicHandler invoked %v", r)
		// print debug stack
		debug.PrintStack()
		if err := server.Shutdown(context.Background()); err != nil {
			log.WithError(err).Error("HTTP server Shutdown", err)
		}
		runtime.Goexit()
	}()

	//init MQ connection. One connection to be shared among threads
	rabbitURL := fmt.Sprintf("amqp://%s:%s@%s", os.Getenv("RABBIT_USER"), os.Getenv("RABBIT_PASSWD"), os.Getenv("RABBIT_URL"))
	log.Debugf("connecting with url %s", rabbitURL)
	conn, err := amqp.Dial(rabbitURL)
	if err != nil {
		log.WithError(err).Fatal("Failed to connect to MQ")
	}
	defer conn.Close()

	log.Debugf("starting the application...")

	controller, err := controller.NewController(
		conn,
		os.Getenv("OPERATIONS_URL"),
		os.Getenv("TOSGW_URL"),
		os.Getenv("DEVICE_MANAGER_URL"),
		os.Getenv("CACERT_PATH"))

	if err != nil {
		log.WithError(err).Fatal("cannot create bff controller")
	}
	defer controller.DeInit()

	// r := router.Router(controller)

	// handler := cors.Default().Handler(r)
	// server.Handler = handler
	server.Addr = port

	go func() {
		log.Debugln("starting server on the port", server.Addr, "...")
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.WithError(err).Fatalf("ListenAndServe failed")
		}
	}()

	//wait on exit
	sigint := make(chan os.Signal, 1)
	// interrupt signal sent from terminal
	signal.Notify(sigint, os.Interrupt)
	// sigterm signal sent from kubernetes
	signal.Notify(sigint, syscall.SIGTERM)
	<-sigint
	log.Debugln("exiting server on the port", port, "...")
	if err := server.Shutdown(context.Background()); err != nil {
		log.WithError(err).Error("HTTP server Shutdown", err)
	}
	runtime.Goexit()

}
