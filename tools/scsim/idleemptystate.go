package scsim

import (
	"ecs/api/generated/models"
	"reflect"
	"time"

	log "github.com/sirupsen/logrus"
)

type idleEmpty struct {
	simContext *ScraneSimulator
}

func (state *idleEmpty) enterState() error {
	log.Traceln("entering state", reflect.TypeOf(state).String())
	status := &state.simContext.scraneStatus
	status.ResourceState = models.R_IDLE
	return nil
}

func (state *idleEmpty) exitState() error {
	return nil
}

func (state *idleEmpty) handleTick(time.Duration) error {
	status := state.simContext.GetStatus()
	if true == status.AssignedJobStatus.IsRunning {
		log.Fatalln("non-empty job or wrong status in idle state:", status)
	}

	curAbsPos := &state.simContext.scraneStatus.CraneMainAxesPosition
	log.Println("Current position", curAbsPos)

	return nil
}
