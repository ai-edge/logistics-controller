package scsim

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"
	"time"

	log "github.com/sirupsen/logrus"
)

type movingState struct {
	simContext *ScraneSimulator
}

func (state *movingState) enterState() error {
	log.Traceln("entering state", reflect.TypeOf(state).String())
	status := &state.simContext.scraneStatus

	//update job status
	status.AssignedJobStatus.IsRunning = true
	status.AssignedJobStatus.JobCompleted = false
	status.ResourceState = models.R_MOVING
	return nil
}

func (state *movingState) exitState() error {
	//update job status
	status := &state.simContext.scraneStatus

	status.AssignedJobStatus.IsRunning = false

	return nil
}

func (state *movingState) handleTick(time.Duration) error {
	currentJob := &state.simContext.activeOrder

	if 0 == currentJob.OrderID {
		log.Errorln(" empty job or wrong status in moving state. currentJob:", currentJob)
		defer state.simContext.goToNextState(state.simContext.abortedEmptyState)
		return fmt.Errorf("invalid job")
	}

	scraneObject := &state.simContext.stackingCraneInformation
	curAbsPos := &state.simContext.scraneStatus.CraneMainAxesPosition
	destAbsPos, err := convertToAbsoluteCoordinates(currentJob.DestinationLogicalLocation)
	if err != nil {
		defer state.simContext.goToNextState(state.simContext.abortedEmptyState)
		return fmt.Errorf("convertToAbsoluteCoordinates failed %w", err)
	}

	//one interval equals to one second in real life
	nomSpeedsMmSec := convertToMmSec(scraneObject.ScraneNominalSpeeds)

	if curAbsPos.HoistPosition <= 0 || curAbsPos.HoistPosition > safetyHoistHeightMm {
		log.Errorln(" invalid hoist height. curAbsPos:", curAbsPos)
		defer state.simContext.goToNextState(state.simContext.abortedEmptyState)
		return fmt.Errorf("invalid hoist height %d", curAbsPos.HoistPosition)
	}
	if destAbsPos.HoistPosition <= 0 || destAbsPos.HoistPosition > safetyHoistHeightMm {
		log.Errorln(" invalid dest height. destAbsPos:", destAbsPos)
		defer state.simContext.goToNextState(state.simContext.abortedEmptyState)
		return fmt.Errorf("invalid hoist height %d", destAbsPos.HoistPosition)
	}

	/*****hoist*****/
	//are we in the target (bay,row)?
	if (curAbsPos.GantryPosition == destAbsPos.GantryPosition) && (curAbsPos.TrolleyPosition == destAbsPos.TrolleyPosition) {
		if curAbsPos.HoistPosition < destAbsPos.HoistPosition {
			//lift hoist
			zDistance := destAbsPos.HoistPosition - curAbsPos.HoistPosition
			if zDistance > nomSpeedsMmSec.HoistSpeedAbsolute {
				curAbsPos.HoistPosition = curAbsPos.HoistPosition + nomSpeedsMmSec.HoistSpeedAbsolute
			} else {
				//destination height reached
				curAbsPos.HoistPosition = destAbsPos.HoistPosition
			}
		} else if curAbsPos.HoistPosition > destAbsPos.HoistPosition {
			//lower hoist
			zDistance := curAbsPos.HoistPosition - destAbsPos.HoistPosition
			if zDistance > nomSpeedsMmSec.HoistSpeedAbsolute {
				curAbsPos.HoistPosition = curAbsPos.HoistPosition - nomSpeedsMmSec.HoistSpeedAbsolute
			} else {
				//destination height reached
				curAbsPos.HoistPosition = destAbsPos.HoistPosition
			}
		} else {
			//target position reached! complete the order without waiting
			log.Infof("Target Position Reached!")
			state.simContext.scraneStatus.AssignedJobStatus.JobCompleted = true
			defer state.simContext.goToNextState(state.simContext.idleEmptyState)
		}
	} else if safetyHoistHeightMm != curAbsPos.HoistPosition {
		//we are at the initial position, lift the hoist
		zDistance := safetyHoistHeightMm - curAbsPos.HoistPosition

		//move the hoist to target y
		if zDistance > nomSpeedsMmSec.HoistSpeedAbsolute {
			curAbsPos.HoistPosition = curAbsPos.HoistPosition + nomSpeedsMmSec.HoistSpeedAbsolute
		} else {
			//destination height reached
			curAbsPos.HoistPosition = safetyHoistHeightMm
		}
	} else {
		//move the crane
		/*****gantry*****/
		//move gantry
		xDistance := curAbsPos.GantryPosition - destAbsPos.GantryPosition
		if xDistance > 0 {
			//gantry moving to negative direction
			if xDistance > nomSpeedsMmSec.GantrySpeedAbsolute {
				curAbsPos.GantryPosition = curAbsPos.GantryPosition - nomSpeedsMmSec.GantrySpeedAbsolute
			} else {
				//destination x reached
				curAbsPos.GantryPosition = destAbsPos.GantryPosition
			}
		} else if xDistance < 0 {
			//gantry moving to positive direction
			if -(xDistance) > nomSpeedsMmSec.GantrySpeedAbsolute {
				curAbsPos.GantryPosition = curAbsPos.GantryPosition + nomSpeedsMmSec.GantrySpeedAbsolute
			} else {
				//destination x reached
				curAbsPos.GantryPosition = destAbsPos.GantryPosition
			}
		}

		/*****trolley*****/
		//move trolley
		yDistance := curAbsPos.TrolleyPosition - destAbsPos.TrolleyPosition
		if yDistance > 0 {
			//trolley moving to negative direction
			if yDistance > nomSpeedsMmSec.TrolleySpeedAbsolute {
				curAbsPos.TrolleyPosition = curAbsPos.TrolleyPosition - nomSpeedsMmSec.TrolleySpeedAbsolute
			} else {
				//destination x reached
				curAbsPos.TrolleyPosition = destAbsPos.TrolleyPosition
			}
		} else if yDistance < 0 {
			//trolley moving to positive direction
			if -(yDistance) > nomSpeedsMmSec.TrolleySpeedAbsolute {
				curAbsPos.TrolleyPosition = curAbsPos.TrolleyPosition + nomSpeedsMmSec.TrolleySpeedAbsolute
			} else {
				//destination x reached
				curAbsPos.TrolleyPosition = destAbsPos.TrolleyPosition
			}
		}

	}
	log.Println("Current position", curAbsPos)
	log.Println("Destination position", destAbsPos)
	return nil
}
