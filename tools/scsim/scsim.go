package scsim

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

const (
	safetyDistance               int32 = 6000
	safetyHoistHeightMm          int32 = 22500
	mtoMM                        int32 = 1000
	minToSec                     int32 = 60
	decelerationGantryDistanceMm       = 2400

	stackingFieldWidth  = 6458
	stackingFieldLength = 2840
	containerHeight     = 2438
	tzOffset            = 17540

	//distance to target target position (gantry) to start moving the trolley to target pos
	startTrolleyFinalMoveDistanceMm = 2400
	midTrolleyPosition              = 2480 * 3
)

type scraneState interface {
	enterState() error
	exitState() error

	handleTick(time.Duration) error
}

//ScraneSimulator simulates a stacking crane
type ScraneSimulator struct {
	//TODO: read it from DB via Scraneid which is passed via an env var by kubernetes
	/*Stacking Crane object defined by configuration*/
	stackingCraneInformation models.StackingCrane

	/*assigned job order to scrane. State of the job defines if it's finished*/
	activeOrder models.StackingCraneJobOrderBase

	/*current status of the scrane, which is published*/
	scraneStatus models.StackingCraneStatusBase

	/*current state of the scrane*/
	currentState scraneState

	/*scrane state machine states*/
	idleEmptyState     scraneState
	abortedEmptyState  scraneState
	abortedLoadedState scraneState
	//idleLoadedState   scraneState
	//lockingBrakesState scraneState
	movingState scraneState
	//pickingState scraneState
	//placingState scraneState
	//startupState scraneState
	//unlockingBrakesState scraneState

	//async channels
	commandCh      chan models.StackingCraneJobOrderBase
	neighbourStCh  chan models.StackingCraneStatusBase
	newIntervalCh  chan time.Duration
	clearCommandCh chan struct{}
	stopCh         chan bool

	ticker         *time.Ticker //Ticker
	statusInterval time.Duration
	lastUpdate     time.Time
}

//Abs retusn absolute value
func Abs(x int32) int32 {
	if x < 0 {
		return -x
	}
	return x
}

//NeWScraneSimulator is the factory method for stacking crane simulator
func NeWScraneSimulator(sc models.StackingCrane, statusInterval time.Duration) (*ScraneSimulator, error) {
	scSim := &(ScraneSimulator{})

	scSim.stopCh = make(chan bool)
	scSim.newIntervalCh = make(chan time.Duration)
	scSim.neighbourStCh = make(chan models.StackingCraneStatusBase)
	scSim.clearCommandCh = make(chan struct{})
	scSim.commandCh = make(chan models.StackingCraneJobOrderBase)

	/***********Scrane Object************/
	scSim.stackingCraneInformation = sc

	/***********Create states************/
	scSim.idleEmptyState = &(idleEmpty{simContext: scSim})
	scSim.abortedEmptyState = &(abortedEmpty{simContext: scSim})
	scSim.abortedLoadedState = &(abortedLoaded{simContext: scSim})
	//scSim.idleLoadedState = &(idleLoadedState{simContext: scSim})
	//scSim.lockingBrakesState = &(lockingBrakesState{simContext: scSim})
	scSim.movingState = &(movingState{simContext: scSim})
	//scSim.pickingState = &(pickingState{simContext: scSim})
	//scSim.startupState = &(startupState{simContext: scSim})
	//scSim.unlockingBrakesState = &(unlockingBrakesState{simContext: scSim})

	scSim.setState(scSim.idleEmptyState)

	/***********Job ORders************/
	//scSim.activeOrder

	/***********Initialize Scrane Status************/
	status := &(scSim.scraneStatus)
	(*status).ResourceState = models.R_IDLE
	(*status).StackingCraneID = sc.ResourceID
	(*status).AssignedJobStatus.IsRunning = false
	(*status).AssignedJobStatus.JobCompleted = false

	// no container
	(*status).HandledItemIDInfo = nil

	// Crane takes 0 offset position of the block
	(*status).CraneMainAxesPosition.GantryPosition = sc.CraneMainAxesPosition.GantryPosition
	(*status).CraneMainAxesPosition.TrolleyPosition = sc.CraneMainAxesPosition.TrolleyPosition
	(*status).CraneMainAxesPosition.HoistPosition = sc.CraneMainAxesPosition.HoistPosition

	//+/-0…200% Current speed relative to the nominal speed.
	//Direction degree is indicated with resource's rotationAngle
	(*status).CraneSpeedsPercentageOfNominal.GantrySpeedPercentage = 0
	(*status).CraneSpeedsPercentageOfNominal.TrolleySpeedPercentage = 0
	(*status).CraneSpeedsPercentageOfNominal.HoistSpeedPercentage = 0

	/***********Ticker************/
	scSim.lastUpdate = time.Now()
	scSim.statusInterval = statusInterval
	err := scSim.startTimer()
	if err != nil {
		return nil, fmt.Errorf("start timer failed %w", err)
	}

	go scSim.asyncHandler()

	return scSim, nil
}

//DeInit deinitializes
func (scSim *ScraneSimulator) DeInit() {
	scSim.stopTimer()
	scSim.stopCh <- true
}

//GetInterval gets status cycle interval time
func (scSim *ScraneSimulator) GetInterval(scraneid string) (time.Duration, error) {
	if scraneid != scSim.scraneStatus.StackingCraneID {
		return time.Duration(0), fmt.Errorf("invalid scraneid %s", scraneid)
	}
	return scSim.statusInterval, nil
}

//SetInterval sets status cycle interval time
func (scSim *ScraneSimulator) SetInterval(scraneid string, interval time.Duration) error {
	if scraneid != scSim.scraneStatus.StackingCraneID {
		return fmt.Errorf("invalid scraneid %s", scraneid)
	}
	if interval.Nanoseconds() <= 0 {
		return fmt.Errorf("invalid interval %d", interval.Nanoseconds())
	}
	if scSim.statusInterval == interval {
		return nil
	}
	scSim.newIntervalCh <- interval
	return nil
}

//SetCommand sets a new command for the simulator
func (scSim *ScraneSimulator) SetCommand(newJobOrder models.StackingCraneJobOrderBase) error {
	if scSim.scraneStatus.ResourceState == models.R_MOVING && newJobOrder.JobType != 9 { //if not a stop job
		return fmt.Errorf("Cannot set a new command. A job is being executed. Status:%v", scSim.scraneStatus)
	}
	if "" == newJobOrder.JobOrderName {
		return fmt.Errorf("Invalid new job order. joborder:%v", newJobOrder)
	}

	if scSim.activeOrder.OrderID == newJobOrder.OrderID {
		return fmt.Errorf("job already in execution %d", scSim.activeOrder.OrderID)
	}
	scSim.commandCh <- newJobOrder
	return nil
}

//GetStatus gets status of the simulated crane
func (scSim *ScraneSimulator) GetStatus() models.StackingCraneStatusBase {
	return scSim.scraneStatus
}

//UpdateNeighboringStatus sends the simulator the status of a neighboring crane
func (scSim *ScraneSimulator) UpdateNeighboringStatus(otherStatus models.StackingCraneStatusBase) error {
	scSim.neighbourStCh <- otherStatus
	return nil
}

func (scSim *ScraneSimulator) clearCommand() error {
	if scSim.scraneStatus.ResourceState == models.R_MOVING {
		return fmt.Errorf("cannot clear command. A job is being executed. Status:%v", scSim.scraneStatus)
	}
	scSim.clearCommandCh <- struct{}{}
	return nil
}

func (scSim *ScraneSimulator) startTimer() error {
	if scSim.statusInterval.Nanoseconds() <= 0 {
		return fmt.Errorf("cannot start the ticker for sim: %v", scSim)
	}
	log.Debugln("starting ticker with IntervalDuration:", scSim.statusInterval)
	scSim.ticker = time.NewTicker(scSim.statusInterval)
	return nil
}

func (scSim *ScraneSimulator) stopTimer() error {
	scSim.ticker.Stop()
	log.Debugln("ticker STOPPED with interval", scSim.statusInterval)
	return nil
}

func (scSim *ScraneSimulator) setState(newstate scraneState) {
	scSim.currentState = newstate
}

func (scSim *ScraneSimulator) goToNextState(nextState scraneState) error {
	log.Debugf("exiting state %s. Going to next state %s", reflect.TypeOf(scSim.currentState).String(), reflect.TypeOf(nextState).String())
	err := scSim.currentState.exitState()
	if err != nil {
		log.WithError(err).Errorln("cannot exit current state for sim:", scSim)
		return err
	}
	scSim.currentState = nextState
	scSim.currentState.enterState()
	return nil
}

//asyncHandler handles async events
func (scSim *ScraneSimulator) asyncHandler() {
	for {
		select {
		case newJobOrder := <-scSim.commandCh:
			scSim.activeOrder = newJobOrder
			status := &scSim.scraneStatus
			status.JobInfo.JobOrderName = newJobOrder.JobOrderName
			status.JobInfo.OrderID = newJobOrder.OrderID

			if newJobOrder.JobType == 9 { //if stop job
				scSim.goToNextState(scSim.abortedLoadedState)
			} else {
				scSim.goToNextState(scSim.movingState) //the crane is in idle mode
			}

			log.Debugln("job order set", newJobOrder)

		case <-scSim.clearCommandCh:
			scSim.activeOrder = models.StackingCraneJobOrderBase{}

		case neighbourStatus := <-scSim.neighbourStCh:
			//check if another crane is inside the safety distance
			dist := Abs(scSim.scraneStatus.CraneMainAxesPosition.GantryPosition - neighbourStatus.CraneMainAxesPosition.GantryPosition)
			//if less than 6 meters, stop the crane
			if dist < safetyDistance && scSim.currentState != scSim.abortedEmptyState {
				log.Errorln("Another crane in safety distance. Swtiching to idle state. Distance", dist)
				scSim.goToNextState(scSim.abortedEmptyState)
			}

		case t := <-scSim.ticker.C: //Ticker Events
			log.Traceln("Tick at ", t)
			timePassed := time.Now().Sub(scSim.lastUpdate)
			scSim.lastUpdate = time.Now()
			//log.Println("ms passed since last simulation step: ", timePassed)
			scSim.currentState.handleTick(timePassed)

		case newInt := <-scSim.newIntervalCh: //new time interval for status updates
			err := scSim.stopTimer()
			if err != nil {
				log.WithError(err).Errorln("stopTimer failed, interval", scSim.statusInterval)
			}
			scSim.statusInterval = newInt
			err = scSim.startTimer() //creates new ticker channel inside this loop
			if err != nil {
				log.WithError(err).Errorln("startTimer failed, interval", scSim.statusInterval)
			}
			log.Warningln("new interval set", scSim.statusInterval)

		case <-scSim.stopCh:
			log.Warningln("stopping asyncHandler", scSim.GetStatus())
			return

		}
	}
}

//helper func to convert m/min to mm/sec
func convertToMmSec(absSpeedMmin models.CraneSpeedsAbsolute) models.CraneSpeedsAbsolute {
	absSpeedMmSec := models.CraneSpeedsAbsolute{}
	absSpeedMmSec.GantrySpeedAbsolute = absSpeedMmin.GantrySpeedAbsolute * mtoMM / minToSec
	absSpeedMmSec.TrolleySpeedAbsolute = absSpeedMmin.TrolleySpeedAbsolute * mtoMM / minToSec
	absSpeedMmSec.HoistSpeedAbsolute = absSpeedMmin.HoistSpeedAbsolute * mtoMM / minToSec
	return absSpeedMmSec
}

//helper func. Assumes standard container height 2438mm. Stacking field's width: 6458, length: 2840
//CraneMainAxesOffsets e.g. (x,y) = (10000, 10000), are ignored in this convertion
func convertToAbsoluteCoordinates(logLoc models.LogicalLocation3D) (models.LiftingDevicePosition, error) {
	absPos := models.LiftingDevicePosition{}
	gantryPosition, err := strconv.Atoi(logLoc.Bay)
	if err != nil {
		log.WithError(err).Errorln("invalid location format. location: ", logLoc)
		return absPos, err
	}
	trolleyPosition, err := strconv.Atoi(logLoc.Row)
	if err != nil {
		log.WithError(err).Errorln("invalid location format. location: ", logLoc)
		return absPos, err
	}
	hoistPosition, err := strconv.Atoi(logLoc.Tier)
	if err != nil {
		log.WithError(err).Errorln("invalid location format. location: ", logLoc)
		return absPos, err
	}
	absPos.GantryPosition = int32(gantryPosition * stackingFieldWidth / 2)
	if logLoc.AreaName == "TZ01" {
		absPos.TrolleyPosition = int32(tzOffset + stackingFieldLength/2)
	} else {
		absPos.TrolleyPosition = int32(trolleyPosition*stackingFieldLength - stackingFieldLength/2)
	}

	if hoistPosition == 0 {
		//max tier
		absPos.HoistPosition = safetyHoistHeightMm
	} else {
		absPos.HoistPosition = int32(hoistPosition * containerHeight)
	}

	return absPos, nil
}
