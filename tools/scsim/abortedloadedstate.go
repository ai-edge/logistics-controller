package scsim

import (
	"ecs/api/generated/models"
	"reflect"
	"time"

	log "github.com/sirupsen/logrus"
)

type abortedLoaded struct {
	simContext *ScraneSimulator
}

func (state *abortedLoaded) enterState() error {
	log.Warningln("entering state", reflect.TypeOf(state).String())
	status := &state.simContext.scraneStatus

	status.AssignedJobStatus.IsRunning = false

	status.AssignedJobStatus.JobCompleted = false
	status.AssignedJobStatus.JobCanceled = true

	status.ResourceState = models.R_IDLE
	return nil
}

func (state *abortedLoaded) exitState() error {
	status := &state.simContext.scraneStatus
	status.AssignedJobStatus.JobCanceled = false
	return nil
}

func (state *abortedLoaded) handleTick(time.Duration) error {
	status := state.simContext.GetStatus()
	if true == status.AssignedJobStatus.IsRunning {
		log.Fatalln("non-empty job or wrong status in abortedLoaded state:", status)
	}

	curAbsPos := &state.simContext.scraneStatus.CraneMainAxesPosition
	log.Println("Current position", curAbsPos)

	return nil
}
