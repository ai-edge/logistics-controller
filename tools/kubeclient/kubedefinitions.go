package kubeclient

import "fmt"

// DeploymentType is similar to resource type
type DeploymentType string

// OperationStatus stop, run etc.
type OperationStatus string

// ConnectionStatus offline, online etc.
type ConnectionStatus string

// operation modes run/stop
const (
	Stop OperationStatus = "Stop"
	Run                  = "Run"
)

// connected or not
const (
	Offline ConnectionStatus = "Offline"
	Online                   = "Online"
)

// types of deployments
const (
	ScGwInternal   DeploymentType = "ScGwInternal"
	ScGwOpcUA                     = "ScGwOpcUA"
	ScraneOpcUASim                = "ScraneOpcUASim"
)

// Deployment structure includes short info useful for specific kubernetes deployments
type Deployment struct {
	Name     string
	Type     DeploymentType
	Address  string
	NodePort int32
	Status   string
}

const serviceIdKey = "SERVICE_ID"
const addressKey = "address"

const opcURLStart = "opc.tcp://"
const opcUaPort = 4334
const resourcePath = "/UA/Crane"

const gitlabRegistry = "registry.gitlab.com/ecs-kube/equipment-sim/"
const gitlabSecret = "gitlab-registry"

func getOpcUaServiceURL(name string, namespace string) string {
	return fmt.Sprint(opcURLStart, name, ".", namespace, ":", opcUaPort, resourcePath)
}

func int32Ptr(i int32) *int32 { return &i }
