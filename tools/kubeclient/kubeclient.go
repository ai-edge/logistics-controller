package kubeclient

import (
	"context"
	"errors"
	"flag"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

// KubeConnection stores the clientset
type KubeConnection struct {
	clientset *kubernetes.Clientset
}

// NewExternalClient is used for testing outside cluster
func NewExternalClient() (*KubeConnection, error) {
	log.Debugf("kubeclient init \n")
	var kubeconfig *string
	if home := homeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		return nil, err
	}

	log.Printf("kubeclient new config \n")
	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	log.Printf("kubeclient initialized \n")
	kbc := &KubeConnection{clientset: clientset}
	return kbc, nil
}

// NewInternalClient use for production
func NewInternalClient() (*KubeConnection, error) {
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	kbc := &KubeConnection{clientset: clientset}
	return kbc, nil
}

// GetPodCount return number of pods
func (kbc *KubeConnection) GetPodCount(ctx context.Context, namespace string) (int, error) {
	pods, err := kbc.clientset.CoreV1().Pods(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return -1, err
	}
	return len(pods.Items), nil
}

// GetDeployment returns shortened info about a kube deployment
func (kbc *KubeConnection) GetDeployment(ctx context.Context, deployment *Deployment, namespace string) (*Deployment, error) {
	var response Deployment
	var err error
	kubeDeployment, err := kbc.clientset.AppsV1().Deployments(namespace).Get(ctx, deployment.Name, metav1.GetOptions{})
	if err != nil {
		log.WithError(err).Error("Unexpected error")
		return deployment, err
	}

	response.Name = kubeDeployment.GetName()
	response.Status = deploymentStatus(kubeDeployment)
	if deployment.Type == ScraneOpcUASim {
		service, err := kbc.clientset.CoreV1().Services(namespace).Get(ctx, deployment.Name, metav1.GetOptions{})
		if err != nil {
			response.NodePort = service.Spec.Ports[0].NodePort
		}
	}
	return &response, err
}

// CreateDeployment creates a deployment and service
func (kbc *KubeConnection) CreateDeployment(ctx context.Context, deployment *Deployment, namespace string) (*Deployment, error) {
	var err error
	switch deployment.Type {
	case ScGwInternal:
		_, err = createDeployment(ctx, kbc, &scGwInternalDeployment, &scGwInternalConfigMap, deployment, namespace)
	case ScGwOpcUA:
		_, err = createDeployment(ctx, kbc, &scGwOpcUADeployment, &scGwOpcUAConfigMap, deployment, namespace)
	case ScraneOpcUASim:
		_, err = createDeployment(ctx, kbc, &scraneOpcUASimDeployment, &scraneOpcUASimConfigMap, deployment, namespace)
		if err == nil {
			kubeService := createService(&scraneOpcUASimService, deployment, namespace)
			kubeService, err = kbc.clientset.CoreV1().Services(namespace).Create(ctx, kubeService, metav1.CreateOptions{})
			if err == nil {
				deployment.NodePort = kubeService.Spec.Ports[0].NodePort
				deployment.Address = getOpcUaServiceURL(kubeService.Name, namespace)
			}
		}
	default:
		return deployment, errors.New("wrong deployment type")
	}
	return deployment, err
}

// DeleteDeployment deletes deployments, configmap and if needed service for the given name
func (kbc *KubeConnection) DeleteDeployment(ctx context.Context, deployment *Deployment, namespace string) error {
	err := kbc.clientset.AppsV1().Deployments(namespace).Delete(ctx, deployment.Name, metav1.DeleteOptions{})
	if err != nil {
		return err
	}
	log.Debugf("deleted deployment")
	err = kbc.clientset.CoreV1().ConfigMaps(namespace).Delete(ctx, deployment.Name, metav1.DeleteOptions{})
	if err != nil {
		return err
	}
	log.Println("deleted configmap")
	if deployment.Type == ScraneOpcUASim {
		err = kbc.clientset.CoreV1().Services(namespace).Delete(ctx, deployment.Name, metav1.DeleteOptions{})
		if err != nil {
			return err
		}
		log.Println("deleted service")
	}
	return err
}

func createDeployment(ctx context.Context, kbc *KubeConnection, deploymentTemplate *appsv1.Deployment, configMapTemplate *v1.ConfigMap,
	deployment *Deployment, namespace string) (*appsv1.Deployment, error) {
	kubeDeployment := createKubeDeployment(deploymentTemplate, deployment)
	kubeConfigMap := createKubeConfigMap(configMapTemplate, deployment)
	config, err := kbc.clientset.CoreV1().ConfigMaps(namespace).Create(ctx, kubeConfigMap, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}
	configMapSource := v1.ConfigMapEnvSource{LocalObjectReference: v1.LocalObjectReference{
		Name: config.ObjectMeta.Name,
	}}
	kubeDeployment.Spec.Template.Spec.Containers[0].EnvFrom[0].ConfigMapRef = &configMapSource
	return kbc.clientset.AppsV1().Deployments(namespace).Create(ctx, kubeDeployment, metav1.CreateOptions{})
}

func createKubeDeployment(template *appsv1.Deployment, deployment *Deployment) *appsv1.Deployment {
	kubeDeployment := template.DeepCopy()
	kubeDeployment.SetName(deployment.Name)
	kubeDeployment.Spec.Template.Labels[serviceIdKey] = deployment.Name
	kubeDeployment.Spec.Selector.MatchLabels[serviceIdKey] = deployment.Name
	return kubeDeployment
}

func createKubeConfigMap(template *v1.ConfigMap, deployment *Deployment) *v1.ConfigMap {
	kubeConfigMap := template.DeepCopy()
	kubeConfigMap.SetName(deployment.Name)
	kubeConfigMap.Data[addressKey] = deployment.Address
	kubeConfigMap.Data[serviceIdKey] = deployment.Name
	return kubeConfigMap
}

func createService(template *v1.Service, deployment *Deployment, namespace string) *v1.Service {
	kubeService := template.DeepCopy()
	kubeService.SetName(deployment.Name)
	kubeService.Spec.Selector[serviceIdKey] = deployment.Name
	return kubeService
}

func deploymentStatus(deployment *appsv1.Deployment) string {
	if (deployment.Status.Replicas - deployment.Status.ReadyReplicas) == 0 {
		return string(Online)
	}
	return string(Offline)
}

func homeDir() string {
	if h := os.Getenv("HOME"); h != "" {
		return h
	}
	return os.Getenv("USERPROFILE") // windows
}
