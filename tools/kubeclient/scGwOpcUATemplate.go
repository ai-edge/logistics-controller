package kubeclient

import (
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const scGwOpcUA = "scgw-opcua"

var scGwOpcUADeployment = appsv1.Deployment{
	ObjectMeta: metav1.ObjectMeta{
		Name: scGwOpcUA,
	},
	Spec: appsv1.DeploymentSpec{
		Replicas: int32Ptr(1),
		Selector: &metav1.LabelSelector{
			MatchLabels: map[string]string{
				"app":        scGwOpcUA,
				serviceIdKey: "",
			},
		},
		Template: v1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Labels: map[string]string{
					"app":        scGwOpcUA,
					serviceIdKey: "",
				},
			},
			Spec: v1.PodSpec{
				Containers: []v1.Container{
					{
						Name:  scGwOpcUA,
						Image: gitlabRegistry + scGwOpcUA,
						Ports: []v1.ContainerPort{
							{
								Name:          "opcua",
								Protocol:      v1.ProtocolTCP,
								ContainerPort: opcUaPort,
							},
						},
						EnvFrom: []v1.EnvFromSource{
							{
								ConfigMapRef: &v1.ConfigMapEnvSource{},
							},
							{
								ConfigMapRef: &v1.ConfigMapEnvSource{
									LocalObjectReference: v1.LocalObjectReference{
										Name: scGwOpcUA + "-config",
									},
								},
							},
							{
								SecretRef: &v1.SecretEnvSource{
									LocalObjectReference: v1.LocalObjectReference{
										Name: scGwOpcUA + "-secret",
									},
								},
							},
						},
					},
				},
				ImagePullSecrets: []v1.LocalObjectReference{
					{
						Name: gitlabSecret,
					},
				},
			},
		},
	},
}

var scGwOpcUAConfigMap = v1.ConfigMap{
	ObjectMeta: metav1.ObjectMeta{
		Name: scGwOpcUA,
	},
	Data: map[string]string{
		addressKey:   "",
		serviceIdKey: "",
	},
}
