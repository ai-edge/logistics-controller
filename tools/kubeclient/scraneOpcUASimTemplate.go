package kubeclient

import (
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const scraneSimOpcUA = "scrane-sim-opcua"

var scraneOpcUASimDeployment = appsv1.Deployment{
	ObjectMeta: metav1.ObjectMeta{
		Name: scraneSimOpcUA,
	},
	Spec: appsv1.DeploymentSpec{
		Replicas: int32Ptr(1),
		Selector: &metav1.LabelSelector{
			MatchLabels: map[string]string{
				"app":        scraneSimOpcUA,
				"protocol":   "opcua",
				serviceIdKey: "",
			},
		},
		Template: v1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Labels: map[string]string{
					"app":        scraneSimOpcUA,
					"protocol":   "opcua",
					serviceIdKey: "",
				},
			},
			Spec: v1.PodSpec{
				Containers: []v1.Container{
					{
						Name:  scraneSimOpcUA,
						Image: gitlabRegistry + scraneSimOpcUA,
						Ports: []v1.ContainerPort{
							{
								Name:          "opcua",
								Protocol:      v1.ProtocolTCP,
								ContainerPort: opcUaPort,
							},
						},
						EnvFrom: []v1.EnvFromSource{
							{
								ConfigMapRef: &v1.ConfigMapEnvSource{},
							},
							{
								ConfigMapRef: &v1.ConfigMapEnvSource{
									LocalObjectReference: v1.LocalObjectReference{
										Name: scraneSimOpcUA + "-config",
									},
								},
							},
							{
								SecretRef: &v1.SecretEnvSource{
									LocalObjectReference: v1.LocalObjectReference{
										Name: scraneSimOpcUA + "-secret",
									},
								},
							},
						},
					},
				},
				ImagePullSecrets: []v1.LocalObjectReference{
					{
						Name: gitlabSecret,
					},
				},
			},
		},
	},
}

var scraneOpcUASimConfigMap = v1.ConfigMap{
	ObjectMeta: metav1.ObjectMeta{
		Name: scraneSimOpcUA,
	},
	Data: map[string]string{
		"resourcePath": resourcePath,
		serviceIdKey:   "",
	},
}

var scraneOpcUASimService = v1.Service{
	ObjectMeta: metav1.ObjectMeta{
		Name: scraneSimOpcUA,
	},
	Spec: v1.ServiceSpec{
		Type: v1.ServiceTypeNodePort,
		Ports: []v1.ServicePort{
			{
				Protocol: v1.ProtocolTCP,
				Port:     opcUaPort,
			},
		},
		Selector: map[string]string{
			"app":        scraneSimOpcUA,
			"protocol":   "opcua",
			serviceIdKey: "",
		},
	},
}
