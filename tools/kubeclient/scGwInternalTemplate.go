package kubeclient

import (
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const scGwInternal = "scgw"

var scGwInternalDeployment = appsv1.Deployment{
	ObjectMeta: metav1.ObjectMeta{
		Name: "opcua",
	},
	Spec: appsv1.DeploymentSpec{
		Replicas: int32Ptr(1),
		Selector: &metav1.LabelSelector{
			MatchLabels: map[string]string{
				"app":        scGwInternal,
				"protocol":   "INTERNAL",
				serviceIdKey: "",
			},
		},
		Template: v1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Labels: map[string]string{
					"app":        scGwInternal,
					"protocol":   "INTERNAL",
					serviceIdKey: "",
				},
			},
			Spec: v1.PodSpec{
				Containers: []v1.Container{
					{
						Name:  scGwInternal,
						Image: gitlabRegistry + scGwInternal,
						EnvFrom: []v1.EnvFromSource{
							{
								ConfigMapRef: &v1.ConfigMapEnvSource{},
							},
							{
								ConfigMapRef: &v1.ConfigMapEnvSource{
									LocalObjectReference: v1.LocalObjectReference{
										Name: scGwInternal + "-config",
									},
								},
							},
							{
								SecretRef: &v1.SecretEnvSource{
									LocalObjectReference: v1.LocalObjectReference{
										Name: scGwInternal + "-secret",
									},
								},
							},
						},
					},
				},
				ImagePullSecrets: []v1.LocalObjectReference{
					{
						Name: gitlabSecret,
					},
				},
			},
		},
	},
}

var scGwInternalConfigMap = v1.ConfigMap{
	ObjectMeta: metav1.ObjectMeta{
		Name: scGwInternal,
	},
	Data: map[string]string{
		serviceIdKey: "",
	},
}
