package mongodb

import (
	"context"
	"fmt"
	"reflect"
	"time"

	log "github.com/sirupsen/logrus"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// DBConnection stores the client
type DBConnection struct {
	Client *mongo.Client
}

// InsertOne inserts an item
func (dbc *DBConnection) InsertOne(dbName string, collectionName string, item interface{}) error {
	collection, ctx, cancel := dbc.getContext(dbName, collectionName)
	defer cancel()
	item, err := collection.InsertOne(ctx, item)
	if err != nil {
		log.WithError(err).Error("insert failed")
		return err
	}
	return err
}

// ReplaceOne replaces an item with given query
func (dbc *DBConnection) ReplaceOne(dbName string, collectionName string, query map[string]interface{}, item interface{}) error {
	collection, ctx, cancel := dbc.getContext(dbName, collectionName)
	defer cancel()
	item, err := collection.ReplaceOne(ctx, query, item)
	if err != nil {
		log.WithError(err).Error("replace failed")
		return err
	}
	return err
}

// GetOne returns an item with given query
func (dbc *DBConnection) GetOne(dbName string, collectionName string, query map[string]interface{}, item interface{}) error {
	collection, ctx, cancel := dbc.getContext(dbName, collectionName)
	defer cancel()
	err := collection.FindOne(ctx, query).Decode(item)
	if err != nil {
		log.WithError(err).Trace("get one failed")
		return err
	}
	return nil
}

// DeleteOne deletes an item with given query
func (dbc *DBConnection) DeleteOne(dbName string, collectionName string, query map[string]interface{}) error {
	collection, ctx, cancel := dbc.getContext(dbName, collectionName)
	defer cancel()
	_, err := collection.DeleteOne(ctx, query)
	if err != nil {
		log.WithError(err).Error("delete one failed")
		return err
	}
	return err
}

// DeleteMany deletes all item with given query
func (dbc *DBConnection) DeleteMany(dbName string, collectionName string, query map[string]interface{}) error {
	collection, ctx, cancel := dbc.getContext(dbName, collectionName)
	defer cancel()
	_, err := collection.DeleteMany(ctx, query)
	if err != nil {
		log.WithError(err).Error("delete one failed")
		return err
	}
	return err
}

// GetAll returns items with given query
func (dbc *DBConnection) GetAll(dbName string, collectionName string, query map[string]interface{}, items interface{}, limit int64) error {

	collection, ctx, cancel := dbc.getContext(dbName, collectionName)
	defer cancel()
	cursor, err := collection.Find(ctx, query)
	if err != nil {
		log.WithError(err).Trace("find failed")
		return err
	}
	defer cursor.Close(ctx)

	itemsValues := reflect.ValueOf(items)
	results := itemsValues.Elem()
	if results.Kind() == reflect.Interface {
		results = results.Elem()
	}
	results = results.Slice(0, results.Cap())
	resType := results.Type().Elem()
	i := 0
	for cursor.Next(ctx) {
		i++
		item := reflect.New(resType)
		err := cursor.Decode(item.Interface())
		if err != nil {
			log.WithError(err).Error("decode failed")
			break
		}
		results = reflect.Append(results, item.Elem())
		if int64(i) == limit {
			break
		}
	}
	itemsValues.Elem().Set(results.Slice(0, i))

	if i == 0 {
		return fmt.Errorf("not found in db %s collection %s", dbName, collectionName)
	}
	return err
}

// DeInit deinitializes mongo client
func (dbc *DBConnection) DeInit() {
	//TODO:
}

//getContext get DB context
func (dbc *DBConnection) getContext(dbName string, collectionName string) (*mongo.Collection, context.Context, context.CancelFunc) {
	collection := dbc.Client.Database(dbName).Collection(collectionName)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	return collection, ctx, cancel
}

// NewMongoDBClient create connection with mongo db
func NewMongoDBClient(dbURL string, authDB string, username string, password string) (*DBConnection, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	clientOptions := options.Client().ApplyURI(dbURL).SetAuth(options.Credential{
		AuthSource: authDB, Username: username, Password: password,
	})
	mongoClient, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.WithError(err).Error("connect failed")
		return nil, err
	}

	// Check the connection
	err = mongoClient.Ping(context.TODO(), nil)

	if err != nil {
		log.WithError(err).Error("ping failed")
		return nil, err
	}

	log.Debugf("connected to MongoDB!")
	dbc := &DBConnection{Client: mongoClient}
	return dbc, nil
}
