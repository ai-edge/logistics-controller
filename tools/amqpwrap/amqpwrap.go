package amqpwrap

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"github.com/streadway/amqp"
)

const (
	prefetchCount = 20
	prefetchSize  = 0
	//AutoAck amqp ack
	AutoAck = false
	ttlSec  = 3600000 //1 hour in ms
)

//AMQPWrapper AMQP client for RabbitMQ
type AMQPWrapper struct {
	connection *amqp.Connection
	//threads should not share channels
	pubChannel  *amqp.Channel
	subChannel  *amqp.Channel
	closeConnCh chan *amqp.Error
	subCloseCh  chan *amqp.Error
	subCancelCh chan string
}

//NewAMQPWrapper returns wrapper struct without channels initialized
func NewAMQPWrapper(connection *amqp.Connection) (*AMQPWrapper, error) {
	if connection == nil {
		return nil, fmt.Errorf("nil connection")
	}
	amqpWrapper := AMQPWrapper{connection: connection}
	amqpWrapper.connection = connection

	//init error chan
	amqpWrapper.closeConnCh = connection.NotifyClose(make(chan *amqp.Error))

	//log.Println("amqp wrapper connection:", connection)
	return &amqpWrapper, nil
}

//DeInit closes the channels
func (context *AMQPWrapper) DeInit() {
	if context.pubChannel != nil {
		context.pubChannel.Close()
	}
	if context.subChannel != nil {
		context.subChannel.Close()
	}
}

//GetConnCloseChan returns closed connection notification channel
func (context *AMQPWrapper) GetConnCloseChan() chan *amqp.Error {
	return context.closeConnCh
}

//GetSubCloseChan returns closed channel notification of subscription channel
func (context *AMQPWrapper) GetSubCloseChan() chan *amqp.Error {
	return context.subCloseCh
}

//GetSubCancelChan returns cancel notification of subscription channel
func (context *AMQPWrapper) GetSubCancelChan() chan string {
	return context.subCancelCh
}

//InitPubChannel initializes publish channel
func (context *AMQPWrapper) InitPubChannel() error {
	ch, err := context.connection.Channel()
	if err != nil {
		log.WithError(err).Error("Failed to open channel")
		return err
	}
	err = ch.Qos(
		prefetchCount, // prefetch count
		prefetchSize,  // prefetch size
		false,         // global
	)
	if err != nil {
		log.WithError(err).Error("Failed to set qos of channel")
		return err
	}
	context.pubChannel = ch
	return nil
}

//InitSubChannel initializes subscribe channel
func (context *AMQPWrapper) InitSubChannel() error {
	ch, err := context.connection.Channel()
	if err != nil {
		log.WithError(err).Error("Failed to open channel")
		return err
	}
	err = ch.Qos(
		prefetchCount, // prefetch count
		prefetchSize,  // prefetch size
		false,         // global
	)
	if err != nil {
		log.WithError(err).Error("Failed to set qos of channel")
		return err
	}

	context.subChannel = ch
	context.subCloseCh = ch.NotifyClose(make(chan *amqp.Error))
	context.subCancelCh = ch.NotifyCancel(make(chan string))

	return nil
}

//InitAMQPConsumer creates and binds a queue and an exchange, creates a consume channel.
func (context *AMQPWrapper) InitAMQPConsumer(
	queueName string,
	exchangeName string,
	route string) (<-chan amqp.Delivery, error) {
	if context.subChannel == nil || context.subCloseCh == nil || context.subCancelCh == nil {
		return nil, fmt.Errorf("sub channel not initialized: %v", context)
	}

	args := make(amqp.Table)
	args["x-message-ttl"] = ttlSec
	queue, err := context.subChannel.QueueDeclare(
		queueName, // name
		true,      // durable
		false,     // delete when unused
		false,     // exclusive
		true,      // no-wait, channel exception shall be catched if fails
		args,      // arguments
	)

	if err != nil {
		log.WithError(err).Error("Failed to create the queue")
		return nil, err
	}
	log.Debugf("queue declared %s", queueName)

	//each service declares its own layer's exchange
	err = context.subChannel.ExchangeDeclare(
		exchangeName, // name
		"topic",      // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		true,         // no-wait, channel exception shall be catched if fails
		nil,          // arguments
	)
	if err != nil {
		log.WithError(err).Error("Failed to declare exchange")
		return nil, err
	}
	log.Debugf("exchange declared %s", exchangeName)

	err = context.subChannel.QueueBind(
		queueName,    // queue name
		route,        // routing key
		exchangeName, // exchange
		true,         // no wait
		nil,          // args
	)
	if err != nil {
		log.WithError(err).Error("Failed to bind a queue")
		return nil, err
	}
	log.Debugf("queue bound %s %s %s", queueName, route, exchangeName)

	consumerTag := queueName + route
	msgChan, err := context.subChannel.Consume(
		queueName,   // queue
		consumerTag, // consumer
		AutoAck,     // auto ack
		false,       // exclusive
		false,       // no local
		true,        // no wait
		nil,         // args
	)
	if err != nil {
		log.WithError(err).Error("Failed to register a consumer")
		return nil, err
	}
	log.Debugln("consumer initialized. queue:", queue, "exchange:",
		exchangeName, "route:", route, "consumerTag", consumerTag)

	return msgChan, nil
}

//PublishTo publishes a new msg to an exchange with route
func (context *AMQPWrapper) PublishTo(exchange string, route string, jsonData []byte) error {
	if context.pubChannel == nil {
		return fmt.Errorf("pub channel not initialized: %v", context)
	}
	err := context.pubChannel.Publish(
		exchange, // exchange
		route,    // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         []byte(jsonData),
		})

	if err != nil {
		log.WithError(err).Error("Failed to publish a message")
		return err
	}

	log.Traceln("AMQP sent to exchange:", exchange, " with route:", route)
	log.Traceln("AMQP data: ", jsonData)
	return err
}
