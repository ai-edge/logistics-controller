package mqttwrap

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

//MQTTConnection for RabbitMQ pub/sub
type MQTTConnection struct {
	client mqtt.Client
}

const (
	keepAliveSec   = 10
	pingTimeOutSec = 10
)

//defaultHandler mqtt default consumer handler
func defaultHandler(client mqtt.Client, msg mqtt.Message) {
	log.Errorln("default handler for topic:", msg.Topic(), "msg:", msg.Payload())
	return
}

//conLostHandler mqtt connection lost handler
func conLostHandler(client mqtt.Client, err error) {
	log.WithError(err).Errorf("connection lost. Shutting down")
	panic(fmt.Errorf("connection lost"))
}

//NewMQTTConn Initialize rabbitmq connection,subscribe to L3
func NewMQTTConn(brokerAddress string, clientID string, username string, password string) (*MQTTConnection, error) {

	rabbitConn := MQTTConnection{}
	logger := logrus.StandardLogger()
	mqtt.DEBUG = logger
	mqtt.ERROR = logger

	opts := mqtt.NewClientOptions().AddBroker(brokerAddress).SetClientID(clientID).SetUsername(username).SetPassword(password)
	opts.SetKeepAlive(keepAliveSec * time.Second)
	opts.SetDefaultPublishHandler(defaultHandler)
	opts.SetConnectionLostHandler(conLostHandler)
	opts.SetPingTimeout(pingTimeOutSec * time.Second)
	//deletes the queue and its bindings
	opts.SetCleanSession(false)
	opts.SetAutoReconnect(false)

	rabbitConn.client = (mqtt.NewClient(opts))
	if token := rabbitConn.client.Connect(); token.Wait() && token.Error() != nil {
		log.Fatalln(token.Error())
	}

	return &rabbitConn, nil
}

//DeInit unsubscribes from all topics and disconnects client
func (mqconn *MQTTConnection) DeInit() {
	if token := mqconn.client.Unsubscribe(""); token.Wait() && token.Error() != nil {
		log.Fatalln(token.Error())
	}
	log.Debugf("Unsubscribed ")
	mqconn.client.Disconnect(250)
}

//InitConsumer consumes messages sent to L2
func (mqconn *MQTTConnection) InitConsumer(topicName string, qos byte, handler mqtt.MessageHandler) error {

	//Subscribes to topic
	if token := mqconn.client.Subscribe(topicName, qos, handler); token.Wait() && token.Error() != nil {
		log.Fatalln(token.Error())
	}
	log.Debugln("subscribed to topic", topicName)

	return nil
}

//PublishTo publishes a new msg to L2's topic. Client thread safe
func (mqconn *MQTTConnection) PublishTo(topicName string, qos byte, jsonData []byte) error {

	log.Printf("publishing msg to topic %s", topicName)
	//qos == 1 for persistent messages
	token := mqconn.client.Publish(topicName, qos, true, jsonData)

	token.Wait()
	return nil
}
