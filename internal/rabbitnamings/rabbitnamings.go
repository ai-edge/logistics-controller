//Package rabbitnamings consists of routing and exchange namings.
package rabbitnamings

//L4RouteBase is how routing key starts
const L4RouteBase = "tosgw"

//L3RouteBase is how routing key starts
const L3RouteBase = "operations"

//L2RouteBase is how routing key starts
const L2RouteBase = "scgw"

//ScheRouteBase is how routing key starts
const ScheRouteBase = "scheduler"

//DispRouteBase is how routing key starts
const DispRouteBase = "dispatcher"

//HtRouteBase is how routing key starts
const HtRouteBase = "ht"

//ServiceStatusRouteBase is how routing starts for internal service status
const ServiceStatusRouteBase = "servicestatus"

//JobRequestRouteKey name of the job orders routing
const JobRequestRouteKey = "jobrequest"

//WorkOrderRequestRouteKey name of the work orders routing
const WorkOrderRequestRouteKey = "workorderrequest"

//ContainerUpdateRequestRouteKey name of the container update routing
const ContainerUpdateRequestRouteKey = "containerupdaterequest"

//ContainerStatusRouteKey name of the container progress routing
const ContainerStatusRouteKey = "containerstatus"

//CraneStatusRouteKey name of the job orders routing
const CraneStatusRouteKey = "cranestatus"

//WorkOrderProgressRouteKey name of the work order progress routing
const WorkOrderProgressRouteKey = "workorderprogress"

//ScheduleRequestRouteKey name of the schedule routing
const ScheduleRequestRouteKey = "schedulerequest"

//ScheduleStatusRouteKey name of the schedule progress routing
const ScheduleStatusRouteKey = "schedulestatus"

//DispatchRequestRouteKey name of the dispatch routing
const DispatchRequestRouteKey = "dispatchrequest"

//DispatchStatusRouteKey name of the dispatch progress routing
const DispatchStatusRouteKey = "dispatchstatus"

//TruckStatusRouteKey name of the truck status progress routing
const TruckStatusRouteKey = "truckstatus"

//TZReservationRequestRouteKey name of the TZ reservation request routing
const TZReservationRequestRouteKey = "tzreservationrequest"

//StFieldStatusRouteKey name of the stacking field status routing
const StFieldStatusRouteKey = "stackingfieldstatus"

//TrFieldStatusRouteKey name of the transfer field status routing
const TrFieldStatusRouteKey = "transferfieldstatus"

//CreatedWorkOrderRouteKey name of the newly created work orders routing
const CreatedWorkOrderRouteKey = "createdworkorder"

//RemovedWorkOrderRouteKey name of the deleted work order from terminal db
const RemovedWorkOrderRouteKey = "removedworkorder"

//WorkOrderAddedToOperationsRouteKey name of work order that is received by operations
const WorkOrderAddedToOperationsRouteKey = "addedworkorder"

//WorkOrderDeletedFromOperationsRouteKey name of the work order that is deleted from operations
const WorkOrderDeletedFromOperationsRouteKey = "deletedworkorder"

//MQTTExchange is the name of the MQTT exchange (default amq.topic for Rabbit)
const MQTTExchange = "amq.topic"
