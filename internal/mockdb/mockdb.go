package mockdb

import (
	log "github.com/sirupsen/logrus"
)

// Client is a handle simulating a pool of connections to a mock db
type Client struct {
	id             [16]byte
	keyVaultClient *Client
}

// DBConnection stores the client
type DBConnection struct {
	Client *Client
}

// InsertOne inserts an item
func (dbc *DBConnection) InsertOne(dbName string, collectionName string, item interface{}) error {
	log.Printf("Pretend insert one\n")
	return nil
}

// ReplaceOne replaces an item with given query
func (dbc *DBConnection) ReplaceOne(dbName string, collectionName string, query map[string]interface{}, item interface{}) error {
	log.Printf("Pretend replace one\n")
	return nil
}

// GetOne returns an item with given query
func (dbc *DBConnection) GetOne(dbName string, collectionName string, query map[string]interface{}, item interface{}) error {
	log.Printf("Pretend get one\n")
	return nil
}

// DeleteOne deletes an item with given query
func (dbc *DBConnection) DeleteOne(dbName string, collectionName string, query map[string]interface{}) error {
	log.Printf("Pretend delete one\n")
	return nil
}

// DeleteMany deletes all item with given query
func (dbc *DBConnection) DeleteMany(dbName string, collectionName string, query map[string]interface{}) error {
	log.Printf("Pretend delete one\n")
	return nil
}

// GetAll returns items with given query
func (dbc *DBConnection) GetAll(dbName string, collectionName string, query map[string]interface{}, items interface{}, limit int64) error {

	log.Printf("Pretend get all\n")
	return nil
}

// DeInit deinitializes mongo client
func (dbc *DBConnection) DeInit() {
	//TODO:
}

// NewMockDBClient create connection with mock db
func NewMockDBClient(dbURL string, authDB string, username string, password string) (*DBConnection, error) {

	log.Printf("Connected to mock db!\n")
	dbc := &DBConnection{nil}
	return dbc, nil
}
