//Package dbservice defines the interface for DB's.
package dbservice

// DBService defines the api actions for the DB service
type DBService interface {
	DeInit()
	InsertOne(dbName string, collectionName string, item interface{}) error
	ReplaceOne(dbName string, collectionName string, query map[string]interface{}, item interface{}) error
	GetOne(dbName string, collectionName string, query map[string]interface{}, item interface{}) error
	DeleteOne(dbName string, collectionName string, query map[string]interface{}) error
	DeleteMany(dbName string, collectionName string, query map[string]interface{}) error
	GetAll(dbName string, collectionName string, query map[string]interface{}, items interface{}, limit int64) error
}
