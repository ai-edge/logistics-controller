//Package orders is used for work order-job order creation and manipulation mainly for simulation purposes
package orders

import (
	"ecs/api/generated/models"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/google/uuid"
)

var getOrderTypetest = []struct {
	fromAreaName      string
	toAreaName        string
	stackingAreaNames []string
	transferZoneNames []string
	orderType         models.OrderTypes
	err               error
}{
	{"BA01", "TZ01", []string{"BA01"}, []string{"TZ01"}, models.TRANSFER_OUT, nil},
	{"TZ01", "BA01", []string{"BA01"}, []string{"TZ01"}, models.TRANSFER_IN, nil},
	{"BA01", "BA01", []string{"BA01"}, []string{"TZ01"}, models.INTRASTACK, nil},
	{"TZ01", "TZ01", []string{"BA01"}, []string{"TZ01"}, "", fmt.Errorf("request root intpretation failed")},
	{"BA01", "TZ01", []string{"BA01", "BA01"}, []string{"TZ01"}, "", fmt.Errorf("invalid block configuration for area BA01")},
	{"BA01", "TZ01", []string{"BA01"}, []string{"TZ01", "TZ01"}, "", fmt.Errorf("invalid block configuration for area TZ01")},
}

var generatecontainertest = []struct {
	stackingAreaNames []string
	transferZoneNames []string
	root              models.WorkItemRoot
	dimensions        models.Dimensions3D
	err               error
}{
	{[]string{"BA01"}, []string{"TZ01"}, models.WorkItemRoot{From: models.LogicalLocation3D{AreaName: "TZ01", Bay: "007", Row: "001", Tier: "01"},
		To: models.LogicalLocation3D{AreaName: "BA01", Bay: "009", Row: "002", Tier: "01"}, ContainerType: models.C20_G0}, models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2438}, nil},
	{[]string{"BA01"}, []string{"TZ01"}, models.WorkItemRoot{From: models.LogicalLocation3D{AreaName: "TZ01", Bay: "007", Row: "001", Tier: "01"},
		To: models.LogicalLocation3D{AreaName: "BA01", Bay: "009", Row: "002", Tier: "01"}, ContainerType: models.C40_G0}, models.Dimensions3D{Width: 12192, Length: 2438, Depth: 2438}, nil},
	{[]string{"BA01"}, []string{"TZ01"}, models.WorkItemRoot{From: models.LogicalLocation3D{AreaName: "TZ01", Bay: "007", Row: "001", Tier: "01"},
		To: models.LogicalLocation3D{AreaName: "BA01", Bay: "009", Row: "002", Tier: "01"}, ContainerType: models.C42_G0}, models.Dimensions3D{Width: 12192, Length: 2438, Depth: 2591}, nil},
	{[]string{"BA01"}, []string{"TZ01"}, models.WorkItemRoot{From: models.LogicalLocation3D{AreaName: "TZ01", Bay: "007", Row: "001", Tier: "01"},
		To: models.LogicalLocation3D{AreaName: "BA01", Bay: "009", Row: "002", Tier: "01"}, ContainerType: models.C0000}, models.Dimensions3D{Width: 0, Length: 0, Depth: 0}, fmt.Errorf("not supported container type C0000")},
}

var getblocktest = []struct {
	blockName string
	err       error
}{
	{"RTG002", nil},
	{"RTG004", fmt.Errorf("not supported container type C0000")},
}

func TestTablegetOrderType(t *testing.T) {

	for _, tt := range getOrderTypetest {

		var root models.WorkItemRoot
		var bl models.ContainerBlockComposite
		bl.StackingAreaNames = tt.stackingAreaNames
		bl.TransferZoneNames = tt.transferZoneNames
		root.From.AreaName = tt.fromAreaName
		root.To.AreaName = tt.toAreaName

		orderType, err := getOrderType(root, bl)
		if (err == nil && tt.err != nil) || (err != nil && tt.err == nil) {
			t.Errorf("getOrderType(root, bl) = %s, %s", orderType, err)
		}
		if orderType != tt.orderType {
			t.Errorf("Wrong order type is returned = %s", orderType)
		}
	}
}

func TestTableGenerateContainer(t *testing.T) {
	for _, tt := range generatecontainertest {

		var bl models.ContainerBlockComposite
		bl.StackingAreaNames = tt.stackingAreaNames
		bl.TransferZoneNames = tt.transferZoneNames

		var currentTime = int32(time.Now().Unix())
		guid := uuid.New().String()
		isoid := "BBBU111111" + strconv.Itoa(int(currentTime))
		newCon, err := GenerateContainer(tt.root.ContainerType, tt.root.From, guid, isoid)
		if (err == nil && tt.err != nil) || (err != nil && tt.err == nil) {
			t.Errorf("GenerateContainer(root.ContainerType, root.From) failed. %v, %s", newCon, err)
		}

		if newCon.ItemDimensions != tt.dimensions {
			t.Errorf("container dimensions does not match: %v", newCon.ItemDimensions)
		}
	}
}

func TestTableGetBlock(t *testing.T) {
	for _, tt := range getblocktest {
		bls := GetBlocks()
		bl, err := getBlock(tt.blockName, bls)

		if (err == nil && tt.err != nil) || (err != nil && tt.err == nil) {
			t.Errorf("getBlock(%s, bls) failed. %v, %s", tt.blockName, bl, err)
		}
	}
}

func GetBlocks() []models.ContainerBlockComposite {
	var bls []models.ContainerBlockComposite
	var bl models.ContainerBlockComposite
	bl.StackingCraneIDs = append(bl.StackingCraneIDs, "RTG001")
	bl.StackingCraneIDs = append(bl.StackingCraneIDs, "RTG002")

	bls = append(bls, bl)
	return bls
}
