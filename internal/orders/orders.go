//Package orders is used for work order-job order creation and manipulation mainly for simulation purposes
package orders

import (
	"ecs/api/generated/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
)

const (
	maxTzTier                   = int(1)
	maxSaTier                   = int(6)
	compositeLocationNameLength = int(15)
)

//ContainerSize includes dimensions and weight of the container
type ContainerSize struct {
	Dimensions models.Dimensions3D
	weight     int32
}

// Orders context for keeping the URL's.
type Orders struct {
	operationsURL string
	client        *http.Client
}

// NewOrders creates an Orders with specific URL
func NewOrders(operationsURL string) *Orders {
	var client = http.DefaultClient

	orders := &Orders{
		operationsURL: operationsURL,
		client:        client,
	}
	return orders
}

func isEven(nm int) bool {
	return nm%2 == 0
}

// GetContainerSizeMap returns container sizes
func GetContainerSizeMap() map[models.ContainerType]ContainerSize {
	return map[models.ContainerType]ContainerSize{
		models.C20_G0: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2438}, weight: 15000},
		models.C20_G1: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2438}, weight: 15000},
		models.C20_G2: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2438}, weight: 15000},
		models.C20_G3: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2438}, weight: 15000},
		models.C22_G0: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2591}, weight: 15000},
		models.C22_G1: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2591}, weight: 15000},
		models.C22_G2: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2591}, weight: 15000},
		models.C22_G3: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2591}, weight: 15000},
		models.C22_G4: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2591}, weight: 15000},
		models.C22_G8: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2591}, weight: 15000},
		models.C22_G9: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2591}, weight: 15000},
		models.C25_G0: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2895}, weight: 15000},
		models.C26_G0: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2438, Depth: 2895}, weight: 15000},
		models.C2_EG0: ContainerSize{Dimensions: models.Dimensions3D{Width: 6058, Length: 2500, Depth: 2895}, weight: 15000},
		models.C40_G0: ContainerSize{Dimensions: models.Dimensions3D{Width: 12192, Length: 2438, Depth: 2438}, weight: 27000},
		models.C42_G0: ContainerSize{Dimensions: models.Dimensions3D{Width: 12192, Length: 2438, Depth: 2591}, weight: 30000},
		models.C42_G1: ContainerSize{Dimensions: models.Dimensions3D{Width: 12192, Length: 2438, Depth: 2591}, weight: 30000},
	}
}

//GetNameFromLocation converts give area, bay, row and tier to composite field name
func GetNameFromLocation(location models.LogicalLocation3D) (string, error) {
	// this composite name is used in fieldstatus; and only tier 01 is used in the db.
	// Occupancy of other tiers are represented by item count
	nameComposite := location.AreaName + "-" + location.Bay + "-" + location.Row + "-01"
	if len(nameComposite) != compositeLocationNameLength {
		return "", fmt.Errorf("invalid name %s", nameComposite)
	}
	return nameComposite, nil
}

//getOrderType returns order type of a given work item based on block data
func getOrderType(root models.WorkItemRoot, bl models.ContainerBlockComposite) (models.OrderTypes, error) {
	//inticates the origin and destination of the work item
	fromSA := false
	toSA := false
	fromTZ := false
	toTZ := false
	for _, name := range bl.StackingAreaNames {
		if root.From.AreaName == name {
			if fromSA {
				return "", fmt.Errorf("invalid block configuration for area %s", name)
			}
			fromSA = true
		}
		if root.To.AreaName == name {
			if toSA {
				return "", fmt.Errorf("invalid block configuration for area %s", name)
			}
			toSA = true
		}
	}
	for _, name := range bl.TransferZoneNames {
		if root.From.AreaName == name {
			if fromTZ {
				return "", fmt.Errorf("invalid block configuration for area %s", name)
			}
			fromTZ = true
		}
		if root.To.AreaName == name {
			if toTZ {
				return "", fmt.Errorf("invalid block configuration for area %s", name)
			}
			toTZ = true
		}
	}
	if fromSA && toSA && !fromTZ && !toTZ {
		return models.INTRASTACK, nil
	} else if fromSA && !toSA && !fromTZ && toTZ {
		return models.TRANSFER_OUT, nil
	} else if !fromSA && toSA && fromTZ && !toTZ {
		return models.TRANSFER_IN, nil
	}
	return "", fmt.Errorf("request root intpretation failed %v", root)
}

//GenerateContainer generates a container with random uuid
func GenerateContainer(conType models.ContainerType, location models.LogicalLocation3D, guid string, isoid string) (models.Container, error) {
	var con = models.Container{}
	containerSizeMap := GetContainerSizeMap()
	if _, ok := containerSizeMap[conType]; !ok {
		return con, fmt.Errorf("not supported container type %s", conType)
	}
	con.ItemDimensions = containerSizeMap[conType].Dimensions
	con.Weight = containerSizeMap[conType].weight
	con.ItemType = models.CONTAINER
	con.ItemGUID = guid
	con.ContainerIDInfo.ISOID = isoid
	con.ContainerIDInfo.ContainerType = conType
	con.DoorDirection = models.IGNORE
	//starting location of work item is the container's actual location
	con.ContainerLogicalLocation = location
	con.Deviation3D = models.Position3D{}
	return con, nil
}

//getBlock returns assigned block of a given stacking crane
func getBlock(sc string, bls []models.ContainerBlockComposite) (bl models.ContainerBlockComposite, err error) {
	for _, bl := range bls {
		for _, id := range bl.StackingCraneIDs {
			if id == sc {
				return bl, nil
			}
		}
	}
	return bl, fmt.Errorf("cannot find stacking crane id")
}

func (orders *Orders) sendRequest(req *http.Request) ([]byte, error) {
	resp, err := orders.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, err
}

// GetStackingFieldStatus for container check
func (orders *Orders) GetStackingFieldStatus(fieldName string) (models.StackingFieldStatus, error) {
	req, err := http.NewRequest(http.MethodGet, orders.operationsURL+"/stackingfieldstatuses/"+fieldName, nil)
	fieldStat := models.StackingFieldStatus{}
	if err != nil {
		return fieldStat, fmt.Errorf("new request failed %w", err)
	}
	// req.URL.RawQuery = request.URL.Query().Encode()

	body, err := orders.sendRequest(req)
	if err != nil {
		return fieldStat, fmt.Errorf("send request failed %w", err)
	}
	err = json.Unmarshal(body, &fieldStat)

	return fieldStat, err
}

//GetStackingFields requests stacking fields from operations service
func (orders *Orders) GetStackingFields(limit int) ([]models.StackingField, error) {
	req, err := http.NewRequest(http.MethodGet, orders.operationsURL+"/stackingfields?limit="+strconv.FormatInt(int64(limit), 10), nil)
	if err != nil {
		return nil, fmt.Errorf("send request failed %w", err)
	}

	body, err := orders.sendRequest(req)
	if err != nil {
		return nil, fmt.Errorf("send request failed %w", err)
	}
	stFields := []models.StackingField{}
	err = json.Unmarshal(body, &stFields)

	return stFields, err
}

//checkDestinationValidForType requests isDestinationValid from operations
func (orders *Orders) checkDestinationValidForType(root models.WorkItemRoot, width int32) error {
	widthStr := strconv.FormatInt(int64(width), 10)
	containerType := string(root.ContainerType)
	originField, err := GetNameFromLocation(root.From)
	if err != nil {
		return fmt.Errorf("failed to get name from location %s err: %w", root.From, err)
	}
	destinationField, err := GetNameFromLocation(root.To)
	if err != nil {
		return fmt.Errorf("failed to get name from location %s err: %w", root.To, err)
	}

	req, err := http.NewRequest(http.MethodGet, orders.operationsURL+"/isdestinationvalid?width="+widthStr+"&containertype="+containerType+"&originfield="+originField+"&destinationfield="+destinationField, nil)
	if err != nil {
		return fmt.Errorf("send request failed %w", err)
	}

	body, err := orders.sendRequest(req)
	if err != nil {
		return fmt.Errorf("send request failed %w", err)
	}
	isValid := false
	err = json.Unmarshal(body, &isValid)

	if isValid {
		return nil
	}
	return fmt.Errorf("Destination is not valid")
}

//CreateWorkOrder creates a work order with work items provided that the request is valid
func (orders *Orders) CreateWorkOrder(req models.WorkOrderCreateRequest,
	bls []models.ContainerBlockComposite) (models.OperationsWorkOrderBase, error) {
	var wo = models.OperationsWorkOrderBase{WorkOrderReqType: models.NEW_WO}
	if len(req.WorkItemRequests) == 0 {
		return wo, fmt.Errorf("invalid request %v", req)
	}
	bl, err := getBlock(req.ResourceID, bls)
	if err != nil {
		return wo, fmt.Errorf("no matching block %w", err)
	}

	var currentTime = int32(time.Now().Unix())
	wo.WorkOrderName = "woc" + req.ResourceID + strconv.Itoa(int(currentTime))
	wo.WorkOrderSequence = currentTime
	wo.DispatchTime = time.Now()
	wo.L3workID = strconv.Itoa(int(currentTime))
	wo.ResourceID = req.ResourceID
	for i, root := range req.WorkItemRequests {
		var wi = models.OperationsWorkOrderItemBase{}
		wi.WorkItemName = "wic" + req.ResourceID + strconv.Itoa(int(wo.WorkOrderSequence)+i)
		wi.WorkOrderReference = wo.WorkOrderName
		wi.CreationTime = time.Now()
		wi.UpdateTime = time.Time{}
		wi.AckTime = time.Time{}
		wi.Priority = root.Priority
		wi.ItemStatus = models.ENTERED
		wi.StackingCraneID = req.ResourceID

		wi.StartingPointLogicalLocation = root.From
		wi.FinalPointLogicalLocation = root.To
		wi.OrderType, err = getOrderType(root, bl)
		if err != nil {
			return wo, fmt.Errorf("getting orderType failed %w", err)
		}

		guid := uuid.New().String()
		isoid := "BBBU111111" + strconv.Itoa(int(currentTime))
		newCon, err := GenerateContainer(root.ContainerType, wi.StartingPointLogicalLocation, guid, isoid)
		if err != nil {
			return wo, fmt.Errorf("container generation failed %w", err)
		}

		// first check for validity of the move, if the destination field is stacking area
		for _, name := range bl.StackingAreaNames {
			if root.To.AreaName == name {
				err = orders.checkDestinationValidForType(root, newCon.ItemDimensions.Width)
				if err != nil {
					return wo, err
				}
				break
			}
		}
		err = checkLocationValid(root, wi.OrderType)
		if err != nil {
			return wo, fmt.Errorf("container location is not valid %v", root.To)
		}

		//this is kinda simulation run where the entering, in-block-moving and leaving containers are tracked and reused
		switch wi.OrderType {
		case models.TRANSFER_IN:
			//store a new container in the queue and use the same container for instack and out moves
			wi.HandledItem = newCon
			wi.StartingTransportResource = models.HtOrderRes{
				ResourceType:       "CTR",
				ResourceID:         "CTR" + wi.WorkItemName,
				TrailerInformation: []models.ContainerTrailerInformation{},
				HtType:             "InternalTruck",
			}
			wi.StartingChasisInformation = models.TargetChasisInformation{
				TargetChasis:           0,
				TargetLocationOnChasis: "empty",
			}
		case models.INTRASTACK:
			//reuse already stacked container
			err := func() error {
				fieldName, err := GetNameFromLocation(root.From)
				if err != nil {
					return fmt.Errorf("failed to get name from location %s err: %w", root.From, err)
				}
				fieldStatus, err := orders.GetStackingFieldStatus(fieldName)
				if err == nil {
					if fieldStatus.ItemCount > 0 {
						stackedCon := fieldStatus.StackedItems[fieldStatus.ItemCount-1]
						if stackedCon.ContainerType == root.ContainerType {
							newCon.ItemGUID = stackedCon.ItemGUID
							newCon.ContainerIDInfo.ISOID = stackedCon.ISOID
							wi.HandledItem = newCon
							return nil
						}
						return fmt.Errorf("another type of container found at the starting point %s", root.From)
					}
				}
				return fmt.Errorf("invalid intrastack request")
			}()
			if err != nil {
				return wo, fmt.Errorf("cannot create work order %w", err)
			}
		case models.TRANSFER_OUT:
			//reuse already stacked container
			err := func() error {
				fieldName, err := GetNameFromLocation(root.From)
				if err != nil {
					return fmt.Errorf("failed to get name from location %s err: %w", root.From, err)
				}
				fieldStatus, err := orders.GetStackingFieldStatus(fieldName)
				if err == nil {
					if fieldStatus.ItemCount > 0 {
						stackedCon := fieldStatus.StackedItems[fieldStatus.ItemCount-1]
						if stackedCon.ContainerType == root.ContainerType {
							newCon.ItemGUID = stackedCon.ItemGUID
							newCon.ContainerIDInfo.ISOID = stackedCon.ISOID
							wi.HandledItem = newCon
							return nil
						}
						return fmt.Errorf("another type of container found at the starting point %s", root.From)
					}
				}
				return fmt.Errorf("invalid transfer out request, fieldName: %s", fieldName)
			}()
			if err != nil {
				return wo, fmt.Errorf("cannot create work order %w", err)
			}
			wi.FinalTransportResource = models.HtOrderRes{
				ResourceType:       "CTR",
				ResourceID:         "CTR" + wi.WorkItemName,
				TrailerInformation: []models.ContainerTrailerInformation{},
				HtType:             "InternalTruck",
			}
			wi.FinalChasisInformation = models.TargetChasisInformation{
				TargetChasis:           0,
				TargetLocationOnChasis: "empty",
			}
			if err != nil {
				return wo, fmt.Errorf("cannot create work order %w", err)
			}
		default:
			return wo, fmt.Errorf("not supported orderType %s", wi.OrderType)
		}

		wo.OperationsWorkOrderItems = append(wo.OperationsWorkOrderItems, wi)
	}
	if len(wo.OperationsWorkOrderItems) > 1 {
		wo.ResourceCardinality = models.MULTIPLE
	} else {
		wo.ResourceCardinality = models.SINGLE
	}
	return wo, nil
}

//checkLocationValid checks if the destination location is valid
func checkLocationValid(root models.WorkItemRoot, orderType models.OrderTypes) error {
	destination := root.To
	tier, err := strconv.Atoi(destination.Tier)

	if err != nil {
		return err
	}

	if tier == 1 {
		return nil
	}

	maxTier := maxSaTier
	if orderType == models.TRANSFER_OUT {
		maxTier = maxTzTier
	}

	if tier > maxTier {
		return fmt.Errorf("tier is too high for this type. Current tier: %d, max allowed tier: %d", tier, maxTier)
	}

	return nil
}
