package ht

import (
	"ecs/api/generated/models"

	"testing"
)

func createHt(t *testing.T) (*Ht, error) {
	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovered in f: %v", r)
		}
	}()

	ht := &Ht{}
	return ht, nil
}

// func TestCreateNewHTNil(t *testing.T) {

// 	ds, err := NewHT(nil, nil)
// 	if err == nil {
// 		t.Errorf("NewHT(nil) = %v, %s", ds, err)
// 	} else {
// 		t.Logf("NewHT(nil) = %v, %s", ds, err)
// 	}
// }

func TestProcessOperationsOrder(t *testing.T) {
	ht, err := createHt(t)
	if err != nil {
		t.Fatalf("cannot create ht %s", err)
	}
	t.Logf("ht created successfully %v", ht)
	st := models.ScheduleStatus{ScheduleState: models.S_IN_PROGRESS}
	err = ht.NotifyScheduleStatus(st)

	if err != nil {
		t.Errorf("NotifyScheduleStatus failed with empty status %s", err)
	} else {
		t.Logf("NotifyScheduleStatus succeed with empty status %s", err)
	}
}

func TestProcessDispatchStatusUpdate(t *testing.T) {
	ht, err := createHt(t)
	if err != nil {
		t.Fatalf("createHt(wrap) = %v, %s", ht, err)
	}
	t.Logf("ht created %v", ht)
	ds := models.DispatchStatus{}
	err = ht.NotifyDispatchStatusUpdate(ds)
	if err != nil {
		t.Errorf("NotifyDispatchStatusUpdate failed with empty status %s", err)
	} else {
		t.Logf("NotifyDispatchStatusUpdate succeed with empty status %s", err)
	}
}
