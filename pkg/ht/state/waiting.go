//Package state maintains the ht state machine
package state

import (
	"ecs/api/generated/models"
	"ecs/pkg/setup/repo/blockcfg"
	"reflect"
	"time"

	log "github.com/sirupsen/logrus"
)

type waiting struct {
	stContext *STM
}

func (state *waiting) enterState() error {
	status := &state.stContext.currentHtStatus
	status.HtState = models.HT_WAITING

	//request tz reservation
	tzr := models.TzReservation{ReserveField: state.stContext.currentHtStatus.DestinationLogicalLocation,
		ResDimensions: state.stContext.currentHTResource.TDimensions,
		HtID:          state.stContext.currentHtStatus.HtID, IsRelease: false}
	defer state.stContext.publishTZReservationRequest(tzr)

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *waiting) exitState() error {
	return nil
}

//HandleContainerStatus handles container status updates
func (state *waiting) HandleContainerStatus(coStat models.ContainerStatus) error {
	return nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *waiting) HandleDispatchStatusUpdate(dst models.DispatchStatus) error {
	return nil
}

// HandleTransferFieldStatus handles transfer field status updates
func (state *waiting) HandleTransferFieldStatus(trStat models.TransferFieldStatus) error {
	//check if the location matches.
	dest := state.stContext.currentHtStatus.DestinationLogicalLocation
	cn, _ := blockcfg.GetCompositeReservationName(dest.AreaName, dest.Bay, dest.Row, dest.Tier)
	if trStat.FieldNameComposite == cn && trStat.ReservedHT == state.stContext.currentHtStatus.HtID {
		//stm reserved the tz field.
		log.Debugf("tz reserved %s for stm %s", cn, trStat.ReservedHT)
		defer state.stContext.GoToNextState(models.HT_ENTERING)
		return nil
	}

	//resend even in case of status updates from other blocks
	log.Debugf("new status received for tz %s. Initiate reservation again", trStat.FieldNameComposite)
	tzr := models.TzReservation{ReserveField: state.stContext.currentHtStatus.DestinationLogicalLocation,
		ResDimensions: state.stContext.currentHTResource.TDimensions,
		HtID:          state.stContext.currentHtStatus.HtID, IsRelease: false}
	defer state.stContext.publishTZReservationRequest(tzr)

	return nil
}

//HandleTick handles generated ticks according to HT configuration
func (state *waiting) HandleTick(timePassed time.Duration) error {
	passedSinceTransition := time.Now().Sub(state.stContext.transitionTime)

	timeout := state.stContext.htCfg.MaxWaitOnReservation * state.stContext.htCfg.StatusInterval

	if passedSinceTransition.Milliseconds() >= int64(timeout) {
		log.Errorln("timeout in state", reflect.TypeOf(state).String())
		defer state.stContext.GoToNextState(models.HT_FAILED)
	}
	return nil
}
