//Package state maintains the ht state machine
package state

import (
	"ecs/api/generated/models"
	"time"
)

type leaving struct {
	stContext *STM
}

func (state *leaving) enterState() error {
	status := &state.stContext.currentHtStatus
	status.HtState = models.HT_LEAVING

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *leaving) exitState() error {

	//release reserved TZ
	tzr := models.TzReservation{ReserveField: state.stContext.currentHtStatus.DestinationLogicalLocation,
		HtID: state.stContext.currentHtStatus.HtID, IsRelease: true}
	defer state.stContext.publishTZReservationRequest(tzr)

	//publish container info if loaded container leaves the TZ
	if state.stContext.handledContainerStatus.ContainerPlaced == false {
		defer state.stContext.publishContainerStatus(state.stContext.handledContainerStatus)
	}
	return nil
}

//HandleContainerStatus handles container status updates
func (state *leaving) HandleContainerStatus(coStat models.ContainerStatus) error {
	return nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *leaving) HandleDispatchStatusUpdate(dst models.DispatchStatus) error {
	return nil
}

// HandleTransferFieldStatus handles transfer field status updates
func (state *leaving) HandleTransferFieldStatus(trStat models.TransferFieldStatus) error {
	return nil
}

//HandleTick handles generated ticks according to HT configuration
func (state *leaving) HandleTick(timePassed time.Duration) error {
	passedSinceTransition := time.Now().Sub(state.stContext.transitionTime)

	timeout := state.stContext.htCfg.LeavingTzTime * state.stContext.htCfg.StatusInterval

	if passedSinceTransition.Milliseconds() >= int64(timeout) {
		defer state.stContext.GoToNextState(models.HT_LEFT)
	}
	return nil
}
