//Package state maintains the ht state machine
package state

import (
	"ecs/api/generated/models"
	"ecs/internal/rabbitnamings"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	log "github.com/sirupsen/logrus"
)

//htState is the interface of an ht state
type htState interface {
	enterState() error
	exitState() error

	HandleContainerStatus(models.ContainerStatus) error
	HandleDispatchStatusUpdate(models.DispatchStatus) error
	HandleTransferFieldStatus(models.TransferFieldStatus) error
	HandleTick(time.Duration) error
}

//STM is the LSP complient sub-state of HSM
type STM struct {
	currentHTResource models.CTruck //truck config

	//current state manipulates current status
	currentHtStatus        models.HtStatus
	handledContainerStatus models.ContainerStatus

	currentState htState

	//state transition time
	transitionTime time.Time

	/*states*/
	arrivingState htState
	waitingState  htState
	enteringState htState
	arrivedState  htState
	leavingState  htState
	leftState     htState
	failedState   htState

	amqpWrap *amqpwrap.AMQPWrapper

	blocks []models.ContainerBlockComposite
	htCfg  models.Ht
	stmID  string //internally used only. unique id of the stm
}

//NewSTM returns a new state machine
func NewSTM(stmID string, htCfg models.Ht, st models.HtStatus, cs models.ContainerStatus, ctr models.CTruck,
	blocks []models.ContainerBlockComposite, amqpWrap *amqpwrap.AMQPWrapper) (*STM, error) {
	stm := &(STM{stmID: stmID, htCfg: htCfg, currentHtStatus: st, handledContainerStatus: cs,
		currentHTResource: ctr, blocks: blocks, amqpWrap: amqpWrap})

	//init state contexts
	stm.arrivingState = &(arriving{stContext: stm})
	stm.waitingState = &(waiting{stContext: stm})
	stm.enteringState = &(entering{stContext: stm})
	stm.arrivedState = &(arrived{stContext: stm})
	stm.leavingState = &(leaving{stContext: stm})
	stm.leftState = &(left{stContext: stm})
	stm.failedState = &(failed{stContext: stm})

	sta, err := stm.getState(stm.currentHtStatus.HtState)
	if err != nil {
		log.WithError(err).Fatalln("cannot create stm", stm)
	}
	stm.setState(sta)

	log.Debugf("created ht stm %s in %s state for status %s", stmID,
		reflect.TypeOf(stm.currentState).String(), stm.currentHtStatus.HtState)

	//trigger
	defer stm.GoToNextState(stm.currentHtStatus.HtState)

	return stm, nil
}

//DeInit deinitializes the stm
func (stm *STM) DeInit() {
	return
}

func (stm *STM) getState(sta models.HtState) (htState, error) {
	switch sta {
	case models.HT_ARRIVING:
		return stm.arrivingState, nil
	case models.HT_WAITING:
		return stm.waitingState, nil
	case models.HT_ENTERING:
		return stm.enteringState, nil
	case models.HT_ARRIVED:
		return stm.arrivedState, nil
	case models.HT_LEAVING:
		return stm.leavingState, nil
	case models.HT_LEFT:
		return stm.leftState, nil
	case models.HT_FAILED:
		return stm.failedState, nil
	default:
		return nil, fmt.Errorf("unsupported state %s", sta)
	}
}

func (stm *STM) setState(newstate htState) {
	stm.currentState = newstate
}

//GetHTStatus returns current ht status
func (stm *STM) GetHTStatus() models.HtStatus {
	return stm.currentHtStatus
}

//GoToNextState initiates state transition
func (stm *STM) GoToNextState(sta models.HtState) error {

	nextState, err := stm.getState(sta)
	if err != nil {
		log.WithError(err).Errorln("getState failed:", sta)
		return err
	}

	log.Tracef("exiting state %s. Going to next state %s", reflect.TypeOf(stm.currentState).String(), reflect.TypeOf(nextState).String())
	err = stm.currentState.exitState()
	if err != nil {
		log.WithError(err).Errorln("cannot exit state:", stm.currentState)
		return err
	}
	stm.currentState = nextState
	err = stm.currentState.enterState()
	if err != nil {
		log.WithError(err).Errorln("cannot enter state:", stm.currentState)
		return err
	}
	stm.transitionTime = time.Now()
	return nil
}

//publishTZReservationRequest publishes TzReservation
func (stm *STM) publishTZReservationRequest(tzR models.TzReservation) error {
	log.Debugln("publishTZReservationRequest", tzR)
	blockID, err := stm.getBlockID(tzR.ReserveField.AreaName)
	if err != nil {
		return err
	}
	bytes, err := json.Marshal(tzR)
	if err != nil {
		return fmt.Errorf("marshalling failed for container status :%v", tzR)
	}
	//publish the container status : operations/{blockid}/tzreservationrequest
	err = stm.amqpWrap.PublishTo(rabbitnamings.L3RouteBase,
		rabbitnamings.L3RouteBase+"."+blockID+"."+rabbitnamings.TZReservationRequestRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing TZ reservation failed: %w", err)
	}
	return err
}

//publishHtStatus publishes HtState
func (stm *STM) publishHtStatus(trStatus models.HtStatus) error {
	log.Debugln("publishHtStatus", trStatus)

	bytes, err := json.Marshal(trStatus)
	if err != nil {
		return fmt.Errorf("marshalling failed for HtStatus :%v", trStatus)
	}
	//publish truckStatus : ht/{HtID}/truckstatus
	err = stm.amqpWrap.PublishTo(rabbitnamings.HtRouteBase,
		rabbitnamings.HtRouteBase+"."+trStatus.HtID+"."+rabbitnamings.TruckStatusRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing truckStatus failed: %w", err)
	}
	return err
}

//publishContainerStatus publishes container status
func (stm *STM) publishContainerStatus(coStat models.ContainerStatus) error {
	log.Debugln("publishContainerStatus", coStat)
	blockID, err := stm.getBlockID(coStat.ContainerLogicalLocation.AreaName)
	if err != nil {
		return err
	}
	bytes, err := json.Marshal(coStat)
	if err != nil {
		return fmt.Errorf("marshalling failed for container status :%v", coStat)
	}
	//publish the container status : operations/{blockid}/containerstatus
	err = stm.amqpWrap.PublishTo(rabbitnamings.L3RouteBase,
		rabbitnamings.L3RouteBase+"."+blockID+"."+rabbitnamings.ContainerStatusRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing container status failed: %w", err)
	}
	return err
}

//saveAndPublishState saves status in repo and publishes status
func (stm *STM) saveAndPublishState() error {
	// _, err := stm.disRepo.PutHtStatusByNr(stm.currentHtStatus.HtNr, stm.currentHtStatus)
	// if err != nil {
	// 	return fmt.Errorf("PutHtStatusByNr failed: %w", err)
	// }
	err := stm.publishHtStatus(stm.currentHtStatus)
	if err != nil {
		return fmt.Errorf("publishHtStatus failed: %w", err)
	}
	return nil
}

//isTransferField returns if given fieldname is a transfer zone name
func (stm *STM) isTransferField(fieldName string) bool {
	for _, bl := range stm.blocks {
		for _, tzName := range bl.TransferZoneNames {
			if tzName == fieldName {
				return true
			}
		}
	}
	return false
}

//getBlockID returns block id of the given field name
func (stm *STM) getBlockID(fieldName string) (string, error) {
	for _, bl := range stm.blocks {
		for _, tzName := range bl.TransferZoneNames {
			if tzName == fieldName {
				return bl.AreaName, nil
			}
		}
		for _, saName := range bl.StackingAreaNames {
			if saName == fieldName {
				return bl.AreaName, nil
			}
		}
	}
	return "", fmt.Errorf("invalid field name %s", fieldName)
}

//IsFinished returns if HT STM is finished its lifecycle
func (stm *STM) IsFinished() bool {
	return stm.currentHtStatus.HtState == models.HT_LEFT || stm.currentHtStatus.HtState == models.HT_FAILED
}

/**************************Event Handlers**************************/

//HandleContainerStatus handles container status updates
func (stm *STM) HandleContainerStatus(coStat models.ContainerStatus) error {
	return stm.currentState.HandleContainerStatus(coStat)
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (stm *STM) HandleDispatchStatusUpdate(dst models.DispatchStatus) error {
	return stm.currentState.HandleDispatchStatusUpdate(dst)
}

// HandleTransferFieldStatus handles transfer field status updates
func (stm *STM) HandleTransferFieldStatus(trStat models.TransferFieldStatus) error {
	return stm.currentState.HandleTransferFieldStatus(trStat)
}

//HandleTick handles generated ticks according to HT configuration
func (stm *STM) HandleTick(timePassed time.Duration) error {
	return stm.currentState.HandleTick(timePassed)
}
