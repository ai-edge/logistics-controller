//Package state maintains the ht state machine
package state

import (
	"ecs/api/generated/models"
	"time"
)

type left struct {
	stContext *STM
}

func (state *left) enterState() error {
	status := &state.stContext.currentHtStatus
	status.HtState = models.HT_LEFT

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *left) exitState() error {
	return nil
}

//HandleContainerStatus handles container status updates
func (state *left) HandleContainerStatus(coStat models.ContainerStatus) error {
	return nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *left) HandleDispatchStatusUpdate(dst models.DispatchStatus) error {
	return nil
}

// HandleTransferFieldStatus handles transfer field status updates
func (state *left) HandleTransferFieldStatus(trStat models.TransferFieldStatus) error {
	return nil
}

//HandleTick handles generated ticks according to HT configuration
func (state *left) HandleTick(timePassed time.Duration) error {
	return nil
}
