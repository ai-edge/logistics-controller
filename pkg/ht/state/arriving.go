//Package state maintains the ht state machine
package state

import (
	"ecs/api/generated/models"
	"time"
)

type arriving struct {
	stContext *STM
}

func (state *arriving) enterState() error {
	status := &state.stContext.currentHtStatus
	status.HtState = models.HT_ARRIVING

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *arriving) exitState() error {
	return nil
}

//HandleContainerStatus handles container status updates
func (state *arriving) HandleContainerStatus(coStat models.ContainerStatus) error {
	return nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *arriving) HandleDispatchStatusUpdate(dst models.DispatchStatus) error {
	return nil
}

// HandleTransferFieldStatus handles transfer field status updates
func (state *arriving) HandleTransferFieldStatus(trStat models.TransferFieldStatus) error {
	return nil
}

//HandleTick handles generated ticks according to HT configuration
func (state *arriving) HandleTick(timePassed time.Duration) error {

	passedSinceTransition := time.Now().Sub(state.stContext.transitionTime)

	timeout := state.stContext.htCfg.ArrivingTzTime * state.stContext.htCfg.StatusInterval

	if passedSinceTransition.Milliseconds() >= int64(timeout) {
		defer state.stContext.GoToNextState(models.HT_WAITING)
	}

	return nil
}
