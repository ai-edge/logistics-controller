//Package state maintains the ht state machine
package state

import (
	"ecs/api/generated/models"
	"time"
)

type entering struct {
	stContext *STM
}

func (state *entering) enterState() error {
	status := &state.stContext.currentHtStatus
	status.HtState = models.HT_ENTERING

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *entering) exitState() error {
	return nil
}

//HandleContainerStatus handles container status updates
func (state *entering) HandleContainerStatus(coStat models.ContainerStatus) error {
	return nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *entering) HandleDispatchStatusUpdate(dst models.DispatchStatus) error {
	return nil
}

// HandleTransferFieldStatus handles transfer field status updates
func (state *entering) HandleTransferFieldStatus(trStat models.TransferFieldStatus) error {
	return nil
}

//HandleTick handles generated ticks according to HT configuration
func (state *entering) HandleTick(timePassed time.Duration) error {
	passedSinceTransition := time.Now().Sub(state.stContext.transitionTime)

	timeout := state.stContext.htCfg.EnteringTzTime * state.stContext.htCfg.StatusInterval

	if passedSinceTransition.Milliseconds() >= int64(timeout) {
		defer state.stContext.GoToNextState(models.HT_ARRIVED)
	}
	return nil
}
