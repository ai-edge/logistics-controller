//Package state maintains the ht state machine
package state

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"
	"time"

	log "github.com/sirupsen/logrus"
)

type arrived struct {
	stContext *STM
}

func (state *arrived) enterState() error {
	status := &state.stContext.currentHtStatus
	status.HtState = models.HT_ARRIVED

	//publish container info if discharge is required
	if state.stContext.handledContainerStatus.ContainerPlaced == true {
		defer state.stContext.publishContainerStatus(state.stContext.handledContainerStatus)
	}

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *arrived) exitState() error {
	return nil
}

//HandleContainerStatus handles container status updates
func (state *arrived) HandleContainerStatus(coStat models.ContainerStatus) error {
	htCo := state.stContext.handledContainerStatus
	//check if ht if served. Served means ht notified a placed container and got unloaded, or ht is loaded.
	if coStat.ContainerIDInfo == htCo.ContainerIDInfo {
		if coStat.ContainerLogicalLocation != htCo.ContainerLogicalLocation {
			return fmt.Errorf("logical location %s mismatch with container %s %s isplace %t. Container not in place yet?",
				coStat.ContainerLogicalLocation, htCo.ContainerIDInfo.ISOID,
				htCo.ContainerLogicalLocation, coStat.ContainerPlaced)
		}
		if htCo.ContainerPlaced != coStat.ContainerPlaced {
			//container handled (pick for place or vice versa). HT can leave.
			log.Debugf("container %s was handled by stm %s", htCo.ContainerIDInfo.ISOID, state.stContext.currentHtStatus.HtID)
			defer state.stContext.GoToNextState(models.HT_LEAVING)
		}
	}
	return nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *arrived) HandleDispatchStatusUpdate(dst models.DispatchStatus) error {
	return nil
}

// HandleTransferFieldStatus handles transfer field status updates
func (state *arrived) HandleTransferFieldStatus(trStat models.TransferFieldStatus) error {
	return nil
}

//HandleTick handles generated ticks according to HT configuration
func (state *arrived) HandleTick(timePassed time.Duration) error {
	passedSinceTransition := time.Now().Sub(state.stContext.transitionTime)

	timeout := state.stContext.htCfg.MaxWaitOnService * state.stContext.htCfg.StatusInterval

	if passedSinceTransition.Milliseconds() >= int64(timeout) {
		log.Errorln("timeout in state", reflect.TypeOf(state).String())
		defer state.stContext.GoToNextState(models.HT_FAILED)
	}
	return nil
}
