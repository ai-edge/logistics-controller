//Package state maintains the ht state machine
package state

import (
	"ecs/api/generated/models"
	"time"
)

type failed struct {
	stContext *STM
}

func (state *failed) enterState() error {
	status := &state.stContext.currentHtStatus
	status.HtState = models.HT_FAILED

	//release tz no matter if it's reserved
	tzr := models.TzReservation{ReserveField: state.stContext.currentHtStatus.DestinationLogicalLocation,
		HtID: state.stContext.currentHtStatus.HtID, IsRelease: true}
	defer state.stContext.publishTZReservationRequest(tzr)

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *failed) exitState() error {
	return nil
}

//HandleContainerStatus handles container status updates
func (state *failed) HandleContainerStatus(coStat models.ContainerStatus) error {
	return nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *failed) HandleDispatchStatusUpdate(dst models.DispatchStatus) error {
	return nil
}

// HandleTransferFieldStatus handles transfer field status updates
func (state *failed) HandleTransferFieldStatus(trStat models.TransferFieldStatus) error {
	return nil
}

//HandleTick handles generated ticks according to HT configuration
func (state *failed) HandleTick(timePassed time.Duration) error {
	return nil
}
