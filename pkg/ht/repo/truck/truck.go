package truck

import (
	"ecs/api/generated/models"
	"ecs/internal/dbservice"
	"ecs/pkg/setup/repo/blockcfg"
	"fmt"
)

//Truck repository for HT
type Truck struct {
	//ServiceID is the id of the service created the repository
	ServiceID string
	dbNames   truckDBNames

	dbs dbservice.DBService
}

type truckDBNames struct {
	//DB names
	truckDB string
	//Collection Names
	truckStatusCollectionName string
	//queryLimit maximum items to be returned
	queryLimit int64
	//IDField defines the name of the ID field in the DB
	IDField string
}

//NewTruckRepository returns a new repository of a HT
func NewTruckRepository(dbs dbservice.DBService, serviceID string) (*Truck, error) {

	if dbs == nil || serviceID == "" {
		return nil, fmt.Errorf("invalid arguments dbs % serviceID %s", dbs, serviceID)
	}
	var dbNames truckDBNames

	//MovesBL01DB is the name of the block DB
	dbNames.truckDB = "Truck"

	dbNames.truckStatusCollectionName = "truckstatus"
	dbNames.queryLimit = 1000
	dbNames.IDField = "_id"

	truck := Truck{dbs: dbs, ServiceID: serviceID, dbNames: dbNames}

	return &truck, nil
}

//Init initializes entities
func (htRepo *Truck) Init(blCfg *blockcfg.BlockCfg) error {

	//TODO: update fields according to items
	return nil
}

//DeInit closes connection
func (htRepo *Truck) DeInit() {
	htRepo.dbs.DeInit()
}

//GetQueryLimit returns query limit parameter
func (htRepo *Truck) GetQueryLimit() int64 {
	return htRepo.dbNames.queryLimit
}

// GetTruckStatusDestStr gets a truck status
func (htRepo *Truck) GetTruckStatusDestStr(destLoc models.LogicalLocation3D) (models.HtStatus, error) {
	var trStat models.HtStatus
	query := map[string]interface{}{"destinationlogicallocation": destLoc}
	err := htRepo.dbs.GetOne(htRepo.dbNames.truckDB, htRepo.dbNames.truckStatusCollectionName, query, &trStat)
	return trStat, err
}

//PutTruckStatusDestStr puts a truck status
func (htRepo *Truck) PutTruckStatusDestStr(destLoc models.LogicalLocation3D, newTrStat models.HtStatus) (models.HtStatus, error) {
	var trStat models.HtStatus
	query := map[string]interface{}{"destinationlogicallocation": destLoc}
	err := htRepo.dbs.GetOne(htRepo.dbNames.truckDB, htRepo.dbNames.truckStatusCollectionName, query, &trStat)
	if err != nil {
		err = htRepo.dbs.InsertOne(htRepo.dbNames.truckDB, htRepo.dbNames.truckStatusCollectionName, &newTrStat)
	} else {
		err = htRepo.dbs.ReplaceOne(htRepo.dbNames.truckDB, htRepo.dbNames.truckStatusCollectionName, query, &newTrStat)
	}
	return newTrStat, err
}

// DeleteTruckStatusDestStr deletes a truck status
func (htRepo *Truck) DeleteTruckStatusDestStr(destLoc models.LogicalLocation3D) error {
	query := map[string]interface{}{"destinationlogicallocation": destLoc}
	err := htRepo.dbs.DeleteOne(htRepo.dbNames.truckDB, htRepo.dbNames.truckStatusCollectionName, query)
	return err
}
