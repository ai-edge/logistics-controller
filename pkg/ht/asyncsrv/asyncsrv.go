//Package asyncsrv consumes incoming async messages
package asyncsrv

import (
	"ecs/api/generated/models"
	"ecs/internal/rabbitnamings"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"fmt"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type htService interface {
	NotifyScheduleStatus(models.ScheduleStatus) error
	NotifyDispatchStatusUpdate(models.DispatchStatus) error
	NotifyContainerStatus(models.ContainerStatus) error
	NotifyTransferFieldStatus(models.TransferFieldStatus) error
	HandleTick(time.Duration) error
}

//Asyncsrv is the async service
type Asyncsrv struct {
	ScheStCh           <-chan amqp.Delivery
	L3DispatchStatusCh <-chan amqp.Delivery
	ContainerStatusCh  <-chan amqp.Delivery
	TFStatusCh         <-chan amqp.Delivery
	newIntervalCh      chan time.Duration
	stopCh             chan bool

	connClosedCh <-chan *amqp.Error
	subCloseCh   <-chan *amqp.Error
	subCancelCh  <-chan string

	amqpWrap *amqpwrap.AMQPWrapper

	ticker         *time.Ticker //Ticker
	statusInterval time.Duration
	LastUpdate     time.Time

	ht htService
}

//NewAsyncsrv returns a new async service. Creates exchanges only for consumed messages.
func NewAsyncsrv(ht htService, amqpWrap *amqpwrap.AMQPWrapper, statusInterval int32) (*Asyncsrv, error) {
	asyncsrv := Asyncsrv{ht: ht, amqpWrap: amqpWrap}
	var err error

	asyncsrv.newIntervalCh = make(chan time.Duration)
	asyncsrv.stopCh = make(chan bool)

	//subscribe to scheduler/*/schedulestatus
	route := rabbitnamings.ScheRouteBase + ".*." + rabbitnamings.ScheduleStatusRouteKey
	asyncsrv.ScheStCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.ScheduleStatusRouteKey, //queue name contains route key
		rabbitnamings.ScheRouteBase,                                  //exchange name
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to dispatcher/*/dispatchstatus
	route = rabbitnamings.DispRouteBase + ".*." + rabbitnamings.DispatchStatusRouteKey
	asyncsrv.L3DispatchStatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.DispatchStatusRouteKey, //queue name contains route key
		rabbitnamings.DispRouteBase,                                  //exchange name
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to operations/#/containerstatus
	route = rabbitnamings.L3RouteBase + "." + "*" + "." + rabbitnamings.ContainerStatusRouteKey
	asyncsrv.ContainerStatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.ContainerStatusRouteKey, //queue name contains route key
		rabbitnamings.L3RouteBase, //exchange name
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to operations/*/transferFieldstatus
	route = rabbitnamings.L3RouteBase + "." + "*" + "." + rabbitnamings.TrFieldStatusRouteKey
	asyncsrv.TFStatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.TrFieldStatusRouteKey, //queue name contains route key
		rabbitnamings.L3RouteBase,                                   //exchange name
		route)

	if err != nil {
		return nil, err
	}

	asyncsrv.connClosedCh = amqpWrap.GetConnCloseChan()
	asyncsrv.subCloseCh = amqpWrap.GetSubCloseChan()
	asyncsrv.subCancelCh = amqpWrap.GetSubCancelChan()

	/***********Ticker************/
	asyncsrv.LastUpdate = time.Now()
	asyncsrv.statusInterval = time.Duration(statusInterval) * time.Millisecond
	err = asyncsrv.startTimer()
	if err != nil {
		return nil, fmt.Errorf("start timer failed %w", err)
	}

	return &asyncsrv, nil
}

//DeInit closes connection
func (as *Asyncsrv) DeInit() {
	as.stopCh <- true
}

//GetInterval gets status cycle interval time
func (as *Asyncsrv) GetInterval() (time.Duration, error) {
	return as.statusInterval, nil
}

//SetInterval sets status cycle interval time
func (as *Asyncsrv) SetInterval(interval time.Duration) error {
	if interval.Nanoseconds() <= 0 {
		return fmt.Errorf("invalid interval %d", interval.Nanoseconds())
	}
	if as.statusInterval == interval {
		return nil
	}
	as.newIntervalCh <- interval
	return nil
}

func (as *Asyncsrv) startTimer() error {
	if as.statusInterval.Nanoseconds() <= 0 {
		return fmt.Errorf("cannot start the ticker for sim: %v", as)
	}
	log.Debugln("starting ticker with IntervalDuration:", as.statusInterval)
	as.ticker = time.NewTicker(as.statusInterval)
	return nil
}

func (as *Asyncsrv) stopTimer() error {
	as.ticker.Stop()
	log.Debugln("ticker STOPPED with interval", as.statusInterval)
	return nil
}

//Asyncsrv is the main loop for consuming incoming async messages
func (as *Asyncsrv) Asyncsrv() error {

	for {
		select {
		//new work order request event
		case delivery := <-as.ScheStCh:
			st := models.ScheduleStatus{}
			err := json.Unmarshal(delivery.Body, &st)
			if err != nil {
				log.WithError(err).Errorln("unmarshal schedule status failed for delivery", delivery)
			} else {
				log.Traceln("received schedule status:", st)
				err = as.ht.NotifyScheduleStatus(st)
				if err != nil {
					log.WithError(err).Error("error in NotifyScheduleStatus")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//dispatch status event
		case delivery := <-as.L3DispatchStatusCh:
			dstatus := models.DispatchStatus{}
			err := json.Unmarshal(delivery.Body, &dstatus)
			if err != nil {
				log.WithError(err).Errorln("unmarshal dispatch status failed for delivery", delivery)
			} else {
				log.Debugln("receiveddispatch status", dstatus.DispatchName, dstatus.StackingCraneID, dstatus.DispatchState)
				log.Traceln("dispatch status", dstatus)
				err = as.ht.NotifyDispatchStatusUpdate(dstatus)
				if err != nil {
					log.WithError(err).Error("error in NotifyDispatchStatusUpdate")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//container status event
		case delivery := <-as.ContainerStatusCh:
			coStat := models.ContainerStatus{}
			err := json.Unmarshal(delivery.Body, &coStat)
			if err != nil {
				log.WithError(err).Errorln("unmarshal ContainerStatus failed for delivery", delivery)
			} else {
				log.Debugln("received containerStatus:", coStat)
				err = as.ht.NotifyContainerStatus(coStat)
				if err != nil {
					log.WithError(err).Error("error in ContainerStatus")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//tz status event
		case delivery := <-as.TFStatusCh:
			tfStat := models.TransferFieldStatus{}
			err := json.Unmarshal(delivery.Body, &tfStat)
			if err != nil {
				log.WithError(err).Errorln("unmarshal transfer field status failed for delivery", delivery)
			} else {
				log.Debugln("received Transfer Field Status:", tfStat)
				err = as.ht.NotifyTransferFieldStatus(tfStat)
				if err != nil {
					log.WithError(err).Error("NotifyTransferFieldStatus failed")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		case t := <-as.ticker.C: //Ticker Handler
			log.Traceln("tick at ", t)
			timePassed := time.Now().Sub(as.LastUpdate)
			as.LastUpdate = time.Now()
			//log.Println("ms passed since last simulation step: ", timePassed)
			err := as.ht.HandleTick(timePassed)
			if err != nil {
				log.WithError(err).Error("HandleTick failed")
			}

		case newInt := <-as.newIntervalCh: //new time interval for status updates
			err := as.stopTimer()
			if err != nil {
				log.WithError(err).Errorln("stopTimer failed, interval", as.statusInterval)
			}
			as.statusInterval = newInt
			err = as.startTimer() //creates new ticker channel inside this loop
			if err != nil {
				log.WithError(err).Errorln("startTimer failed, interval", as.statusInterval)
			}
			log.Warningln("new interval set", as.statusInterval)

		case <-as.stopCh:
			log.Warningln("received stop")
			return nil

		case err := <-as.connClosedCh:
			if err != nil {
				log.WithError(err).Errorf("connection error. Shutting down.")
			} else {
				log.Warningf("connection closed. Shutting down")
			}
			panic(fmt.Errorf("connection closed"))

		case err := <-as.subCloseCh:
			log.WithError(err).Errorf("channel error. Shutting down")
			panic(fmt.Errorf("channel closed"))

		case msg := <-as.subCancelCh:
			log.Errorf("channel canceled. Shutting down %s", msg)
			panic(fmt.Errorf("channel canceled"))

		}
	}
}
