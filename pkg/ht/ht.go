//Package ht horizontal transfer
package ht

import (
	"ecs/api/generated/models"
	"ecs/pkg/ht/asyncsrv"
	"ecs/pkg/ht/state"
	"ecs/pkg/setup/repo/terminal"
	"ecs/tools/amqpwrap"
	"fmt"
	"time"

	"github.com/jinzhu/copier"
	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

//Ht is the horizontal transfer service for orders
type Ht struct {
	As       *asyncsrv.Asyncsrv
	amqpWrap *amqpwrap.AMQPWrapper

	serviceID string

	truckSTMs map[string]*state.STM

	tr *terminal.Terminal
}

//NewHT returns a new Ht
func NewHT(serviceID string, tr *terminal.Terminal, conn *amqp.Connection) (*Ht, error) {
	ht := &Ht{serviceID: serviceID, tr: tr}
	htCfg, err := tr.GetHtService(serviceID)
	if err != nil {
		log.WithError(err).Fatal("cannot get cfg")
	}

	ht.truckSTMs = make(map[string]*state.STM)

	//init AMQP wrapper
	wrap, err := amqpwrap.NewAMQPWrapper(conn)
	if err != nil {
		return nil, fmt.Errorf("Could not create amqp wrapper: %w", err)
	}
	//different channels for publish and subscribe
	err = wrap.InitPubChannel()
	if err != nil {
		return nil, fmt.Errorf("Init channel failed %w", err)
	}
	err = wrap.InitSubChannel()
	if err != nil {
		return nil, fmt.Errorf("Init channel failed %w", err)
	}

	ht.amqpWrap = wrap

	//Init consuming async messages
	as, err := asyncsrv.NewAsyncsrv(ht, wrap, htCfg.StatusInterval)
	if err != nil {
		return nil, err
	}
	ht.As = as

	go as.Asyncsrv()

	return ht, nil
}

//DeInit close ht
func (ht *Ht) DeInit() {
	ht.As.DeInit()
	ht.amqpWrap.DeInit()
}

//addTruckSTM adds a new ht state machine to be executed
func (ht *Ht) addTruckSTM(wi models.OperationsWorkOrderItemBase) error {
	var err error
	var loc models.LogicalLocation3D
	var htRes models.CTruck
	var isDischarge bool

	//TODO: read it from resources database
	htRes.TDimensions = models.Dimensions3D{Width: 12000, Length: 2300, Depth: 2000}

	switch wi.OrderType {
	case models.TRANSFER_IN:
		loc = wi.StartingPointLogicalLocation
		err := copier.Copy(&htRes, &wi.StartingTransportResource)
		if err != nil {
			return err
		}
		isDischarge = true
	case models.BASIC_PICKUP:
		loc = wi.FinalPointLogicalLocation
		err := copier.Copy(&htRes, &wi.FinalTransportResource)
		if err != nil {
			return err
		}
		isDischarge = true
	case models.TRANSFER_OUT, models.BASIC_GROUNDING:
		loc = wi.FinalPointLogicalLocation
		err := copier.Copy(&htRes, &wi.FinalTransportResource)
		if err != nil {
			return err
		}
		isDischarge = false
	case models.PREPOSITION:
		return nil
	default:
		return fmt.Errorf("not supported order type %s", wi.OrderType)
	}

	if htRes.ResourceID == "" || htRes.ResourceType == "" {
		return fmt.Errorf("invalid ht fields of wi %s", wi.WorkItemName)
	}

	stmID, err := getSTMID(loc.AreaName, htRes.ResourceID)
	if err != nil {
		return fmt.Errorf("invalid stm id of wi %s %w", wi.WorkItemName, err)
	}
	_, ok := ht.truckSTMs[stmID]
	if ok {
		return fmt.Errorf("same resource id and area received %s", stmID)
	}

	htStat := models.HtStatus{
		ResourceState:              models.R_MOVING,
		HtID:                       htRes.ResourceID,
		HtState:                    models.HT_ARRIVING,
		DestinationLogicalLocation: loc,
	}
	var coStat = models.ContainerStatus{ContainerIDInfo: wi.HandledItem.ContainerIDInfo,
		ContainerPlaced: isDischarge, ContainerLogicalLocation: loc}

	//get blocks from terminal repo
	blocks, err := ht.tr.GetAllBlocks(ht.tr.GetQueryLimit())
	if err != nil {
		log.WithError(err).Fatal("cannot get blocks")
	}
	htCfg, err := ht.tr.GetHtService(ht.serviceID)
	if err != nil {
		log.WithError(err).Fatal("cannot get cfg")
	}
	//init HT STM
	ht.truckSTMs[stmID], err = state.NewSTM(stmID, htCfg, htStat, coStat, htRes, blocks, ht.amqpWrap)

	return err
}

//abortTruckSTM aborts a ht state machine
func (ht *Ht) abortTruckSTM(wi models.OperationsWorkOrderItemBase) error {
	var stmID string
	var err error
	switch wi.OrderType {
	case models.TRANSFER_IN:
		stmID, err = getSTMID(wi.StartingPointLogicalLocation.AreaName, wi.StartingTransportResource.ResourceID)
	case models.BASIC_PICKUP:
		stmID, err = getSTMID(wi.FinalPointLogicalLocation.AreaName, wi.FinalTransportResource.ResourceID)
	case models.TRANSFER_OUT, models.BASIC_GROUNDING:
		stmID, err = getSTMID(wi.FinalPointLogicalLocation.AreaName, wi.FinalTransportResource.ResourceID)
	case models.PREPOSITION:
		return nil
	default:
		return fmt.Errorf("not supported order type %s", wi.OrderType)
	}

	if err != nil {
		return fmt.Errorf("invalid stm id of wi %s %w", wi.WorkItemName, err)
	}

	stm, ok := ht.truckSTMs[stmID]
	if !ok {
		return fmt.Errorf("no STM found %s", stmID)
	}
	log.Debugf("abort truck stm %s", stmID)
	stm.GoToNextState(models.HT_FAILED)
	return nil
}

//handleFinishedSTMs removes finished HT STMs
func (ht *Ht) handleFinishedSTMs() error {
	for id, stm := range ht.truckSTMs {
		if stm.IsFinished() {
			log.Debugf("removing HT stm %s .Nr. of stms %d", id, len(ht.truckSTMs))
			delete(ht.truckSTMs, id)
		}
	}
	return nil
}

/****************************************Command and Event Handlers******************************************/

//NotifyContainerStatus handles a newly received container status
func (ht *Ht) NotifyContainerStatus(coStat models.ContainerStatus) error {
	defer ht.handleFinishedSTMs()

	for id, stm := range ht.truckSTMs {
		err := stm.HandleContainerStatus(coStat)
		if err != nil {
			log.WithError(err).Errorln("HandleContainerStatus failed in stm", id)
		}
	}
	return nil
}

//NotifyDispatchStatusUpdate processes a dispatch status update
func (ht *Ht) NotifyDispatchStatusUpdate(ds models.DispatchStatus) error {
	defer ht.handleFinishedSTMs()

	for id, stm := range ht.truckSTMs {
		err := stm.HandleDispatchStatusUpdate(ds)
		if err != nil {
			log.WithError(err).Errorln("HandleDispatchStatusUpdate failed in stm", id)
		}
	}
	return nil
}

//NotifyScheduleStatus adds truck STMs according to schedule status
func (ht *Ht) NotifyScheduleStatus(st models.ScheduleStatus) error {
	defer ht.handleFinishedSTMs()

	if st.ScheduleReqType == models.NEW_SCHE {
		if st.ScheduleState == models.S_IN_PROGRESS {
			return ht.addTruckSTM(st.WorkItem)
		}
		if st.ScheduleState == models.S_ABORTED || st.ScheduleState == models.S_CANCELED {
			return ht.abortTruckSTM(st.WorkItem)
		}
	}
	return nil

}

// NotifyTransferFieldStatus handles transfer field status updates
func (ht *Ht) NotifyTransferFieldStatus(trStat models.TransferFieldStatus) error {
	defer ht.handleFinishedSTMs()

	for id, stm := range ht.truckSTMs {
		err := stm.HandleTransferFieldStatus(trStat)
		if err != nil {
			log.WithError(err).Errorln("HandleTransferFieldStatus failed in stm", id)
		}
	}
	return nil
}

//HandleTick handles generated ticks according to HT configuration
func (ht *Ht) HandleTick(timePassed time.Duration) error {
	defer ht.handleFinishedSTMs()

	for id, stm := range ht.truckSTMs {
		err := stm.HandleTick(timePassed)
		if err != nil {
			log.WithError(err).Errorln("HandleTick failed in stm", id)
		}
	}
	return nil
}

/**************************Helpers**************************/

// getSTMID converts location and resource id info into into stm id
func getSTMID(area string, resID string) (string, error) {
	if area == "" || resID == "" {
		return "", fmt.Errorf("invalid stm id %s %s", area, resID)
	}
	return resID + "_" + area, nil
}
