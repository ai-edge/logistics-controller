package asyncsrv

import (
	"ecs/tools/amqpwrap"

	"fmt"
	"testing"

	mockamqp "github.com/NeowayLabs/wabbit/amqp"
	log "github.com/sirupsen/logrus"
)

func TestCreateNewAsyncsrvNil(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovered in f: %v", r)
		} else {
			t.Errorf("NewAsyncsrv(nil, nil) did not panic")
		}
	}()

	as, err := NewAsyncsrv(nil, nil)
	if err != nil {
		t.Errorf("NewAsyncsrv(nil,nil) = %v, %s", as, err)
	} else {
		t.Logf("Asyncsrv(nil,nil) created successfully. as = %v", as)
	}
}

func TestCreateNewAsyncsrv(t *testing.T) {

	createNewAsyncsrv(t)
}

func createNewAsyncsrv(t *testing.T) (*Asyncsrv, error) {
	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovered in f: %v", r)
		}
	}()

	mockURL := fmt.Sprintf("amqp://guest:guest@localhost:%s", "5672")
	log.Debugf("connecting with url %s", mockURL)
	conn, err := mockamqp.Dial(mockURL)
	if err != nil {
		return nil, nil
	}
	wrap, err := amqpwrap.NewAMQPWrapper(conn.Connection)
	if err != nil {
		return nil, err
	}
	//different channels for publish and subscribe
	wrap.InitPubChannel()
	wrap.InitSubChannel()

	as, err := NewAsyncsrv(nil, wrap)
	if err != nil {
		t.Errorf("NewAsyncsrv(nil, wrap) = %v, %s", as, err)
	} else {
		t.Logf("NewAsyncsrv(nil, wrap) created successfully. as = %v", as)
	}

	return as, err
}
