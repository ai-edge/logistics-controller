//Package asyncsrv consumes incoming async messages
package asyncsrv

import (
	"ecs/api/generated/models"
	"ecs/internal/rabbitnamings"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type tosgwService interface {
	NotifyOperationsStatus(models.OperationsWorkOrderItemStatusBase) error
}

//Asyncsrv is the async service
type Asyncsrv struct {
	L3WorkOrderProgressCh <-chan amqp.Delivery
	connClosedCh          <-chan *amqp.Error
	subCloseCh            <-chan *amqp.Error
	subCancelCh           <-chan string

	stopCh chan bool

	amqpWrap *amqpwrap.AMQPWrapper

	tosgw tosgwService
}

//NewAsyncsrv returns a new async service. Creates exchanges only for consumed messages.
func NewAsyncsrv(tosgw tosgwService, amqpWrap *amqpwrap.AMQPWrapper) (*Asyncsrv, error) {
	asyncsrv := Asyncsrv{amqpWrap: amqpWrap, tosgw: tosgw}
	var err error

	asyncsrv.stopCh = make(chan bool)

	//subscribe to operations/*/workorderprogress
	route := rabbitnamings.L3RouteBase + ".*." + rabbitnamings.WorkOrderProgressRouteKey
	asyncsrv.L3WorkOrderProgressCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.WorkOrderProgressRouteKey, //queue name contains route key
		rabbitnamings.L3RouteBase, //exchange name
		route)

	if err != nil {
		return nil, err
	}

	asyncsrv.connClosedCh = amqpWrap.GetConnCloseChan()
	asyncsrv.subCloseCh = amqpWrap.GetSubCloseChan()
	asyncsrv.subCancelCh = amqpWrap.GetSubCancelChan()

	return &asyncsrv, nil
}

//DeInit closes connection
func (as *Asyncsrv) DeInit() {
	as.stopCh <- true
}

//Asyncsrv is the main loop for consuming incoming async messages
func (as *Asyncsrv) Asyncsrv() error {

	for {
		select {

		case delivery := <-as.L3WorkOrderProgressCh:
			wis := models.OperationsWorkOrderItemStatusBase{}
			err := json.Unmarshal(delivery.Body, &wis)
			if err != nil {
				log.WithError(err).Errorln("unmarshal work item status failed for delivery", delivery)
			} else {
				log.Traceln("received work item status:", wis)
				err = as.tosgw.NotifyOperationsStatus(wis)
				if err != nil {
					log.WithError(err).Error("error in NotifyOperationsStatus")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		case <-as.stopCh:
			log.Warningln("received stop")
			return nil

		case err := <-as.connClosedCh:
			if err != nil {
				log.WithError(err).Errorf("connection error. Shutting down.")
			} else {
				log.Warningf("connection closed. Shutting down")
			}
			panic(fmt.Errorf("connection closed"))

		case err := <-as.subCloseCh:
			log.WithError(err).Errorf("channel error. Shutting down")
			panic(fmt.Errorf("channel closed"))

		case msg := <-as.subCancelCh:
			log.Errorf("channel canceled. Shutting down %s", msg)
			panic(fmt.Errorf("channel canceled"))

		}
	}
}
