package tosgw

import (
	"ecs/internal/mockdb"
	"ecs/pkg/setup/repo/terminal"

	"fmt"
	"testing"

	mockamqp "github.com/NeowayLabs/wabbit/amqp"
	amqp "github.com/streadway/amqp"
)

func TestNewTOSGW(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovered in f: %v", r)
		} else {
			t.Errorf("NewTOSGW(&tr, &conn) did not panic")
		}
	}()

	tr := terminal.Terminal{}
	conn := amqp.Connection{}
	instance, err := NewTOSGW(&tr, &conn)
	if err != nil {
		t.Errorf("NewTOSGW(&tr, &conn) = %v, %s", instance, err)
	}
}

func TestNewOperationsNil(t *testing.T) {

	instance, err := NewTOSGW(nil, nil)
	if err == nil {
		t.Errorf("NewTOSGW(nil,nil) = %v, must have returned error", instance)
	}
}

func TestOperationsWithMockConnection(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovered in f: %v", r)
		}
	}()

	db, err := mockdb.NewMockDBClient("mockdb://localhost:2700", "admin", "test", "test")
	if err != nil {
		t.Errorf("mockdb.NewMockDBClient(...) = %v, %s", db, err)
	} else {
		t.Logf("mockdb.NewMockDBClient(...) created successfully. db = %v", db)
	}
	mockURL := fmt.Sprintf("amqp://guest:guest@localhost:%s", "5672")
	t.Logf("connecting with url %s", mockURL)
	conn, err := mockamqp.Dial(mockURL)
	if err != nil {
		t.Logf("Failed to connect to RabbitMQ: %s", err)
	} else {
		t.Logf("Mock connection is established to RabbitMQ.")
		tr, err := terminal.NewTerminalRepository(db)
		instance, err := NewTOSGW(tr, conn.Connection)
		if err != nil {
			t.Errorf("NewTOSGW(tr, conn) = %v, %s", instance, err)
		}
	}

}
