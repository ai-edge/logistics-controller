//Package dispatch dispatches new operations orders
package dispatch

import (
	"ecs/api/generated/models"
	"ecs/pkg/setup/repo/terminal"
	"ecs/tools/amqpwrap"
)

type tosgwService interface {
	NewDispatchedJobOrder(models.StackingCraneJobOrderBase)
}

//Dispatch is the dispatching service
type Dispatch struct {
	tr       *terminal.Terminal
	amqpWrap *amqpwrap.AMQPWrapper
}

//NewDispatch returns a new dispatcher
func NewDispatch(tr *terminal.Terminal, amqpWrap *amqpwrap.AMQPWrapper) (*Dispatch, error) {
	dispatch := Dispatch{tr: tr, amqpWrap: amqpWrap}
	return &dispatch, nil

}

//DeInit close dispatcher
func (ds *Dispatch) DeInit() {
	//TODO
}

//OperationsOrderUpdate notifies a job order update
func (ds *Dispatch) OperationsOrderUpdate(jo models.StackingCraneJobOrderBase) error {
	//check job type
	//check if existing job order
	//check crane status, error if it's busy
	return nil
}

//OperationsStatusUpdate notifies crane status update
func (ds *Dispatch) OperationsStatusUpdate(wis models.OperationsWorkOrderItemStatusBase) error {
	return nil
}
