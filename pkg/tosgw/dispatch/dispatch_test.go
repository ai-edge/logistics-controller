package dispatch

import (
	"ecs/api/generated/models"
	"ecs/internal/mockdb"
	"ecs/pkg/setup/repo/terminal"
	"ecs/tools/amqpwrap"

	"fmt"
	"testing"

	mockamqp "github.com/NeowayLabs/wabbit/amqp"
)

func TestCreateNewDispatcherNil(t *testing.T) {

	ds, err := NewDispatch(nil, nil)
	if err != nil {
		t.Errorf("NewDispatch(nil,nil) = %v, %s", ds, err)
	} else {
		t.Logf("NewDispatch(nil,nil) created successfully. ds = %v", ds)
	}
}

func TestCreateNewDispatcher(t *testing.T) {

	tr := terminal.Terminal{}
	wrap, err := amqpwrap.NewAMQPWrapper(nil)
	ds, err := NewDispatch(&tr, wrap)
	if err != nil {
		t.Errorf("NewDispatch(&bl, wrap) = %v, %s", ds, err)
	} else {
		t.Logf("NewDispatch(&bl, wrap) created successfully. ds = %v", ds)
	}
}

func TestOperationsOrderUpdateNoJob(t *testing.T) {
	ds, err := createDispatch(t)

	if err != nil {
		t.Logf("NewDispatch(&bl, wrap) = %v, %s", ds, err)
	} else {
		t.Logf("NewDispatch(&bl, wrap) created successfully. ds = %v", ds)
		job := models.StackingCraneJobOrderBase{}
		err = ds.OperationsOrderUpdate(job)

		if err != nil {
			t.Errorf("OperationsOrderUpdate failed. Err: %s", err)
		} else {
			t.Logf("OperationsOrderUpdate is successful")
		}
	}
}

// TODO TestOperationsOrderUpdateValid

func TestOperationsStatusUpdateNoWis(t *testing.T) {
	ds, err := createDispatch(t)

	if err != nil {
		t.Logf("NewDispatch(&bl, wrap) = %v, %s", ds, err)
	} else {
		t.Logf("NewDispatch(&bl, wrap) created successfully. ds = %v", ds)
		wis := models.OperationsWorkOrderItemStatusBase{}
		err = ds.OperationsStatusUpdate(wis)

		if err != nil {
			t.Errorf("OperationsOrderUpdate failed. Err: %s", err)
		} else {
			t.Logf("OperationsOrderUpdate is successful.")
		}
	}
}

// TODO : TestOperationsStatusUpdateValidWis

func createDispatch(t *testing.T) (*Dispatch, error) {
	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovered in f: %v", r)
		}
	}()

	db, err := mockdb.NewMockDBClient("mockdb://localhost:2700", "admin", "test", "test")
	if err != nil {
		t.Errorf("mockdb.NewMockDBClient(...) = %v, %s", db, err)
	} else {
		t.Logf("mockdb.NewMockDBClient(...) created successfully. db = %v", db)
	}

	tr, err := terminal.NewTerminalRepository(db)
	if err != nil {
		t.Errorf("terminal.NewTerminalRepository(...) = %v, %s", tr, err)
	} else {
		t.Logf("terminal.NewTerminalRepository(...) created successfully. bl = %v", tr)
	}

	mockURL := fmt.Sprintf("amqp://guest:guest@localhost:%s", "5672")
	t.Logf("connecting with url %s", mockURL)
	conn, err := mockamqp.Dial(mockURL)
	if err != nil {
		t.Logf("mockamqp.Dial(*uri) = %v, %s", conn, err)
	} else {
		t.Logf("mockamqp.Dial(*uri) connected successfully. conn = %v", conn)
		wrap, err := amqpwrap.NewAMQPWrapper(conn.Connection)
		ds, err := NewDispatch(tr, wrap)

		return ds, err
	}
	return nil, err
}
