//Package tosgw is the root package for tos gateway app.
package tosgw

import (
	"ecs/api/generated/models"
	"ecs/pkg/setup/repo/terminal"
	"ecs/pkg/tosgw/asyncsrv"
	"ecs/pkg/tosgw/dispatch"
	"ecs/tools/amqpwrap"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

//TOSGW tos gateway
type TOSGW struct {
	amqpWrap *amqpwrap.AMQPWrapper

	as  *asyncsrv.Asyncsrv
	dis *dispatch.Dispatch
	tr  *terminal.Terminal
}

//NewTOSGW initialize TOSGW
func NewTOSGW(tr *terminal.Terminal, conn *amqp.Connection) (*TOSGW, error) {
	if tr == nil {
		return nil, fmt.Errorf("nil terminal")
	}
	tosgw := &TOSGW{tr: tr}

	//init AMQP wrapper
	wrap, err := amqpwrap.NewAMQPWrapper(conn)
	if err != nil {
		log.WithError(err).Fatal("Could not create amqp wrapper")
	}

	err = wrap.InitPubChannel()
	if err != nil {
		return nil, fmt.Errorf("Init channel failed %w", err)
	}
	err = wrap.InitSubChannel()
	if err != nil {
		return nil, fmt.Errorf("Init channel failed %w", err)
	}

	tosgw.amqpWrap = wrap

	//init dispatcher
	dis, err := dispatch.NewDispatch(tr, wrap)
	if err != nil {
		return nil, err
	}
	tosgw.dis = dis

	//Init consuming async messages
	as, err := asyncsrv.NewAsyncsrv(tosgw, wrap)
	if err != nil {
		return nil, err
	}
	tosgw.as = as
	go as.Asyncsrv()

	//TODO: add service status messages
	return tosgw, err
}

//DeInit deinitialize the tosgw
func (tosgw *TOSGW) DeInit() {
	tosgw.as.DeInit()
	tosgw.amqpWrap.DeInit()
	tosgw.dis.DeInit()
}

//NotifyOperationsStatus notifies a newly received work item status
func (tosgw *TOSGW) NotifyOperationsStatus(wis models.OperationsWorkOrderItemStatusBase) error {
	//check if any queued job orders waiting
	err := tosgw.dis.OperationsStatusUpdate(wis)
	return err
}
