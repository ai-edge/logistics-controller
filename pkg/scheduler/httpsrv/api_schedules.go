/*
 * Scheduler REST API
 *
 * Scheduler Service REST API for simulation
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package httpsrv

import (
	"ecs/api/generated/models"
	"encoding/json"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

// A SchedulesApiController binds http requests to an api service and writes the service results to the http response
type SchedulesApiController struct {
	service SchedulesApiServicer
}

// NewSchedulesApiController creates a default api controller
func NewSchedulesApiController(s SchedulesApiServicer) Router {
	return &SchedulesApiController{service: s}
}

// Routes returns all of the api route for the SchedulesApiController
func (c *SchedulesApiController) Routes() Routes {
	return Routes{
		{
			"DeleteSchedule",
			strings.ToUpper("Delete"),
			"/api/v1/schedules/{scheduleid}",
			c.DeleteSchedule,
		},
		{
			"PutSchedule",
			strings.ToUpper("Put"),
			"/api/v1/schedules/{scheduleid}",
			c.PutSchedule,
		},
	}
}

// DeleteSchedule - deletes a schedule and its entries (status, dispatches) if schedule is not executed
func (c *SchedulesApiController) DeleteSchedule(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	scheduleid := params["scheduleid"]
	result, err := c.service.DeleteSchedule(scheduleid)
	if err != nil {
		w.WriteHeader(500)
		return
	}

	EncodeJSONResponse(result, nil, w)
}

// PutSchedule - Put a schedule request
func (c *SchedulesApiController) PutSchedule(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	scheduleid := params["scheduleid"]
	schedule := &models.Schedule{}
	if err := json.NewDecoder(r.Body).Decode(&schedule); err != nil {
		w.WriteHeader(500)
		return
	}

	result, err := c.service.PutSchedule(scheduleid, *schedule)
	if err != nil {
		w.WriteHeader(500)
		return
	}

	EncodeJSONResponse(result, nil, w)
}
