package schedule

import (
	"ecs/api/generated/models"
	"ecs/internal/dbservice"
	"ecs/pkg/setup/repo/blockcfg"
	"fmt"
)

//Schedule repository for Scheduler
type Schedule struct {
	//ServiceID is the id of the service created the repository
	ServiceID string
	//BlockID is the id of the block of that repository
	BlockID string
	dbNames scheduleDBNames

	dbs dbservice.DBService

	//scranes of this scheduler
	ScIDs []string
}

type scheduleDBNames struct {
	//DB names
	scheduleDB string
	//Collection Names
	dispatchCollectionName,
	dispatchStatusCollectionName,
	scheduleStatusCollectionName,
	truckStatusCollectionName string
	//queryLimit maximum items to be returned
	queryLimit int64
	//IDField defines the name of the ID field in the DB
	IDField string
}

//NewScheduleRepository returns a new repository of a scheduler for a given block
func NewScheduleRepository(dbs dbservice.DBService, serviceID string, blockID string, scIDs []string) (*Schedule, error) {

	if dbs == nil || serviceID == "" {
		return nil, fmt.Errorf("invalid arguments dbs % serviceID %s", dbs, serviceID)
	}
	var dbNames scheduleDBNames

	//MovesBL01DB is the name of the block DB
	dbNames.scheduleDB = blockID + "-schedule"

	dbNames.dispatchCollectionName = "dispatch"
	dbNames.dispatchStatusCollectionName = "dispatchstatus"
	dbNames.scheduleStatusCollectionName = "schedulestatus"
	dbNames.truckStatusCollectionName = "truckstatus"
	dbNames.queryLimit = 1000
	dbNames.IDField = "_id"

	schedule := Schedule{dbs: dbs, ServiceID: serviceID, BlockID: blockID, dbNames: dbNames, ScIDs: scIDs}

	return &schedule, nil
}

//Init initializes entities
func (scRepo *Schedule) Init(blCfg *blockcfg.BlockCfg) error {
	return nil
}

//DeInit closes connection
func (scRepo *Schedule) DeInit() {
	scRepo.dbs.DeInit()
}

//GetQueryLimit returns query limit parameter
func (scRepo *Schedule) GetQueryLimit() int64 {
	return scRepo.dbNames.queryLimit
}

// GetScheduleStatusByWorkItemName gets scheduleStatus
func (scRepo *Schedule) GetScheduleStatusByWorkItemName(workitemname string) (models.ScheduleStatus, error) {
	var scStat models.ScheduleStatus
	query := map[string]interface{}{"workitem.workitemname": workitemname}
	err := scRepo.dbs.GetOne(scRepo.dbNames.scheduleDB, scRepo.dbNames.scheduleStatusCollectionName, query, &scStat)
	return scStat, err
}

// GetScheduleStatuses gets scheduleStatuses
func (scRepo *Schedule) GetScheduleStatuses(limit int64) ([]models.ScheduleStatus, error) {
	var statuses []models.ScheduleStatus
	query := map[string]interface{}{}
	err := scRepo.dbs.GetAll(scRepo.dbNames.scheduleDB, scRepo.dbNames.scheduleStatusCollectionName, query, &statuses, limit)
	return statuses, err
}

// GetScheduleStatusesByParentName gets scheduleStatus by parent schedule name
func (scRepo *Schedule) GetScheduleStatusesByParentName(parent string, limit int64) ([]models.ScheduleStatus, error) {
	var statuses []models.ScheduleStatus
	query := map[string]interface{}{"parentschedule": parent}
	err := scRepo.dbs.GetAll(scRepo.dbNames.scheduleDB, scRepo.dbNames.scheduleStatusCollectionName, query, &statuses, limit)
	return statuses, err
}

// GetScheduleStatusesByState gets schedule statuses by state
func (scRepo *Schedule) GetScheduleStatusesByState(craneid string, state models.ScheduleState, limit int64) ([]models.ScheduleStatus, error) {
	var statuses []models.ScheduleStatus
	query := map[string]interface{}{
		"$and": []map[string]interface{}{
			map[string]interface{}{"schedulestate": state},
			map[string]interface{}{"workitem.stackingcraneid": craneid},
		},
	}
	err := scRepo.dbs.GetAll(scRepo.dbNames.scheduleDB, scRepo.dbNames.scheduleStatusCollectionName, query, &statuses, limit)
	return statuses, err
}

//PutScheduleStatusByWorkItemName puts a scheduleStatus
func (scRepo *Schedule) PutScheduleStatusByWorkItemName(workitemname string, newscSt models.ScheduleStatus) (models.ScheduleStatus, error) {
	var scStat models.ScheduleStatus
	query := map[string]interface{}{"workitem.workitemname": workitemname}
	err := scRepo.dbs.GetOne(scRepo.dbNames.scheduleDB, scRepo.dbNames.scheduleStatusCollectionName, query, &scStat)
	if err != nil {
		err = scRepo.dbs.InsertOne(scRepo.dbNames.scheduleDB, scRepo.dbNames.scheduleStatusCollectionName, &newscSt)
	} else {
		err = scRepo.dbs.ReplaceOne(scRepo.dbNames.scheduleDB, scRepo.dbNames.scheduleStatusCollectionName, query, &newscSt)
	}
	return newscSt, err
}

// DeleteScheduleStatusByWorkItemName deletes scheduletatus of a work item
func (scRepo *Schedule) DeleteScheduleStatusByWorkItemName(workitemname string) error {
	query := map[string]interface{}{"workitem.workitemname": workitemname}
	err := scRepo.dbs.DeleteOne(scRepo.dbNames.scheduleDB, scRepo.dbNames.scheduleStatusCollectionName, query)
	return err
}

/**************TRUCK Related*****************/

//PutTruckStatusByLogicalLocation puts a truck status
func (scRepo *Schedule) PutTruckStatusByLogicalLocation(location models.LogicalLocation3D, newTs models.HtStatus) (models.HtStatus, error) {
	var ts models.HtStatus
	query := map[string]interface{}{"destinationlogicallocation": location}
	err := scRepo.dbs.GetOne(scRepo.dbNames.scheduleDB, scRepo.dbNames.truckStatusCollectionName, query, &ts)
	if err != nil {
		err = scRepo.dbs.InsertOne(scRepo.dbNames.scheduleDB, scRepo.dbNames.truckStatusCollectionName, &newTs)
	} else {
		err = scRepo.dbs.ReplaceOne(scRepo.dbNames.scheduleDB, scRepo.dbNames.truckStatusCollectionName, query, &newTs)
	}
	return newTs, err
}

// DeleteTruckStatusByLogicalLocation deletes a truck status
func (scRepo *Schedule) DeleteTruckStatusByLogicalLocation(location models.LogicalLocation3D) error {
	query := map[string]interface{}{"destinationlogicallocation": location}
	err := scRepo.dbs.DeleteOne(scRepo.dbNames.scheduleDB, scRepo.dbNames.truckStatusCollectionName, query)
	return err
}

// GetTruckStatusByLogicalLocation gets truck status
func (scRepo *Schedule) GetTruckStatusByLogicalLocation(location models.LogicalLocation3D) (models.HtStatus, error) {
	var ts models.HtStatus
	query := map[string]interface{}{"destinationlogicallocation": location}
	err := scRepo.dbs.GetOne(scRepo.dbNames.scheduleDB, scRepo.dbNames.truckStatusCollectionName, query, &ts)
	return ts, err
}
