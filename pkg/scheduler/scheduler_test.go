package scheduler

import (
	"ecs/api/generated/models"
	"ecs/internal/mockdb"
	"ecs/pkg/scheduler/repo/schedule"
	"ecs/pkg/setup/repo/terminal"
	"testing"
)

func createScheduler(t *testing.T) (*Scheduler, error) {
	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovered in f: %v", r)
		}
	}()

	db, err := mockdb.NewMockDBClient("mockdb://localhost:2700", "admin", "test", "test")
	if err != nil {
		t.Fatalf("mockdb.NewMockDBClient(...) = %v, %s", db, err)
	}
	t.Logf("mockdb.NewMockDBClient(...) created. db = %v", db)

	tr, err := terminal.NewTerminalRepository(db)
	if err != nil {
		t.Fatalf("cannot create terminal repository, err: %s", err)
	}

	bl, err := tr.GetBlockWithAreaName("BL01")
	if err != nil {
		t.Fatalf("cannot create terminal repository, err: %s", err)
	}

	scRepo, err := schedule.NewScheduleRepository(db, "sche0001", "BL01", bl.StackingCraneIDs)
	if err != nil {
		t.Fatalf("schedule.NewScheduleRepository(...) = %v, %s", scRepo, err)
	}
	t.Logf("schedule.NewScheduleRepository(...) created. bl = %v", scRepo)

	scheduler := &Scheduler{scRepo: scRepo}
	return scheduler, nil
}

func TestProcessNewScheduleNoWi(t *testing.T) {

	sc, err := createScheduler(t)
	if err != nil {
		t.Fatalf("NewScheduler(&bl, wrap) = %v, %s", sc, err)
	}
	t.Logf("scheduler created %v", sc)
	sche := models.Schedule{WorkItem: models.OperationsWorkOrderItemBase{}, ScheduleReqType: models.NEW_SCHE, ParentSchedule: ""}
	err = sc.NotifyScheduleRequest(sche)
	if err == nil {
		t.Errorf("NotifyScheduleRequest not failed despite empyt Wi.")
	} else {
		t.Logf("NotifyScheduleRequest failed with empty wi %s", err)
	}
}
