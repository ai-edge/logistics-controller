//Package scheduler schedules dispatch requests based on scheduling configurations
package scheduler

import (
	"ecs/api/generated/models"
	"ecs/pkg/scheduler/asyncsrv"
	"ecs/pkg/scheduler/repo/schedule"
	"ecs/pkg/scheduler/state"
	"ecs/tools/amqpwrap"

	"fmt"

	"github.com/streadway/amqp"
)

//Scheduler handles scheduling tasks
type Scheduler struct {
	schedulerState *state.HSM

	amqpWrap *amqpwrap.AMQPWrapper
	as       *asyncsrv.Asyncsrv
	scRepo   *schedule.Schedule
}

//NewScheduler returns a new scsheduler
func NewScheduler(scRepo *schedule.Schedule, conn *amqp.Connection, operationsURL string, dispatcherURL string) (*Scheduler, error) {
	scheduler := &Scheduler{scRepo: scRepo}

	//init AMQP wrapper
	wrap, err := amqpwrap.NewAMQPWrapper(conn)
	if err != nil {
		return nil, fmt.Errorf("Could not create amqp wrapper: %w", err)
	}
	//different channels for publish and subscribe
	err = wrap.InitPubChannel()
	if err != nil {
		return nil, fmt.Errorf("Init channel failed %w", err)
	}
	err = wrap.InitSubChannel()
	if err != nil {
		return nil, fmt.Errorf("Init channel failed %w", err)
	}
	scheduler.amqpWrap = wrap

	hsm, err := state.NewHSM(wrap, scRepo, operationsURL, dispatcherURL)
	if err != nil {
		return nil, fmt.Errorf("cannot create hsm %w", err)
	}
	scheduler.schedulerState = hsm

	//Init consuming async messages
	as, err := asyncsrv.NewAsyncsrv(scRepo.BlockID, scheduler, wrap)
	if err != nil {
		return nil, err
	}
	scheduler.as = as
	go as.Asyncsrv()

	return scheduler, nil
}

//DeInit deinitializes
func (sche *Scheduler) DeInit() {
	sche.as.DeInit()
	sche.amqpWrap.DeInit()
	sche.schedulerState.DeInit()
}

//NotifyScheduleRequest handles a new schedule request
func (sche *Scheduler) NotifyScheduleRequest(schedule models.Schedule) error {
	//checks for inconsistency
	if schedule.WorkItem.WorkItemName == "" {
		return fmt.Errorf("schedule is empty")
	}
	_, err := sche.schedulerState.HandleScheduleRequest(schedule)
	return err
}

//NotifyDispatchStatusUpdate processes a dispatch status update
func (sche *Scheduler) NotifyDispatchStatusUpdate(disStatus models.DispatchStatus) error {
	_, err := sche.schedulerState.HandleDispatchStatusUpdate(disStatus)
	return err
}
