//Package state maintains a scheduler state machine for one block
package state

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"
	"time"

	"github.com/jinzhu/copier"
	log "github.com/sirupsen/logrus"
)

type ready struct {
	stContext *HSM
}

func (state *ready) enterState() error {
	return nil
}

func (state *ready) exitState() error {
	return nil
}

//createScheduletatus creates an initial schedule status for a schedule
func createScheduletatus(schedule models.Schedule) (models.ScheduleStatus, error) {
	var st = models.ScheduleStatus{}
	if schedule.ScheduleReqType != models.NEW_SCHE {
		return st, fmt.Errorf("cannot create schedule status for %s %s", schedule.ScheduleReqType, schedule.WorkItem.ItemStatus)
	}
	//copy fields with identical names and types
	err := copier.Copy(&st, &schedule)
	if err != nil {
		return st, err
	}
	st.WorkItem.UpdateTime = time.Now()
	st.ScheduleState = models.S_WAITING

	return st, nil
}

//HandleDispatchStatusUpdate processes a dispatch status update
func (state *ready) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	if dst.DispatchState == models.D_VOID {
		return false, nil
	}

	sta, err := state.stContext.getActiveSchedule(dst.DispatchName)
	if err != nil {
		//no schedule for this dispatch
		return false, err
	}
	sta.LastGantryPosition = dst.LastGantryPosition

	switch dst.DispatchState {
	case models.D_ENTERED:
		//TODO: initialize state
	case models.D_FAULT:
		log.Errorf("received disp state %s in hsm state: %s", dst.DispatchState, reflect.TypeOf(state).String())
		//TODO: check this via dispatcher state, not dispatch state, as dispatch state might be faulted and continued.
		defer state.stContext.goToNextState(state.stContext.suspendedState)
	case models.D_FINISHED:
		return true, state.stContext.finalizeSchedule(sta, models.S_FINISHED)
	case models.D_CANCELED:
		log.Warningf("wi %s received disp state %s in hsm state: %s", sta.WorkItem.WorkItemName,
			dst.DispatchState, reflect.TypeOf(state).String())
		return true, state.stContext.finalizeSchedule(sta, models.S_CANCELED)
	case models.D_ABORTED:
		log.Warningf("wi %s received disp state %s in hsm state: %s", sta.WorkItem.WorkItemName,
			dst.DispatchState, reflect.TypeOf(state).String())
		return true, state.stContext.finalizeSchedule(sta, models.S_ABORTED)
	default:
		return false, nil
	}
	return false, nil
}

//HandleScheduleRequest polls queues for new schedules
func (state *ready) HandleScheduleRequest(sche models.Schedule) (bool, error) {
	switch sche.ScheduleReqType {
	case models.DELETE_SCHE:
		//clear all entries of this schedule
		return true, state.stContext.deleteSchedule(sche.WorkItem.WorkItemName)
	case models.CANCEL_SCHE:
		return true, state.stContext.cancelSchedule(sche.WorkItem.WorkItemName)
	case models.NEW_SCHE:
		return true, state.stContext.addSchedule(sche)
	default:
		return true, fmt.Errorf("invalid schedule request %s in state %s", sche.ScheduleReqType, reflect.TypeOf(state).String())
	}
}
