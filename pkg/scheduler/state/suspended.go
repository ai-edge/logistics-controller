//Package state maintains a scheduler state machine for one block
package state

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
)

type suspended struct {
	stContext *HSM
}

func (state *suspended) enterState() error {
	//abort all active schedule stms
	log.Warningln("aborting all schedules in", reflect.TypeOf(state).String())
	err := state.stContext.abortExecutingSchedules()
	if err != nil {
		log.WithError(err).Errorln("abortActiveScheduleStms failed", reflect.TypeOf(state).String())
	}
	return nil
}

func (state *suspended) exitState() error {
	return nil
}

//HandleDispatchStatusUpdate processes a dispatch status update
func (state *suspended) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	switch dst.DispatchState {
	case models.D_INITIALIZE, models.D_READY, models.D_RUNNING, models.D_COMPLETED,
		models.D_FINISHED, models.D_CANCELED, models.D_ENTERED, models.D_BLOCKED:
		log.Errorf("invalid disp state %s in stm state: %s", dst.DispatchState, reflect.TypeOf(state).String())
	case models.D_ABORTED:
		log.Warningf("received disp % state %s in hsm state: %s",
			dst.DispatchName, dst.DispatchState, reflect.TypeOf(state).String())
	case models.D_VOID:
		log.Warningf("received disp % state %s in hsm state: %s",
			dst.DispatchName, dst.DispatchState, reflect.TypeOf(state).String())
		defer state.stContext.goToNextState(state.stContext.readyState)
		return true, nil
	case models.D_FAULT:
		log.Warningf("received disp state %s in hsm state: %s", dst.DispatchState, reflect.TypeOf(state).String())
	default:
		return true, fmt.Errorf("invalid disp state %s in hsm: %s", dst.DispatchState, reflect.TypeOf(state).String())
	}
	return false, nil
}

//HandleScheduleRequest polls queues for new schedules
func (state *suspended) HandleScheduleRequest(sche models.Schedule) (bool, error) {
	switch sche.ScheduleReqType {
	case models.DELETE_SCHE:
		//clear all entries for this schedule
		return true, state.stContext.deleteSchedule(sche.WorkItem.WorkItemName)
	default:
		return true, fmt.Errorf("invalid schedule request %s in state %s", sche.ScheduleReqType, reflect.TypeOf(state).String())
	}
}
