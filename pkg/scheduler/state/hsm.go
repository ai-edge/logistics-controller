//Package state maintains a scheduler state machine for one block
package state

import (
	"ecs/api/generated/models"
	"ecs/internal/rabbitnamings"
	"ecs/pkg/scheduler/repo/schedule"
	"ecs/pkg/scheduler/state/interlock"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"time"

	"fmt"
	"math"
	"reflect"

	log "github.com/sirupsen/logrus"
)

//schedulerState is the interface of "scheduler" state
type schedulerState interface {
	enterState() error
	exitState() error

	HandleDispatchStatusUpdate(models.DispatchStatus) (bool, error)
	HandleScheduleRequest(schedule models.Schedule) (bool, error)
}

//HSM is the scheduler state machine. It is the superstate of schedule stm.
type HSM struct {

	//receivedschedules. key == WorkItemName
	activeSchedules map[string]*models.ScheduleStatus

	//last received dispatch state
	lastDispState map[string]models.DispatchState

	//current state manipulates schedule stms
	currentState schedulerState

	/*states*/
	readyState     schedulerState
	suspendedState schedulerState

	amqpWrap *amqpwrap.AMQPWrapper
	scRepo   *schedule.Schedule
	intr     *interlock.Interlock
}

//NewHSM returns a new state machine
func NewHSM(amqpWrap *amqpwrap.AMQPWrapper, scRepo *schedule.Schedule, operationsURL string, dispatcherURL string) (*HSM, error) {
	hsm := &(HSM{amqpWrap: amqpWrap, scRepo: scRepo})
	//Init interlock
	intr, err := interlock.NewInterlock(operationsURL, dispatcherURL, scRepo.BlockID)
	if err != nil {
		return nil, err
	}
	hsm.intr = intr

	hsm.activeSchedules = make(map[string]*models.ScheduleStatus)
	hsm.lastDispState = make(map[string]models.DispatchState)

	//initialize active schedules
	for _, scid := range scRepo.ScIDs {
		hsm.lastDispState[scid] = models.D_VOID
		ips, _ := hsm.scRepo.GetScheduleStatusesByState(scid, models.S_IN_PROGRESS, scRepo.GetQueryLimit())
		if len(ips) > 1 {
			//TODO: update it for merging, splitting schedules
			log.Fatalf("more than one in progress schedule %d for one resource %s", len(ips), scid)
		}
		//get queued schedules from repo
		var schests []models.ScheduleStatus
		schests = append(schests, ips...)
		ws, _ := hsm.scRepo.GetScheduleStatusesByState(scid, models.S_WAITING, hsm.scRepo.GetQueryLimit())
		schests = append(schests, ws...)
		is, _ := hsm.scRepo.GetScheduleStatusesByState(scid, models.S_INITIALIZE, hsm.scRepo.GetQueryLimit())
		schests = append(schests, is...)

		//create one stm for each queued state
		for i := range schests {
			sta := schests[i]
			err := hsm.enqueueActiveSchedule(&sta)
			if err != nil {
				log.WithError(err).Fatalln("enqueueActiveSchedule failed for hsm", hsm)
			}
		}
	}

	//init state contexts
	hsm.readyState = &(ready{stContext: hsm})
	hsm.suspendedState = &(suspended{stContext: hsm})
	hsm.setState(hsm.readyState)

	return hsm, nil
}

//DeInit deinitializes the hsm
func (hsm *HSM) DeInit() {
	return
}

func (hsm *HSM) setState(newstate schedulerState) {
	hsm.currentState = newstate
}

func (hsm *HSM) goToNextState(nextState schedulerState) error {
	log.Debugf("exiting state %s. Going to next state %s", reflect.TypeOf(hsm.currentState).String(), reflect.TypeOf(nextState).String())
	err := hsm.currentState.exitState()
	if err != nil {
		log.WithError(err).Errorln("cannot exit state:", hsm.currentState)
		return err
	}
	hsm.currentState = nextState
	err = hsm.currentState.enterState()
	if err != nil {
		log.WithError(err).Errorln("cannot enter state:", hsm.currentState)
		return err
	}
	return nil
}

//getExecutingSchedule gets a list of executing schedules
func (hsm *HSM) getExecutingSchedule(resourceID string) (*models.ScheduleStatus, error) {
	var stas []*models.ScheduleStatus
	for i := range hsm.activeSchedules {
		sta := hsm.activeSchedules[i]
		if resourceID == sta.WorkItem.StackingCraneID && sta.ScheduleState == models.S_IN_PROGRESS {
			stas = append(stas, sta)
		}
	}
	if len(stas) > 1 {
		log.Fatalln("more than one executing schedule in hsm", hsm)
	}
	if len(stas) == 1 {
		return stas[0], nil
	}
	return nil, fmt.Errorf("no executing schedule found for resource %s", resourceID)
}

//abortExecutingSchedules aborts executing schedules
func (hsm *HSM) abortExecutingSchedules() error {
	for _, scid := range hsm.scRepo.ScIDs {
		sta, err := hsm.getExecutingSchedule(scid)
		if err != nil {
			continue
		}
		err = hsm.abortDispatch(*sta)
		if err != nil {
			log.Errorln("abortSchedule failed", sta.WorkItem.WorkItemName)
		}
		err = hsm.finalizeSchedule(sta, models.S_ABORTED)
		if err != nil {
			log.Errorln("finalizeSchedule failed", sta.WorkItem.WorkItemName)
		}
	}
	return nil
}

//getNextHighesPrioSchedule gets next highest prio waiting schedule of a resource
func (hsm *HSM) getNextHighesPrioSchedule(resourceID string) (*models.ScheduleStatus, error) {
	//A schedule is executed by one resource
	var foundSta *models.ScheduleStatus
	err := fmt.Errorf("schedule stm not found for resource %s", resourceID)
	highestPrio := int32(math.MaxInt32)
	for i := range hsm.activeSchedules {
		sta := hsm.activeSchedules[i]
		if resourceID == sta.WorkItem.StackingCraneID && sta.ScheduleState == models.S_WAITING {
			prio := sta.WorkItem.Priority
			if prio < highestPrio {
				err = nil
				foundSta = sta
				highestPrio = prio
			}
		}
	}
	return foundSta, err
}

//getActiveSchedule gets a schedule stored in hsm
func (hsm *HSM) getActiveSchedule(workItemName string) (*models.ScheduleStatus, error) {
	sta, found := hsm.activeSchedules[workItemName]
	if !found {
		return nil, fmt.Errorf("schedule not found %s", workItemName)
	}
	return sta, nil
}

//processSchedulesList tries to initiate ready schedules
func (hsm *HSM) processSchedulesList() error {
	//A schedule is executed by one resource
	for _, scid := range hsm.scRepo.ScIDs {
		_, err := hsm.getExecutingSchedule(scid)
		if hsm.lastDispState[scid] != models.D_VOID || err == nil {
			continue //when resource is busy
		}

		//if no active schedules for this resource, try to assign one
		for {
			sta, err := hsm.getNextHighesPrioSchedule(scid)
			if err != nil {
				break //no more schedules
			}
			//start schedule stm
			err = hsm.startSchedule(sta)
			if err != nil {
				log.WithError(err).Errorln("cannot start schedule", sta.WorkItem.WorkItemName)
				hsm.finalizeSchedule(sta, models.S_ABORTED)
				continue
			}
			break
		}

	}
	return nil
}

//enqueueScheduleStm adds a new active schedule status
func (hsm *HSM) enqueueActiveSchedule(sta *models.ScheduleStatus) error {

	fs, ok := hsm.activeSchedules[sta.WorkItem.WorkItemName]
	if ok {
		log.Fatalln("more than one schedule with same id", fs)
	}
	hsm.activeSchedules[sta.WorkItem.WorkItemName] = sta
	return nil
}

//dequeueActiveSchedule removes a schedule status from hsm
func (hsm *HSM) dequeueActiveSchedule(workItemName string) error {
	_, found := hsm.activeSchedules[workItemName]
	if !found {
		log.Fatalln("schedule not found", workItemName)
	}
	delete(hsm.activeSchedules, workItemName)
	return nil
}

//addSchedule adds a schedule into the waiting queue
func (hsm *HSM) addSchedule(sche models.Schedule) error {
	//check if schedule has already been received
	sta, err := hsm.scRepo.GetScheduleStatusByWorkItemName(sche.WorkItem.WorkItemName)
	if err == nil {
		hsm.publishScheduleStatus(sta)
		return fmt.Errorf("This schedule request is already received %s", sche.WorkItem.WorkItemName)
	}
	sta, err = createScheduletatus(sche)
	if err != nil {
		return fmt.Errorf("createScheduletatus failed: %w", err)
	}
	return hsm.createSchedule(&sta)
}

//cancelSchedule cancels a schedule
func (hsm *HSM) cancelSchedule(itemname string) error {
	//sends cancel dispatch request for child schedules
	cSts, err := hsm.scRepo.GetScheduleStatusesByParentName(itemname, hsm.scRepo.GetQueryLimit())
	if err == nil {
		for _, cSt := range cSts {
			log.Debugln("canceling child schedule", cSt.WorkItem.WorkItemName)
			if cSt.WorkItem.WorkItemName == itemname {
				log.Fatalln("parent schedule identical with child", cSt)
			}
			canSche := models.Schedule{ScheduleReqType: models.CANCEL_SCHE,
				WorkItem: models.OperationsWorkOrderItemBase{WorkItemName: cSt.WorkItem.WorkItemName}, ParentSchedule: ""}
			err = hsm.publishSchedule(canSche)
			if err != nil {
				log.WithError(err).Errorln("publishSchedule failed", canSche)
			}
		}
	}

	//sends cancel dispatch request for parent schedule
	sta, err := hsm.getActiveSchedule(itemname)
	if err != nil {
		return err
	}

	//handle cancel confirmation from dispatcher
	if sta.ScheduleState == models.S_IN_PROGRESS {
		return hsm.cancelDispatch(*sta)
	}
	//directly update state if not executed
	return hsm.finalizeSchedule(sta, models.S_CANCELED)
}

//deleteSchedule deletes historical entries of a non-active schedule
func (hsm *HSM) deleteSchedule(itemname string) error {
	//TODO: this operation is not safe and shall not be called if parent or child schedules are active.
	//delete child schedules
	dSts, err := hsm.scRepo.GetScheduleStatusesByParentName(itemname, hsm.scRepo.GetQueryLimit())
	if err == nil {
		for _, dSt := range dSts {
			log.Traceln("child schedule to delete", dSt.WorkItem.WorkItemName)
			if dSt.WorkItem.WorkItemName == itemname {
				log.Fatalln("parent schedule identical with child", dSt)
			}
			delSche := models.Schedule{ScheduleReqType: models.DELETE_SCHE,
				WorkItem: models.OperationsWorkOrderItemBase{WorkItemName: dSt.WorkItem.WorkItemName}, ParentSchedule: ""}
			err = hsm.publishSchedule(delSche)
			if err != nil {
				log.WithError(err).Errorln("publishSchedule failed", delSche)
			}
		}
	}

	fSta, err := hsm.scRepo.GetScheduleStatusByWorkItemName(itemname)
	if err != nil {
		return fmt.Errorf("Schedule not found %s", itemname)
	}
	err = hsm.abortDispatch(fSta)
	if err != nil {
		log.WithError(err).Errorln("abortDispatch failed", fSta.WorkItem.WorkItemName)
	}
	//send delete signal to dispatcher
	err = hsm.deleteDispatch(fSta)
	if err != nil {
		log.WithError(err).Errorln("deleteDispatch failed", fSta.WorkItem.WorkItemName)
	}

	//finalize if active
	sta, err := hsm.getActiveSchedule(fSta.WorkItem.WorkItemName)
	if err == nil {
		err = hsm.finalizeSchedule(sta, models.S_ABORTED)
		if err != nil {
			log.Errorln("finalizeSchedule failed", sta.WorkItem.WorkItemName)
		}
	}

	//remove schedule status from repo
	return hsm.scRepo.DeleteScheduleStatusByWorkItemName(fSta.WorkItem.WorkItemName)
}

/**************************Publish Related**************************/

//startSchedule initiates dispatching of a schedule
func (hsm *HSM) startSchedule(sta *models.ScheduleStatus) error {
	var executingSchedules []models.ScheduleStatus
	log.Debugf("starting schedule %s %s .Nr of active schedules %d",
		sta.WorkItem.WorkItemName, sta.WorkItem.StackingCraneID, len(hsm.activeSchedules))
	for _, scid := range hsm.scRepo.ScIDs {
		executingSta, err := hsm.getExecutingSchedule(scid)
		if err != nil {
			continue
		}
		executingSchedules = append(executingSchedules, *executingSta)
	}

	//get interlocking schedules and tasks
	startingScheduleDispatch, interlockingSchedules, err := hsm.intr.GenerateInterlockingSchedules(*sta, executingSchedules)
	if err != nil {
		//abort schedule
		return fmt.Errorf("GenerateInterlockingSchedules failed %w", err)
	}

	//queue newly created interlocking schedules
	for _, sche := range interlockingSchedules {
		//err = hsm.publishSchedule(sche)
		err = hsm.addSchedule(sche) //add directly instead of publishing a new schedule to prevent delay
		if err != nil {
			return fmt.Errorf("addSchedule failed %w", err)
		}
	}

	//update dispatch name field with newly created dispatch
	sta.DispatchName = startingScheduleDispatch.DispatchName

	//start the dispatch of starting schedule
	err = hsm.publishDispatchRequest(startingScheduleDispatch)
	if err != nil {
		return fmt.Errorf("publishDispatchRequest failed %s", sta.WorkItem.WorkItemName)
	}

	//update schedule status and publish it
	sta.ScheduleState = models.S_IN_PROGRESS
	sta.DispatchTime = time.Now()

	err = hsm.saveAndPublishState(sta)
	if err != nil {
		log.Errorln("saveAndPublishState failed", sta.WorkItem.WorkItemName)
	}

	return nil
}

//startSchedule initiates dispatching of a schedule
func (hsm *HSM) createSchedule(sta *models.ScheduleStatus) error {
	//update schedule status and publish it
	sta.ScheduleState = models.S_WAITING
	err := hsm.saveAndPublishState(sta)
	if err != nil {
		log.Errorln("saveAndPublishState failed", sta.WorkItem.WorkItemName)
	}
	return hsm.enqueueActiveSchedule(sta)
}

//finishSchedule finishes a schedule and updates its state
func (hsm *HSM) finalizeSchedule(sta *models.ScheduleStatus, finishState models.ScheduleState) error {
	log.Debugf("finalizeSchedule %s with state %s .Nr of active schedules %d",
		sta.WorkItem.WorkItemName, finishState, len(hsm.activeSchedules))
	//update schedule status and publish it
	sta.ScheduleState = finishState
	err := hsm.saveAndPublishState(sta)
	if err != nil {
		log.Errorln("saveAndPublishState failed", sta.WorkItem.WorkItemName)
	}
	//remove from active schedules
	return hsm.dequeueActiveSchedule(sta.WorkItem.WorkItemName)
}

//abortDispatch aborts a schedule's dispatch
func (hsm *HSM) abortDispatch(sta models.ScheduleStatus) error {
	log.Warnln("Aborting the dispatch", sta.WorkItem.WorkItemName)
	dispatch := models.Dispatch{
		StackingCraneID:  sta.WorkItem.StackingCraneID,
		DispatchPriority: sta.WorkItem.Priority,
		DispatchName:     sta.WorkItem.WorkItemName,
		DispatchNr:       math.MaxInt32,
	}
	dispatch.DispatchReqType = models.ABORT_DISP
	return hsm.publishDispatchRequest(dispatch)
}

//cancelDispatch aborts a schedule's dispatch
func (hsm *HSM) cancelDispatch(sta models.ScheduleStatus) error {
	log.Warnln("Aborting the dispatch", sta.WorkItem.WorkItemName)
	dispatch := models.Dispatch{
		StackingCraneID:  sta.WorkItem.StackingCraneID,
		DispatchPriority: sta.WorkItem.Priority,
		DispatchName:     sta.WorkItem.WorkItemName,
		DispatchNr:       math.MaxInt32,
	}
	dispatch.DispatchReqType = models.CANCEL_DISP
	return hsm.publishDispatchRequest(dispatch)
}

//deleteDispatch deletes a schedule's dispatch
func (hsm *HSM) deleteDispatch(sta models.ScheduleStatus) error {
	log.Warnln("Deleting the dispatch", sta.WorkItem.WorkItemName)
	dispatch := models.Dispatch{
		StackingCraneID:  sta.WorkItem.StackingCraneID,
		DispatchPriority: sta.WorkItem.Priority,
		DispatchName:     sta.WorkItem.WorkItemName,
		DispatchNr:       math.MaxInt32,
	}
	dispatch.DispatchReqType = models.DELETE_DISP
	return hsm.publishDispatchRequest(dispatch)
}

//publishDispatchRequest publishes scrane job order
func (hsm *HSM) publishDispatchRequest(ro models.Dispatch) error {
	log.Debugln("publishDispatchRequest", ro)

	bytes, err := json.Marshal(ro)
	if err != nil {
		return fmt.Errorf("marshalling failed for dispatch :%v", ro)
	}
	//publish the dispatch : dispatcher/{blockid}/dispatchrequest
	err = hsm.amqpWrap.PublishTo(rabbitnamings.DispRouteBase,
		rabbitnamings.DispRouteBase+"."+hsm.scRepo.BlockID+"."+rabbitnamings.DispatchRequestRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing dispatch failed: %w", err)
	}
	return err
}

// publishSchedule publishes schedule to scheduler service
func (hsm *HSM) publishSchedule(sch models.Schedule) error {
	log.Debugln("publishSchedule", sch.WorkItem.WorkItemName)
	bytes, err := json.Marshal(sch)
	if err != nil {
		return fmt.Errorf("marshalling failed for schedule :%v", sch)
	}
	//publish the schedule : scheduler/{blockid}/schedulerequest
	err = hsm.amqpWrap.PublishTo(rabbitnamings.ScheRouteBase,
		rabbitnamings.ScheRouteBase+"."+hsm.scRepo.BlockID+"."+rabbitnamings.ScheduleRequestRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing schedule failed: %w", err)
	}
	return err
}

//publishScheduleStatus publishes Schedule Status
func (hsm *HSM) publishScheduleStatus(scStat models.ScheduleStatus) error {
	log.Debugln("publishScheduleStatus", scStat.WorkItem.WorkItemName, scStat.ScheduleState)
	log.Traceln("scheduleStatus", scStat)

	bytes, err := json.Marshal(scStat)
	if err != nil {
		return fmt.Errorf("marshalling failed for scheduleStatus :%v", scStat)
	}
	//publish scheduleStatus : scheduler/{blockid}/schedulestatus
	err = hsm.amqpWrap.PublishTo(rabbitnamings.ScheRouteBase,
		rabbitnamings.ScheRouteBase+"."+hsm.scRepo.BlockID+"."+rabbitnamings.ScheduleStatusRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing scheduleStatus failed: %w", err)
	}
	return err
}

//saveAndPublishState saves and publishes status
func (hsm *HSM) saveAndPublishState(sta *models.ScheduleStatus) error {
	sta.WorkItem.UpdateTime = time.Now()
	_, err := hsm.scRepo.PutScheduleStatusByWorkItemName(sta.WorkItem.WorkItemName, *sta)
	if err != nil {
		return fmt.Errorf("PutScheduleStatusByWorkItemName failed: %w", err)
	}
	err = hsm.publishScheduleStatus(*sta)
	if err != nil {
		return fmt.Errorf("publishScheduleStatus failed: %w", err)
	}
	return nil
}

/**************************Event Handlers**************************/

//HandleScheduleRequest hanles a schedule request
func (hsm *HSM) HandleScheduleRequest(sche models.Schedule) (bool, error) {
	defer hsm.processSchedulesList()
	return hsm.currentState.HandleScheduleRequest(sche)
}

//HandleDispatchStatusUpdate processes a dispatch status update
func (hsm *HSM) HandleDispatchStatusUpdate(disStatus models.DispatchStatus) (bool, error) {
	_, ok := hsm.lastDispState[disStatus.StackingCraneID]
	if !ok {
		return true, fmt.Errorf("crane not assigned in scheduler failed: %s", disStatus.StackingCraneID)
	}

	hsm.lastDispState[disStatus.StackingCraneID] = disStatus.DispatchState

	defer hsm.processSchedulesList()
	return hsm.currentState.HandleDispatchStatusUpdate(disStatus)
}

//TODO: handle dispatcher statuses func (hsm *HSM) HandleDispatcherStatusUpdate(dispatcherStatus models.DispatcherStatus)
