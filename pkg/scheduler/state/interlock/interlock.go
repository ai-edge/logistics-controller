//Package interlock manages interlocking of two cranes in the same area
package interlock

import (
	"ecs/api/generated/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

const (
	maxTzTier                   = int(1)
	maxSaTier                   = int(6)
	compositeLocationNameLength = int(15)
)

//Interlock handles interlocking tasks
type Interlock struct {
	operationsURL string
	dispatcherURL string
	client        *http.Client
	cranes        []string
	tzAreaNames   []string
	saAreaNames   []string
	bayWidth      int64
	fieldOffset   int64
	blockID       string
}

// internalDispatchTask adds workitem info for collision calculations
type internalDispatchTask struct {
	dispatchTask models.DispatchTask
	workItemName string
}

// claimArea containes min and max bay for a given schedule status
type claimArea struct {
	minAbsPos                  int64
	maxAbsPos                  int64
	workItemName               string
	resourceID                 string
	destinationLogicalLocation models.LogicalLocation3D
}

//NewInterlock returns a new interlock
func NewInterlock(operationsURL string, dispatcherURL string, blockID string) (*Interlock, error) {
	interlock := &Interlock{operationsURL: operationsURL, dispatcherURL: dispatcherURL, bayWidth: int64(6458), fieldOffset: int64(10000), blockID: blockID}
	interlock.client = http.DefaultClient
	return interlock, nil
}

// GetAreaNames gets the blocks configuration from operations and fills area names
func (intr *Interlock) GetAreaNames(blockID string) error {
	req, err := http.NewRequest(http.MethodGet, intr.operationsURL+"/setup/blocks/"+blockID, nil)
	if err != nil {
		return fmt.Errorf("new request failed %w", err)
	}

	body, err := intr.sendRequest(req)
	if err != nil {
		return fmt.Errorf("send request failed %w", err)
	}

	block := models.ContainerBlockComposite{}
	err = json.Unmarshal(body, &block)
	if err != nil {
		return fmt.Errorf("unmarshal %w", err)
	}
	intr.tzAreaNames = nil
	for _, tz := range block.TransferZoneNames {
		intr.tzAreaNames = append(intr.tzAreaNames, tz)
	}
	intr.saAreaNames = nil
	for _, sa := range block.StackingAreaNames {
		intr.saAreaNames = append(intr.saAreaNames, sa)
	}
	return nil
}

// GetDeviceList fills the crane list in the Interlock structure
func (intr *Interlock) GetDeviceList(limit int) ([]models.StackingCrane, error) {
	req, err := http.NewRequest(http.MethodGet, intr.operationsURL+"/scranes?limit="+strconv.FormatInt(int64(limit), 10), nil)
	if err != nil {
		return nil, fmt.Errorf("new request failed %w", err)
	}
	// req.URL.RawQuery = request.URL.Query().Encode()

	body, err := intr.sendRequest(req)
	if err != nil {
		return nil, fmt.Errorf("send request failed %w", err)
	}
	cranes := []models.StackingCrane{}
	err = json.Unmarshal(body, &cranes)

	return cranes, err
}

// GetTransferFields gets transfer field data for logical location to physical location conversion parameters
func (intr *Interlock) GetTransferFields(limit int) ([]models.TransferField, error) {
	req, err := http.NewRequest(http.MethodGet, intr.operationsURL+"/transferfields?limit="+strconv.FormatInt(int64(limit), 10), nil)
	if err != nil {
		return nil, fmt.Errorf("new request failed %w", err)
	}
	// req.URL.RawQuery = request.URL.Query().Encode()

	body, err := intr.sendRequest(req)
	if err != nil {
		return nil, fmt.Errorf("send request failed %w", err)
	}
	fields := []models.TransferField{}
	err = json.Unmarshal(body, &fields)

	return fields, err
}

// GetCraneStatus returns the crane that with the given resourceID
func (intr *Interlock) GetCraneStatus(resourceID string) (models.StackingCraneStatusBase, error) {
	req, err := http.NewRequest(http.MethodGet, intr.operationsURL+"/scranestatuses/"+resourceID, nil)
	if err != nil {
		return models.StackingCraneStatusBase{}, fmt.Errorf("new request failed %w", err)
	}
	// req.URL.RawQuery = request.URL.Query().Encode()

	body, err := intr.sendRequest(req)
	if err != nil {
		return models.StackingCraneStatusBase{}, fmt.Errorf("send request failed %w", err)
	}
	crane := models.StackingCraneStatusBase{}
	err = json.Unmarshal(body, &crane)

	return crane, err
}

// GetStackingFieldStatus for container check
func (intr *Interlock) GetStackingFieldStatus(fieldName string) (models.StackingFieldStatus, error) {
	req, err := http.NewRequest(http.MethodGet, intr.operationsURL+"/stackingfieldstatuses/"+fieldName, nil)
	fieldStat := models.StackingFieldStatus{}
	if err != nil {
		return fieldStat, fmt.Errorf("new request failed %w", err)
	}
	// req.URL.RawQuery = request.URL.Query().Encode()

	body, err := intr.sendRequest(req)
	if err != nil {
		return fieldStat, fmt.Errorf("send request failed %w", err)
	}
	err = json.Unmarshal(body, &fieldStat)

	return fieldStat, err
}

// GetDispatchStatus gets the dispatch with the given work item name
func (intr *Interlock) GetDispatchStatus(dispatchName string) (models.Dispatch, error) {
	req, err := http.NewRequest(http.MethodGet, intr.dispatcherURL+"/dispatchstatus/"+dispatchName, nil)
	dis := models.Dispatch{}
	if err != nil {
		return dis, fmt.Errorf("new request failed %w", err)
	}

	body, err := intr.sendRequest(req)
	if err != nil {
		return dis, fmt.Errorf("send request failed %w", err)
	}
	err = json.Unmarshal(body, &dis)

	return dis, err
}

// send request sends the http request to operations
func (intr *Interlock) sendRequest(req *http.Request) ([]byte, error) {
	resp, err := intr.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, err
}

//DeInit deinitializes
func (intr *Interlock) DeInit() {
	return
}

// GetInfoFromOperations gets crane and field information which will be used in collision calculations
func (intr *Interlock) GetInfoFromOperations() error {
	cranes, err := intr.GetDeviceList(100)
	if err != nil {
		return fmt.Errorf("GetDeviceList failed %w", err)
	}
	intr.cranes = nil
	for _, crane := range cranes {
		intr.cranes = append(intr.cranes, crane.ResourceID)
	}

	fields, err := intr.GetTransferFields(100)
	if err != nil {
		return fmt.Errorf("GetTransferFields failed %w", err)
	}

	intr.bayWidth = int64(0)
	for _, field := range fields {
		if field.FieldBase.Bay == "001" {
			intr.bayWidth = int64(field.FieldBase.Width)
			intr.fieldOffset = int64(field.FieldBase.X)
			break
		}
	}

	return nil
}

//GenerateInterlockingSchedules returns the starting schedule status with tasks and a list of new interlocking schedules
func (intr *Interlock) GenerateInterlockingSchedules(startingScheduleStatus models.ScheduleStatus,
	activeScheduleStatuses []models.ScheduleStatus) (models.Dispatch, []models.Schedule, error) {

	_ = intr.GetAreaNames(intr.blockID)
	err := intr.GetInfoFromOperations()
	if err != nil {
		log.WithError(err).Error("interlocking: Cannot get crane info.")
		return models.Dispatch{}, nil, fmt.Errorf("cannot get crane info %w", err)
	}

	log.Debugf("interlocking: GenerateInterlockingSchedules. startingSchedulStatus: %v, activeSchedulesStatuses: %v",
		startingScheduleStatus, activeScheduleStatuses)
	var interlockingSchedules []models.Schedule
	startingScheduleTasks, err := intr.translateToTasks(startingScheduleStatus, activeScheduleStatuses)
	if err != nil {
		log.WithError(err).Error("interlocking: Cannot create starting tasks.")
		return models.Dispatch{}, nil, fmt.Errorf("cannot create starting tasks %w", err)
	}

	cranes := make(map[string]models.StackingCraneStatusBase)
	currentCrane := models.StackingCraneStatusBase{}
	for _, resourceID := range intr.cranes {
		crane, err := intr.GetCraneStatus(resourceID)
		if err != nil {
			log.WithError(err).Error("interlocking: Get crane failed.")
			return models.Dispatch{}, nil, fmt.Errorf("Get crane with id %s failed %w", resourceID, err)
		}
		crane.StackingCraneID = resourceID
		if resourceID == startingScheduleStatus.WorkItem.StackingCraneID {
			currentCrane = crane
		}
		cranes[crane.StackingCraneID] = crane
	}

	// if active tasks is empty, check positions of other cranes.
	// Return if there is no collision. Create a new schedule if there is a collision
	if len(activeScheduleStatuses) == 0 {
		log.Debug("interlocking: no active tasks")
		for _, crane := range cranes {
			if crane.StackingCraneID == startingScheduleStatus.WorkItem.StackingCraneID {
				// new schedule's crane; ignore and continue
				continue
			}

			if crane.AssignedJobStatus.IsRunning {
				log.Errorf("Crane %s is running without any schedule.", crane.StackingCraneID)
				return models.Dispatch{}, nil, fmt.Errorf("Crane %s is running without any schedule", crane.StackingCraneID)
			}

			finalPoint, inRoute := intr.craneInRoute(crane, currentCrane, startingScheduleStatus)
			if inRoute {
				newWorkItemName := crane.StackingCraneID + "_" + startingScheduleStatus.WorkItem.WorkItemName
				var wi = models.OperationsWorkOrderItemBase{
					WorkOrderReference:           startingScheduleStatus.WorkItem.WorkOrderReference,
					WorkItemName:                 newWorkItemName,
					OrderType:                    "Preposition",
					CreationTime:                 time.Now(),
					UpdateTime:                   time.Time{},
					AckTime:                      time.Time{},
					Priority:                     0,
					ItemStatus:                   models.ENTERED,
					StackingCraneID:              crane.StackingCraneID,
					StartingPointLogicalLocation: models.LogicalLocation3D{},
					FinalPointLogicalLocation:    finalPoint,
				}
				sc := models.Schedule{
					WorkItem:        wi,
					ParentSchedule:  startingScheduleStatus.WorkItem.WorkItemName,
					ScheduleReqType: models.NEW_SCHE,
				}
				interlockingSchedules = append(interlockingSchedules, sc)
				log.Debugln("interlocking: a new schedule is created: ", sc)

				collidingTaskWait := internalDispatchTask{
					dispatchTask: models.DispatchTask{
						HandledItems: []models.Container{},
						TaskNr:       0,
						TaskState:    models.TS_READY,
						TaskType:     models.T_WAIT_ON_SCHEDULE,
						WaitedEvent: models.WaitedEvent{
							WaitedEventName: newWorkItemName,
						},
					},
					workItemName: startingScheduleStatus.WorkItem.WorkItemName,
				}

				var updatedSchTasks []internalDispatchTask
				updatedSchTasks = append(updatedSchTasks, collidingTaskWait)
				for _, task := range startingScheduleTasks {
					task.dispatchTask.TaskNr++
					updatedSchTasks = append(updatedSchTasks, task)
				}
				startingScheduleTasks = updatedSchTasks
			}
		}
		di, err := TranslateTasksToDispatch(startingScheduleStatus, startingScheduleTasks)
		if err != nil {
			log.WithError(err).Error("interlocking: TranslateTasksToDispatch failed.")
			return di, interlockingSchedules, fmt.Errorf("TranslateTasksToDispatch failed %w", err)
		}
		log.Debugf("interlocking: GenerateInterlockingSchedules is finished. dispatch: %v, interlockingSchedules: %v", di, interlockingSchedules)
		return di, interlockingSchedules, nil
	}

	var activeClaimAreas []claimArea
	for _, activeSchStatus := range activeScheduleStatuses {
		if startingScheduleStatus.WorkItem.OrderType == models.PREPOSITION {
			if startingScheduleStatus.WorkItem.WorkOrderReference == activeSchStatus.WorkItem.WorkOrderReference {
				log.Debug("interlocking: schedules belong to same work order")
				continue
			}
		}

		activeClaimArea := intr.calculateClaimArea(activeSchStatus, cranes[activeSchStatus.WorkItem.StackingCraneID])
		activeClaimAreas = append(activeClaimAreas, activeClaimArea)
	}

	startingClaimArea := intr.calculateClaimArea(startingScheduleStatus, currentCrane)

	startingScheduleTasks, interlockingSchedules, err = intr.findCollisions(activeClaimAreas, startingClaimArea, startingScheduleTasks, startingScheduleStatus, cranes)
	if err != nil {
		log.WithError(err).Error("interlocking: Cannot create crane tasks.")
		return models.Dispatch{}, nil, fmt.Errorf("cannot create crane tasks %w", err)
	}

	di, err := TranslateTasksToDispatch(startingScheduleStatus, startingScheduleTasks)
	if err != nil {
		log.WithError(err).Error("interlocking: TranslateTasksToDispatch failed.")
		return di, interlockingSchedules, fmt.Errorf("TranslateTasksToDispatch failed %w", err)
	}
	log.Debugf("interlocking: GenerateInterlockingSchedules is finished. dispatch: %v, interlockingSchedules: %v",
		di, interlockingSchedules)
	return di, interlockingSchedules, nil
}

// craneInRoute checks if the idle crain in collision route.
// If yes, returns the start and final positions for the crane
func (intr *Interlock) craneInRoute(crane models.StackingCraneStatusBase, currentCrane models.StackingCraneStatusBase, startingScheduleStatus models.ScheduleStatus) (models.LogicalLocation3D, bool) {
	startingClaimArea := intr.calculateClaimArea(startingScheduleStatus, currentCrane)

	finalPoint := startingScheduleStatus.WorkItem.FinalPointLogicalLocation
	cranePos := int64(crane.CraneMainAxesPosition.GantryPosition)

	if cranePos < startingClaimArea.minAbsPos || cranePos > startingClaimArea.maxAbsPos {
		log.Debugf("interlocking: crane is not on the route. Returning final point %s. Cranebay: %d", finalPoint, cranePos)
		return finalPoint, false
	}

	finalBay := convertAbsPositionToBay(startingClaimArea.minAbsPos-intr.bayWidth, intr.bayWidth)
	currentCranePos := int64(currentCrane.CraneMainAxesPosition.GantryPosition)
	if cranePos > currentCranePos {
		finalBay = convertAbsPositionToBay(startingClaimArea.maxAbsPos+intr.bayWidth, intr.bayWidth)
	}
	log.Debugf("interlocking: final bay is set: %d", finalBay)
	log.Debugf("interlocking: crane: %d", cranePos)
	log.Debugf("interlocking: starting claim area min pos: %d", startingClaimArea.minAbsPos)
	log.Debugf("interlocking: starting claim area max pos: %d", startingClaimArea.maxAbsPos)
	finalPoint.Bay = fmt.Sprintf("%03d", finalBay)
	finalPoint.Tier = "00"
	return finalPoint, true
}

// calculateClaimArea finds min and max bays for the given schedule status
func (intr *Interlock) calculateClaimArea(schStatus models.ScheduleStatus, crane models.StackingCraneStatusBase) claimArea {
	clArea := claimArea{
		workItemName:               schStatus.WorkItem.WorkItemName,
		resourceID:                 schStatus.WorkItem.StackingCraneID,
		destinationLogicalLocation: schStatus.WorkItem.FinalPointLogicalLocation,
	}

	startBay, _ := strconv.ParseInt(schStatus.WorkItem.StartingPointLogicalLocation.Bay, 10, 64)
	finalBay, _ := strconv.ParseInt(schStatus.WorkItem.FinalPointLogicalLocation.Bay, 10, 64)

	halfWidth := intr.bayWidth / 2
	startAbsPosition := startBay * halfWidth
	finalAbsPosition := finalBay * halfWidth

	clArea.minAbsPos = min(startAbsPosition, finalAbsPosition)
	clArea.maxAbsPos = max(startAbsPosition, finalAbsPosition)

	cranePos := int64(crane.CraneMainAxesPosition.GantryPosition)
	if schStatus.WorkItem.OrderType == models.PREPOSITION {
		// starting point is empty. Update initial point with crane info
		if cranePos > clArea.maxAbsPos {
			clArea.minAbsPos = clArea.maxAbsPos - halfWidth
			clArea.maxAbsPos = cranePos + halfWidth
		} else {
			clArea.minAbsPos = cranePos - halfWidth
		}
		return clArea
	}

	if cranePos < clArea.minAbsPos {
		clArea.minAbsPos = cranePos
	}
	if cranePos > clArea.maxAbsPos {
		clArea.maxAbsPos = cranePos
	}

	clArea.minAbsPos -= halfWidth
	clArea.maxAbsPos += halfWidth
	return clArea
}

// findCollisions calculates the collisions during operating the tasks between the given cranes
func (intr *Interlock) findCollisions(activeClaimAreas []claimArea,
	startingClaimArea claimArea, startingScheduleTasks []internalDispatchTask,
	startingScheduleStatus models.ScheduleStatus,
	cranes map[string]models.StackingCraneStatusBase) ([]internalDispatchTask, []models.Schedule, error) {

	updatedStartingScheduleTasks := startingScheduleTasks
	var schedules []models.Schedule

	for _, activeClArea := range activeClaimAreas {
		for _, task := range startingScheduleTasks {
			taskBay, _ := strconv.ParseInt(task.dispatchTask.DestinationLogicalLocation.Bay, 10, 64)
			taskAbsPos := taskBay * (intr.bayWidth / 2)
			if taskAbsPos >= activeClArea.minAbsPos && taskBay <= activeClArea.maxAbsPos {

				taskNr := task.dispatchTask.TaskNr
				baseLoc := startingScheduleStatus.WorkItem.StartingPointLogicalLocation
				if (baseLoc == models.LogicalLocation3D{}) {
					baseLoc = startingScheduleStatus.WorkItem.FinalPointLogicalLocation
				}
				if (baseLoc != models.LogicalLocation3D{}) {
					newBay := convertAbsPositionToBay(activeClArea.minAbsPos-intr.bayWidth, intr.bayWidth)
					newDestination := models.LogicalLocation3D{
						AreaName: baseLoc.AreaName,
						Bay:      fmt.Sprintf("%03d", newBay),
						Row:      baseLoc.Row,
						Tier:     "00",
					}
					if cranes[startingClaimArea.resourceID].CraneMainAxesPosition.GantryPosition > cranes[activeClArea.resourceID].CraneMainAxesPosition.GantryPosition {
						// TODO: this is a hack to understand the position of the crane, but there can be a better way
						newBay := convertAbsPositionToBay(activeClArea.maxAbsPos+intr.bayWidth, intr.bayWidth)
						newDestination.Bay = fmt.Sprintf("%03d", newBay)
					}
					collidingTaskMove := internalDispatchTask{
						dispatchTask: models.DispatchTask{
							DestinationLogicalLocation: newDestination,
							HandledItems:               task.dispatchTask.HandledItems,
							TaskNr:                     taskNr,
							TaskState:                  models.TS_READY,
							TaskType:                   models.T_MOVE,
						},
						workItemName: task.workItemName,
					}
					updatedStartingScheduleTasks = append(updatedStartingScheduleTasks, internalDispatchTask{})
					updatedStartingScheduleTasks = append(updatedStartingScheduleTasks, internalDispatchTask{})
					// move two steps, because we will add move and wait tasks
					copy(updatedStartingScheduleTasks[(taskNr+2):], updatedStartingScheduleTasks[taskNr:])
					updatedStartingScheduleTasks[taskNr] = collidingTaskMove
				}
				collidingTaskWait := internalDispatchTask{
					dispatchTask: models.DispatchTask{
						HandledItems: task.dispatchTask.HandledItems,
						TaskNr:       taskNr + 1,
						TaskState:    models.TS_READY,
						TaskType:     models.T_WAIT_ON_SCHEDULE,
						WaitedEvent: models.WaitedEvent{
							WaitedEventName: activeClArea.workItemName,
						},
					},
					workItemName: task.workItemName,
				}
				updatedStartingScheduleTasks[taskNr+1] = collidingTaskWait
				break // no need to continue for the next tasks; a wait is already added for the active schedule
			}
		}
	}

	// check for each task of starting schedule status if there is a collision with any of active tasks. If yes,
	// add a waiting task
	for _, activeClArea := range activeClaimAreas {
		// if the end position of the active schedule is inside starting schedule task,
		// then a move schedule should be added
		destinationBay, _ := strconv.ParseInt(activeClArea.destinationLogicalLocation.Bay, 10, 64)
		destinationPos := destinationBay * (intr.bayWidth / 2)
		if destinationPos >= startingClaimArea.minAbsPos && destinationPos <= startingClaimArea.maxAbsPos {
			destinationBay = convertAbsPositionToBay(startingClaimArea.minAbsPos-intr.bayWidth, intr.bayWidth)
			currentCranePos := int64(cranes[startingClaimArea.resourceID].CraneMainAxesPosition.GantryPosition)
			cranePos := int64(cranes[activeClArea.resourceID].CraneMainAxesPosition.GantryPosition)
			if cranePos > currentCranePos {
				destinationBay = convertAbsPositionToBay(startingClaimArea.maxAbsPos+intr.bayWidth, intr.bayWidth)
			}
			newDestination := models.LogicalLocation3D{
				AreaName: activeClArea.destinationLogicalLocation.AreaName,
				Bay:      fmt.Sprintf("%03d", destinationBay),
				Row:      activeClArea.destinationLogicalLocation.Row,
				Tier:     "00",
			}
			newWorkItemName := activeClArea.resourceID + "_" + startingClaimArea.workItemName

			wi := models.OperationsWorkOrderItemBase{
				WorkOrderReference:           startingScheduleStatus.WorkItem.WorkOrderReference,
				WorkItemName:                 newWorkItemName,
				OrderType:                    "Preposition",
				CreationTime:                 time.Now(),
				UpdateTime:                   time.Time{},
				AckTime:                      time.Time{},
				Priority:                     0,
				ItemStatus:                   models.ENTERED,
				StackingCraneID:              activeClArea.resourceID,
				StartingPointLogicalLocation: models.LogicalLocation3D{},
				FinalPointLogicalLocation:    newDestination,
			}
			sc := models.Schedule{
				WorkItem:        wi,
				ParentSchedule:  startingScheduleStatus.WorkItem.WorkItemName,
				ScheduleReqType: models.NEW_SCHE,
			}
			schedules = append(schedules, sc)
			log.Debugln("interlocking: a new schedule is created: ", sc)

			// add wait task to starting schedules
			collidingTaskWait := internalDispatchTask{
				dispatchTask: models.DispatchTask{
					HandledItems: []models.Container{},
					TaskNr:       0,
					TaskState:    models.TS_READY,
					TaskType:     models.T_WAIT_ON_SCHEDULE,
					WaitedEvent: models.WaitedEvent{
						WaitedEventName: newWorkItemName,
					},
				},
				workItemName: startingClaimArea.workItemName,
			}
			updatedStartingScheduleTasks = append(updatedStartingScheduleTasks, internalDispatchTask{})
			taskNr := 0
			copy(updatedStartingScheduleTasks[(taskNr+1):], updatedStartingScheduleTasks[taskNr:])
			updatedStartingScheduleTasks[taskNr] = collidingTaskWait

			// remove wait events for the active schedule, because we added another wait
			// for the schedule that we have created.
			for i := 0; i < len(updatedStartingScheduleTasks); i++ {
				if updatedStartingScheduleTasks[i].dispatchTask.WaitedEvent.WaitedEventName == activeClArea.workItemName {
					copy(updatedStartingScheduleTasks[i:], updatedStartingScheduleTasks[i+1:])
					updatedStartingScheduleTasks[len(updatedStartingScheduleTasks)-1] = internalDispatchTask{}
					updatedStartingScheduleTasks = updatedStartingScheduleTasks[:len(updatedStartingScheduleTasks)-1]
					if i-2 >= 0 && updatedStartingScheduleTasks[i-1].dispatchTask.TaskType == models.T_MOVE {
						if updatedStartingScheduleTasks[i-2].dispatchTask.WaitedEvent.WaitedEventName == newWorkItemName {
							tempTask := updatedStartingScheduleTasks[i-1]
							updatedStartingScheduleTasks[i-1] = updatedStartingScheduleTasks[i-2]
							updatedStartingScheduleTasks[i-2] = tempTask
						}
					}
				}
			}
		}
	}

	// update task numbers in the task list
	for i := 0; i < len(updatedStartingScheduleTasks); i++ {
		updatedStartingScheduleTasks[i].dispatchTask.TaskNr = int32(i)
	}
	return updatedStartingScheduleTasks, schedules, nil
}

// CheckContainerExists returns nil if container present in the starting location or any
// of the active schedules brings the container.
func (intr *Interlock) CheckContainerExists(wi models.OperationsWorkOrderItemBase, activeScheduleStatuses []models.ScheduleStatus) error {
	logicalLocation := wi.StartingPointLogicalLocation
	fieldName := logicalLocation.AreaName + "-" + logicalLocation.Bay + "-" + logicalLocation.Row + "-01"
	fieldStatus, err := intr.GetStackingFieldStatus(fieldName)
	if err != nil {
		// error received while retrieving the field status. Check active statuses if
		// any of them brings the container
		for _, activeSchedule := range activeScheduleStatuses {
			if activeSchedule.WorkItem.FinalPointLogicalLocation == logicalLocation &&
				(activeSchedule.WorkItem.OrderType == models.TRANSFER_IN ||
					activeSchedule.WorkItem.OrderType == models.INTRASTACK ||
					activeSchedule.WorkItem.OrderType == models.BASIC_PICKUP) {
				if activeSchedule.WorkItem.HandledItem.ContainerIDInfo.ContainerType != wi.HandledItem.ContainerIDInfo.ContainerType {
					return fmt.Errorf("Another type of container is found at the starting point")
				}
				if activeSchedule.WorkItem.HandledItem.ItemGUID != wi.HandledItem.ItemGUID {
					return fmt.Errorf("itemGUID does not match with the item that will be picked")
				}
				return nil
			}
		}
		return err
	}

	tier, _ := strconv.ParseInt(logicalLocation.Tier, 10, 64)
	if int64(fieldStatus.ItemCount) >= tier {
		if fieldStatus.ItemCount > 0 {
			if fieldStatus.StackedItems[0].ContainerType != wi.HandledItem.ContainerIDInfo.ContainerType {
				return fmt.Errorf("Another type of container is found at the starting point")
			}
		}
		for _, item := range fieldStatus.StackedItems {
			if item.ItemGUID == wi.HandledItem.ItemGUID {
				return nil
			}
		}
		return fmt.Errorf("No item with the itemGUID of the handled container found in the stacked items")
	}
	return fmt.Errorf("Container does not exist on location %s", logicalLocation)
}

//translateToTasks creates a dispatch request from given schedule
func (intr *Interlock) translateToTasks(st models.ScheduleStatus, activeScheduleStatuses []models.ScheduleStatus) ([]internalDispatchTask, error) {
	var tasks []internalDispatchTask

	err := intr.CheckTier(st.WorkItem)
	if err != nil {
		return tasks, fmt.Errorf("container location is not valid %w", err)
	}

	if (st.WorkItem.HandledItem != models.Container{}) {
		for _, name := range intr.saAreaNames {
			if st.WorkItem.FinalPointLogicalLocation.AreaName == name {
				err = intr.CheckDestinationValidForType(st.WorkItem, activeScheduleStatuses)
				if err != nil {
					return tasks, err
				}
				break
			}
		}
	}

	taskNr := int32(0)
	if st.WorkItem.OrderType != models.PREPOSITION {
		if existsInArray(intr.tzAreaNames, st.WorkItem.StartingPointLogicalLocation.AreaName) {
			if st.WorkItem.OrderType == models.TRANSFER_IN ||
				st.WorkItem.OrderType == models.BASIC_PICKUP ||
				st.WorkItem.OrderType == models.INTRASTACK {
				// first check resource id; if empty, return error
				if st.WorkItem.StartingTransportResource.ResourceID == "" {
					return nil, fmt.Errorf("Resource id for ht at starting position is empty")
				}
				// starting point is in transfer zone; add wait for truck task
				taskWait := internalDispatchTask{
					dispatchTask: models.DispatchTask{
						HandledItems: []models.Container{},
						TaskNr:       taskNr,
						TaskState:    models.TS_READY,
						TaskType:     models.T_WAIT_ON_TRUCK_PARK,
						WaitedEvent: models.WaitedEvent{
							WaitedEventName: st.WorkItem.StartingTransportResource.ResourceID,
						},
					},
					workItemName: st.WorkItem.WorkItemName,
				}
				taskNr++
				taskWait.dispatchTask.HandledItems = append(taskWait.dispatchTask.HandledItems, st.WorkItem.HandledItem)
				tasks = append(tasks, taskWait)
			}
		} else {
			err := intr.CheckContainerExists(st.WorkItem, activeScheduleStatuses)
			if err != nil {
				return nil, fmt.Errorf("Container does not exists %s", st.WorkItem.StartingPointLogicalLocation)
			}
		}
	}

	switch st.WorkItem.OrderType {
	case models.TRANSFER_IN, models.TRANSFER_OUT, models.INTRASTACK:
		// first check if container exists
		//create pick task
		task := internalDispatchTask{
			workItemName: st.WorkItem.WorkItemName,
			dispatchTask: models.DispatchTask{
				TaskNr:                     taskNr,
				DestinationLogicalLocation: st.WorkItem.StartingPointLogicalLocation,
				TaskState:                  models.TS_READY,
				TaskType:                   models.T_PICK,
			},
		}
		taskNr++
		task.dispatchTask.HandledItems = append(task.dispatchTask.HandledItems, st.WorkItem.HandledItem)
		tasks = append(tasks, task)

		if st.WorkItem.OrderType == models.TRANSFER_OUT {
			// first check resource id; if empty, return error
			if st.WorkItem.FinalTransportResource.ResourceID == "" {
				return nil, fmt.Errorf("Resource id for ht at final location is empty")
			}
			task := internalDispatchTask{
				dispatchTask: models.DispatchTask{
					HandledItems: []models.Container{},
					TaskNr:       taskNr,
					TaskState:    models.TS_READY,
					TaskType:     models.T_WAIT_ON_TRUCK_PARK,
					WaitedEvent: models.WaitedEvent{
						WaitedEventName: st.WorkItem.FinalTransportResource.ResourceID,
					},
				},
				workItemName: st.WorkItem.WorkItemName,
			}
			taskNr++
			task.dispatchTask.HandledItems = append(task.dispatchTask.HandledItems, st.WorkItem.HandledItem)
			tasks = append(tasks, task)
		}
		//create place order
		task = internalDispatchTask{
			workItemName: st.WorkItem.WorkItemName,
			dispatchTask: models.DispatchTask{
				TaskNr:                     taskNr,
				DestinationLogicalLocation: st.WorkItem.FinalPointLogicalLocation,
				TaskState:                  models.TS_READY,
				TaskType:                   models.T_PLACE,
			},
		}
		taskNr++
		task.dispatchTask.HandledItems = append(task.dispatchTask.HandledItems, st.WorkItem.HandledItem)
		tasks = append(tasks, task)

	case models.BASIC_PICKUP:
		//create pick task
		task := internalDispatchTask{
			workItemName: st.WorkItem.WorkItemName,
			dispatchTask: models.DispatchTask{
				TaskNr:                     taskNr,
				DestinationLogicalLocation: st.WorkItem.FinalPointLogicalLocation,
				TaskState:                  models.TS_READY,
				TaskType:                   models.T_PICK,
			},
		}
		taskNr++
		task.dispatchTask.HandledItems = append(task.dispatchTask.HandledItems, st.WorkItem.HandledItem)
		tasks = append(tasks, task)

	case models.BASIC_GROUNDING:
		//create place task
		if existsInArray(intr.tzAreaNames, st.WorkItem.FinalPointLogicalLocation.AreaName) {
			if st.WorkItem.FinalTransportResource.ResourceID == "" {
				return nil, fmt.Errorf("Resource id for ht at final location is empty")
			}
			task := internalDispatchTask{
				dispatchTask: models.DispatchTask{
					HandledItems: []models.Container{},
					TaskNr:       taskNr,
					TaskState:    models.TS_READY,
					TaskType:     models.T_WAIT_ON_TRUCK_PARK,
					WaitedEvent: models.WaitedEvent{
						WaitedEventName: st.WorkItem.FinalTransportResource.ResourceID,
					},
				},
				workItemName: st.WorkItem.WorkItemName,
			}
			taskNr++
			task.dispatchTask.HandledItems = append(task.dispatchTask.HandledItems, st.WorkItem.HandledItem)
			tasks = append(tasks, task)
		}
		task := internalDispatchTask{
			workItemName: st.WorkItem.WorkItemName,
			dispatchTask: models.DispatchTask{
				TaskNr:                     taskNr,
				DestinationLogicalLocation: st.WorkItem.FinalPointLogicalLocation,
				TaskState:                  models.TS_READY,
				TaskType:                   models.T_PLACE,
			},
		}
		taskNr++
		task.dispatchTask.HandledItems = append(task.dispatchTask.HandledItems, st.WorkItem.HandledItem)
		tasks = append(tasks, task)

	case models.PREPOSITION:
		//create move task
		task := internalDispatchTask{
			workItemName: st.WorkItem.WorkItemName,
			dispatchTask: models.DispatchTask{
				TaskNr:                     taskNr,
				DestinationLogicalLocation: st.WorkItem.FinalPointLogicalLocation,
				TaskState:                  models.TS_READY,
				TaskType:                   models.T_MOVE,
			},
		}
		taskNr++
		task.dispatchTask.HandledItems = append(task.dispatchTask.HandledItems, st.WorkItem.HandledItem)
		tasks = append(tasks, task)

	default:
		return nil, fmt.Errorf("not supported order type %s", st.WorkItem.OrderType)
	}

	return tasks, nil
}

//CheckDestinationValidForType checks if the destination address is valid for the given type
func (intr *Interlock) CheckDestinationValidForType(wi models.OperationsWorkOrderItemBase, activeScheduleStatuses []models.ScheduleStatus) error {

	widthStr := strconv.FormatInt(int64(wi.HandledItem.ItemDimensions.Width), 10)
	containerType := string(wi.HandledItem.ContainerIDInfo.ContainerType)
	originField, err := GetNameFromLocation(wi.StartingPointLogicalLocation)
	if err != nil {
		return fmt.Errorf("failed to get name from location %s err: %w", wi.StartingPointLogicalLocation, err)
	}
	destinationField, err := GetNameFromLocation(wi.FinalPointLogicalLocation)
	if err != nil {
		return fmt.Errorf("failed to get name from location %s err: %w", wi.FinalPointLogicalLocation, err)
	}

	req, err := http.NewRequest(http.MethodGet, intr.operationsURL+"/isdestinationvalid?width="+widthStr+"&containertype="+containerType+"&originfield="+originField+"&destinationfield="+destinationField, nil)
	if err != nil {
		return fmt.Errorf("send request failed %w", err)
	}

	body, err := intr.sendRequest(req)
	if err != nil {
		return fmt.Errorf("send request failed %w", err)
	}
	isValid := false
	err = json.Unmarshal(body, &isValid)

	if !isValid {
		return fmt.Errorf("Destination is not valid")
	}

	destBay, _ := strconv.ParseInt(wi.FinalPointLogicalLocation.Bay, 10, 64)
	currentContCenter := intr.fieldOffset + ((destBay-1)/2)*intr.bayWidth
	currContLeftmostPoint := int32(currentContCenter) - wi.HandledItem.ItemDimensions.Width/2

	// check active statuses
	for _, activeSchStatus := range activeScheduleStatuses {
		if wi.FinalPointLogicalLocation == activeSchStatus.WorkItem.FinalPointLogicalLocation {
			if containerType != string(activeSchStatus.WorkItem.HandledItem.ContainerIDInfo.ContainerType) {
				return fmt.Errorf("another type of container will be put on the destination %s", wi.FinalPointLogicalLocation)
			}
			continue
		}
		if wi.FinalPointLogicalLocation.Row == activeSchStatus.WorkItem.FinalPointLogicalLocation.Row {
			activeSchBay, _ := strconv.ParseInt(activeSchStatus.WorkItem.FinalPointLogicalLocation.Bay, 10, 64)
			actScheContCenter := intr.fieldOffset + ((activeSchBay-1)/2)*intr.bayWidth
			actScheContLeftmostPoint := int32(actScheContCenter) - activeSchStatus.WorkItem.HandledItem.ItemDimensions.Width/2
			if isOverlap(currContLeftmostPoint, wi.HandledItem.ItemDimensions.Width, actScheContLeftmostPoint, activeSchStatus.WorkItem.HandledItem.ItemDimensions.Width) {
				return fmt.Errorf("overlaping with the container in the active work item")
			}
		}
	}

	return nil
}

//CheckTier checks if the destination location is valid
func (intr *Interlock) CheckTier(wi models.OperationsWorkOrderItemBase) error {
	destination := wi.FinalPointLogicalLocation
	tier, err := strconv.Atoi(destination.Tier)

	if err != nil {
		return err
	}

	if tier == 1 {
		return nil
	}

	maxTier := maxSaTier
	if wi.OrderType == models.TRANSFER_OUT {
		maxTier = maxTzTier
	}

	if tier > maxTier {
		return fmt.Errorf("tier is too high for this type. Current tier: %d, max allowed tier: %d", tier, maxTier)
	}

	return nil
}

//TranslateTasksToDispatch creates a dispatch request from given task
func TranslateTasksToDispatch(st models.ScheduleStatus, tasks []internalDispatchTask) (models.Dispatch, error) {

	dispatch := models.Dispatch{
		StackingCraneID:  st.WorkItem.StackingCraneID,
		DispatchPriority: st.WorkItem.Priority,
		DispatchName:     st.WorkItem.WorkItemName,
		DispatchNr:       int32(time.Now().Unix()),
	}

	for _, task := range tasks {
		dispatch.DispatchTasks = append(dispatch.DispatchTasks, task.dispatchTask)
	}
	dispatch.DispatchReqType = models.NEW_DISP

	return dispatch, nil
}

//GetNameFromLocation converts give area, bay, row and tier to composite field name
func GetNameFromLocation(location models.LogicalLocation3D) (string, error) {
	// this composite name is used in fieldstatus; and only tier 01 is used in the db.
	// Occupancy of other tiers are represented by item count
	nameComposite := location.AreaName + "-" + location.Bay + "-" + location.Row + "-01"
	if len(nameComposite) != compositeLocationNameLength {
		return "", fmt.Errorf("invalid name %s", nameComposite)
	}
	return nameComposite, nil
}

func min(a, b int64) int64 {
	if a < b {
		return a
	}
	return b
}

func max(a, b int64) int64 {
	if a > b {
		return a
	}
	return b
}

func existsInArray(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func convertAbsPositionToBay(cranePos int64, bayWidth int64) int64 {
	bay := (cranePos / bayWidth) * 2
	return bay
}

//isOverlap checks if 2 containers overlap
func isOverlap(X1 int32, Width1 int32, X2 int32, Width2 int32) bool {
	xOverlap := ((X1 <= X2) && (X2 <= X1+Width1)) || ((X2 <= X1) && (X1 <= X2+Width2))
	return xOverlap
}
