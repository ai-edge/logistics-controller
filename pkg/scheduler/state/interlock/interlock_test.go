package interlock

import (
	"ecs/api/generated/models"
	"ecs/internal/orders"
	"fmt"
	"net/http"
	"strconv"
	"testing"
	"time"

	"github.com/google/uuid"
)

//TestInterlock returns a new interlock
func GetTestInterlock() (*Interlock, error) {
	interlock := &Interlock{operationsURL: "", dispatcherURL: "", bayWidth: int64(6458), blockID: "BL01"}
	interlock.client = http.DefaultClient

	return interlock, nil
}

func createWorkItem(resourceID string, orderType models.OrderTypes, root models.WorkItemRoot) (models.OperationsWorkOrderItemBase, error) {
	var currentTime = int32(time.Now().Unix())
	var wi = models.OperationsWorkOrderItemBase{}
	wi.WorkItemName = "wic" + resourceID + strconv.Itoa(int(currentTime))
	wi.CreationTime = time.Now()
	wi.UpdateTime = time.Time{}
	wi.AckTime = time.Time{}
	wi.Priority = root.Priority
	wi.ItemStatus = models.ENTERED
	wi.StackingCraneID = resourceID

	wi.StartingPointLogicalLocation = root.From
	wi.FinalPointLogicalLocation = root.To
	wi.OrderType = orderType

	guid := uuid.New().String()
	isoid := "BBBU111111" + strconv.Itoa(int(currentTime))
	newCon, err := orders.GenerateContainer(root.ContainerType, wi.StartingPointLogicalLocation, guid, isoid)
	if err != nil {
		return wi, fmt.Errorf("container generation failed %w", err)
	}
	wi.HandledItem = newCon

	switch wi.OrderType {
	case models.TRANSFER_IN:
		wi.StartingTransportResource = models.HtOrderRes{
			ResourceType:       "CTR",
			ResourceID:         "CTR" + wi.WorkItemName,
			TrailerInformation: []models.ContainerTrailerInformation{},
			HtType:             "InternalTruck",
		}
		wi.StartingChasisInformation = models.TargetChasisInformation{
			TargetChasis:           0,
			TargetLocationOnChasis: "empty",
		}
	case models.TRANSFER_OUT:
		wi.FinalTransportResource = models.HtOrderRes{
			ResourceType:       "CTR",
			ResourceID:         "CTR" + wi.WorkItemName,
			TrailerInformation: []models.ContainerTrailerInformation{},
			HtType:             "InternalTruck",
		}
		wi.FinalChasisInformation = models.TargetChasisInformation{
			TargetChasis:           0,
			TargetLocationOnChasis: "empty",
		}
	}
	return wi, nil
}

func TestCraneInRouteWhenCraneInRoute(t *testing.T) {

	intr, err := GetTestInterlock()
	if err != nil {
		t.Errorf("GetTestInterlock() = %v, %s", intr, err)
	}

	root := models.WorkItemRoot{
		ContainerType: models.C20_G0,
		From: models.LogicalLocation3D{
			AreaName: "TZ01",
			Bay:      "005",
			Row:      "001",
			Tier:     "01"},
		To: models.LogicalLocation3D{
			AreaName: "BA01",
			Bay:      "009",
			Row:      "006",
			Tier:     "01"},
		Priority: 6,
	}
	wi, err := createWorkItem("RTG002", models.TRANSFER_IN, root)
	if err != nil {
		t.Errorf("failed in createWorkItem: %s", err)
	}
	schStatus := models.ScheduleStatus{
		WorkItem:      wi,
		ScheduleState: models.S_WAITING,
	}

	crane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 20000,
		},
	}
	currentCrane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 16145,
		},
	}
	finalPoint, inRoute := intr.craneInRoute(crane, currentCrane, schStatus)
	if !inRoute {
		t.Errorf("craneInRoute failed.")
	}
	t.Logf("starting bay: %s, final bay: %s", wi.StartingPointLogicalLocation.Bay, wi.FinalPointLogicalLocation.Bay)
	t.Logf("final point: %v", finalPoint)
}

func TestCraneInRouteWhenCraneNotInRoute(t *testing.T) {

	intr, err := GetTestInterlock()
	if err != nil {
		t.Errorf("GetTestInterlock() = %v, %s", intr, err)
	}

	root := models.WorkItemRoot{
		ContainerType: models.C20_G0,
		From: models.LogicalLocation3D{
			AreaName: "TZ01",
			Bay:      "001",
			Row:      "001",
			Tier:     "01"},
		To: models.LogicalLocation3D{
			AreaName: "BA01",
			Bay:      "007",
			Row:      "003",
			Tier:     "01"},
		Priority: 6,
	}
	wi, err := createWorkItem("RTG001", models.TRANSFER_IN, root)
	if err != nil {
		t.Errorf("failed in createWorkItem: %s", err)
	}
	schStatus := models.ScheduleStatus{
		WorkItem:      wi,
		ScheduleState: models.S_WAITING,
	}

	crane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 35519,
		},
	}
	currentCrane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 16145,
		},
	}
	finalPoint, inRoute := intr.craneInRoute(crane, currentCrane, schStatus)
	if inRoute {
		t.Errorf("craneInRoute failed.")
	}
	t.Logf("starting bay: %s, final bay: %s", wi.StartingPointLogicalLocation.Bay, wi.FinalPointLogicalLocation.Bay)
	t.Logf("final point: %v", finalPoint)
}

func TestCraneInRouteWhenCraneInRoute2(t *testing.T) {

	intr, err := GetTestInterlock()
	if err != nil {
		t.Errorf("GetTestInterlock() = %v, %s", intr, err)
	}

	root := models.WorkItemRoot{
		ContainerType: models.C20_G0,
		From: models.LogicalLocation3D{
			AreaName: "TZ01",
			Bay:      "003",
			Row:      "001",
			Tier:     "01"},
		To: models.LogicalLocation3D{
			AreaName: "BA01",
			Bay:      "005",
			Row:      "002",
			Tier:     "01"},
		Priority: 6,
	}
	wi, err := createWorkItem("RTG001", models.TRANSFER_IN, root)
	if err != nil {
		t.Errorf("failed in createWorkItem: %s", err)
	}
	schStatus := models.ScheduleStatus{
		WorkItem:      wi,
		ScheduleState: models.S_WAITING,
	}

	crane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 19374,
		},
	}
	currentCrane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 14187,
		},
	}
	finalPoint, inRoute := intr.craneInRoute(crane, currentCrane, schStatus)
	if !inRoute {
		t.Errorf("craneInRoute failed.")
	}
	t.Logf("starting bay: %s, final bay: %s", wi.StartingPointLogicalLocation.Bay, wi.FinalPointLogicalLocation.Bay)
	t.Logf("final point: %v", finalPoint)
}

func TestCraneInRouteWhenCraneInRoute3(t *testing.T) {

	intr, err := GetTestInterlock()
	if err != nil {
		t.Errorf("GetTestInterlock() = %v, %s", intr, err)
	}

	root := models.WorkItemRoot{
		ContainerType: models.C40_G0,
		From: models.LogicalLocation3D{
			AreaName: "TZ01",
			Bay:      "006",
			Row:      "001",
			Tier:     "01"},
		To: models.LogicalLocation3D{
			AreaName: "BA01",
			Bay:      "005",
			Row:      "002",
			Tier:     "01"},
		Priority: 6,
	}
	wi, err := createWorkItem("RTG001", models.TRANSFER_IN, root)
	if err != nil {
		t.Errorf("failed in createWorkItem: %s", err)
	}
	schStatus := models.ScheduleStatus{
		WorkItem:      wi,
		ScheduleState: models.S_WAITING,
	}

	crane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 9687,
		},
	}
	currentCrane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 3229,
		},
	}
	finalPoint, inRoute := intr.craneInRoute(crane, currentCrane, schStatus)
	if !inRoute {
		t.Errorf("craneInRoute failed.")
	}
	t.Logf("starting bay: %s, final bay: %s", wi.StartingPointLogicalLocation.Bay, wi.FinalPointLogicalLocation.Bay)
	t.Logf("final point: %v", finalPoint)
}

func TestCraneInRouteWhenCraneInRoute4(t *testing.T) {

	intr, err := GetTestInterlock()
	if err != nil {
		t.Errorf("GetTestInterlock() = %v, %s", intr, err)
	}

	root := models.WorkItemRoot{
		ContainerType: models.C40_G0,
		From: models.LogicalLocation3D{
			AreaName: "TZ01",
			Bay:      "003",
			Row:      "001",
			Tier:     "01"},
		To: models.LogicalLocation3D{
			AreaName: "BA01",
			Bay:      "013",
			Row:      "001",
			Tier:     "01"},
		Priority: 6,
	}
	wi, err := createWorkItem("RTG002", models.TRANSFER_IN, root)
	if err != nil {
		t.Errorf("failed in createWorkItem: %s", err)
	}
	schStatus := models.ScheduleStatus{
		WorkItem:      wi,
		ScheduleState: models.S_WAITING,
	}

	crane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 6458,
		},
	}
	currentCrane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 48435,
		},
	}
	finalPoint, inRoute := intr.craneInRoute(crane, currentCrane, schStatus)
	if !inRoute {
		t.Errorf("craneInRoute failed.")
	}
	t.Logf("starting bay: %s, final bay: %s", wi.StartingPointLogicalLocation.Bay, wi.FinalPointLogicalLocation.Bay)
	t.Logf("final point: %v", finalPoint)
}

func TestCalculateClaimAreaWi1(t *testing.T) {
	intr, err := GetTestInterlock()
	if err != nil {
		t.Errorf("GetTestInterlock() = %v, %s", intr, err)
	}

	root := models.WorkItemRoot{
		ContainerType: models.C20_G0,
		From: models.LogicalLocation3D{
			AreaName: "TZ01",
			Bay:      "001",
			Row:      "001",
			Tier:     "01"},
		To: models.LogicalLocation3D{
			AreaName: "BA01",
			Bay:      "007",
			Row:      "003",
			Tier:     "01"},
		Priority: 6,
	}
	wi, err := createWorkItem("RTG001", models.TRANSFER_IN, root)
	if err != nil {
		t.Errorf("failed in createWorkItem: %s", err)
	}
	schStatus := models.ScheduleStatus{
		WorkItem:      wi,
		ScheduleState: models.S_WAITING,
	}

	crane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 9687,
		},
	}

	claimArea := intr.calculateClaimArea(schStatus, crane)
	if claimArea.minAbsPos != int64(0) || claimArea.maxAbsPos != int64(25832) {
		t.Errorf("claim area is wrong: %v", claimArea)
	}
}

func TestCalculateClaimAreaWi2(t *testing.T) {
	intr, err := GetTestInterlock()
	if err != nil {
		t.Errorf("GetTestInterlock() = %v, %s", intr, err)
	}

	root := models.WorkItemRoot{
		ContainerType: models.C20_G0,
		From: models.LogicalLocation3D{
			AreaName: "TZ01",
			Bay:      "005",
			Row:      "001",
			Tier:     "01"},
		To: models.LogicalLocation3D{
			AreaName: "BA01",
			Bay:      "009",
			Row:      "006",
			Tier:     "01"},
		Priority: 6,
	}
	wi, err := createWorkItem("RTG002", models.TRANSFER_IN, root)
	if err != nil {
		t.Errorf("failed in createWorkItem: %s", err)
	}
	schStatus := models.ScheduleStatus{
		WorkItem:      wi,
		ScheduleState: models.S_WAITING,
	}

	crane := models.StackingCraneStatusBase{
		CraneMainAxesPosition: models.LiftingDevicePosition{
			GantryPosition: 19374,
		},
	}

	claimArea := intr.calculateClaimArea(schStatus, crane)
	if claimArea.minAbsPos != int64(12916) || claimArea.maxAbsPos != int64(32290) {
		t.Errorf("claim area is wrong: %v", claimArea)
	}
}

func TestCheckDestinationValidForTypeFail(t *testing.T) {
	intr, err := GetTestInterlock()
	if err != nil {
		t.Errorf("GetTestInterlock() = %v, %s", intr, err)
	}

	root := models.WorkItemRoot{
		ContainerType: models.C20_G0,
		From: models.LogicalLocation3D{
			AreaName: "TZ01",
			Bay:      "010",
			Row:      "001",
			Tier:     "01"},
		To: models.LogicalLocation3D{
			AreaName: "BA01",
			Bay:      "008",
			Row:      "001",
			Tier:     "01"},
		Priority: 6,
	}
	wi1, err := createWorkItem("RTG001", models.TRANSFER_IN, root)
	if err != nil {
		t.Errorf("failed in createWorkItem: %s", err)
	}
	activeStatus := models.ScheduleStatus{
		WorkItem:      wi1,
		ScheduleState: models.S_IN_PROGRESS,
	}

	var activeStatuses []models.ScheduleStatus
	activeStatuses = append(activeStatuses, activeStatus)

	root = models.WorkItemRoot{
		ContainerType: models.C20_G0,
		From: models.LogicalLocation3D{
			AreaName: "TZ01",
			Bay:      "014",
			Row:      "001",
			Tier:     "01"},
		To: models.LogicalLocation3D{
			AreaName: "BA01",
			Bay:      "007",
			Row:      "001",
			Tier:     "01"},
		Priority: 6,
	}
	wi2, err := createWorkItem("RTG002", models.TRANSFER_IN, root)
	if err != nil {
		t.Errorf("failed in createWorkItem: %s", err)
	}

	intr.tzAreaNames = append(intr.tzAreaNames, "TZ01")
	err = intr.CheckDestinationValidForType(wi2, activeStatuses)
	if err == nil {
		t.Errorf("CheckDestinationValidForType(..) failed. Should have generated error")
	}
}
