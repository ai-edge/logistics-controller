//Package asyncsrv consumes incoming async messages
package asyncsrv

import (
	"ecs/api/generated/models"
	"ecs/internal/rabbitnamings"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type schedulerService interface {
	NotifyScheduleRequest(models.Schedule) error
	NotifyDispatchStatusUpdate(models.DispatchStatus) error
}

//Asyncsrv is the async service
type Asyncsrv struct {
	L3ScheduleCh       <-chan amqp.Delivery
	L3DispatchStatusCh <-chan amqp.Delivery
	connClosedCh       <-chan *amqp.Error
	subCloseCh         <-chan *amqp.Error
	subCancelCh        <-chan string

	stopCh chan bool

	amqpWrap *amqpwrap.AMQPWrapper

	sch     schedulerService
	blockID string
}

//NewAsyncsrv returns a new async service. Creates exchanges only for consumed messages.
func NewAsyncsrv(blockID string, sch schedulerService, amqpWrap *amqpwrap.AMQPWrapper) (*Asyncsrv, error) {
	asyncsrv := Asyncsrv{amqpWrap: amqpWrap, sch: sch, blockID: blockID}
	var err error

	asyncsrv.stopCh = make(chan bool)

	//subscribe to scheduler/{blockid}/schedulerequest
	route := rabbitnamings.ScheRouteBase + "." + blockID + "." + rabbitnamings.ScheduleRequestRouteKey
	asyncsrv.L3ScheduleCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.ScheduleRequestRouteKey, //queue name contains route key
		rabbitnamings.ScheRouteBase,                                   //exchange name
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to dispatcher/{blockid}/dispatchstatus
	route = rabbitnamings.DispRouteBase + "." + blockID + "." + rabbitnamings.DispatchStatusRouteKey
	asyncsrv.L3DispatchStatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.DispatchStatusRouteKey, //queue name contains route key
		rabbitnamings.DispRouteBase,                                  //exchange name e.g. "amq.topic" for mqtt
		route)

	if err != nil {
		return nil, err
	}

	asyncsrv.connClosedCh = amqpWrap.GetConnCloseChan()
	asyncsrv.subCloseCh = amqpWrap.GetSubCloseChan()
	asyncsrv.subCancelCh = amqpWrap.GetSubCancelChan()

	return &asyncsrv, nil
}

//DeInit closes connection
func (as *Asyncsrv) DeInit() {
	as.stopCh <- true
}

//Asyncsrv is the main loop for consuming incoming async messages
func (as *Asyncsrv) Asyncsrv() error {

	for {
		select {
		//dispatch status event
		case delivery := <-as.L3DispatchStatusCh:
			dstatus := models.DispatchStatus{}
			err := json.Unmarshal(delivery.Body, &dstatus)
			if err != nil {
				log.WithError(err).Errorln("unmarshal dispatch status failed for delivery", delivery)
			} else {
				log.Debugln("receiveddispatch status", dstatus.DispatchName, dstatus.StackingCraneID, dstatus.DispatchState)
				log.Traceln("dispatch status", dstatus)
				err = as.sch.NotifyDispatchStatusUpdate(dstatus)
				if err != nil {
					log.WithError(err).Error("error in NotifyDispatchStatusUpdate")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		case delivery := <-as.L3ScheduleCh:
			sche := models.Schedule{}
			err := json.Unmarshal(delivery.Body, &sche)
			if err != nil {
				log.WithError(err).Errorln("unmarshal schedule failed for delivery", delivery)
			} else {
				log.Debugln("received Schedule Request:", sche)
				err = as.sch.NotifyScheduleRequest(sche)
				if err != nil {
					log.WithError(err).Error("error in NotifyScheduleRequest")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		case <-as.stopCh:
			log.Warningln("received stop", as.blockID)
			return nil

		case err := <-as.connClosedCh:
			if err != nil {
				log.WithError(err).Errorf("connection error. Shutting down %s", as.blockID)
			} else {
				log.Warningf("connection closed %s. Shutting down", as.blockID)
			}
			panic(fmt.Errorf("connection closed %s", as.blockID))

		case err := <-as.subCloseCh:
			log.WithError(err).Errorf("channel error. Shutting down %s", as.blockID)
			panic(fmt.Errorf("channel closed %s", as.blockID))

		case msg := <-as.subCancelCh:
			log.Errorf("channel canceled. Shutting down %s", msg)
			panic(fmt.Errorf("channel canceled %s", as.blockID))

		}
	}
}
