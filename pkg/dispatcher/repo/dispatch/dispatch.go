package dispatch

import (
	"ecs/api/generated/models"
	"ecs/internal/dbservice"
	"fmt"
)

//Dispatch repository for dispatcher
type Dispatch struct {
	//ServiceID is the id of the service created the repository
	ServiceID string
	//BlockID is the id of the block of that repository
	BlockID string
	dbNames dispatchDBNames

	dbs dbservice.DBService

	//ResourceID of this dispatcher
	ResID string
}

type dispatchDBNames struct {
	//DB names
	dispatchDB string
	//Collection Names
	itemsCollectionName,
	dispatchStatusCollectionName string
	//queryLimit maximum items to be returned
	queryLimit int64
	//IDField defines the name of the ID field in the DB
	IDField string
}

//NewDispatchRepository returns a new repository of a dispatcher for given block
func NewDispatchRepository(dbs dbservice.DBService, serviceID string, blockID string, resID string) (*Dispatch, error) {

	if dbs == nil || serviceID == "" {
		return nil, fmt.Errorf("invalid arguments dbs % serviceID %s", dbs, serviceID)
	}
	var dbNames dispatchDBNames

	dbNames.dispatchDB = blockID + "-dispatch"

	dbNames.itemsCollectionName = "items"
	dbNames.dispatchStatusCollectionName = "dispatchstatus"
	dbNames.queryLimit = 1000
	dbNames.IDField = "_id"

	dispatch := Dispatch{dbs: dbs, ServiceID: serviceID, BlockID: blockID, dbNames: dbNames, ResID: resID}

	return &dispatch, nil
}

//DeInit closes connection
func (disRepo *Dispatch) DeInit() {
	disRepo.dbs.DeInit()
}

//GetQueryLimit returns query limit parameter
func (disRepo *Dispatch) GetQueryLimit() int64 {
	return disRepo.dbNames.queryLimit
}

/**************DispatchStatus Related*****************/

//GetExecutingDispatchStatuses returns list of executing dispatch
func (disRepo *Dispatch) GetExecutingDispatchStatuses(craneid string) ([]models.DispatchStatus, error) {
	var disps []models.DispatchStatus
	dispReady, _ := disRepo.GetDispatchStatusesByState(craneid, models.D_READY, disRepo.GetQueryLimit())
	disps = append(disps, dispReady...)
	dispInit, _ := disRepo.GetDispatchStatusesByState(craneid, models.D_INITIALIZE, disRepo.GetQueryLimit())
	disps = append(disps, dispInit...)
	dispBlocked, _ := disRepo.GetDispatchStatusesByState(craneid, models.D_BLOCKED, disRepo.GetQueryLimit())
	disps = append(disps, dispBlocked...)
	dspEntered, _ := disRepo.GetDispatchStatusesByState(craneid, models.D_ENTERED, disRepo.GetQueryLimit())
	disps = append(disps, dspEntered...)
	dispRunning, _ := disRepo.GetDispatchStatusesByState(craneid, models.D_RUNNING, disRepo.GetQueryLimit())
	disps = append(disps, dispRunning...)
	dispCompleted, _ := disRepo.GetDispatchStatusesByState(craneid, models.D_COMPLETED, disRepo.GetQueryLimit())
	disps = append(disps, dispCompleted...)
	return disps, nil
}

// PutDispatchStatusByNr puts a work order item status
func (disRepo *Dispatch) PutDispatchStatusByNr(dispatchnr int32, newStatus models.DispatchStatus) (models.DispatchStatus, error) {
	var status models.DispatchStatus
	query := map[string]interface{}{"dispatchnr": dispatchnr}
	err := disRepo.dbs.GetOne(disRepo.dbNames.dispatchDB, disRepo.dbNames.dispatchStatusCollectionName, query, &status)
	if err != nil {
		err = disRepo.dbs.InsertOne(disRepo.dbNames.dispatchDB, disRepo.dbNames.dispatchStatusCollectionName, &newStatus)
	} else {
		err = disRepo.dbs.ReplaceOne(disRepo.dbNames.dispatchDB, disRepo.dbNames.dispatchStatusCollectionName, query, &newStatus)
	}
	return newStatus, err
}

// GetDispatchStatusByNr gets dispatch status
func (disRepo *Dispatch) GetDispatchStatusByNr(dispatchNr int32) (models.DispatchStatus, error) {
	var dispSt models.DispatchStatus
	query := map[string]interface{}{"dispatchnr": dispatchNr}
	err := disRepo.dbs.GetOne(disRepo.dbNames.dispatchDB, disRepo.dbNames.dispatchStatusCollectionName, query, &dispSt)
	return dispSt, err
}

// GetDispatchStatusByID gets dispatch status
func (disRepo *Dispatch) GetDispatchStatusByID(dispatchid string) (models.DispatchStatus, error) {
	var dispSt models.DispatchStatus
	query := map[string]interface{}{"dispatchname": dispatchid}
	err := disRepo.dbs.GetOne(disRepo.dbNames.dispatchDB, disRepo.dbNames.dispatchStatusCollectionName, query, &dispSt)
	return dispSt, err
}

// DeleteDispatchStatusesByID deletes dispatch statuses
func (disRepo *Dispatch) DeleteDispatchStatusesByID(dispatchid string) error {
	query := map[string]interface{}{"dispatchname": dispatchid}
	err := disRepo.dbs.DeleteMany(disRepo.dbNames.dispatchDB, disRepo.dbNames.dispatchStatusCollectionName, query)
	return err
}

// DeleteDispatchStatusByID deletes a dispatchstatus
func (disRepo *Dispatch) DeleteDispatchStatusByID(dispatchid string) error {
	query := map[string]interface{}{"dispatchname": dispatchid}
	err := disRepo.dbs.DeleteOne(disRepo.dbNames.dispatchDB, disRepo.dbNames.dispatchStatusCollectionName, query)
	return err
}

// GetDispatchStatuses gets all dispatchtatuses
func (disRepo *Dispatch) GetDispatchStatuses(limit int64) ([]models.DispatchStatus, error) {
	var statuses []models.DispatchStatus
	query := map[string]interface{}{}
	err := disRepo.dbs.GetAll(disRepo.dbNames.dispatchDB, disRepo.dbNames.dispatchStatusCollectionName, query, &statuses, limit)
	return statuses, err
}

// GetDispatchStatusesByState gets dispatch statuses by state
func (disRepo *Dispatch) GetDispatchStatusesByState(craneid string, state models.DispatchState, limit int64) ([]models.DispatchStatus, error) {
	var statuses []models.DispatchStatus
	query := map[string]interface{}{
		"$and": []map[string]interface{}{
			map[string]interface{}{"dispatchstate": state},
			map[string]interface{}{"stackingcraneid": craneid},
		},
	}
	err := disRepo.dbs.GetAll(disRepo.dbNames.dispatchDB, disRepo.dbNames.dispatchStatusCollectionName, query, &statuses, limit)
	return statuses, err
}
