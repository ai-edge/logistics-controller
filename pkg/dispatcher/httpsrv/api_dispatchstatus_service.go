/*
 * Dispatcher REST API
 *
 * Dispatcher Service REST API for simulation
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package httpsrv

import (
	"ecs/pkg/dispatcher/repo/dispatch"
)

// DispatchstatusApiService is a service that implents the logic for the DispatchstatusApiServicer
// This service should implement the business logic for every endpoint for the DispatchstatusApi API.
// Include any external packages or services that will be required by this service.
type DispatchstatusApiService struct {
	disRepo *dispatch.Dispatch
}

// NewDispatchstatusApiService creates a default api service
func NewDispatchstatusApiService(disRepo *dispatch.Dispatch) DispatchstatusApiServicer {
	return &DispatchstatusApiService{disRepo: disRepo}
}

// GetDispatchStatus - Get status of dispatch to be dispatched
func (s *DispatchstatusApiService) GetDispatchStatus(limit int64) (interface{}, error) {
	return s.disRepo.GetDispatchStatuses(limit)
}

// GetDispatchStatusByID - Get info about the status of a dispatch
func (s *DispatchstatusApiService) GetDispatchStatusByID(dispatchid string) (interface{}, error) {
	return s.disRepo.GetDispatchStatusByID(dispatchid)
}
