//Package state maintains a dispatcher state machine for every resource
package state

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
)

type active struct {
	stContext *HSM
}

func (state *active) enterState() error {

	return nil
}

func (state *active) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *active) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {

	state.stContext.dispatchSTM.SetLastGantryPos(st.CraneMainAxesPosition.GantryPosition)

	switch st.ResourceState {
	case models.R_STARTUP, models.R_OFF, models.R_FAULT:
		log.Errorln("receivedresource status", st.ResourceState, " in state", reflect.TypeOf(state).String())
		defer state.stContext.dispatchSTM.GoToNextState(models.D_FAULT)
		defer state.stContext.GoToNextState(models.DISP_SUSPENDED)
		return true, nil
	case models.R_MOVING:
	case models.R_IDLE:
		if state.stContext.dispatchSTM.IsFinished() {
			defer state.stContext.dispatchSTM.GoToNextState(models.D_VOID)
			defer state.stContext.GoToNextState(models.DISP_IDLE)
		}
	default:
		return true, fmt.Errorf("unspported resource state: %s in state %s", st.ResourceState, reflect.TypeOf(state).String())
	}
	return true, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *active) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	switch di.DispatchReqType {
	case models.DELETE_DISP:
		//unsafe, deletes a dispatch
		log.Warningf("received request %s in dispatch state: %s", di.DispatchReqType, reflect.TypeOf(state).String())
		if state.stContext.dispatchSTM.GetDispatchStatus().DispatchName == di.DispatchName {
			log.Warningf("%s, %s dispatch in progress. name %s", di.DispatchReqType, reflect.TypeOf(state).String(), di.DispatchName)
			defer state.stContext.dispatchSTM.GoToNextState(models.D_VOID)
			defer state.stContext.GoToNextState(models.DISP_IDLE)
		}
		return true, state.stContext.disRepo.DeleteDispatchStatusesByID(di.DispatchName)
	case models.ABORT_DISP:
		log.Warningf("received request %s in dispatch state: %s", di.DispatchReqType, reflect.TypeOf(state).String())
		if state.stContext.dispatchSTM.GetDispatchStatus().DispatchName != di.DispatchName {
			return true, fmt.Errorf("%s, %s invalid name %s", di.DispatchReqType, reflect.TypeOf(state).String(), di.DispatchName)
		}
		defer state.stContext.dispatchSTM.GoToNextState(models.D_ABORTED)
		//TODO: inform the resource simulator
		return true, nil
	case models.CANCEL_DISP:
		log.Warningf("received request %s in dispatch state: %s", di.DispatchReqType, reflect.TypeOf(state).String())
		if state.stContext.dispatchSTM.GetDispatchStatus().DispatchName != di.DispatchName {
			return true, fmt.Errorf("%s, %s invalid name %s", di.DispatchReqType, reflect.TypeOf(state).String(), di.DispatchName)
		}
		defer state.stContext.dispatchSTM.GoToNextState(models.D_CANCELED)
		//TODO: inform the resource simulator
		return true, nil
	case models.NEW_DISP:
		return true, fmt.Errorf("invalid request %s in dispatch state: %s", di.DispatchReqType, reflect.TypeOf(state).String())

	default:
		return true, fmt.Errorf("invalid request %s in dispatch state: %s", di.DispatchReqType, reflect.TypeOf(state).String())

	}
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *active) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	log.Traceln("HandleDispatchStatusUpdate", dst)
	switch dst.DispatchState {
	case models.D_CANCELED, models.D_ABORTED:
		//if we wait an event from this schedule
		for _, tsk := range state.stContext.dispatchSTM.GetDispatchStatus().DispatchTasks {
			if tsk.TaskType == models.T_WAIT_ON_TASK || tsk.TaskType == models.T_WAIT_ON_SCHEDULE {
				//if the event we are waiting on is no more
				if tsk.WaitedEvent.WaitedEventName == dst.DispatchName {
					log.Warningln("blocking event does not exist anymore", dst)
					defer state.stContext.dispatchSTM.GoToNextState(models.D_ABORTED)
					return true, nil
				}
			}
		}
	case models.D_COMPLETED:
		//find if schedule waits on this task
		var found = false
		for _, tsk := range state.stContext.dispatchSTM.GetDispatchStatus().DispatchTasks {
			//if we have a wait task in this dispatch
			if tsk.TaskType == models.T_WAIT_ON_TASK &&
				tsk.WaitedEvent.WaitedEventName == dst.DispatchName {
				if found {
					log.Fatalln("schedule waits on the same event twice", state)
				}
				//check if other dispatcher has completed the task
				for _, otherTsk := range dst.DispatchTasks {
					if otherTsk.TaskState == models.TS_COMPLETED &&
						otherTsk.TaskNr == tsk.WaitedEvent.WaitedTaskNumber {
						log.Debugf("complete %s event %s %d in state %s", tsk.TaskType, tsk.WaitedEvent.WaitedEventName,
							tsk.WaitedEvent.WaitedTaskNumber, reflect.TypeOf(state).String())
						state.stContext.dispatchSTM.CompleteTask(int(tsk.TaskNr))
					}
					found = true
				}
			}
		}

	case models.D_FINISHED:
		//find if schedule waits on this schedule
		var found = false
		for _, tsk := range state.stContext.dispatchSTM.GetDispatchStatus().DispatchTasks {
			if tsk.TaskType == models.T_WAIT_ON_SCHEDULE && tsk.WaitedEvent.WaitedEventName == dst.DispatchName {
				if found {
					log.Fatalln("schedule waits on the same event twice", state)
				}
				log.Debugf("complete %s event %s in state %s", tsk.TaskType, tsk.WaitedEvent.WaitedEventName,
					reflect.TypeOf(state).String())
				state.stContext.dispatchSTM.CompleteTask(int(tsk.TaskNr))
				found = true
			}
		}

	default:
	}
	return true, nil
}

//HandleTruckStatus handles truck status updates
func (state *active) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	log.Traceln("HandleTruckStatus", ts)
	switch ts.HtState {
	case models.HT_FAILED:
		//if the event we are waiting on is no more
		for _, tsk := range state.stContext.dispatchSTM.GetDispatchStatus().DispatchTasks {
			if tsk.TaskType == models.T_WAIT_ON_TRUCK_PARK || tsk.TaskType == models.T_WAIT_ON_TRUCK_LEAVE {
				if tsk.WaitedEvent.WaitedEventName == ts.HtID {
					log.Warningln("blocking event does not exist anymore", ts)
					defer state.stContext.dispatchSTM.GoToNextState(models.D_ABORTED)
					return true, nil
				}
			}
		}

	case models.HT_ARRIVED:
		var found = false
		for _, tsk := range state.stContext.dispatchSTM.GetDispatchStatus().DispatchTasks {
			//if we have a wait task in this dispatch
			if tsk.TaskType == models.T_WAIT_ON_TRUCK_PARK && tsk.WaitedEvent.WaitedEventName == ts.HtID {
				if found {
					log.Fatalln("schedule waits on the same event twice", state)
				}
				log.Debugf("complete %s event %s in state %s", tsk.TaskType, tsk.WaitedEvent.WaitedEventName,
					reflect.TypeOf(state).String())
				state.stContext.dispatchSTM.CompleteTask(int(tsk.TaskNr))
				found = true
			}
		}

	case models.HT_LEFT:
		var found = false
		for _, tsk := range state.stContext.dispatchSTM.GetDispatchStatus().DispatchTasks {
			//if we have a wait task in this dispatch
			if tsk.TaskType == models.T_WAIT_ON_TRUCK_LEAVE && tsk.WaitedEvent.WaitedEventName == ts.HtID {
				if found {
					log.Fatalln("schedule waits on the same event twice", state)
				}
				log.Debugf("complete %s event %s in state %s", tsk.TaskType, tsk.WaitedEvent.WaitedEventName,
					reflect.TypeOf(state).String())
				state.stContext.dispatchSTM.CompleteTask(int(tsk.TaskNr))
				found = true
			}
		}

	default:
	}
	return true, nil
}
