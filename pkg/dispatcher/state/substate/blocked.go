//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
)

type blocked struct {
	stContext *STM
}

func (state *blocked) enterState() error {
	status := &state.stContext.currentDispatchStatus
	status.DispatchState = models.D_BLOCKED

	//start next dispatch when entering this state
	err := state.stContext.startNextReadyTask()
	if err != nil {
		defer state.stContext.GoToNextState(models.D_ABORTED)
		return fmt.Errorf("startNextReadyTask failed %s %w", status.DispatchName, err)
	}

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *blocked) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *blocked) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	switch st.ResourceState {
	case models.R_STARTUP, models.R_OFF, models.R_FAULT:
	case models.R_IDLE:
		//TODO: if distance < safety distance then wait till distance > safety distance before starting this dispatch
		//save this dispatch as blocked

	case models.R_MOVING:
		log.Errorln("receivedresource status", st.ResourceState, " in state", reflect.TypeOf(state).String())
		defer state.stContext.GoToNextState(models.D_ABORTED)
		return true, nil

	default:
		return true, fmt.Errorf("unspported resource state: %s in state %s", st.ResourceState, reflect.TypeOf(state).String())
	}
	return false, nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *blocked) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	log.Traceln("HandleDispatchStatusUpdate", dst)

	tsk, err := state.stContext.getExecutingTask()
	if err != nil {
		log.WithError(err).Errorln("getExecutingTask failed", dst)
		defer state.stContext.GoToNextState(models.D_ABORTED)
		return false, err
	}

	//handle incoming statuses from other resource's dispatcher
	switch dst.DispatchState {
	case models.D_CANCELED, models.D_ABORTED:
		if tsk.TaskType == models.T_WAIT_ON_TASK || tsk.TaskType == models.T_WAIT_ON_SCHEDULE {
			//if we wait an event from this schedule
			if tsk.WaitedEvent.WaitedEventName == dst.DispatchName {
				log.Warningln("blocking event does not exist anymore", dst)
				defer state.stContext.GoToNextState(models.D_ABORTED)
				return true, nil
			}
		}

	case models.D_COMPLETED:
		//if the task we are waiting on is completed, then unblock
		//if we have a wait task in this dispatch
		if tsk.TaskType == models.T_WAIT_ON_TASK &&
			tsk.WaitedEvent.WaitedEventName == dst.DispatchName {
			//check if other dispatcher has completed the task
			for _, otherTsk := range dst.DispatchTasks {
				if otherTsk.TaskState == models.TS_COMPLETED &&
					otherTsk.TaskNr == tsk.WaitedEvent.WaitedTaskNumber {
					log.Debugf("complete %s event %s %d in state %s", tsk.TaskType, tsk.WaitedEvent.WaitedEventName,
						tsk.WaitedEvent.WaitedTaskNumber, reflect.TypeOf(state).String())
					state.stContext.CompleteTask(int(tsk.TaskNr))

					defer state.stContext.GoToNextState(models.D_READY)
					return true, nil
				}

			}
		}

	case models.D_FINISHED:
		//if schedule is completed
		if tsk.TaskType == models.T_WAIT_ON_SCHEDULE && tsk.WaitedEvent.WaitedEventName == dst.DispatchName {
			log.Debugf("complete %s event %s in state %s", tsk.TaskType, tsk.WaitedEvent.WaitedEventName,
				reflect.TypeOf(state).String())
			state.stContext.CompleteTask(int(tsk.TaskNr))

			defer state.stContext.GoToNextState(models.D_READY)
			return true, nil
		}

	default:
	}
	return false, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *blocked) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *blocked) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	log.Traceln("HandleTruckStatus", ts)
	tsk, err := state.stContext.getExecutingTask()
	if err != nil {
		log.WithError(err).Errorln("getExecutingTask failed", ts)
		defer state.stContext.GoToNextState(models.D_ABORTED)
		return false, err
	}

	switch ts.HtState {
	case models.HT_FAILED:
		//if the event we are waiting on is no more
		if tsk.TaskType == models.T_WAIT_ON_TRUCK_PARK || tsk.TaskType == models.T_WAIT_ON_TRUCK_LEAVE {
			if tsk.WaitedEvent.WaitedEventName == ts.HtID {
				log.Warningln("blocking event does not exist anymore", ts)

				defer state.stContext.GoToNextState(models.D_ABORTED)
				return true, nil
			}
		}

	case models.HT_ARRIVED:
		if tsk.TaskType == models.T_WAIT_ON_TRUCK_PARK && tsk.WaitedEvent.WaitedEventName == ts.HtID {
			log.Debugf("complete %s event %s in state %s", tsk.TaskType, tsk.WaitedEvent.WaitedEventName,
				reflect.TypeOf(state).String())

			state.stContext.CompleteTask(int(tsk.TaskNr))
			defer state.stContext.GoToNextState(models.D_READY)
			return true, nil
		}

	case models.HT_LEFT:
		if tsk.TaskType == models.T_WAIT_ON_TRUCK_LEAVE && tsk.WaitedEvent.WaitedEventName == ts.HtID {
			log.Debugf("complete %s event %s in state %s", tsk.TaskType, tsk.WaitedEvent.WaitedEventName,
				reflect.TypeOf(state).String())

			state.stContext.CompleteTask(int(tsk.TaskNr))
			defer state.stContext.GoToNextState(models.D_READY)
			return true, nil
		}

	default:
	}
	return false, nil
}
