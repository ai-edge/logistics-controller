//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"
)

type aborted struct {
	stContext *STM
}

func (state *aborted) enterState() error {
	status := &state.stContext.currentDispatchStatus
	status.DispatchState = models.D_ABORTED

	//abort crane job
	defer state.stContext.stopResource()

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *aborted) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *aborted) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	return false, nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *aborted) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	return false, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *aborted) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *aborted) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	return false, nil
}
