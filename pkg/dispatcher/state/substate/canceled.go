//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"
)

type canceled struct {
	stContext *STM
}

func (state *canceled) enterState() error {
	status := &state.stContext.currentDispatchStatus
	status.DispatchState = models.D_CANCELED

	//abort crane job
	defer state.stContext.stopResource()

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *canceled) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *canceled) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	return false, nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *canceled) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	return false, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *canceled) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *canceled) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	return false, nil
}
