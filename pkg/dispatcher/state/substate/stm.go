//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"
	"ecs/internal/rabbitnamings"
	"ecs/pkg/dispatcher/repo/dispatch"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"

	log "github.com/sirupsen/logrus"
)

//stopTskNr defines the task number of a stop jo
const stopTskNr = 0xFFFF

//dispatchState is the interface of a "dispatch" state
type dispatchState interface {
	enterState() error
	exitState() error

	HandleCraneStatusUpdate(models.StackingCraneStatusBase) (bool, error)
	HandleTruckStatus(models.HtStatus) (bool, error)
	HandleDispatchRequest(models.Dispatch) (bool, error)
	HandleDispatchStatusUpdate(models.DispatchStatus) (bool, error)
}

//STM is the LSP complient sub-state of HSM
type STM struct {
	//current state manipulates current status
	currentDispatchStatus models.DispatchStatus

	currentState dispatchState

	resourceID string

	/*states*/
	voidState       dispatchState
	initializeState dispatchState
	readyState      dispatchState
	finishedState   dispatchState
	blockedState    dispatchState
	enteredState    dispatchState
	runningState    dispatchState
	completedState  dispatchState
	abortedState    dispatchState
	canceledState   dispatchState
	faultState      dispatchState

	amqpWrap *amqpwrap.AMQPWrapper
	disRepo  *dispatch.Dispatch
}

//NewSTM returns a new state machine
func NewSTM(resourceID string, st models.DispatchStatus, amqpWrap *amqpwrap.AMQPWrapper, disRepo *dispatch.Dispatch) (*STM, error) {
	stm := &(STM{resourceID: resourceID, currentDispatchStatus: st, amqpWrap: amqpWrap, disRepo: disRepo})

	//init state contexts
	stm.voidState = &(void{stContext: stm})
	stm.initializeState = &(initialize{stContext: stm})
	stm.readyState = &(ready{stContext: stm})
	stm.finishedState = &(finished{stContext: stm})
	stm.blockedState = &(blocked{stContext: stm})
	stm.enteredState = &(entered{stContext: stm})
	stm.runningState = &(running{stContext: stm})
	stm.completedState = &(completed{stContext: stm})
	stm.abortedState = &(aborted{stContext: stm})
	stm.canceledState = &(canceled{stContext: stm})
	stm.faultState = &(fault{stContext: stm})

	sta, err := stm.getState(stm.currentDispatchStatus.DispatchState)
	if err != nil {
		log.WithError(err).Fatalln("cannot create stm", stm)
	}
	stm.setState(sta)

	log.Debugf("created dispatch stm in %s state for disp. status %s",
		reflect.TypeOf(stm.currentState).String(), stm.currentDispatchStatus.DispatchState)

	//trigger storing in repo and publishing status of new stm
	defer stm.GoToNextState(stm.currentDispatchStatus.DispatchState)

	return stm, nil
}

//DeInit deinitializes the stm
func (stm *STM) DeInit() {
	return
}

func (stm *STM) getState(sta models.DispatchState) (dispatchState, error) {
	switch sta {
	case models.D_VOID:
		return stm.voidState, nil
	case models.D_INITIALIZE:
		return stm.initializeState, nil
	case models.D_READY:
		return stm.readyState, nil
	case models.D_BLOCKED:
		return stm.blockedState, nil
	case models.D_FINISHED:
		return stm.finishedState, nil
	case models.D_ENTERED:
		return stm.enteredState, nil
	case models.D_RUNNING:
		return stm.runningState, nil
	case models.D_COMPLETED:
		return stm.completedState, nil
	case models.D_ABORTED:
		return stm.abortedState, nil
	case models.D_CANCELED:
		return stm.canceledState, nil
	case models.D_FAULT:
		return stm.faultState, nil
	default:
		return nil, fmt.Errorf("unsupported state %s", sta)
	}
}

func (stm *STM) setState(newstate dispatchState) {
	stm.currentState = newstate
}

//SetDispStatus sets current state
func (stm *STM) SetDispStatus(st models.DispatchStatus) error {
	stm.currentDispatchStatus = st
	return nil
}

//GetDispStatus returns current state
func (stm *STM) GetDispStatus() models.DispatchStatus {
	return stm.currentDispatchStatus
}

//SetLastGantryPos sets last gantry position
func (stm *STM) SetLastGantryPos(pos int32) {
	stm.currentDispatchStatus.LastGantryPosition = pos
}

//GoToNextState initiates state transition
func (stm *STM) GoToNextState(sta models.DispatchState) error {

	nextState, err := stm.getState(sta)
	if err != nil {
		log.WithError(err).Errorln("getState failed:", sta)
		return err
	}

	log.Tracef("exiting state %s. Going to next state %s", reflect.TypeOf(stm.currentState).String(), reflect.TypeOf(nextState).String())
	err = stm.currentState.exitState()
	if err != nil {
		log.WithError(err).Errorln("cannot exit state:", stm.currentState)
		return err
	}
	stm.currentState = nextState
	err = stm.currentState.enterState()
	if err != nil {
		log.WithError(err).Errorln("cannot enter state:", stm.currentState)
		return err
	}
	return nil
}

//publishResourceOrder publishes scrane job order
func (stm *STM) publishResourceOrder(ro models.StackingCraneJobOrderBase) error {
	log.Debugln("publishResourceOrder", ro)
	bytes, err := json.Marshal(ro)
	if err != nil {
		return fmt.Errorf("marshalling failed for job :%v", ro)
	}
	//publish the job order : scgw/{resourceid}/jobrequest
	err = stm.amqpWrap.PublishTo(rabbitnamings.MQTTExchange,
		rabbitnamings.L2RouteBase+"."+ro.StackingCraneID+"."+rabbitnamings.JobRequestRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing job order failed: %w", err)
	}
	return err
}

//publishDispatchStatus publishes status event
func (stm *STM) publishDispatchStatus(disSt models.DispatchStatus) error {
	log.Debugln("publishDispatchStatus", disSt.DispatchName, disSt.StackingCraneID, disSt.DispatchState)
	log.Traceln("dispatch status", disSt)
	bytes, err := json.Marshal(disSt)
	if err != nil {
		return fmt.Errorf("marshalling failed for dispatch status :%v", disSt)
	}
	//publish the dispatchStatus : dispatcher/{blockid}/dispatchstatus
	err = stm.amqpWrap.PublishTo(rabbitnamings.DispRouteBase,
		rabbitnamings.DispRouteBase+"."+stm.disRepo.BlockID+"."+rabbitnamings.DispatchStatusRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing dispatch status failed: %w", err)
	}
	return err
}

//saveAndPublishState saves status in repo and publishes status
func (stm *STM) saveAndPublishState() error {
	_, err := stm.disRepo.PutDispatchStatusByNr(stm.currentDispatchStatus.DispatchNr, stm.currentDispatchStatus)
	if err != nil {
		return fmt.Errorf("PutDispatchStatusByNr failed: %w", err)
	}
	err = stm.publishDispatchStatus(stm.currentDispatchStatus)
	if err != nil {
		return fmt.Errorf("publishDispatchStatus failed: %w", err)
	}
	return nil
}

//getNextReadyTask return next ready task in the dispatch
func (stm *STM) getNextReadyTask() (models.DispatchTask, error) {
	var tsk models.DispatchTask
	for _, tsk = range stm.currentDispatchStatus.DispatchTasks {
		if tsk.TaskState == models.TS_EXECUTING {
			return tsk, fmt.Errorf("cannot return ready task, one task in executing state %s %d",
				stm.currentDispatchStatus.DispatchName, tsk.TaskNr)
		}
		if tsk.TaskState == models.TS_READY {
			return tsk, nil
		}
	}
	return tsk, fmt.Errorf("no ready task %s", stm.currentDispatchStatus.DispatchName)
}

//getExecutingTask return the task in executing state
func (stm *STM) getExecutingTask() (models.DispatchTask, error) {
	var tsk models.DispatchTask
	for _, tsk = range stm.currentDispatchStatus.DispatchTasks {
		//no ready task shall be found in the slice till the executing task
		if tsk.TaskState == models.TS_READY {
			return tsk, fmt.Errorf("one task prior executing task in ready state %s %d",
				stm.currentDispatchStatus.DispatchName, tsk.TaskNr)
		}
		if tsk.TaskState == models.TS_EXECUTING {
			return tsk, nil
		}
	}
	return tsk, fmt.Errorf("no executing task %s", stm.currentDispatchStatus.DispatchName)
}

//CompleteTask marks a task as completed
func (stm *STM) CompleteTask(tskNr int) error {
	if tskNr >= len(stm.currentDispatchStatus.DispatchTasks) {
		return fmt.Errorf("invalid tsk nr %d", tskNr)
	}
	stm.currentDispatchStatus.DispatchTasks[tskNr].TaskState = models.TS_COMPLETED
	return nil
}

//GetDispatchStatus returns current dispatch status
func (stm *STM) GetDispatchStatus() models.DispatchStatus {
	return stm.currentDispatchStatus
}

//startNextReadyTask sends next ready task in the queue and updates its state
func (stm *STM) startNextReadyTask() error {
	for i, tsk := range stm.currentDispatchStatus.DispatchTasks {
		if tsk.TaskState == models.TS_EXECUTING {
			log.Errorln("cannot dispatch, task in executing state",
				tsk, stm.currentDispatchStatus.DispatchName)
			defer stm.GoToNextState(models.D_ABORTED)
		}
		if tsk.TaskState == models.TS_READY {
			//start dispatching
			tsk.TaskState = models.TS_EXECUTING
			stm.currentDispatchStatus.DispatchTasks[i] = tsk
			if isWaitTask(tsk) {
				return nil
			}

			jo, err := translateToOrder(stm.currentDispatchStatus, int(tsk.TaskNr))
			if err != nil {
				log.WithError(err).Errorf("translateToOrder failed %s %s",
					stm.currentDispatchStatus.DispatchName, err)
				return err
			}
			return stm.publishResourceOrder(jo)
		}
	}
	return fmt.Errorf("no ready task in schedule %s", stm.currentDispatchStatus.DispatchName)
}

//stopResource sends a stop job to the resource
func (stm *STM) stopResource() error {
	disp := stm.currentDispatchStatus
	if disp.DispatchName == "" {
		return fmt.Errorf("invalid dispatch name %v", disp)
	}
	scro := models.StackingCraneJobOrderBase{
		DestinationLogicalLocation: models.LogicalLocation3D{},
		JobOrderName:               GetJobName(disp.DispatchName, GetJobNr(disp.DispatchNr, stopTskNr)),
		JobPriority:                disp.DispatchPriority,
		OrderID:                    GetJobNr(disp.DispatchNr, stopTskNr),
		StackingCraneID:            disp.StackingCraneID,
		JobType:                    9, //stop job nr
		WorkItemReference:          disp.DispatchName,
	}
	log.Warningf("disp %s sending stop to resource %s ", disp.DispatchName, disp.StackingCraneID)
	return stm.publishResourceOrder(scro)
}

//switchToNextTaskState moves the stm to next task's state
func (stm *STM) switchToNextTaskState() error {
	tsk, err := stm.getNextReadyTask()
	if err != nil {
		log.Debugln("last task completed", stm.currentDispatchStatus.DispatchName, tsk)
		defer stm.GoToNextState(models.D_FINISHED)
		return err
	}
	if isWaitTask(tsk) {
		defer stm.GoToNextState(models.D_BLOCKED)
	} else {
		defer stm.GoToNextState(models.D_ENTERED)
	}
	return nil
}

//IsFinished returns true if dispatch req. is executed
func (stm *STM) IsFinished() bool {
	switch stm.currentDispatchStatus.DispatchState {
	case models.D_ABORTED, models.D_CANCELED, models.D_FINISHED:
		return true
	default:
		return false
	}
}

/**************************Container Related**************************/

//publishContainerStatus publishes container status
func (stm *STM) publishContainerStatus(coStat models.ContainerStatus) error {
	log.Debugln("publishContainerStatus", coStat)
	bytes, err := json.Marshal(coStat)
	if err != nil {
		return fmt.Errorf("marshalling failed for container status :%v", coStat)
	}
	//publish the container status : operations/{blockid}/containerstatus
	err = stm.amqpWrap.PublishTo(rabbitnamings.L3RouteBase,
		rabbitnamings.L3RouteBase+"."+stm.disRepo.BlockID+"."+rabbitnamings.ContainerStatusRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing container status failed: %w", err)
	}
	return err
}

//updateAndPublishItemsAfterCompletedTask updates items' fields and publishes it after a dispatch is finished
func (stm *STM) updateAndPublishItemsAfterCompletedTask(tsk models.DispatchTask) error {
	for _, co := range tsk.HandledItems {
		var coStat models.ContainerStatus
		switch tsk.TaskType {
		case models.T_MOVE:
			//no action
			return nil
		case models.T_PICK: //container picked, inform about pick location
			coStat.ContainerIDInfo = co.ContainerIDInfo
			coStat.ContainerPlaced = false
			coStat.ContainerLogicalLocation = tsk.DestinationLogicalLocation
			err := stm.publishContainerStatus(coStat)
			if err != nil {
				log.WithError(err).Errorf("cannot publish container %s", coStat.ContainerIDInfo.ISOID)
			}
		case models.T_PLACE: //container placed, update the location information
			coStat.ContainerIDInfo = co.ContainerIDInfo
			coStat.ContainerPlaced = true
			coStat.ContainerLogicalLocation = tsk.DestinationLogicalLocation
			err := stm.publishContainerStatus(coStat)
			if err != nil {
				log.WithError(err).Errorf("cannot publish container %s", coStat.ContainerIDInfo.ISOID)
			}
		case models.T_WAIT_ON_TASK, models.T_WAIT_ON_SCHEDULE, models.T_WAIT_ON_TRUCK_PARK, models.T_WAIT_ON_TRUCK_LEAVE:
			return fmt.Errorf("not supported task type %s", tsk.TaskType)
		default:
			return fmt.Errorf("invalid task type %s", tsk.TaskType)
		}
	}
	return nil
}

/**************************Helpers**************************/

func isWaitTask(tsk models.DispatchTask) bool {
	switch tsk.TaskType {
	case models.T_WAIT_ON_TASK, models.T_WAIT_ON_SCHEDULE, models.T_WAIT_ON_TRUCK_PARK, models.T_WAIT_ON_TRUCK_LEAVE:
		return true
	default:
		return false
	}
}

func translateToOrder(disp models.DispatchStatus, tskNr int) (models.StackingCraneJobOrderBase, error) {
	var scro models.StackingCraneJobOrderBase
	if disp.DispatchName == "" {
		return scro, fmt.Errorf("invalid dispatch name %v", disp)
	}

	if tskNr >= len(disp.DispatchTasks) {
		return scro, fmt.Errorf("invalid tsk nr %d", tskNr)
	}
	tsk := disp.DispatchTasks[tskNr]
	switch tsk.TaskType {
	case models.T_MOVE:
		scro.JobType = 1
	case models.T_PICK:
		scro.JobType = 2
	case models.T_PLACE:
		scro.JobType = 3
	default:
		return scro, fmt.Errorf("not supported type %s", tsk.TaskType)
	}

	scro = models.StackingCraneJobOrderBase{
		DestinationLogicalLocation: tsk.DestinationLogicalLocation,
		JobOrderName:               GetJobName(disp.DispatchName, GetJobNr(disp.DispatchNr, tsk.TaskNr)),
		JobPriority:                disp.DispatchPriority,
		OrderID:                    GetJobNr(disp.DispatchNr, tsk.TaskNr),
		StackingCraneID:            disp.StackingCraneID,
		WorkItemReference:          disp.DispatchName,
	}

	for _, hi := range tsk.HandledItems {
		scro.HandledItems = append(scro.HandledItems, hi)
	}
	return scro, nil
}

//GetJobName creates a job number
func GetJobName(dispatchName string, jobNr int32) string {
	return dispatchName + strconv.Itoa(int(jobNr))
}

//GetJobNr creates a job number
func GetJobNr(dispNr int32, tskNr int32) int32 {
	return (dispNr << 8) + tskNr
}

//GetdispNr gets the dispatch number
func GetdispNr(jobNr int32) int32 {
	return jobNr >> 8
}

//GetTskNr gets the task number
func GetTskNr(jobNr int32) int32 {
	return jobNr & 0xFF
}

/**************************Event Handlers**************************/

//HandleCraneStatusUpdate handles crane status updates
func (stm *STM) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	return stm.currentState.HandleCraneStatusUpdate(st)
}

//HandleDispatchRequest handles dispatch requests
func (stm *STM) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return stm.currentState.HandleDispatchRequest(di)
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (stm *STM) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	return stm.currentState.HandleDispatchStatusUpdate(dst)
}

//HandleTruckStatus handles truck status updates
func (stm *STM) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	return stm.currentState.HandleTruckStatus(ts)
}
