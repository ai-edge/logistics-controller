//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"
)

type ready struct {
	stContext *STM
}

func (state *ready) enterState() error {
	status := &state.stContext.currentDispatchStatus
	status.DispatchState = models.D_READY

	//switch to next task's state
	defer state.stContext.switchToNextTaskState()

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *ready) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *ready) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	return false, nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *ready) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	return false, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *ready) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *ready) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	return false, nil
}
