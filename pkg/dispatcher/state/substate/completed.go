//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"

	log "github.com/sirupsen/logrus"
)

type completed struct {
	stContext *STM
}

func (state *completed) enterState() error {
	status := &state.stContext.currentDispatchStatus
	status.DispatchState = models.D_COMPLETED

	//transient
	//update completed task
	tsk, err := state.stContext.getExecutingTask()
	if err != nil {
		log.WithError(err).Errorln("getExecutingTask failed ", status.DispatchName)
		defer state.stContext.GoToNextState(models.D_ABORTED)
		return err
	}
	state.stContext.CompleteTask(int(tsk.TaskNr))

	//update destination logical location i.e. it varies
	defer state.stContext.updateAndPublishItemsAfterCompletedTask(tsk)

	//transient state.
	defer state.stContext.GoToNextState(models.D_READY)

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *completed) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *completed) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	return false, nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *completed) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	return false, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *completed) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *completed) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	return false, nil
}
