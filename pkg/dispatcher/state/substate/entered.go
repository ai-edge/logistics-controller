//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
)

type entered struct {
	stContext *STM
}

func (state *entered) enterState() error {
	status := &state.stContext.currentDispatchStatus
	status.DispatchState = models.D_ENTERED

	//start next dispatch when entering this state
	err := state.stContext.startNextReadyTask()
	if err != nil {
		defer state.stContext.GoToNextState(models.D_ABORTED)
		return fmt.Errorf("startNextReadyTask failed %s %w", status.DispatchName, err)
	}

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *entered) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *entered) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	tsk, err := state.stContext.getExecutingTask()
	if err != nil {
		log.WithError(err).Errorln("getExecutingTask failed", st)
		defer state.stContext.GoToNextState(models.D_ABORTED)
		return false, err
	}
	jobNr := GetJobNr(state.stContext.currentDispatchStatus.DispatchNr, tsk.TaskNr)

	switch st.ResourceState {
	case models.R_STARTUP, models.R_OFF, models.R_FAULT:
	case models.R_IDLE:
		//this happens when the target position is identical with crane's current position, crane doesn't move
		if jobNr == st.JobInfo.OrderID {
			defer state.stContext.GoToNextState(models.D_RUNNING)
			return true, nil
		}
		//TODO: implement a timeout in case crane offline and haven't responded
	case models.R_MOVING:

		if jobNr == st.JobInfo.OrderID {
			defer state.stContext.GoToNextState(models.D_RUNNING)
			return true, nil
		}

		log.Errorln("dispatch nr", state.stContext.currentDispatchStatus.DispatchNr, " mismatch. Resource state", st)

	default:
		return true, fmt.Errorf("unspported resource state: %s in state %s", st.ResourceState, reflect.TypeOf(state).String())
	}
	return false, nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *entered) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	return false, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *entered) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *entered) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	return false, nil
}
