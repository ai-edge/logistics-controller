//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"
)

type initialize struct {
	stContext *STM
}

func (state *initialize) enterState() error {
	status := &state.stContext.currentDispatchStatus
	status.DispatchState = models.D_INITIALIZE

	//TODO: safe distance wait
	//transient
	defer state.stContext.GoToNextState(models.D_READY)

	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *initialize) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *initialize) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	return false, nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *initialize) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	return false, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *initialize) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *initialize) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	return false, nil
}
