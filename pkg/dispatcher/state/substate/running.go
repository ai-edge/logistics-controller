//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
)

type running struct {
	stContext *STM
}

func (state *running) enterState() error {
	status := &state.stContext.currentDispatchStatus
	status.DispatchState = models.D_RUNNING
	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *running) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *running) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	tsk, err := state.stContext.getExecutingTask()
	if err != nil {
		log.WithError(err).Errorln("getExecutingTask failed", st)
		defer state.stContext.GoToNextState(models.D_ABORTED)
		return false, err
	}
	jobNr := GetJobNr(state.stContext.currentDispatchStatus.DispatchNr, tsk.TaskNr)

	switch st.ResourceState {
	case models.R_STARTUP, models.R_OFF, models.R_FAULT:
	case models.R_IDLE:
		//if resource has restarted or does not have job's info anymore
		if st.JobInfo.JobOrderName == "" {
			log.Errorln("crane does not have any job info. Resource state", st)
			defer state.stContext.GoToNextState(models.D_ABORTED)
			return true, nil
		} else if jobNr == st.JobInfo.OrderID {
			//update completed or cancelled job's state
			if st.AssignedJobStatus.JobCompleted {
				defer state.stContext.GoToNextState(models.D_COMPLETED)
				return true, nil
			} else if st.AssignedJobStatus.JobCanceled {
				log.Warningln("job canceled. Resource state", st)
				defer state.stContext.GoToNextState(models.D_CANCELED)
				return true, nil
			} else if st.AssignedJobStatus.JobRejected ||
				st.AssignedJobStatus.JobCancellationRejected {
				log.Errorln("job rejected. Resource state", st)
				defer state.stContext.GoToNextState(models.D_ABORTED)
				return true, nil
			}
		}
	case models.R_MOVING:
		if jobNr != st.JobInfo.OrderID {
			log.Errorln("dispatch nr", state.stContext.currentDispatchStatus.DispatchNr, " mismatch. Resource state", st)
		}
	default:
		return true, fmt.Errorf("unspported resource state: %s in state %s", st.ResourceState, reflect.TypeOf(state).String())
	}
	return false, nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *running) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	return false, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *running) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *running) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	return false, nil
}
