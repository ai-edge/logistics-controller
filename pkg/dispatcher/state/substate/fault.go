//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"
)

type fault struct {
	stContext *STM
}

func (state *fault) enterState() error {
	status := &state.stContext.currentDispatchStatus
	status.DispatchState = models.D_FAULT
	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *fault) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *fault) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	switch st.ResourceState {
	case models.R_STARTUP, models.R_OFF, models.R_FAULT:
	case models.R_IDLE:
	case models.R_MOVING:
	default:
		return true, fmt.Errorf("unspported resource state: %s in state %s", st.ResourceState, reflect.TypeOf(state).String())
	}
	return false, nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *fault) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	return false, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *fault) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *fault) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	return false, nil
}
