//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"
)

type finished struct {
	stContext *STM
}

func (state *finished) enterState() error {
	status := &state.stContext.currentDispatchStatus
	status.DispatchState = models.D_FINISHED
	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *finished) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *finished) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	return false, nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *finished) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	return false, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *finished) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *finished) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	return false, nil
}
