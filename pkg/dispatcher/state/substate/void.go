//Package substate maintains the dispatch state machine
package substate

import (
	"ecs/api/generated/models"
)

type void struct {
	stContext *STM
}

func (state *void) enterState() error {
	state.stContext.currentDispatchStatus = models.DispatchStatus{StackingCraneID: state.stContext.resourceID}
	status := &state.stContext.currentDispatchStatus

	status.DispatchState = models.D_VOID
	//save and publish status information
	return state.stContext.saveAndPublishState()
}

func (state *void) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *void) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	return false, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *void) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	return false, nil
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *void) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *void) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	return false, nil
}
