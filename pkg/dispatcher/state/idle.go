//Package state maintains a dispatcher state machine for every resource
package state

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"

	"github.com/jinzhu/copier"
	log "github.com/sirupsen/logrus"
)

type idle struct {
	stContext *HSM
}

func (state *idle) enterState() error {

	return nil
}

func (state *idle) exitState() error {
	return nil
}

//createDispatchtatus creates an initial dispatch status for a dispatch
func createDispatchtatus(di models.Dispatch) (models.DispatchStatus, error) {
	var st = models.DispatchStatus{}
	if di.DispatchReqType != models.NEW_DISP {
		return st, fmt.Errorf("cannot create dispatch status for type %s", di.DispatchReqType)
	}
	//copy fields with identical names and types
	err := copier.Copy(&st, &di)
	if err != nil {
		return st, err
	}
	st.DispatchState = models.D_VOID
	return st, nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *idle) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {

	state.stContext.dispatchSTM.SetLastGantryPos(st.CraneMainAxesPosition.GantryPosition)

	switch st.ResourceState {
	case models.R_STARTUP, models.R_OFF, models.R_FAULT:
		log.Errorln("receivedresource status", st.ResourceState, " in state", reflect.TypeOf(state).String())
		defer state.stContext.dispatchSTM.GoToNextState(models.D_FAULT)
		defer state.stContext.GoToNextState(models.DISP_SUSPENDED)
		return true, nil
	case models.R_MOVING:
		log.Errorln("unexpected resource status", st.ResourceState, " in state", reflect.TypeOf(state).String())
	case models.R_IDLE:
	default:
		return true, fmt.Errorf("unspported resource state: %s in state %s", st.ResourceState, reflect.TypeOf(state).String())
	}
	return true, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *idle) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	switch di.DispatchReqType {
	case models.DELETE_DISP:
		//unsafe, deletes a dispatch
		log.Warningln("delete req dis", di.DispatchName)
		return true, state.stContext.disRepo.DeleteDispatchStatusesByID(di.DispatchName)
	case models.ABORT_DISP:
		return true, fmt.Errorf("invalid request %s in dispatch state: %s", di.DispatchReqType, reflect.TypeOf(state).String())
	case models.CANCEL_DISP:
		return true, fmt.Errorf("invalid request %s in dispatch state: %s", di.DispatchReqType, reflect.TypeOf(state).String())
	case models.NEW_DISP:
		st, err := createDispatchtatus(di)
		if err != nil {
			return true, fmt.Errorf("createDispatchtatus failed: %w", err)
		}
		state.stContext.dispatchSTM.SetDispStatus(st)

		defer state.stContext.dispatchSTM.GoToNextState(models.D_INITIALIZE)
		defer state.stContext.GoToNextState(models.DISP_ACTIVE)
		return true, nil

	default:
		return true, fmt.Errorf("invalid request %s in dispatch state: %s", di.DispatchReqType, reflect.TypeOf(state).String())

	}
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *idle) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	//do not consume
	return false, nil
}

//HandleTruckStatus handles truck status updates
func (state *idle) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	//do not consume
	return false, nil
}
