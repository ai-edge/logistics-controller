//Package state maintains state machines
package state

import (
	"ecs/api/generated/models"
	"ecs/pkg/dispatcher/repo/dispatch"
	"ecs/pkg/dispatcher/state/substate"
	"ecs/tools/amqpwrap"
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
)

type dispatcherState interface {
	enterState() error
	exitState() error

	HandleCraneStatusUpdate(models.StackingCraneStatusBase) (bool, error)
	HandleTruckStatus(models.HtStatus) (bool, error)
	HandleDispatchRequest(models.Dispatch) (bool, error)
	HandleDispatchStatusUpdate(models.DispatchStatus) (bool, error)
}

//HSM is dispatcher state machine. It is the superstate of dispatch stm.
type HSM struct {
	//dispatch stm
	dispatchSTM *substate.STM

	currentState dispatcherState

	resourceID string

	/*states*/
	idleState      dispatcherState
	activeState    dispatcherState
	suspendedState dispatcherState

	amqpWrap *amqpwrap.AMQPWrapper
	disRepo  *dispatch.Dispatch
}

//NewHSM returns a new state machine
func NewHSM(resourceID string, st models.DispatchStatus, amqpWrap *amqpwrap.AMQPWrapper, disRepo *dispatch.Dispatch) (*HSM, error) {
	hsm := &(HSM{resourceID: resourceID, amqpWrap: amqpWrap, disRepo: disRepo})

	//init state contexts
	hsm.idleState = &(idle{stContext: hsm})
	hsm.activeState = &(active{stContext: hsm})
	hsm.suspendedState = &(suspended{stContext: hsm})

	//TODO: remove this after implementing a timeout in ready state. We miss the first moving signal while offline.
	if st.DispatchState == models.D_ENTERED {
		st.DispatchState = models.D_RUNNING
	}
	mappedState, err := hsm.getMappedState(st.DispatchState)
	if err != nil {
		log.WithError(err).Fatalln("cannot create hsm", hsm)
	}

	dispSt, _ := hsm.getState(mappedState)
	hsm.setState(dispSt)

	//init substate
	stm, err := substate.NewSTM(resourceID, st, amqpWrap, disRepo)
	if err != nil {
		log.WithError(err).Fatalln("cannot create dispatch stm", st)
	}
	hsm.dispatchSTM = stm

	log.Debugf("created dispatcher hsm in %s state for disp. status %s",
		reflect.TypeOf(hsm.currentState).String(), st.DispatchState)
	return hsm, nil
}

//DeInit deinitializes the hsm
func (hsm *HSM) DeInit() {
	return
}

func (hsm *HSM) setState(newstate dispatcherState) {
	hsm.currentState = newstate
}

func (hsm *HSM) getMappedState(sta models.DispatchState) (models.DispatcherState, error) {
	switch sta {
	case models.D_VOID:
		return models.DISP_IDLE, nil
	case models.D_INITIALIZE, models.D_READY, models.D_RUNNING, models.D_COMPLETED,
		models.D_FINISHED, models.D_CANCELED, models.D_ENTERED, models.D_BLOCKED, models.D_ABORTED:
		return models.DISP_ACTIVE, nil
	case models.D_FAULT:
		return models.DISP_SUSPENDED, nil
	default:
		return models.DISP_SUSPENDED, fmt.Errorf("unsupported state %s", sta)
	}
}

func (hsm *HSM) getState(sta models.DispatcherState) (dispatcherState, error) {
	switch sta {
	case models.DISP_IDLE:
		return hsm.idleState, nil
	case models.DISP_ACTIVE:
		return hsm.activeState, nil
	case models.DISP_SUSPENDED:
		return hsm.suspendedState, nil
	default:
		return nil, fmt.Errorf("unsupported state %s", sta)
	}
}

//GoToNextState initiates state transition
func (hsm *HSM) GoToNextState(sta models.DispatcherState) error {

	nextState, err := hsm.getState(sta)
	if err != nil {
		log.WithError(err).Errorln("getState failed:", sta)
		return err
	}

	log.Debugf("exiting state %s. Going to next state %s", reflect.TypeOf(hsm.currentState).String(), reflect.TypeOf(nextState).String())
	err = hsm.currentState.exitState()
	if err != nil {
		log.WithError(err).Errorln("cannot exit state:", hsm.currentState)
		return err
	}
	hsm.currentState = nextState
	err = hsm.currentState.enterState()
	if err != nil {
		log.WithError(err).Errorln("cannot enter state:", hsm.currentState)
		return err
	}
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (hsm *HSM) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {
	//handle in substate first
	isHandled, err := hsm.dispatchSTM.HandleCraneStatusUpdate(st)
	if err != nil {
		log.WithError(err).Errorln("HandleCraneStatusUpdate failed in stm", st)
	}
	if isHandled {
		return true, nil
	}

	return hsm.currentState.HandleCraneStatusUpdate(st)
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (hsm *HSM) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	//handle in substate first
	isHandled, err := hsm.dispatchSTM.HandleDispatchStatusUpdate(dst)
	if err != nil {
		log.WithError(err).Errorln("HandleDispatchStatusUpdate failed in stm", dst)
	}
	if isHandled {
		return true, nil
	}

	return hsm.currentState.HandleDispatchStatusUpdate(dst)
}

//HandleDispatchRequest handles dispatch requests
func (hsm *HSM) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	//handle in substate first
	isHandled, err := hsm.dispatchSTM.HandleDispatchRequest(di)
	if err != nil {
		log.WithError(err).Errorln("HandleDispatchRequest failed in stm", di)
	}
	if isHandled {
		return true, nil
	}

	return hsm.currentState.HandleDispatchRequest(di)
}

//HandleTruckStatus handles truck status updates
func (hsm *HSM) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	log.Traceln("HandleTruckStatus", ts)

	//handle in substate first
	isHandled, err := hsm.dispatchSTM.HandleTruckStatus(ts)
	if err != nil {
		log.WithError(err).Errorln("HandleTruckStatus failed in stm", ts)
	}
	if isHandled {
		return true, nil
	}

	return hsm.currentState.HandleTruckStatus(ts)
}
