//Package state maintains a dispatcher state machine for every resource
package state

import (
	"ecs/api/generated/models"
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
)

//suspended is the suspended state after job has been sent to resource, but the job has not been accepted by the resource
type suspended struct {
	stContext *HSM
}

func (state *suspended) enterState() error {
	return nil
}

func (state *suspended) exitState() error {
	return nil
}

//HandleCraneStatusUpdate handles crane status updates
func (state *suspended) HandleCraneStatusUpdate(st models.StackingCraneStatusBase) (bool, error) {

	state.stContext.dispatchSTM.SetLastGantryPos(st.CraneMainAxesPosition.GantryPosition)

	switch st.ResourceState {
	case models.R_STARTUP, models.R_OFF, models.R_FAULT:
	case models.R_IDLE:
		if !st.AssignedJobStatus.IsRunning {
			defer state.stContext.dispatchSTM.GoToNextState(models.D_VOID)
			defer state.stContext.GoToNextState(models.DISP_IDLE)
			return true, nil
		}
		//another job?
		return true, fmt.Errorf("unspported resource state: %s in state %s", st.ResourceState, reflect.TypeOf(state).String())

	case models.R_MOVING:
		return true, fmt.Errorf("unspported resource state: %s in state %s", st.ResourceState, reflect.TypeOf(state).String())
	default:
		return true, fmt.Errorf("unspported resource state: %s in state %s", st.ResourceState, reflect.TypeOf(state).String())
	}
	return true, nil
}

//HandleDispatchRequest handles dispatch requests
func (state *suspended) HandleDispatchRequest(di models.Dispatch) (bool, error) {
	switch di.DispatchReqType {
	case models.DELETE_DISP:
		log.Warningf("received request %s in dispatch state: %s", di.DispatchReqType, reflect.TypeOf(state).String())
		if state.stContext.dispatchSTM.GetDispatchStatus().DispatchName == di.DispatchName {
			log.Warningf("%s, %s dispatch in progress. name %s", di.DispatchReqType, reflect.TypeOf(state).String(), di.DispatchName)
			defer state.stContext.dispatchSTM.GoToNextState(models.D_VOID)
			defer state.stContext.GoToNextState(models.DISP_IDLE)
		}
		return true, state.stContext.disRepo.DeleteDispatchStatusesByID(di.DispatchName)
	default:
		return true, fmt.Errorf("invalid request %s in dispatch state: %s", di.DispatchReqType, reflect.TypeOf(state).String())
	}
}

//HandleDispatchStatusUpdate handles dispatch status updates
func (state *suspended) HandleDispatchStatusUpdate(dst models.DispatchStatus) (bool, error) {
	log.Traceln("HandleDispatchStatusUpdate", dst)
	return true, nil
}

//HandleTruckStatus handles truck status updates
func (state *suspended) HandleTruckStatus(ts models.HtStatus) (bool, error) {
	log.Traceln("HandleTruckStatus", ts)
	return true, nil
}
