//Package asyncsrv consumes incoming async messages
package asyncsrv

import (
	"ecs/api/generated/models"
	"ecs/internal/rabbitnamings"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type dispatchService interface {
	NotifyDispatchRequest(models.Dispatch) error
	NotifyCraneStatusUpdate(models.StackingCraneStatusBase) error
	NotifyDispatchStatusUpdate(models.DispatchStatus) (bool, error)
	NotifyTruckStatus(models.HtStatus) (bool, error)
}

const delayTime = 3

//Asyncsrv is the async service
type Asyncsrv struct {
	dispatchReqCh    <-chan amqp.Delivery
	craneStatusCh    <-chan amqp.Delivery
	truckStatusCh    <-chan amqp.Delivery
	dispatchStatusCh <-chan amqp.Delivery
	connClosedCh     <-chan *amqp.Error
	subCloseCh       <-chan *amqp.Error
	subCancelCh      <-chan string

	stopCh chan bool

	wg sync.WaitGroup

	amqpWrap *amqpwrap.AMQPWrapper

	ds      dispatchService
	blockID string
}

//NewAsyncsrv returns a new async service. Creates exchanges only for consumed messages.
func NewAsyncsrv(blockID string, ds dispatchService, amqpWrap *amqpwrap.AMQPWrapper) (*Asyncsrv, error) {
	asyncsrv := Asyncsrv{ds: ds, amqpWrap: amqpWrap, blockID: blockID}
	var err error

	asyncsrv.stopCh = make(chan bool)

	//subscribe to dispatcher/{blockid}/dispatchRequest
	route := rabbitnamings.DispRouteBase + "." + blockID + "." + rabbitnamings.DispatchRequestRouteKey
	asyncsrv.dispatchReqCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.DispatchRequestRouteKey, //queue name contains route key
		rabbitnamings.DispRouteBase,                                   //exchange name
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to dispatcher/{blockid}/dispatchstatus
	route = rabbitnamings.DispRouteBase + "." + blockID + "." + rabbitnamings.DispatchStatusRouteKey
	asyncsrv.dispatchStatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.DispatchStatusRouteKey, //queue name contains route key
		rabbitnamings.DispRouteBase,                                  //exchange name "amq.topic" for mqtt
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to scgw/*/cranestatus
	route = rabbitnamings.L2RouteBase + ".*." + rabbitnamings.CraneStatusRouteKey
	asyncsrv.craneStatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.CraneStatusRouteKey, //queue name contains route key
		rabbitnamings.MQTTExchange,                                //exchange name e.g. "amq.topic" for mqtt
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to ht/*/truckstatus
	route = rabbitnamings.HtRouteBase + ".*." + rabbitnamings.TruckStatusRouteKey
	asyncsrv.truckStatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.TruckStatusRouteKey, //queue name contains route key
		rabbitnamings.HtRouteBase,                                 //exchange name e.g. "amq.topic" for mqtt
		route)

	if err != nil {
		return nil, err
	}

	asyncsrv.connClosedCh = amqpWrap.GetConnCloseChan()
	asyncsrv.subCloseCh = amqpWrap.GetSubCloseChan()
	asyncsrv.subCancelCh = amqpWrap.GetSubCancelChan()

	return &asyncsrv, nil
}

//DeInit closes connection
func (as *Asyncsrv) DeInit() {
	as.stopCh <- true
}

//Asyncsrv is the main loop for consuming incoming async messages
func (as *Asyncsrv) Asyncsrv() error {

	for {
		select {
		//new scrane status event
		case delivery := <-as.craneStatusCh:
			cstatus := models.StackingCraneStatusBase{}
			err := json.Unmarshal(delivery.Body, &cstatus)
			if err != nil {
				log.WithError(err).Errorln("unmarshal crane status failed for delivery", delivery)
				break
			}

			log.Traceln("received crane status:", cstatus)
			err = as.ds.NotifyCraneStatusUpdate(cstatus)
			if err != nil {
				log.WithError(err).Error("error in NotifyCraneStatusUpdate")
			}

			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//new dispatch request
		case delivery := <-as.dispatchReqCh:
			di := models.Dispatch{}
			err := json.Unmarshal(delivery.Body, &di)
			if err != nil {
				log.WithError(err).Errorln("unmarshal dispatch failed for delivery", delivery)
				break
			}

			log.Debugln("received dispatch:", di)
			err = as.ds.NotifyDispatchRequest(di)
			if err != nil {
				log.WithError(err).Error("error in NotifyDispatchRequest")
			}

			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

			//dispatch status event
		case delivery := <-as.dispatchStatusCh:
			ds := models.DispatchStatus{}
			err := json.Unmarshal(delivery.Body, &ds)
			if err != nil {
				log.WithError(err).Errorln("unmarshal dispatch status failed for delivery", delivery)
				break
			}

			//log.Debugln("receiveddispatch status", ds.DispatchName, ds.StackingCraneID, ds.DispatchState)
			log.Traceln("received dispatch status", ds)
			isHandled, err := as.ds.NotifyDispatchStatusUpdate(ds)
			if err != nil {
				log.WithError(err).Error("error in NotifyDispatchStatusUpdate")
			}
			if !isHandled {
				log.Tracef("not handled %s %s", reflect.TypeOf(ds).String(), ds.DispatchName)
			}
			as.ackWithDelay(delivery, isHandled)

		//truck status event
		case delivery := <-as.truckStatusCh:
			ts := models.HtStatus{}
			err := json.Unmarshal(delivery.Body, &ts)
			if err != nil {
				log.WithError(err).Errorln("unmarshal ht status failed for delivery", delivery)
				break
			}

			log.Traceln("received truck status:", ts)
			isHandled, err := as.ds.NotifyTruckStatus(ts)
			if err != nil {
				log.WithError(err).Error("error in NotifyTruckStatus")
			}
			if !isHandled {
				log.Tracef("not handled %s %s", reflect.TypeOf(ts).String(), ts.HtID)
			}
			as.ackWithDelay(delivery, isHandled)

		case <-as.stopCh:
			log.Warningln("received stop", as.blockID)
			as.wg.Wait()

			return nil

		case err := <-as.connClosedCh:
			if err != nil {
				log.WithError(err).Errorf("connection error. Shutting down %s", as.blockID)
			} else {
				log.Warningf("connection closed %s. Shutting down", as.blockID)
			}
			as.wg.Wait()
			panic(fmt.Errorf("connection closed %s", as.blockID))

		case err := <-as.subCloseCh:
			log.WithError(err).Errorf("channel error. Shutting down %s", as.blockID)
			as.wg.Wait()
			panic(fmt.Errorf("channel closed %s", as.blockID))

		case msg := <-as.subCancelCh:
			log.Errorf("channel canceled. Shutting down %s", msg)
			as.wg.Wait()
			panic(fmt.Errorf("channel canceled %s", as.blockID))

		}

	} //for

}

//ackWithDelay acks or nacks the delivery based on status
func (as *Asyncsrv) ackWithDelay(delivery amqp.Delivery, isHandled bool) error {
	if !amqpwrap.AutoAck {
		if isHandled {
			delivery.Ack(false)
		} else {
			as.wg.Add(1)
			//nack with delay
			go func() {
				defer as.wg.Done()
				time.Sleep(delayTime * time.Second)
				delivery.Nack(false, true)
			}()
		}
	}
	return nil
}
