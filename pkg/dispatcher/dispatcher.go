//Package dispatcher dispatch new resource orders
package dispatcher

import (
	"ecs/api/generated/models"
	"ecs/pkg/dispatcher/asyncsrv"
	"ecs/pkg/dispatcher/repo/dispatch"
	"ecs/pkg/dispatcher/state"

	"ecs/tools/amqpwrap"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

//Dispatcher is the dispatching service for resource orders
type Dispatcher struct {
	disState *state.HSM

	disRepo  *dispatch.Dispatch
	as       *asyncsrv.Asyncsrv
	amqpWrap *amqpwrap.AMQPWrapper
}

//NewDispatcher returns a new dispatcher
func NewDispatcher(disRepo *dispatch.Dispatch, conn *amqp.Connection) (*Dispatcher, error) {
	dispatcher := &Dispatcher{disRepo: disRepo}

	//init AMQP wrapper
	wrap, err := amqpwrap.NewAMQPWrapper(conn)
	if err != nil {
		return nil, fmt.Errorf("Could not create amqp wrapper: %w", err)
	}
	//different channels for publish and subscribe
	err = wrap.InitPubChannel()
	if err != nil {
		return nil, fmt.Errorf("Init channel failed %w", err)
	}
	err = wrap.InitSubChannel()
	if err != nil {
		return nil, fmt.Errorf("Init channel failed %w", err)
	}
	dispatcher.amqpWrap = wrap

	//init dispatch state machines of scranes
	dspsts, _ := disRepo.GetExecutingDispatchStatuses(disRepo.ResID)
	if len(dspsts) == 1 {
		newStm, err := state.NewHSM(disRepo.ResID, dspsts[0], dispatcher.amqpWrap, disRepo)
		if err != nil {
			return nil, fmt.Errorf("cannot create stm %w", err)
		}
		dispatcher.disState = newStm
	} else if len(dspsts) == 0 {
		newStm, err := state.NewHSM(disRepo.ResID,
			models.DispatchStatus{StackingCraneID: disRepo.ResID,
				DispatchState: models.D_VOID}, dispatcher.amqpWrap, disRepo)
		if err != nil {
			return nil, fmt.Errorf("cannot create stm %w", err)
		}
		dispatcher.disState = newStm
	} else {
		log.Fatalln("more than one active dispatch for scrane", dspsts, disRepo.ResID)
	}

	//Init consuming async messages
	as, err := asyncsrv.NewAsyncsrv(disRepo.BlockID, dispatcher, wrap)
	if err != nil {
		return nil, err
	}
	dispatcher.as = as

	go as.Asyncsrv()

	return dispatcher, nil
}

//DeInit close dispatcher
func (ds *Dispatcher) DeInit() {
	ds.as.DeInit()
	ds.amqpWrap.DeInit()
	ds.disState.DeInit()
}

//NotifyCraneStatusUpdate processes a crane status update
func (ds *Dispatcher) NotifyCraneStatusUpdate(st models.StackingCraneStatusBase) error {
	if st.StackingCraneID != ds.disRepo.ResID {
		return nil
	}
	_, err := ds.disState.HandleCraneStatusUpdate(st)
	return err
}

//NotifyDispatchRequest tries to dispatch the received dispatch
func (ds *Dispatcher) NotifyDispatchRequest(di models.Dispatch) error {
	if di.StackingCraneID != ds.disRepo.ResID {
		return nil
	}
	_, err := ds.disState.HandleDispatchRequest(di)
	return err
}

//NotifyDispatchStatusUpdate processes a dispatch status update
func (ds *Dispatcher) NotifyDispatchStatusUpdate(disStatus models.DispatchStatus) (bool, error) {
	if disStatus.StackingCraneID == ds.disRepo.ResID {
		return true, nil
	}
	return ds.disState.HandleDispatchStatusUpdate(disStatus)
}

//NotifyTruckStatus handles a new truck status
func (ds *Dispatcher) NotifyTruckStatus(ts models.HtStatus) (bool, error) {
	return ds.disState.HandleTruckStatus(ts)
}
