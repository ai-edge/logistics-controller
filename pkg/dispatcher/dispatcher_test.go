package dispatcher

import (
	"ecs/internal/mockdb"
	"ecs/pkg/dispatcher/repo/dispatch"
	"ecs/pkg/setup/repo/terminal"

	"testing"
)

func createDispatcher(t *testing.T) (*Dispatcher, error) {
	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovered in f: %v", r)
		}
	}()

	db, err := mockdb.NewMockDBClient("mockdb://localhost:2700", "admin", "test", "test")
	if err != nil {
		t.Fatalf("mockdb.NewMockDBClient(...) = %v, %s", db, err)
	}
	t.Logf("mockdb.NewMockDBClient(...) created. db = %v", db)

	tr, err := terminal.NewTerminalRepository(db)
	if err != nil {
		t.Fatalf("cannot create terminal repository, err: %s", err)
	}

	scranes, err := tr.GetScranesByBlockID("BL01", tr.GetQueryLimit())
	if err != nil {
		t.Fatalf("cannot create terminal repository, err: %s", err)
	}

	disRepo, err := dispatch.NewDispatchRepository(db, "disp0001", "BL01", scranes[0].ResourceID)
	if err != nil {
		t.Fatalf("dispatch.NewDispatchRepository(...) = %v, %s", disRepo, err)
	}
	t.Logf("dispatch.NewDispatchRepository(...) created. bl = %v", disRepo)

	di := &Dispatcher{disRepo: disRepo}
	return di, nil
}

func TestCreateNewDispatcherNil(t *testing.T) {

	ds, err := NewDispatcher(nil, nil)
	if err == nil {
		t.Errorf("NewDispatcher(nil,nil, false) = %v, %s", ds, err)
	} else {
		t.Logf("NewDispatcher(nil,nil, false) = %v, %s", ds, err)
	}
}
