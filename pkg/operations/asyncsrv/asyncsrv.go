//Package asyncsrv consumes incoming async messages
package asyncsrv

import (
	"ecs/api/generated/models"
	"ecs/internal/rabbitnamings"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type operationsService interface {
	NotifyResourceStatus(models.StackingCraneStatusBase) error
	NotifyOperationsOrderRequest(models.OperationsWorkOrderBase) error
	NotifyScheduleStatus(models.ScheduleStatus) error
	NotifyContainerStatus(models.ContainerStatus) error
	NotifyContainerUpdateRequest(models.OperationsContainerUpdateBase) error
	NotifyTruckStatus(models.HtStatus) error
	NotifyTZReservationRequest(models.TzReservation) error
}

//Asyncsrv is the async service
type Asyncsrv struct {
	L2StatusCh             <-chan amqp.Delivery
	ScheStatusCh           <-chan amqp.Delivery
	ContainerStatusCh      <-chan amqp.Delivery
	L4WorkOrderReqCh       <-chan amqp.Delivery
	L3ContainerUpdateReqCh <-chan amqp.Delivery
	L4TruckStatusCh        <-chan amqp.Delivery
	TZRequestCh            <-chan amqp.Delivery
	//TODO: serviceStatusCh
	connClosedCh <-chan *amqp.Error
	subCloseCh   <-chan *amqp.Error
	subCancelCh  <-chan string

	stopCh chan bool

	amqpWrap *amqpwrap.AMQPWrapper

	op      operationsService
	blockID string
}

//NewAsyncsrv returns a new async service. Creates exchanges only for consumed messages.
func NewAsyncsrv(blockID string, op operationsService, amqpWrap *amqpwrap.AMQPWrapper) (*Asyncsrv, error) {
	asyncsrv := Asyncsrv{amqpWrap: amqpWrap, op: op, blockID: blockID}
	var err error

	asyncsrv.stopCh = make(chan bool)

	//each service declares its own queue on its own layer
	//subscribe to tosgw/*/workorderrequest
	route := rabbitnamings.L4RouteBase + ".*." + rabbitnamings.WorkOrderRequestRouteKey
	asyncsrv.L4WorkOrderReqCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.WorkOrderRequestRouteKey, //queue name contains route key
		rabbitnamings.L4RouteBase, //exchange name
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to scheduler/{blockid}/schedulestatus
	route = rabbitnamings.ScheRouteBase + "." + blockID + "." + rabbitnamings.ScheduleStatusRouteKey
	asyncsrv.ScheStatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.ScheduleStatusRouteKey, //queue name contains route key
		rabbitnamings.ScheRouteBase,                                  //exchange name
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to operations/{blockid}/containerupdaterequest
	route = rabbitnamings.L3RouteBase + "." + blockID + "." + rabbitnamings.ContainerUpdateRequestRouteKey
	asyncsrv.L3ContainerUpdateReqCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.ContainerUpdateRequestRouteKey, //queue name contains route key
		rabbitnamings.L3RouteBase, //exchange name
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to scgw/*/cranestatus
	route = rabbitnamings.L2RouteBase + ".*." + rabbitnamings.CraneStatusRouteKey
	asyncsrv.L2StatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.CraneStatusRouteKey, //queue name contains route key
		rabbitnamings.MQTTExchange,                                //exchange name e.g. "amq.topic" for mqtt
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to operations/{blockid}/containerstatus
	route = rabbitnamings.L3RouteBase + "." + blockID + "." + rabbitnamings.ContainerStatusRouteKey
	asyncsrv.ContainerStatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.ContainerStatusRouteKey, //queue name contains route key
		rabbitnamings.L3RouteBase, //exchange name
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to ht/*/truckstatus
	route = rabbitnamings.HtRouteBase + ".*." + rabbitnamings.TruckStatusRouteKey
	asyncsrv.L4TruckStatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.TruckStatusRouteKey, //queue name contains route key
		rabbitnamings.HtRouteBase,                                 //exchange name e.g. "amq.topic" for mqtt
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to operations/{blockid}/tzreservationrequest
	route = rabbitnamings.L3RouteBase + "." + blockID + "." + rabbitnamings.TZReservationRequestRouteKey
	asyncsrv.TZRequestCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.TZReservationRequestRouteKey, //queue name contains route key
		rabbitnamings.L3RouteBase, //exchange name
		route)

	if err != nil {
		return nil, err
	}

	//TODO: add service status messages.

	asyncsrv.connClosedCh = amqpWrap.GetConnCloseChan()
	asyncsrv.subCloseCh = amqpWrap.GetSubCloseChan()
	asyncsrv.subCancelCh = amqpWrap.GetSubCancelChan()

	return &asyncsrv, nil
}

//DeInit closes connection
func (as *Asyncsrv) DeInit() {
	as.stopCh <- true
}

//Asyncsrv is the main loop for consuming incoming async messages
func (as *Asyncsrv) Asyncsrv() error {

	for {
		select {
		//new scrane status event
		case delivery := <-as.L2StatusCh:
			rstatus := models.StackingCraneStatusBase{}
			err := json.Unmarshal(delivery.Body, &rstatus)
			if err != nil {
				log.WithError(err).Errorln("unmarshal crane status failed for delivery", delivery)
			} else {
				// log.Println("received crane status:", rstatus)
				err = as.op.NotifyResourceStatus(rstatus)
				if err != nil {
					log.WithError(err).Error("error in NotifyResourceStatus")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//new schedule status update
		case delivery := <-as.ScheStatusCh:
			scStat := models.ScheduleStatus{}
			err := json.Unmarshal(delivery.Body, &scStat)
			if err != nil {
				log.WithError(err).Errorln("unmarshal ScheduleStatus failed for delivery", delivery)
			} else {
				log.Debugln("received ScheduleStatus:", scStat)
				err = as.op.NotifyScheduleStatus(scStat)
				if err != nil {
					log.WithError(err).Error("error in NotifyScheduleStatus")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//new container status
		case delivery := <-as.ContainerStatusCh:
			coStat := models.ContainerStatus{}
			err := json.Unmarshal(delivery.Body, &coStat)
			if err != nil {
				log.WithError(err).Errorln("unmarshal ContainerStatus failed for delivery", delivery)
			} else {
				log.Debugln("received containerStatus:", coStat)
				err = as.op.NotifyContainerStatus(coStat)
				if err != nil {
					log.WithError(err).Error("error in ContainerStatus")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//new work order request event
		case delivery := <-as.L4WorkOrderReqCh:
			wo := models.OperationsWorkOrderBase{}
			err := json.Unmarshal(delivery.Body, &wo)
			if err != nil {
				log.WithError(err).Errorln("unmarshal workorder failed for delivery", delivery)
			} else {
				log.Debugln("received workorder:", wo)
				err = as.op.NotifyOperationsOrderRequest(wo)
				if err != nil {
					log.WithError(err).Error("error in NotifyOperationsOrderRequest")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//new container update request
		case delivery := <-as.L3ContainerUpdateReqCh:
			up := models.OperationsContainerUpdateBase{}
			err := json.Unmarshal(delivery.Body, &up)
			if err != nil {
				log.WithError(err).Errorln("unmarshal container update request failed for delivery", delivery)
			} else {
				log.Debugln("received container update request:", up)
				err = as.op.NotifyContainerUpdateRequest(up)
				if err != nil {
					log.WithError(err).Error("error in NotifyOperationsOrderRequest")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		case delivery := <-as.L4TruckStatusCh:
			ts := models.HtStatus{}
			err := json.Unmarshal(delivery.Body, &ts)
			if err != nil {
				log.WithError(err).Errorln("unmarshal truck status failed for delivery", delivery)
			} else {
				log.Debugln("received truck status:", ts)
				err = as.op.NotifyTruckStatus(ts)
				if err != nil {
					log.WithError(err).Error("error in NotifyTruckStatus")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		case delivery := <-as.TZRequestCh:
			tzr := models.TzReservation{}
			err := json.Unmarshal(delivery.Body, &tzr)
			if err != nil {
				log.WithError(err).Errorln("unmarshal tz reservation req. failed for delivery", delivery)
			} else {
				log.Debugln("received tz reservation request", tzr)
				err = as.op.NotifyTZReservationRequest(tzr)
				if err != nil {
					log.WithError(err).Error("error in NotifyTZReservationRequest")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		case <-as.stopCh:
			log.Warningln("received stop", as.blockID)
			return nil

		case err := <-as.connClosedCh:
			if err != nil {
				log.WithError(err).Errorf("connection error. Shutting down %s", as.blockID)
			} else {
				log.Warningf("connection closed %s. Shutting down", as.blockID)
			}
			panic(fmt.Errorf("connection closed %s", as.blockID))

		case err := <-as.subCloseCh:
			log.WithError(err).Errorf("channel error. Shutting down %s", as.blockID)
			panic(fmt.Errorf("channel closed %s", as.blockID))

		case msg := <-as.subCancelCh:
			log.Errorf("channel canceled. Shutting down %s", msg)
			panic(fmt.Errorf("channel canceled %s", as.blockID))

		}
	}
}
