package operation

import (
	"ecs/api/generated/models"
	"ecs/internal/dbservice"
	"fmt"
	"sort"
)

//Operation repository for terminal block
type Operation struct {
	//ServiceID is the id of the service created the repository
	ServiceID string
	//BlockID is the id of the block of that repository
	BlockID string
	dbNames operationDBNames

	dbs dbservice.DBService
}

type operationDBNames struct {
	//DB names
	operationDB string
	//Collection Names
	resourceStatusCollectionName,
	operationsOrdersCollectionName,
	operationsStatusCollectionName,
	scheduleStatusCollectionName,
	htStatusCollectionName string
	//queryLimit maximum items to be returned
	queryLimit int64
	//IDField defines the name of the ID field in the DB
	IDField string
}

//NewOperationRepository returns a new repository of orders
func NewOperationRepository(dbs dbservice.DBService, serviceID string, blockID string) (*Operation, error) {

	if dbs == nil || serviceID == "" {
		return nil, fmt.Errorf("invalid arguments dbs % serviceID %s", dbs, serviceID)
	}
	var dbNames operationDBNames

	//Block01DB is the name of the block DB
	dbNames.operationDB = blockID + "-operation"

	dbNames.resourceStatusCollectionName = "resourcestatus"
	dbNames.operationsOrdersCollectionName = "operationorders"
	dbNames.operationsStatusCollectionName = "operationstatus"
	dbNames.scheduleStatusCollectionName = "schedulestatus"
	dbNames.htStatusCollectionName = "htstatus"

	dbNames.queryLimit = 1000
	dbNames.IDField = "_id"

	operation := Operation{dbs: dbs, ServiceID: serviceID, BlockID: blockID, dbNames: dbNames}

	return &operation, nil
}

//DeInit closes connection
func (op *Operation) DeInit() {
	op.dbs.DeInit()
}

//GetQueryLimit returns query limit parameter
func (op *Operation) GetQueryLimit() int64 {
	return op.dbNames.queryLimit
}

// GetStatusByScraneID gets status info about a specific resource
func (op *Operation) GetStatusByScraneID(scraneid string) (models.StackingCraneStatusBase, error) {
	var scraneStatus models.StackingCraneStatusBase
	query := map[string]interface{}{"stackingcraneid": scraneid}
	err := op.dbs.GetOne(op.dbNames.operationDB, op.dbNames.resourceStatusCollectionName, query, &scraneStatus)
	return scraneStatus, err
}

// GetScraneStatuses gets all scrane status
func (op *Operation) GetScraneStatuses(limit int64) ([]models.StackingCraneStatusBase, error) {
	var scraneStatus []models.StackingCraneStatusBase
	query := map[string]interface{}{}
	err := op.dbs.GetAll(op.dbNames.operationDB, op.dbNames.resourceStatusCollectionName, query, &scraneStatus, limit)
	return scraneStatus, err
}

// PutStatusByScraneID puts status info about a specific resource (i.e. creates if it does not exist)
func (op *Operation) PutStatusByScraneID(scraneid string, newStatus models.StackingCraneStatusBase) (models.StackingCraneStatusBase, error) {
	var st models.StackingCraneStatusBase
	query := map[string]interface{}{"stackingcraneid": scraneid}
	err := op.dbs.GetOne(op.dbNames.operationDB, op.dbNames.resourceStatusCollectionName, query, &st)
	if err != nil {
		err = op.dbs.InsertOne(op.dbNames.operationDB, op.dbNames.resourceStatusCollectionName, &newStatus)
	} else {
		err = op.dbs.ReplaceOne(op.dbNames.operationDB, op.dbNames.resourceStatusCollectionName, query, &newStatus)
	}
	return newStatus, err
}

// GetWorkOrderItemStatuses gets all work order item status
func (op *Operation) GetWorkOrderItemStatuses(limit int64) ([]models.OperationsWorkOrderItemStatusBase, error) {
	var itemStatus []models.OperationsWorkOrderItemStatusBase
	query := map[string]interface{}{}
	err := op.dbs.GetAll(op.dbNames.operationDB, op.dbNames.operationsStatusCollectionName, query, &itemStatus, limit)
	return itemStatus, err
}

// GetWorkOrderItemStatusesByState gets all work order item status
func (op *Operation) GetWorkOrderItemStatusesByState(state models.OrderState, limit int64) ([]models.OperationsWorkOrderItemStatusBase, error) {
	var wis []models.OperationsWorkOrderItemStatusBase
	query := map[string]interface{}{"itemstatus": state}
	err := op.dbs.GetAll(op.dbNames.operationDB, op.dbNames.operationsStatusCollectionName, query, &wis, limit)
	return wis, err
}

// GetWorkOrderItemStatusesByItemGUID gets all work order item statuses of that incudes given item's GUID
func (op *Operation) GetWorkOrderItemStatusesByItemGUID(itemGUID string, limit int64) ([]models.OperationsWorkOrderItemStatusBase, error) {
	var wis []models.OperationsWorkOrderItemStatusBase
	query := map[string]interface{}{"handleditem.itemguid": itemGUID}
	err := op.dbs.GetAll(op.dbNames.operationDB, op.dbNames.operationsStatusCollectionName, query, &wis, limit)
	return wis, err
}

// GetWorkOrderItemStatusesByOrderName gets all work order item statuses of a work order
func (op *Operation) GetWorkOrderItemStatusesByOrderName(orderName string, limit int64) ([]models.OperationsWorkOrderItemStatusBase, error) {
	var wis []models.OperationsWorkOrderItemStatusBase
	query := map[string]interface{}{"workorderreference": orderName}
	err := op.dbs.GetAll(op.dbNames.operationDB, op.dbNames.operationsStatusCollectionName, query, &wis, limit)
	return wis, err
}

// GetWorkOrderItemStatusByID gets all work order item status
func (op *Operation) GetWorkOrderItemStatusByID(statusItemID string) (models.OperationsWorkOrderItemStatusBase, error) {
	var wis models.OperationsWorkOrderItemStatusBase
	query := map[string]interface{}{"workitemname": statusItemID}
	err := op.dbs.GetOne(op.dbNames.operationDB, op.dbNames.operationsStatusCollectionName, query, &wis)
	return wis, err
}

// PutWorkOrderItemStatusByID puts a work order item status
func (op *Operation) PutWorkOrderItemStatusByID(itemID string, newStatus models.OperationsWorkOrderItemStatusBase) (models.OperationsWorkOrderItemStatusBase, error) {
	var status models.OperationsWorkOrderItemStatusBase
	query := map[string]interface{}{"workitemname": itemID}
	err := op.dbs.GetOne(op.dbNames.operationDB, op.dbNames.operationsStatusCollectionName, query, &status)
	if err != nil {
		err = op.dbs.InsertOne(op.dbNames.operationDB, op.dbNames.operationsStatusCollectionName, &newStatus)
	} else {
		err = op.dbs.ReplaceOne(op.dbNames.operationDB, op.dbNames.operationsStatusCollectionName, query, &newStatus)
	}
	return newStatus, err
}

// DeleteWorkItemStatusByID deletes a work item status
func (op *Operation) DeleteWorkItemStatusByID(itemID string) error {
	query := map[string]interface{}{"workitemname": itemID}
	err := op.dbs.DeleteOne(op.dbNames.operationDB, op.dbNames.operationsStatusCollectionName, query)
	return err
}

// GetWorkOrders - Get all work orders
func (op *Operation) GetWorkOrders(limit int64) ([]models.OperationsWorkOrderBase, error) {
	var workorders []models.OperationsWorkOrderBase
	query := map[string]interface{}{}
	err := op.dbs.GetAll(op.dbNames.operationDB, op.dbNames.operationsOrdersCollectionName, query, &workorders, limit)
	return workorders, err
}

// GetWorkOrderBySequence - Get a work order by sequence nr
func (op *Operation) GetWorkOrderBySequence(sequence int32) (models.OperationsWorkOrderBase, error) {
	var workorder models.OperationsWorkOrderBase
	query := map[string]interface{}{"workordersequence": sequence}
	err := op.dbs.GetOne(op.dbNames.operationDB, op.dbNames.operationsOrdersCollectionName, query, &workorder)
	return workorder, err
}

// GetWorkOrderByName - Get a work order by name
func (op *Operation) GetWorkOrderByName(workorderid string) (models.OperationsWorkOrderBase, error) {
	var workorder models.OperationsWorkOrderBase
	query := map[string]interface{}{"workordername": workorderid}
	err := op.dbs.GetOne(op.dbNames.operationDB, op.dbNames.operationsOrdersCollectionName, query, &workorder)
	return workorder, err
}

// PutWorkOrderBySequence puts a work order
func (op *Operation) PutWorkOrderBySequence(sequence int32, newWo models.OperationsWorkOrderBase) (models.OperationsWorkOrderBase, error) {
	var workorder models.OperationsWorkOrderBase
	query := map[string]interface{}{"workordersequence": sequence}
	err := op.dbs.GetOne(op.dbNames.operationDB, op.dbNames.operationsOrdersCollectionName, query, &workorder)
	if err != nil {
		err = op.dbs.InsertOne(op.dbNames.operationDB, op.dbNames.operationsOrdersCollectionName, &newWo)
	} else {
		err = op.dbs.ReplaceOne(op.dbNames.operationDB, op.dbNames.operationsOrdersCollectionName, query, &newWo)
	}
	return newWo, err
}

// DeleteWorkOrder deletes a work order
func (op *Operation) DeleteWorkOrder(workorderid string) error {
	query := map[string]interface{}{"workordername": workorderid}
	err := op.dbs.DeleteOne(op.dbNames.operationDB, op.dbNames.operationsOrdersCollectionName, query)
	return err
}

/**************HT Related*****************/

//PutCTruckStatusByID puts a truck status
func (op *Operation) PutCTruckStatusByID(htID string, newHtSt models.HtStatus) (models.HtStatus, error) {
	var ts models.HtStatus
	query := map[string]interface{}{"htid": htID}
	err := op.dbs.GetOne(op.dbNames.operationDB, op.dbNames.htStatusCollectionName, query, &ts)
	if err != nil {
		err = op.dbs.InsertOne(op.dbNames.operationDB, op.dbNames.htStatusCollectionName, &newHtSt)
	} else {
		err = op.dbs.ReplaceOne(op.dbNames.operationDB, op.dbNames.htStatusCollectionName, query, &newHtSt)
	}
	return newHtSt, err
}

// DeleteCTruckStatusByID deletes a truck status
func (op *Operation) DeleteCTruckStatusByID(htID string) error {
	query := map[string]interface{}{"htid": htID}
	err := op.dbs.DeleteOne(op.dbNames.operationDB, op.dbNames.htStatusCollectionName, query)
	return err
}

// GetCTruckStatusByID gets truck status
func (op *Operation) GetCTruckStatusByID(htID string) (models.HtStatus, error) {
	var ts models.HtStatus
	query := map[string]interface{}{"htid": htID}
	err := op.dbs.GetOne(op.dbNames.operationDB, op.dbNames.htStatusCollectionName, query, &ts)
	return ts, err
}

// GetCTruckStatuses gets truck statuses
func (op *Operation) GetCTruckStatuses(limit int64) ([]models.HtStatus, error) {
	var tss []models.HtStatus
	query := map[string]interface{}{}
	err := op.dbs.GetAll(op.dbNames.operationDB, op.dbNames.htStatusCollectionName, query, &tss, limit)
	sort.Slice(tss, func(i, j int) bool {
		return tss[i].LastUpdated.After(tss[j].LastUpdated)
	})
	if len(tss) > 8 {
		return tss[:8], err
	}

	return tss, err
}
