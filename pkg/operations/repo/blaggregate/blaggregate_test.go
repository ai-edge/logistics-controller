package blaggregate

import (
	"ecs/api/generated/models"
	"fmt"
	"testing"
)

func CreateStackingField(bay int32, row int32) (models.StackingField, string) {
	field := models.StackingField{
		FieldBase: models.AbstractField{
			AreaName:      "BA01",
			Bay:           fmt.Sprintf("%03d", bay),
			Row:           fmt.Sprintf("%03d", row),
			Tier:          "01",
			X:             10000 + ((bay-1)/2)*6458,
			Y:             10000 + (row-1)*2840,
			Width:         6458,
			Length:        2840,
			RotationAngle: 0,
			FieldType:     models.TSTACKING_FIELD,
		},
		BayType:   models.BRMG,
		IsBlocked: false,
	}
	nameComposite := "BA01-" + fmt.Sprintf("%03d", bay) + "-" + fmt.Sprintf("%03d", row) + "-01"

	return field, nameComposite
}

func CreateTransferField(bay int32) (models.TransferField, string) {
	field := models.TransferField{
		FieldBase: models.AbstractField{
			AreaName:      "TZ01",
			Bay:           fmt.Sprintf("%03d", bay),
			Row:           "001",
			Tier:          "01",
			X:             10000 + ((bay-1)/2)*6458,
			Y:             27540,
			Width:         6458,
			Length:        2840,
			RotationAngle: 0,
			FieldType:     models.TSTACKING_FIELD,
		},
		ZoneType:  models.SIDELOADING,
		IsBlocked: false,
	}
	nameComposite := "TZ01-" + fmt.Sprintf("%03d", bay) + "-001-01"

	return field, nameComposite
}

func TestGetCoveredFieldsWidthZeroBayOdd(t *testing.T) {

	blAggregate := BlAggregate{maxSFBay: 15, maxTFBay: 15}
	blAggregate.stackingFields = make(map[string]models.StackingField)

	stField, name := CreateStackingField(5, 1)
	blAggregate.stackingFields[name] = stField
	loc := models.LogicalLocation3D{
		AreaName: "BA01",
		Bay:      "005",
		Row:      "001",
		Tier:     "01",
	}
	coveredFields, _, err := blAggregate.getCoveredFields(loc, 0)
	if err == nil {
		t.Errorf("Error should have been generated for zero width. covered fields: %d", len(coveredFields))
	}
}

func TestGetCoveredFieldsWidthZeroBayEven(t *testing.T) {

	blAggregate := BlAggregate{maxSFBay: 15, maxTFBay: 15}
	blAggregate.stackingFields = make(map[string]models.StackingField)

	stField, name := CreateStackingField(7, 1)
	blAggregate.stackingFields[name] = stField
	stField, name = CreateStackingField(9, 1)
	blAggregate.stackingFields[name] = stField
	loc := models.LogicalLocation3D{
		AreaName: "BA01",
		Bay:      "008",
		Row:      "001",
		Tier:     "01",
	}
	_, _, err := blAggregate.getCoveredFields(loc, 0)
	if err == nil {
		t.Errorf("Error should have been generated for zero width")
	}
}

func TestGetCoveredFieldsWidthTwentyBayOdd(t *testing.T) {

	blAggregate := BlAggregate{maxSFBay: 15, maxTFBay: 15}
	blAggregate.stackingFields = make(map[string]models.StackingField)

	stField, name := CreateStackingField(5, 1)
	blAggregate.stackingFields[name] = stField
	loc := models.LogicalLocation3D{
		AreaName: "BA01",
		Bay:      "005",
		Row:      "001",
		Tier:     "01",
	}
	coveredFields, _, err := blAggregate.getCoveredFields(loc, 6058)
	if err != nil {
		t.Errorf("getCoveredFields generated error %s for location %s and width 6058", err, loc)
	}
	if len(coveredFields) != 1 {
		t.Errorf("getCoveredFields could not find the correct size of the covered field. Should have been 1; found: %d", len(coveredFields))
	}
}

func TestGetCoveredFieldsWidthTwentyBayEven(t *testing.T) {

	blAggregate := BlAggregate{maxSFBay: 15, maxTFBay: 15}
	blAggregate.stackingFields = make(map[string]models.StackingField)

	stField, name := CreateStackingField(7, 1)
	blAggregate.stackingFields[name] = stField
	stField, name = CreateStackingField(9, 1)
	blAggregate.stackingFields[name] = stField
	loc := models.LogicalLocation3D{
		AreaName: "BA01",
		Bay:      "008",
		Row:      "001",
		Tier:     "01",
	}
	coveredFields, _, err := blAggregate.getCoveredFields(loc, 6058)
	if err != nil {
		t.Errorf("getCoveredFields generated error %s for location %s and width 6058", err, loc)
	}
	if len(coveredFields) != 2 {
		t.Errorf("getCoveredFields could not find the correct size of the covered field. Should have been 2; found: %d", len(coveredFields))
	}
}

func TestGetCoveredFieldsWidthFortyBayOdd(t *testing.T) {

	blAggregate := BlAggregate{maxSFBay: 15, maxTFBay: 15}
	blAggregate.stackingFields = make(map[string]models.StackingField)

	stField, name := CreateStackingField(3, 1)
	blAggregate.stackingFields[name] = stField
	stField, name = CreateStackingField(5, 1)
	blAggregate.stackingFields[name] = stField
	stField, name = CreateStackingField(7, 1)
	blAggregate.stackingFields[name] = stField
	loc := models.LogicalLocation3D{
		AreaName: "BA01",
		Bay:      "005",
		Row:      "001",
		Tier:     "01",
	}
	coveredFields, _, err := blAggregate.getCoveredFields(loc, 12192)
	if err != nil {
		t.Errorf("getCoveredFields generated error %s for location %s and width 12192", err, loc)
	}
	if len(coveredFields) != 3 {
		t.Errorf("getCoveredFields could not find the correct size of the covered field. Should have been 3; found: %d", len(coveredFields))
	}
}

func TestGetCoveredFieldsWidthFortyBayEven(t *testing.T) {

	blAggregate := BlAggregate{maxSFBay: 15, maxTFBay: 15}
	blAggregate.stackingFields = make(map[string]models.StackingField)

	stField, name := CreateStackingField(7, 1)
	blAggregate.stackingFields[name] = stField
	stField, name = CreateStackingField(9, 1)
	blAggregate.stackingFields[name] = stField
	loc := models.LogicalLocation3D{
		AreaName: "BA01",
		Bay:      "008",
		Row:      "001",
		Tier:     "01",
	}
	coveredFields, _, err := blAggregate.getCoveredFields(loc, 12192)
	if err != nil {
		t.Errorf("getCoveredFields generated error %s for location %s and width 12192", err, loc)
	}
	if len(coveredFields) != 2 {
		t.Errorf("getCoveredFields could not find the correct size of the covered field. Should have been 2; found: %d", len(coveredFields))
	}
}

func TestGetCoveredFieldsWidthZeroTransferZoneBayOdd(t *testing.T) {

	blAggregate := BlAggregate{maxSFBay: 15, maxTFBay: 15}
	blAggregate.transferFields = make(map[string]models.TransferField)

	trField, name := CreateTransferField(3)
	blAggregate.transferFields[name] = trField
	trField, name = CreateTransferField(5)
	blAggregate.transferFields[name] = trField
	trField, name = CreateTransferField(7)
	blAggregate.transferFields[name] = trField

	loc := models.LogicalLocation3D{
		AreaName: "TZ01",
		Bay:      "005",
		Row:      "001",
		Tier:     "01",
	}
	coveredFields, _, err := blAggregate.getCoveredFields(loc, 0)
	if err == nil {
		t.Errorf("Error should have been generated for zero width. covered fields: %d", len(coveredFields))
	}
}

func TestGetCoveredFieldsWidthZeroTransferZoneBayEven(t *testing.T) {

	blAggregate := BlAggregate{maxSFBay: 15, maxTFBay: 15}
	blAggregate.transferFields = make(map[string]models.TransferField)

	trField, name := CreateTransferField(3)
	blAggregate.transferFields[name] = trField
	trField, name = CreateTransferField(5)
	blAggregate.transferFields[name] = trField
	trField, name = CreateTransferField(7)
	blAggregate.transferFields[name] = trField

	loc := models.LogicalLocation3D{
		AreaName: "TZ01",
		Bay:      "006",
		Row:      "001",
		Tier:     "01",
	}
	coveredFields, _, err := blAggregate.getCoveredFields(loc, 0)
	if err == nil {
		t.Errorf("Error should have been generated for zero width. covered fields: %d", len(coveredFields))
	}
}
func TestGetCoveredFieldsWidthFortyTransferZoneBayOdd(t *testing.T) {

	blAggregate := BlAggregate{maxSFBay: 15, maxTFBay: 15}
	blAggregate.transferFields = make(map[string]models.TransferField)
	blAggregate.stackingFields = make(map[string]models.StackingField)

	trField, name := CreateTransferField(3)
	blAggregate.transferFields[name] = trField
	trField, name = CreateTransferField(5)
	blAggregate.transferFields[name] = trField
	trField, name = CreateTransferField(7)
	blAggregate.transferFields[name] = trField

	stField, name := CreateStackingField(3, 1)
	blAggregate.stackingFields[name] = stField
	stField, name = CreateStackingField(5, 1)
	blAggregate.stackingFields[name] = stField
	stField, name = CreateStackingField(7, 1)
	blAggregate.stackingFields[name] = stField

	loc := models.LogicalLocation3D{
		AreaName: "TZ01",
		Bay:      "005",
		Row:      "001",
		Tier:     "01",
	}
	coveredFields, _, err := blAggregate.getCoveredFields(loc, 12000)
	if err != nil {
		t.Errorf("getCoveredFields generated error %s for location %s and width 12000", err, loc)
	}
	if len(coveredFields) != 3 {
		t.Errorf("getCoveredFields could not find the correct size of the covered field. Should have been 3; found: %d", len(coveredFields))
	}
}

func TestGetCoveredFieldsWidthFortyTransferZoneBayEven(t *testing.T) {

	blAggregate := BlAggregate{maxSFBay: 15, maxTFBay: 15}
	blAggregate.transferFields = make(map[string]models.TransferField)
	blAggregate.stackingFields = make(map[string]models.StackingField)

	trField, name := CreateTransferField(3)
	blAggregate.transferFields[name] = trField
	trField, name = CreateTransferField(5)
	blAggregate.transferFields[name] = trField
	trField, name = CreateTransferField(7)
	blAggregate.transferFields[name] = trField

	stField, name := CreateStackingField(3, 1)
	blAggregate.stackingFields[name] = stField
	stField, name = CreateStackingField(5, 1)
	blAggregate.stackingFields[name] = stField
	stField, name = CreateStackingField(7, 1)
	blAggregate.stackingFields[name] = stField

	loc := models.LogicalLocation3D{
		AreaName: "TZ01",
		Bay:      "006",
		Row:      "001",
		Tier:     "01",
	}
	coveredFields, _, err := blAggregate.getCoveredFields(loc, 12000)
	if err != nil {
		t.Errorf("getCoveredFields generated error %s for location %s and width 12000", err, loc)
	}
	if len(coveredFields) != 2 {
		t.Errorf("getCoveredFields could not find the correct size of the covered field. Should have been 2; found: %d", len(coveredFields))
	}
}
