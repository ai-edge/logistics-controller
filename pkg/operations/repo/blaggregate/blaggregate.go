package blaggregate

import (
	"ecs/api/generated/models"
	"ecs/internal/rabbitnamings"
	"ecs/pkg/operations/repo/block"
	"ecs/pkg/setup/repo/blockcfg"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

//BlAggregate aggregates fields and fieldstatuses for operations service
type BlAggregate struct {
	amqpWrap       *amqpwrap.AMQPWrapper
	BlockID        string
	bl             *block.Block
	transferFields map[string]models.TransferField
	stackingFields map[string]models.StackingField
	maxSFBay       int
	maxTFBay       int
}

//NewBlAggregate returns a new BlAggregate
func NewBlAggregate(bl *block.Block, blcfg *blockcfg.BlockCfg, amqpWrap *amqpwrap.AMQPWrapper) (*BlAggregate, error) {
	blAggregate := BlAggregate{bl: bl, BlockID: blcfg.BlockID, amqpWrap: amqpWrap}
	blAggregate.transferFields = make(map[string]models.TransferField)
	blAggregate.stackingFields = make(map[string]models.StackingField)

	// update fields according to items
	stFields, err := blcfg.GetStackingFields(blcfg.GetQueryLimit())
	if err != nil {
		err = fmt.Errorf("cannot get fields %w", err)
		return nil, err
	}

	blAggregate.maxSFBay = 0
	for _, field := range stFields {
		name, err := blockcfg.GetCompositeName(field.FieldBase.AreaName, field.FieldBase.Bay, field.FieldBase.Row, field.FieldBase.Tier)
		if err != nil {
			err = fmt.Errorf("field name is not valid %w", err)
			return nil, err
		}
		_, ok := blAggregate.stackingFields[name]
		if ok {
			return nil, fmt.Errorf("field %s in block %s is defined more than once in configuration", name, blcfg.BlockID)
		}
		blAggregate.stackingFields[name] = field

		//max bay nr require in checks when calculating covered areas
		bayNr, err := strconv.Atoi(field.FieldBase.Bay)
		if err != nil {
			return nil, fmt.Errorf("bay %s is not a valid number %w", field.FieldBase.Bay, err)
		}
		if bayNr > blAggregate.maxSFBay {
			blAggregate.maxSFBay = bayNr
		}
	}

	trFields, err := blcfg.GetTransferFields(blcfg.GetQueryLimit())
	if err != nil {
		err = fmt.Errorf("cannot get fields %w", err)
		return nil, err
	}

	blAggregate.maxTFBay = 0
	for _, field := range trFields {
		name, err := blockcfg.GetCompositeName(field.FieldBase.AreaName, field.FieldBase.Bay, field.FieldBase.Row, field.FieldBase.Tier)
		if err != nil {
			err = fmt.Errorf("field name is not valid %w", err)
			return nil, err
		}

		_, ok := blAggregate.transferFields[name]
		if ok {
			return nil, fmt.Errorf("field %s in block %s is defined more than once in configuration", name, blcfg.BlockID)
		}
		blAggregate.transferFields[name] = field

		//max bay nr require in checks when calculating covered areas
		bayNr, err := strconv.Atoi(field.FieldBase.Bay)
		if err != nil {
			return nil, fmt.Errorf("bay %s is not a valid number %w", field.FieldBase.Bay, err)
		}
		if bayNr > blAggregate.maxTFBay {
			blAggregate.maxTFBay = bayNr
		}
	}

	//TODO: implement comparison instead of deletion
	bl.DeleteStackingFieldStatuses()
	bl.DeleteTransferFieldStatuses()
	err = blAggregate.populateFieldStatuses()

	return &blAggregate, err
}

//DeInit closes connection
func (blAggregate *BlAggregate) DeInit() {
	//TODO:
}

func findISOID(ISOID string, items []models.StackedItem) (int, bool) {
	for i, it := range items {
		if it.ISOID == ISOID {
			return i, true
		}
	}
	return -1, false
}

// getFieldBase returns the field's basis object
func (blAggregate *BlAggregate) getFieldBase(compositeName string) (models.AbstractField, error) {
	sf, ok := blAggregate.stackingFields[compositeName]
	if ok {
		return sf.FieldBase, nil
	}
	tf, ok := blAggregate.transferFields[compositeName]
	if ok {
		return tf.FieldBase, nil
	}
	return models.AbstractField{}, fmt.Errorf("field name is invalid %s", compositeName)
}

//populateFieldStatuses updates fieldstatuses with container data
func (blAggregate *BlAggregate) populateFieldStatuses() error {
	containers, _ := blAggregate.bl.GetContainers(blAggregate.bl.GetQueryLimit()) //TODO: define max
	for _, container := range containers {
		err := blAggregate.PlaceContainer(container.ContainerIDInfo.ISOID, container.ContainerLogicalLocation)
		if err != nil {
			return fmt.Errorf("PlaceContainer failed %w", err)
		}
	}
	return nil
}

// getCoveredFields returns fields to be occupied by the item. It assumes that item's length equals to field's length.
func (blAggregate *BlAggregate) getCoveredFields(loc models.LogicalLocation3D, width int32) ([]models.AbstractField, int32, error) {
	var coveredFields []models.AbstractField
	if width <= 0 {
		return coveredFields, 0, fmt.Errorf("width is not acceptable")
	}
	bayNr, err := strconv.Atoi(loc.Bay)
	if err != nil {
		return coveredFields, 0, fmt.Errorf("bay is not a valid number %w", err)
	}
	remainingLen := width

	//iterate over the bays till the iterated fields cover container's total width
	name, err := blockcfg.GetCompositeName(loc.AreaName, loc.Bay, loc.Row, "01")
	if err != nil {
		return coveredFields, 0, fmt.Errorf("location is not valid %s %s %s %w",
			loc.AreaName, loc.Bay, loc.Row, err)
	}

	//tries to find a matching field on the center, then expands and collects adjacent fields.
	field, err := blAggregate.getFieldBase(name)
	if err == nil {
		coveredFields = append(coveredFields, field)
		remainingLen -= field.Width
	}
	remainingLenLeft := remainingLen / 2
	remainingLenRight := remainingLen / 2

	for offset := 1; remainingLenLeft > 0; offset++ {
		nameLeft, _ := blockcfg.GetCompositeName(loc.AreaName, fmt.Sprintf("%03d", bayNr-offset), loc.Row, "01")
		fieldLeft, errLeft := blAggregate.getFieldBase(nameLeft)
		if errLeft == nil { //if any fields on the left side (not out of boundaries and not even field numbers)
			coveredFields = append([]models.AbstractField{fieldLeft}, coveredFields...) //prepend
			remainingLenLeft -= fieldLeft.Width
			log.Tracef("occupied left field added %s for width %v", nameLeft, width)
		} else if bayNr-offset < 0 { //no field on the left side
			break
		}
	}

	for offset := 1; remainingLenRight > 0; offset++ {
		nameRight, _ := blockcfg.GetCompositeName(loc.AreaName, fmt.Sprintf("%03d", bayNr+offset), loc.Row, "01")
		fieldRight, errRight := blAggregate.getFieldBase(nameRight)
		if errRight == nil { //if any fields found on the right side...
			coveredFields = append(coveredFields, fieldRight)
			remainingLenRight -= fieldRight.Width
			log.Tracef("occupied right field added %s for width %v", nameRight, width)
		} else if bayNr+offset > blAggregate.maxSFBay && bayNr+offset > blAggregate.maxTFBay { //no field on the right side
			break
		}
	}

	if coveredFields == nil {
		return coveredFields, 0, fmt.Errorf("no covered fields for %s", name)
	}
	return coveredFields, remainingLenLeft, nil
}

//PlaceContainer informs the aggregate about a placed container.
func (blAggregate *BlAggregate) PlaceContainer(ISOID string, placeLocation models.LogicalLocation3D) error {
	container, err := blAggregate.GetContainerByISOID(ISOID)
	if err != nil {
		return fmt.Errorf("Error in place, container %s not found", ISOID)
	}
	container.ContainerLogicalLocation = placeLocation
	coveredFields, offset, err := blAggregate.getCoveredFields(container.ContainerLogicalLocation, container.ItemDimensions.Width)
	if err != nil {
		return fmt.Errorf("Occupied fields could not be retrieved %w", err)
	}
	fieldType := coveredFields[0].FieldType
	leftmostField := coveredFields[0]
	compositeName, err := blockcfg.GetCompositeName(placeLocation.AreaName, placeLocation.Bay, placeLocation.Row, "01")
	if err != nil {
		return fmt.Errorf("field name is not valid %w", err)
	}

	//Add/Update item & transfer field status
	switch fieldType {
	case models.TTRANSFER_FIELD:
		trStatus, err := blAggregate.bl.GetTransferFieldStatus(compositeName)

		if err != nil {
			//new status
			trStatus = models.TransferFieldStatus{
				FieldNameComposite: compositeName,
				FieldStatus:        models.TOCCUPIED,
				ItemCount:          0,
				Length:             container.ItemDimensions.Length,
				Width:              container.ItemDimensions.Width,
				X:                  leftmostField.X - offset,
				Y:                  leftmostField.Y,
			}
		} else {
			_, ok := findISOID(ISOID, trStatus.StackedItems)
			if ok {
				return fmt.Errorf("Placed container %s already listed in the fied", ISOID)
			}
		}

		stackedItem := models.StackedItem{
			ISOID:         container.ContainerIDInfo.ISOID,
			ContainerType: container.ContainerIDInfo.ContainerType,
			ItemGUID:      container.ItemGUID,
		}
		trStatus.StackedItems = append(trStatus.StackedItems, stackedItem)
		trStatus.ItemCount++
		_, err = blAggregate.bl.PutTransferFieldStatus(trStatus)
		if err != nil {
			return fmt.Errorf("PutTransferFieldStatus failed %w", err)
		}

		err = blAggregate.publishTransferFieldStatus(trStatus)
		_, err = blAggregate.bl.PatchContainerByISOID(container.ContainerIDInfo.ISOID, container)
		return err

	case models.TSTACKING_FIELD:
		stStatus, err := blAggregate.bl.GetStackingFieldStatus(compositeName)

		if err != nil {
			//new status
			stStatus = models.StackingFieldStatus{
				FieldNameComposite: compositeName,
				FieldStatus:        models.SOCCUPIED,
				ItemCount:          0,
				Length:             container.ItemDimensions.Length,
				Width:              container.ItemDimensions.Width,
				X:                  leftmostField.X - offset,
				Y:                  leftmostField.Y,
			}
		} else {
			_, ok := findISOID(ISOID, stStatus.StackedItems)
			if ok {
				return fmt.Errorf("Placed container %s already listed in the fied", ISOID)
			}
		}

		stackedItem := models.StackedItem{
			ISOID:         container.ContainerIDInfo.ISOID,
			ContainerType: container.ContainerIDInfo.ContainerType,
			ItemGUID:      container.ItemGUID,
		}
		stStatus.StackedItems = append(stStatus.StackedItems, stackedItem)
		stStatus.ItemCount++
		_, err = blAggregate.bl.PutStackingFieldStatus(stStatus)
		if err != nil {
			return fmt.Errorf("PutStackingFieldStatus failed %w", err)
		}

		err = blAggregate.publishStackingFieldStatus(stStatus)
		_, err = blAggregate.bl.PatchContainerByISOID(container.ContainerIDInfo.ISOID, container)
		return err

	default:
		return fmt.Errorf("container field is invalid %s", fieldType)
	}
}

//LiftContainer informs the aggregate about a lifted container
func (blAggregate *BlAggregate) LiftContainer(ISOID string, liftLocation models.LogicalLocation3D) error {
	container, err := blAggregate.GetContainerByISOID(ISOID)
	if err != nil {
		return fmt.Errorf("Error in lift, container %s not found", ISOID)
	}
	container.ContainerLogicalLocation = liftLocation
	coveredFields, _, err := blAggregate.getCoveredFields(container.ContainerLogicalLocation, container.ItemDimensions.Width)
	if err != nil {
		return fmt.Errorf("Occupied fields could not be retrieved %w", err)
	}
	fieldType := coveredFields[0].FieldType
	compositeName, err := blockcfg.GetCompositeName(liftLocation.AreaName, liftLocation.Bay, liftLocation.Row, "01")
	if err != nil {
		return fmt.Errorf("field name is not valid %w", err)
	}

	//Update item & transfer field status
	switch fieldType {
	case models.TTRANSFER_FIELD:
		trStatus, err := blAggregate.bl.GetTransferFieldStatus(compositeName)
		if err != nil {
			return fmt.Errorf("Transfer Field Status not found %w", err)
		}

		index, ok := findISOID(ISOID, trStatus.StackedItems)
		if !ok {
			return fmt.Errorf("Picked container %s not listed in the fied", ISOID)
		}
		trStatus.StackedItems = append(trStatus.StackedItems[:index], trStatus.StackedItems[index+1:]...)
		trStatus.ItemCount--

		if trStatus.ItemCount <= 0 {
			err = blAggregate.bl.DeleteTransferFieldStatus(compositeName)
			if err != nil {
				return fmt.Errorf("DeleteTransferFieldStatus failed %w", err)
			}
		} else {
			_, err = blAggregate.bl.PutTransferFieldStatus(trStatus)
			if err != nil {
				return fmt.Errorf("PutTransferFieldStatus failed %w", err)
			}
		}
		err = blAggregate.publishTransferFieldStatus(trStatus)
		_, err = blAggregate.bl.PatchContainerByISOID(ISOID, container)
		return err

	case models.TSTACKING_FIELD:
		stStatus, err := blAggregate.bl.GetStackingFieldStatus(compositeName)
		if err != nil {
			return fmt.Errorf("Stacking Field Status not found %w", err)
		}

		index, ok := findISOID(ISOID, stStatus.StackedItems)
		if !ok {
			return fmt.Errorf("Picked container %s not listed in the fied", ISOID)
		}
		stStatus.StackedItems = append(stStatus.StackedItems[:index], stStatus.StackedItems[index+1:]...)
		stStatus.ItemCount--

		if stStatus.ItemCount == 0 {
			err = blAggregate.bl.DeleteStackingFieldStatus(compositeName)
			if err != nil {
				return fmt.Errorf("DeleteStackingFieldStatus failed %w", err)
			}
		} else {
			_, err = blAggregate.bl.PutStackingFieldStatus(stStatus)
			if err != nil {
				return fmt.Errorf("PutStackingFieldStatus failed %w", err)
			}
		}
		err = blAggregate.publishStackingFieldStatus(stStatus)
		_, err = blAggregate.bl.PatchContainerByISOID(ISOID, container)
		return err

	default:
		return fmt.Errorf("container field is invalid %s", fieldType)
	}
}

//NewContainer defines a new container in the system (does not place it on a field)
func (blAggregate *BlAggregate) NewContainer(con models.Container) error {
	_, err := blAggregate.bl.GetContainerByISOID(con.ContainerIDInfo.ISOID)
	if err == nil {
		return fmt.Errorf("container already defined %v", con)
	}
	_, err = blAggregate.bl.PostContainer(con)
	if err != nil {
		return fmt.Errorf("PostContainer failed %w", err)
	}
	return nil
}

//DeleteContainer removes a container from the system
func (blAggregate *BlAggregate) DeleteContainer(con models.Container) error {
	_, err := blAggregate.bl.GetContainerByISOID(con.ContainerIDInfo.ISOID)
	if err != nil {
		return fmt.Errorf("container not found %v", con)
	}
	err = blAggregate.LiftContainer(con.ContainerIDInfo.ISOID, con.ContainerLogicalLocation)
	if err != nil {
		log.WithError(err).Errorln("cannot lift container")
	}
	return blAggregate.bl.DeleteContainerByISOID(con.ContainerIDInfo.ISOID)
}

//ModifyContainer modifies a container in the system
func (blAggregate *BlAggregate) ModifyContainer(con models.Container) error {
	foundContainer, err := blAggregate.bl.GetContainerByISOID(con.ContainerIDInfo.ISOID)
	if err != nil {
		return fmt.Errorf("container not found %v", con)
	}
	//A container found. Validate.
	err = block.CheckContainerMatch(con, foundContainer)
	if err != nil {
		return fmt.Errorf("CheckContainerMatch failed %w", err)
	}
	//TODO:
	return fmt.Errorf("ModifyContainer not implemented")
}

//HandleReservation handles transfer field reservation requests
func (blAggregate *BlAggregate) HandleReservation(tzr models.TzReservation) error {
	//reservations are stored under different name formating.
	cResName, err := blockcfg.GetCompositeReservationName(tzr.ReserveField.AreaName, tzr.ReserveField.Bay, tzr.ReserveField.Row, "01")
	if err != nil {
		return fmt.Errorf("field name is not valid %w", err)
	}
	resStatus, err := blAggregate.bl.GetTransferFieldStatus(cResName)
	if err != nil {
		//new status.
		if tzr.IsRelease {
			return fmt.Errorf("invalid release request. Field not reserved %s", cResName)
		}
		//reserve field
		coveredFields, offset, err := blAggregate.getCoveredFields(tzr.ReserveField, tzr.ResDimensions.Width)
		if err != nil {
			return fmt.Errorf("Occupied fields could not be retrieved %w", err)
		}
		leftmostField := coveredFields[0]

		resStatus = models.TransferFieldStatus{
			FieldNameComposite: cResName,
			ReservedHT:         tzr.HtID,
			FieldStatus:        models.TRESERVED,
			ItemCount:          0,
			Length:             tzr.ResDimensions.Length,
			Width:              tzr.ResDimensions.Width,
			X:                  leftmostField.X - offset,
			Y:                  leftmostField.Y,
		}
		sts, err := blAggregate.bl.GetTransferFieldStatuses(blAggregate.bl.GetQueryLimit())
		if err == nil {
			//check other transfer fields overlaps, in case containers on them, or reserved
			for _, st := range sts {
				if isOverlap(resStatus.X, resStatus.Width, st.X, st.Width) {
					return fmt.Errorf("overlapping fields %s  %s",
						cResName, st.FieldNameComposite)
				}
			}
		}

		log.Debugf("reserving field %s by %s", cResName, tzr.HtID)

	} else {
		//status exists.
		if tzr.IsRelease {
			if resStatus.ReservedHT != tzr.HtID {
				return fmt.Errorf("invalid release request %s. HT ID mismatch %s %s",
					cResName, resStatus.ReservedHT, tzr.HtID)
			}
			log.Debugf("releasing field %s by %s", cResName, tzr.HtID)
			resStatus.ReservedHT = ""
			resStatus.FieldStatus = models.TFREE
		} else {
			//already reserved
			return fmt.Errorf("Invalid reserve request. Field %s reserved by %s", cResName, resStatus.ReservedHT)
		}
	}

	if resStatus.FieldStatus == models.TRESERVED {
		_, err = blAggregate.bl.PutTransferFieldStatus(resStatus)
		if err != nil {
			return fmt.Errorf("PutTransferFieldStatus failed %w", err)
		}
	} else {
		err = blAggregate.bl.DeleteTransferFieldStatus(cResName)
		if err != nil {
			return fmt.Errorf("DeleteTransferFieldStatus failed %w", err)
		}
	}

	return blAggregate.publishTransferFieldStatus(resStatus)
}

//GetContainerByContainerID returns the container with the given container id
func (blAggregate *BlAggregate) GetContainerByContainerID(guid string) (models.Container, error) {
	return blAggregate.bl.GetContainerByContainerID(guid)
}

// GetContainerByISOID returns the container with the given container isoid
func (blAggregate *BlAggregate) GetContainerByISOID(containerisoid string) (models.Container, error) {
	return blAggregate.bl.GetContainerByISOID(containerisoid)
}

//GetContainers returns containers in the block
func (blAggregate *BlAggregate) GetContainers(limit int64) ([]models.Container, error) {
	return blAggregate.bl.GetContainers(limit)
}

// GetStackingFieldStatuses gets all stacking field status
func (blAggregate *BlAggregate) GetStackingFieldStatuses(limit int64) ([]models.StackingFieldStatus, error) {
	return blAggregate.bl.GetStackingFieldStatuses(limit)
}

// GetStackingFieldStatusWithFieldName gets stacking field status with the given name
func (blAggregate *BlAggregate) GetStackingFieldStatusWithFieldName(fieldNameComposite string) (models.StackingFieldStatus, error) {
	return blAggregate.bl.GetStackingFieldStatus(fieldNameComposite)
}

// GetTransferFieldStatuses gets all transfer field status
func (blAggregate *BlAggregate) GetTransferFieldStatuses(limit int64) ([]models.TransferFieldStatus, error) {
	return blAggregate.bl.GetTransferFieldStatuses(limit)
}

// publishTransferFieldStatus publishes transfer field status
func (blAggregate *BlAggregate) publishTransferFieldStatus(trStat models.TransferFieldStatus) error {
	log.Traceln("publishTransferFieldStatus", trStat)
	bytes, err := json.Marshal(trStat)
	if err != nil {
		return fmt.Errorf("marshalling failed for transfer field status :%v", trStat)
	}
	//publish the transferFieldStatus : operations/{blockid}/transferFieldstatus
	err = blAggregate.amqpWrap.PublishTo(rabbitnamings.L3RouteBase,
		rabbitnamings.L3RouteBase+"."+blAggregate.BlockID+"."+rabbitnamings.TrFieldStatusRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing transferFieldstatus failed: %w", err)
	}
	return err
}

// publishStackingFieldStatus publishes stacking field status
func (blAggregate *BlAggregate) publishStackingFieldStatus(stStat models.StackingFieldStatus) error {
	log.Traceln("publishStackingFieldStatus", stStat)
	bytes, err := json.Marshal(stStat)
	if err != nil {
		return fmt.Errorf("marshalling failed for stacking field status :%v", stStat)
	}
	//publish the schedule : operations/{blockid}/stackingfieldstatus
	err = blAggregate.amqpWrap.PublishTo(rabbitnamings.L3RouteBase,
		rabbitnamings.L3RouteBase+"."+blAggregate.BlockID+"."+rabbitnamings.StFieldStatusRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing stackingfieldstatus failed: %w", err)
	}
	return err
}

// IsDestinationValid checks the stacking field status if the container can be placed to the destination
func (blAggregate *BlAggregate) IsDestinationValid(width int64, containertype models.ContainerType, originfield string, destinationfield string) (bool, error) {

	fieldStatus, err := blAggregate.bl.GetStackingFieldStatus(destinationfield)
	if err == nil {
		if fieldStatus.ItemCount > 0 {
			if fieldStatus.StackedItems[0].ContainerType != containertype {
				log.WithError(err).Errorln("another type of container found at the destination ", destinationfield)
				return false, fmt.Errorf("another type of container found at the destination %s", destinationfield)
			}
			log.Debugln("A container with the same type is already placed. Safe to proceed")
			return true, nil
		}
	}

	s := strings.Split(destinationfield, "-")
	if len(s) < 4 {
		log.Errorln("input destination field is not valid ", destinationfield)
		return false, fmt.Errorf("input destination field is not valid")
	}
	locDestination := models.LogicalLocation3D{
		AreaName: s[0],
		Bay:      s[1],
		Row:      s[2],
		Tier:     s[3],
	}

	coveredFields, offset, err := blAggregate.getCoveredFields(locDestination, int32(width))
	if err != nil {
		log.WithError(err).Errorln("Occupied fields could not be retrieved")
		return false, fmt.Errorf("Occupied fields could not be retrieved %w", err)
	}

	if len(coveredFields) < 1 {
		log.Errorln("size of covered fields is not valid")
		return false, fmt.Errorf("size of covered fields is not valid")
	}

	stFieldsOnRow, err := blAggregate.bl.GetStackingFieldStatusBayRow(coveredFields[0].Y, blAggregate.bl.GetQueryLimit())
	if err != nil {
		log.Debugln("no items on the row, container can be placed")
		return true, nil
	}

	for _, coveredField := range coveredFields {

		for _, st := range stFieldsOnRow {
			if isOverlap(coveredField.X-offset, coveredField.Width, st.X, st.Width) {
				if st.FieldNameComposite == originfield {
					// here the overlap is with the originating field
					if st.ItemCount <= 1 {
						continue
					}
				}
				return false, fmt.Errorf("overlapping fields %s", st.FieldNameComposite)
			}
		}
	}
	return true, nil
}

/****************************************Helpers******************************************/

//isOverlap checks if 2 fields on the same row do overlap
func isOverlap(X1 int32, Width1 int32, X2 int32, Width2 int32) bool {
	xOverlap := ((X1 <= X2) && (X2 <= X1+Width1)) || ((X2 <= X1) && (X1 <= X2+Width2))
	return xOverlap
}
