package block

import (
	"ecs/api/generated/models"
	"ecs/internal/dbservice"
	"fmt"
)

//Block repository for terminal block
type Block struct {
	dbNames blockDBNames

	dbs dbservice.DBService
}

type blockDBNames struct {
	//DB names
	blockDB string
	//Collection Names
	itemsCollectionName,
	stackingFieldStatusCollectionName,
	transferFieldStatusCollectionName string
	//queryLimit maximum items to be returned
	queryLimit int64
	//IDField defines the name of the ID field in the DB
	IDField string
}

//NewBlockRepository returns a new repository of a container terminal's block
func NewBlockRepository(dbs dbservice.DBService, blockID string) (*Block, error) {

	if dbs == nil {
		return nil, fmt.Errorf("invalid dbs %v", dbs)
	}
	var dbNames blockDBNames

	//Block01DB is the name of the block DB
	dbNames.blockDB = blockID + "-block"

	dbNames.itemsCollectionName = "items"
	dbNames.stackingFieldStatusCollectionName = "stackingfieldstatus"
	dbNames.transferFieldStatusCollectionName = "transferfieldstatus"

	dbNames.queryLimit = 1000
	dbNames.IDField = "_id"

	block := Block{dbs: dbs, dbNames: dbNames}

	return &block, nil
}

//DeInit closes connection
func (bl *Block) DeInit() {
	bl.dbs.DeInit()
}

//CheckContainerIntegrity checks container's integrity
func CheckContainerIntegrity(container models.Container) error {
	if container.ItemType != models.CONTAINER {
		return fmt.Errorf("invalid item type %v", container)
	}
	if container.ItemGUID == "" {
		return fmt.Errorf("invalid id %v", container)
	}
	return nil
}

//CheckContainerMatch checks two containers if they are the same containers
func CheckContainerMatch(newContainer models.Container, container models.Container) error {
	if newContainer.ItemGUID != container.ItemGUID ||
		newContainer.ContainerIDInfo != container.ContainerIDInfo {
		return fmt.Errorf("not matching container new %v old %v", newContainer, container)
	}
	return nil
}

//GetFieldType returns field type of given location name
func (bl *Block) GetFieldType(name string) (models.FieldTypeEnum, error) {
	//check if a stacking field or transfer field matches
	_, err := bl.GetStackingFieldStatus(name)
	if err == nil {
		return models.TSTACKING_FIELD, nil
	}
	_, err = bl.GetTransferFieldStatus(name)
	if err != nil {
		return "", fmt.Errorf("cannot get status %w", err)
	}
	return models.TTRANSFER_FIELD, nil
}

//GetQueryLimit returns query limit parameter
func (bl *Block) GetQueryLimit() int64 {
	return bl.dbNames.queryLimit
}

//GetContainers returns containers in the block
func (bl *Block) GetContainers(limit int64) ([]models.Container, error) {
	var containers []models.Container
	query := map[string]interface{}{}
	err := bl.dbs.GetAll(bl.dbNames.blockDB, bl.dbNames.itemsCollectionName, query, &containers, limit)
	return containers, err
}

//GetContainerByContainerID returns a container in the block
func (bl *Block) GetContainerByContainerID(guid string) (models.Container, error) {
	var container models.Container
	query := map[string]interface{}{"itemguid": guid}
	err := bl.dbs.GetOne(bl.dbNames.blockDB, bl.dbNames.itemsCollectionName, query, &container)
	return container, err
}

//GetContainerByISOID returns a container in the block
func (bl *Block) GetContainerByISOID(containerisoid string) (models.Container, error) {
	var container models.Container
	query := map[string]interface{}{"containeridinfo.isoid": containerisoid}
	err := bl.dbs.GetOne(bl.dbNames.blockDB, bl.dbNames.itemsCollectionName, query, &container)
	return container, err
}

//PatchContainerByISOID patches an existing container
func (bl *Block) PatchContainerByISOID(containerisoid string, container models.Container) (models.Container, error) {
	query := map[string]interface{}{"containeridinfo.isoid": containerisoid}
	err := bl.dbs.ReplaceOne(bl.dbNames.blockDB, bl.dbNames.itemsCollectionName, query, &container)
	return container, err
}

// DeleteContainer deletes a container
func (bl *Block) DeleteContainer(guid string) error {
	query := map[string]interface{}{"itemguid": guid}
	err := bl.dbs.DeleteOne(bl.dbNames.blockDB, bl.dbNames.itemsCollectionName, query)
	return err
}

// DeleteContainerByISOID deletes a container
func (bl *Block) DeleteContainerByISOID(containerisoid string) error {
	query := map[string]interface{}{"containeridinfo.isoid": containerisoid}
	err := bl.dbs.DeleteOne(bl.dbNames.blockDB, bl.dbNames.itemsCollectionName, query)
	return err
}

//PostContainer adds a new container
func (bl *Block) PostContainer(container models.Container) (models.Container, error) {
	err := CheckContainerIntegrity(container)
	if err != nil {
		return container, fmt.Errorf("CheckContainerIntegrity failed %w", err)
	}
	err = bl.dbs.InsertOne(bl.dbNames.blockDB, bl.dbNames.itemsCollectionName, &container)
	return container, err
}

// DeleteStackingFieldStatuses deletes stacking field statuses
func (bl *Block) DeleteStackingFieldStatuses() error {
	query := map[string]interface{}{}
	err := bl.dbs.DeleteMany(bl.dbNames.blockDB, bl.dbNames.stackingFieldStatusCollectionName, query)
	return err
}

// GetStackingFieldStatus gets one stacking field status
func (bl *Block) GetStackingFieldStatus(name string) (models.StackingFieldStatus, error) {
	var st models.StackingFieldStatus
	query := map[string]interface{}{"fieldnamecomposite": name}
	err := bl.dbs.GetOne(bl.dbNames.blockDB, bl.dbNames.stackingFieldStatusCollectionName, query, &st)
	return st, err
}

// GetStackingFieldStatusBayRow gets all stacking field statuses on a row
func (bl *Block) GetStackingFieldStatusBayRow(y int32, limit int64) ([]models.StackingFieldStatus, error) {
	var st []models.StackingFieldStatus
	query := map[string]interface{}{"y": y}
	err := bl.dbs.GetAll(bl.dbNames.blockDB, bl.dbNames.stackingFieldStatusCollectionName, query, &st, limit)
	return st, err
}

// PutStackingFieldStatus puts one stacking field status
func (bl *Block) PutStackingFieldStatus(newSt models.StackingFieldStatus) (models.StackingFieldStatus, error) {
	var status models.StackingFieldStatus
	query := map[string]interface{}{"fieldnamecomposite": newSt.FieldNameComposite}
	err := bl.dbs.GetOne(bl.dbNames.blockDB, bl.dbNames.stackingFieldStatusCollectionName, query, &status)
	if err != nil {
		err = bl.dbs.InsertOne(bl.dbNames.blockDB, bl.dbNames.stackingFieldStatusCollectionName, &newSt)
	} else {
		err = bl.dbs.ReplaceOne(bl.dbNames.blockDB, bl.dbNames.stackingFieldStatusCollectionName, query, &newSt)
	}
	return newSt, err
}

// DeleteStackingFieldStatus deletes a stacking field status
func (bl *Block) DeleteStackingFieldStatus(name string) error {
	query := map[string]interface{}{"fieldnamecomposite": name}
	err := bl.dbs.DeleteOne(bl.dbNames.blockDB, bl.dbNames.stackingFieldStatusCollectionName, query)
	return err
}

// GetStackingFieldStatuses gets all stacking field status
func (bl *Block) GetStackingFieldStatuses(limit int64) ([]models.StackingFieldStatus, error) {
	var statuses []models.StackingFieldStatus
	query := map[string]interface{}{}
	err := bl.dbs.GetAll(bl.dbNames.blockDB, bl.dbNames.stackingFieldStatusCollectionName, query, &statuses, limit)
	return statuses, err
}

// DeleteTransferFieldStatuses deletes transfer field statuses
func (bl *Block) DeleteTransferFieldStatuses() error {
	query := map[string]interface{}{}
	err := bl.dbs.DeleteMany(bl.dbNames.blockDB, bl.dbNames.transferFieldStatusCollectionName, query)
	return err
}

// GetTransferFieldStatus gets one transfer field status
func (bl *Block) GetTransferFieldStatus(name string) (models.TransferFieldStatus, error) {
	var st models.TransferFieldStatus
	query := map[string]interface{}{"fieldnamecomposite": name}
	err := bl.dbs.GetOne(bl.dbNames.blockDB, bl.dbNames.transferFieldStatusCollectionName, query, &st)
	return st, err
}

// PutTransferFieldStatus puts one Transfer field status
func (bl *Block) PutTransferFieldStatus(newSt models.TransferFieldStatus) (models.TransferFieldStatus, error) {
	var status models.TransferFieldStatus
	query := map[string]interface{}{"fieldnamecomposite": newSt.FieldNameComposite}
	err := bl.dbs.GetOne(bl.dbNames.blockDB, bl.dbNames.transferFieldStatusCollectionName, query, &status)
	if err != nil {
		err = bl.dbs.InsertOne(bl.dbNames.blockDB, bl.dbNames.transferFieldStatusCollectionName, &newSt)
	} else {
		err = bl.dbs.ReplaceOne(bl.dbNames.blockDB, bl.dbNames.transferFieldStatusCollectionName, query, &newSt)
	}
	return newSt, err
}

// DeleteTransferFieldStatus deletes a transfer field status
func (bl *Block) DeleteTransferFieldStatus(name string) error {
	query := map[string]interface{}{"fieldnamecomposite": name}
	err := bl.dbs.DeleteOne(bl.dbNames.blockDB, bl.dbNames.transferFieldStatusCollectionName, query)
	return err
}

// GetTransferFieldStatuses gets all transfer field status
func (bl *Block) GetTransferFieldStatuses(limit int64) ([]models.TransferFieldStatus, error) {
	var status []models.TransferFieldStatus
	query := map[string]interface{}{}
	err := bl.dbs.GetAll(bl.dbNames.blockDB, bl.dbNames.transferFieldStatusCollectionName, query, &status, limit)
	return status, err
}
