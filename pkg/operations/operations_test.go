package operations

import (
	"ecs/api/generated/models"
	"ecs/internal/mockdb"
	"ecs/pkg/operations/repo/operation"
	"testing"
)

func createOperations(t *testing.T) (*Operations, error) {
	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovered in f: %v", r)
		}
	}()

	db, err := mockdb.NewMockDBClient("mockdb://localhost:2700", "admin", "test", "test")
	if err != nil {
		t.Fatalf("mockdb.NewMockDBClient(...) = %v, %s", db, err)
	}
	t.Logf("mockdb is created: %v", db)
	op, err := operation.NewOperationRepository(db, "oper0001", "BL01")
	if err != nil {
		t.Fatalf("operation.NewBOperationRepository(...) = %v, %s", op, err)
	}
	t.Logf("operation db is created: %v", op)

	ops := &Operations{op: op}
	t.Logf("operations is created: %v", ops)
	return ops, err
}

func TestNewOperations(t *testing.T) {
	ops, err := createOperations(t)

	if err != nil {
		t.Fatalf("NewOperations(&bl, wrap) = %v, %s", ops, err)
	} else {
		t.Logf("NewOperations(&bl, wrap) created successfully. ops = %v", ops)
	}
}

func (ops *Operations) TestNotifyResourceStatusNoStatus(t *testing.T) {
	ops, err := createOperations(t)

	if err != nil {
		t.Logf("NewOperations(&bl, wrap) = %v, %s", ops, err)
	} else {
		t.Logf("NewOperations(&bl, wrap) created successfully. ops = %v", ops)
		rstatus := models.StackingCraneStatusBase{}
		err = ops.NotifyResourceStatus(rstatus)

		if err == nil {
			t.Errorf("NotifyResourceStatus failed. rstatus is empty but error is not generated.")
		} else {
			t.Logf("NotifyResourceStatus is successful. Err: %s", err)
		}
	}
}

// TODO : TestNotifyResourceStatusValidStatus

func TestNotifyOperationsOrderNoWo(t *testing.T) {
	ops, err := createOperations(t)

	if err != nil {
		t.Logf("NewOperations(&bl, wrap) = %v, %s", ops, err)
	} else {
		t.Logf("NewOperations(&bl, wrap) created successfully. ops = %v", ops)
		wo := models.OperationsWorkOrderBase{WorkOrderReqType: models.NEW_WO}
		err = ops.NotifyOperationsOrderRequest(wo)

		if err == nil {
			t.Errorf("NotifyOperationsOrderRequest failed. wo is empty but error is not generated.")
		} else {
			t.Logf("NotifyOperationsOrderRequest is successful. Err: %s", err)
		}
	}
}

// TODO : TestNotifyOperationsOrderValidWo

func TestCreateWorkOrderItemStatusNoWi(t *testing.T) {
	ops, err := createOperations(t)
	if err != nil {
		t.Fatalf("cannot create operations %v, %s", ops, err)
	}
	t.Logf("operations created successfully %v", ops)
	wi := models.OperationsWorkOrderItemBase{}
	wis, err := ops.createWorkOrderItemStatus(wi)

	if err == nil {
		t.Errorf("createWorkOrderItemStatus not failed despite empyt Wi. %v", wis)
	} else {
		t.Logf("createWorkOrderItemStatus failed with empty wi %s", err)
	}
}
