//Package operations is the root package for operations system app.
package operations

import (
	"ecs/api/generated/models"
	"ecs/internal/rabbitnamings"
	"ecs/pkg/operations/asyncsrv"
	"ecs/pkg/operations/repo/blaggregate"
	"ecs/pkg/operations/repo/block"
	"ecs/pkg/operations/repo/operation"
	"ecs/pkg/setup/repo/blockcfg"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"fmt"
	"time"

	"github.com/jinzhu/copier"
	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

//Operations operations system
type Operations struct {
	amqpWrap    *amqpwrap.AMQPWrapper
	as          *asyncsrv.Asyncsrv
	BlAggregate *blaggregate.BlAggregate

	op *operation.Operation
}

//NewOperations initialize Operations
func NewOperations(bl *block.Block, blcfg *blockcfg.BlockCfg, op *operation.Operation, conn *amqp.Connection) (*Operations, error) {
	operations := &Operations{op: op}

	//init AMQP wrapper
	wrap, err := amqpwrap.NewAMQPWrapper(conn)
	if err != nil {
		return nil, fmt.Errorf("Could not create amqp wrapper: %w", err)
	}
	//different channels for publish and subscribe
	err = wrap.InitPubChannel()
	if err != nil {
		return nil, fmt.Errorf("Init channel failed %w", err)
	}
	err = wrap.InitSubChannel()
	if err != nil {
		return nil, fmt.Errorf("Init channel failed %w", err)
	}
	operations.amqpWrap = wrap

	//init BlAggregate
	BlAggregate, err := blaggregate.NewBlAggregate(bl, blcfg, wrap)
	if err != nil {
		return nil, err
	}
	operations.BlAggregate = BlAggregate

	//Init consuming async messages
	as, err := asyncsrv.NewAsyncsrv(blcfg.BlockID, operations, wrap)
	if err != nil {
		return nil, err
	}
	operations.as = as
	go as.Asyncsrv()

	return operations, nil
}

//DeInit deinitialize the operations
func (ops *Operations) DeInit() {
	ops.as.DeInit()
	ops.amqpWrap.DeInit()
	ops.BlAggregate.DeInit()
}

//createWorkOrderItemStatus creates an initial work order item status from a work order item
func (ops *Operations) createWorkOrderItemStatus(wi models.OperationsWorkOrderItemBase) (models.OperationsWorkOrderItemStatusBase, error) {

	var wis models.OperationsWorkOrderItemStatusBase
	if wi.ItemStatus != models.ENTERED {
		return wis, fmt.Errorf("not supported item status %s", wi.ItemStatus)
	}
	//copy fields with identical names and types
	err := copier.Copy(&wis, &wi)
	if err != nil {
		return wis, err
	}
	wis.StackingCraneBayLocation = models.BayLogicalLocation{}
	return wis, nil
}

//isWoActive returns found statuses and nil if wo is active.
func (ops *Operations) isWoActive(woName string) ([]models.OperationsWorkOrderItemStatusBase, error) {
	sts, err := ops.op.GetWorkOrderItemStatusesByOrderName(woName, ops.op.GetQueryLimit())
	if err != nil {
		return sts, fmt.Errorf("work item statuses not found %s", woName)
	}
	for _, st := range sts {
		if isItemExecuting(st) {
			return sts, nil
		}
	}
	return sts, fmt.Errorf("wo inactive %s", woName)
}

// publishSchedule publishes schedule to scheduler service
func (ops *Operations) publishSchedule(sc models.Schedule) error {
	bytes, err := json.Marshal(sc)
	if err != nil {
		return fmt.Errorf("marshalling failed for schedule :%v", sc)
	}
	//publish the schedule : scheduler/{blockid}/schedulerequest
	err = ops.amqpWrap.PublishTo(rabbitnamings.ScheRouteBase,
		rabbitnamings.ScheRouteBase+"."+ops.op.BlockID+"."+rabbitnamings.ScheduleRequestRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing schedule failed: %w", err)
	}
	return err
}

//publishWorkItemStatus publishes work item status event
func (ops *Operations) publishWorkItemStatus(wiSt models.OperationsWorkOrderItemStatusBase) error {
	log.Debugln("publishWorkItemStatus", wiSt)
	bytes, err := json.Marshal(wiSt)
	if err != nil {
		return fmt.Errorf("marshalling failed for work item status :%v", wiSt)
	}
	//publish to operations/{blockid}/workorderprogress
	err = ops.amqpWrap.PublishTo(rabbitnamings.L3RouteBase,
		rabbitnamings.L3RouteBase+"."+ops.op.BlockID+"."+rabbitnamings.WorkOrderProgressRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing work item status failed: %w", err)
	}
	return err
}

// publishContainerUpdate publishes information about a container
func (ops *Operations) publishContainerUpdate(containerUpdate models.OperationsContainerUpdateBase) error {
	log.Debugln("publishContainerUpdate", containerUpdate)
	bytes, err := json.Marshal(containerUpdate)
	if err != nil {
		return fmt.Errorf("marshalling failed for container update :%v", containerUpdate)
	}

	//publish operations/{blockid}/containerupdaterequest
	err = ops.amqpWrap.PublishTo(rabbitnamings.L3RouteBase,
		rabbitnamings.L3RouteBase+"."+ops.op.BlockID+"."+rabbitnamings.ContainerUpdateRequestRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing container update failed: %w", err)
	}
	return err
}

//publishDeleteWorkOrder publishes when a work order is deleted
func (ops *Operations) publishDeleteWorkOrder(wo models.OperationsWorkOrderBase) error {
	log.Debugln("publishDeleteWorkOrder", wo)
	bytes, err := json.Marshal(wo)
	if err != nil {
		return fmt.Errorf("marshalling failed for work order :%v", wo)
	}
	//publish to operations/{blockid}/deletedworkorder
	err = ops.amqpWrap.PublishTo(rabbitnamings.L3RouteBase,
		rabbitnamings.L3RouteBase+"."+ops.op.BlockID+"."+rabbitnamings.WorkOrderDeletedFromOperationsRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing work order failed: %w", err)
	}
	return err
}

//publishAddWorkOrder publishes when a work order is added
func (ops *Operations) publishAddWorkOrder(wo models.OperationsWorkOrderBase) error {
	log.Debugln("publishAddWorkOrder", wo)
	bytes, err := json.Marshal(wo)
	if err != nil {
		return fmt.Errorf("marshalling failed for work order :%v", wo)
	}
	//publish to operations/{blockid}/workorderprogress
	err = ops.amqpWrap.PublishTo(rabbitnamings.L3RouteBase,
		rabbitnamings.L3RouteBase+"."+ops.op.BlockID+"."+rabbitnamings.WorkOrderAddedToOperationsRouteKey,
		bytes)
	if err != nil {
		return fmt.Errorf("publishing work item status failed: %w", err)
	}
	return err
}

/****************************************Command and Event Handlers******************************************/

//NotifyResourceStatus notifies a newly received crane status
func (ops *Operations) NotifyResourceStatus(st models.StackingCraneStatusBase) error {
	//update repository
	_, err := ops.op.PutStatusByScraneID(st.StackingCraneID, st)
	return err
}

//NotifyOperationsOrderRequest handles a newly received work order in an idempotent manner
func (ops *Operations) NotifyOperationsOrderRequest(wo models.OperationsWorkOrderBase) error {

	switch wo.WorkOrderReqType {
	case models.DELETE_WO:
		//deletes only if wo finished
		_, err := ops.op.GetWorkOrderByName(wo.WorkOrderName)
		if err != nil {
			log.WithError(err).Errorln("work order not found", wo.WorkOrderName)
		}
		//Delete item statuses if wo is inactive
		//TODO: force delete atm
		sts, err := ops.isWoActive(wo.WorkOrderName)
		for _, st := range sts {
			err = ops.op.DeleteWorkItemStatusByID(st.WorkItemName)
			if err != nil {
				log.WithError(err).Errorln("cannot delete work item", st.WorkItemName)
			}
		}
		err = ops.op.DeleteWorkOrder(wo.WorkOrderName)
		if err == nil {
			//publish deleteWorkOrder
			err = ops.publishDeleteWorkOrder(wo)
			if err != nil {
				log.WithError(err).Errorln("publishDeleteWorkOrder failed ", wo)
			}
		}
		return err

	case models.CANCEL_WO:
		_, err := ops.op.GetWorkOrderByName(wo.WorkOrderName)
		if err != nil {
			log.WithError(err).Errorln("work order not found", wo.WorkOrderName)
		}
		//Cancel not-finished work items
		sts, err := ops.isWoActive(wo.WorkOrderName)
		for _, st := range sts {
			if isItemWaiting(st) || isItemExecuting(st) {
				sc := models.Schedule{ScheduleReqType: models.CANCEL_SCHE, ParentSchedule: ""}
				err := copier.Copy(&(sc.WorkItem), &st)
				if err != nil {
					log.WithError(err).Errorln("cannot copy schedule types", st)
					break
				}
				err = ops.publishSchedule(sc)
				if err != nil {
					log.WithError(err).Errorln("publishSchedule failed ", sc)
					break
				}
			}
		}
		return err

	case models.NEW_WO:
		//Check if an order with the same sequence number has already been received
		_, err := ops.op.GetWorkOrderBySequence(wo.WorkOrderSequence)
		if err == nil {
			return fmt.Errorf("work order with sequence %d already received", wo.WorkOrderSequence)
		}
		//iterate over newly received items
		for _, wi := range wo.OperationsWorkOrderItems {
			if wi.HandledItem != (models.Container{}) {
				_, err := ops.BlAggregate.GetContainerByContainerID(wi.HandledItem.ItemGUID)
				if err != nil {
					//define a new container in the system
					log.Debugf("container %s not found. Defining a new container entry", wi.HandledItem.ItemGUID)
					containerUpdate := models.OperationsContainerUpdateBase{ConUpdateType: models.NEW_CON, Container: wi.HandledItem}
					err = ops.publishContainerUpdate(containerUpdate)
					if err != nil {
						return fmt.Errorf("publishContainerUpdate failed %w", err)
					}
				}
			}
			//create work item status and put into repository
			wis, err := ops.createWorkOrderItemStatus(wi)
			if err != nil {
				return fmt.Errorf("createWorkOrderItemStatus failed %w", err)
			}

			//publish scheduleRequest
			sc := models.Schedule{WorkItem: wi, ScheduleReqType: models.NEW_SCHE, ParentSchedule: ""}
			err = ops.publishSchedule(sc)
			if err != nil {
				log.WithError(err).Errorln("publishSchedule failed ", sc)
			} else {
				wis.WiDispatchTime = time.Now()
			}

			_, err = ops.op.PutWorkOrderItemStatusByID(wis.WorkItemName, wis)
			if err != nil {
				return fmt.Errorf("PutWorkOrderItemStatusByID failed: %w", err)
			}
			err = ops.publishWorkItemStatus(wis)
			if err != nil {
				return fmt.Errorf("publishWorkItemStatus failed: %w", err)
			}
		}
		//put received work order into repository
		_, err = ops.op.PutWorkOrderBySequence(wo.WorkOrderSequence, wo)
		if err != nil {
			return fmt.Errorf("PutWorkOrderBySequence failed: %w", err)
		}

		//publish addWorkOrder
		err = ops.publishAddWorkOrder(wo)
		if err != nil {
			log.WithError(err).Errorln("publishAddWorkOrder failed ", wo)
		}

		return err

	default:
		return fmt.Errorf("not implemented order type %s", wo.WorkOrderReqType)
	}
}

//NotifyScheduleStatus handles a newly received schedule status
func (ops *Operations) NotifyScheduleStatus(scStat models.ScheduleStatus) error {
	//works in case of duplicates receives
	wis, err := ops.op.GetWorkOrderItemStatusByID(scStat.WorkItem.WorkItemName)
	if err != nil {
		return fmt.Errorf("GetWorkOrderItemStatusByID failed: %w", err)
	}
	switch scStat.ScheduleState {
	case models.S_WAITING:
		wis.AckTime = time.Now()
		wis.ItemStatus = models.WAITING
	case models.S_INITIALIZE:
		wis.ItemStatus = models.WORKING
	case models.S_IN_PROGRESS:
		wis.ItemStatus = models.WORKING
	case models.S_ABORTED:
		wis.ItemStatus = models.ABORTED
	case models.S_CANCELED:
		wis.ItemStatus = models.CANCELED
	case models.S_FINISHED:
		wis.ItemStatus = models.COMPLETED
	default:
		return fmt.Errorf("not supported scheduler state %s", scStat.ScheduleState)
	}

	wis.UpdateTime = time.Now()
	_, err = ops.op.PutWorkOrderItemStatusByID(wis.WorkItemName, wis)
	if err != nil {
		return fmt.Errorf("PutWorkOrderItemStatusByID failed: %w", err)
	}
	return ops.publishWorkItemStatus(wis)
}

//NotifyContainerUpdateRequest notifies a newly received container update request in an idempotent manner
func (ops *Operations) NotifyContainerUpdateRequest(conUpdate models.OperationsContainerUpdateBase) error {
	//works in case of duplicates receives
	switch conUpdate.ConUpdateType {
	case models.NEW_CON:
		return ops.BlAggregate.NewContainer(conUpdate.Container)
	case models.DELETE_CON:
		//check if an active work order deals with this container
		sts, err := ops.op.GetWorkOrderItemStatusesByItemGUID(conUpdate.Container.ItemGUID, ops.op.GetQueryLimit())
		if err == nil {
			for _, st := range sts {
				if isItemWaiting(st) || isItemExecuting(st) {
					return fmt.Errorf("Cannot delete, an ongoing work item %s handles this container %s",
						st.WorkItemName, conUpdate.Container.ItemGUID)
				}
			}
		}
		return ops.BlAggregate.DeleteContainer(conUpdate.Container)
	//case models.MODIFY_CON:
	//	return ops.BlAggregate.ModifyContainer(conUpdate.Container)
	default:
		return fmt.Errorf("invalid container update type %s", conUpdate.ConUpdateType)
	}
}

//NotifyContainerStatus handles a newly received container status
func (ops *Operations) NotifyContainerStatus(coStat models.ContainerStatus) error {
	//works in case of duplicates receives, returns error if trying to place or remove more than once on/from the same location
	if coStat.ContainerPlaced {
		return ops.BlAggregate.PlaceContainer(coStat.ContainerIDInfo.ISOID, coStat.ContainerLogicalLocation)
	}
	return ops.BlAggregate.LiftContainer(coStat.ContainerIDInfo.ISOID, coStat.ContainerLogicalLocation)
}

//NotifyTruckStatus handles a new truck status
func (ops *Operations) NotifyTruckStatus(ts models.HtStatus) error {
	tsRepo, err := ops.op.GetCTruckStatusByID(ts.HtID)
	if err != nil || tsRepo.HtState != ts.HtState {
		// status is new or it is changed
		ts.LastUpdated = time.Now()
	}
	_, err = ops.op.PutCTruckStatusByID(ts.HtID, ts)
	return err
}

//NotifyTZReservationRequest handles a tz reservation request
func (ops *Operations) NotifyTZReservationRequest(tzr models.TzReservation) error {
	err := ops.BlAggregate.HandleReservation(tzr)
	if err != nil {
		log.WithError(err).Errorln("HandleReservation failed")
	}
	return err
}

func isItemWaiting(st models.OperationsWorkOrderItemStatusBase) bool {
	return st.ItemStatus == models.ENTERED ||
		st.ItemStatus == models.WAITING
}

func isItemExecuting(st models.OperationsWorkOrderItemStatusBase) bool {
	return st.ItemStatus == models.QUEUED ||
		st.ItemStatus == models.WORKING
}
