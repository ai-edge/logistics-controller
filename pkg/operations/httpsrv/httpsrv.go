//Package httpsrv is the root package for handling http requests.
package httpsrv

import (
	"ecs/pkg/operations/repo/blaggregate"
	"ecs/pkg/operations/repo/operation"
	"ecs/pkg/setup/repo/blockcfg"
	"ecs/pkg/setup/repo/terminal"
	"ecs/tools/amqpwrap"
	"net/http"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

//TODO: implement return vals according to rest api

//StartHTTPSrv start http server of operations. Blocking function
func StartHTTPSrv(server *http.Server, blAggregate *blaggregate.BlAggregate, op *operation.Operation, blCfg *blockcfg.BlockCfg, tr *terminal.Terminal, conn *amqp.Connection) {

	//init AMQP wrapper
	amqpWrap, err := amqpwrap.NewAMQPWrapper(conn)
	if err != nil {
		log.WithError(err).Fatal("Could not create amqp wrapper")
	}
	defer amqpWrap.DeInit()
	//Publish only
	err = amqpWrap.InitPubChannel()
	if err != nil {
		log.WithError(err).Fatal("Init channel failed")
	}

	//Init services

	//services that access value object dbs
	SetupAPIService := NewSetupAPIService(tr, blCfg, amqpWrap)
	SetupAPIController := NewSetupAPIController(SetupAPIService)

	StackingfieldsAPIService := NewStackingfieldsAPIService(blCfg)
	StackingfieldsAPIController := NewStackingfieldsAPIController(StackingfieldsAPIService)

	TransferfieldsAPIService := NewTransferfieldsAPIService(blCfg)
	TransferfieldsAPIController := NewTransferfieldsAPIController(TransferfieldsAPIService)

	//services that access entity object dbs
	ContainersAPIService := NewContainersAPIService(blAggregate, amqpWrap)
	ContainersAPIController := NewContainersAPIController(ContainersAPIService)

	IsdestinationvalidAPIService := NewIsdestinationvalidAPIService(blAggregate)
	IsdestinationvalidAPIController := NewIsdestinationvalidAPIController(IsdestinationvalidAPIService)

	ScranesAPIService := NewScranesAPIService(tr)
	ScranesAPIController := NewScranesAPIController(ScranesAPIService)

	ScranestatusesAPIService := NewScranestatusesAPIService(op)
	ScranestatusesAPIController := NewScranestatusesAPIController(ScranestatusesAPIService)

	StackingfieldstatusesAPIService := NewStackingfieldstatusesAPIService(blAggregate)
	StackingfieldstatusesAPIController := NewStackingfieldstatusesAPIController(StackingfieldstatusesAPIService)

	TransferfieldstatusesAPIService := NewTransferfieldstatusesAPIService(blAggregate)
	TransferfieldstatusesAPIController := NewTransferfieldstatusesAPIController(TransferfieldstatusesAPIService)

	CtruckstatusesAPIService := NewCtruckstatusesAPIService(op)
	CtruckstatusesAPIController := NewCtruckstatusesAPIController(CtruckstatusesAPIService)

	TzreservationsAPIService := NewTzreservationsAPIService(tr, amqpWrap)
	TzreservationsAPIController := NewTzreservationsAPIController(TzreservationsAPIService)

	WorkorderitemstatusesAPIService := NewWorkorderitemstatusesAPIService(op)
	WorkorderitemstatusesAPIController := NewWorkorderitemstatusesAPIController(WorkorderitemstatusesAPIService)

	WorkordersAPIService := NewWorkordersAPIService(op, amqpWrap)
	WorkordersAPIController := NewWorkordersAPIController(WorkordersAPIService)

	router := NewRouter(ContainersAPIController, IsdestinationvalidAPIController, ScranesAPIController,
		ScranestatusesAPIController, SetupAPIController, StackingfieldsAPIController,
		StackingfieldstatusesAPIController, TransferfieldsAPIController, TransferfieldstatusesAPIController,
		CtruckstatusesAPIController, TzreservationsAPIController, WorkorderitemstatusesAPIController,
		WorkordersAPIController)

	server.Handler = router

	log.Debugln("starting server on the port", server.Addr, "...")
	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.WithError(err).Fatalf("ListenAndServe failed")
	}
	log.Debugln("exiting http server")
}
