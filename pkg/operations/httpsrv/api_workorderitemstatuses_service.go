/*
 * L3 REST API
 *
 * Operations Service REST API for simulation
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package httpsrv

import (
	"ecs/pkg/operations/repo/operation"
)

// WorkorderitemstatusesAPIService is a service that implents the logic for the WorkorderitemstatusesAPIServicer
// This service should implement the business logic for every endpoint for the WorkorderitemstatusesAPI API.
// Include any external packages or services that will be required by this service.
type WorkorderitemstatusesAPIService struct {
	op *operation.Operation
}

// NewWorkorderitemstatusesAPIService creates a default api service
func NewWorkorderitemstatusesAPIService(op *operation.Operation) *WorkorderitemstatusesAPIService {
	return &WorkorderitemstatusesAPIService{op: op}
}

// GetWorkOrderItemStatuses - Get all work order item statuses
func (s *WorkorderitemstatusesAPIService) GetWorkOrderItemStatuses(limit int64) (interface{}, error) {
	return s.op.GetWorkOrderItemStatuses(limit)
}
