/*
 * L3 REST API
 *
 * Operations Service REST API for simulation
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package httpsrv

import "ecs/pkg/setup/repo/blockcfg"

// TransferfieldsAPIService is a service that implents the logic for the TransferfieldsAPIServicer
// This service should implement the business logic for every endpoint for the TransferfieldsAPI API.
// Include any external packages or services that will be required by this service.
type TransferfieldsAPIService struct {
	blCfg *blockcfg.BlockCfg
}

// NewTransferfieldsAPIService creates a default api service
func NewTransferfieldsAPIService(blCfg *blockcfg.BlockCfg) *TransferfieldsAPIService {
	return &TransferfieldsAPIService{blCfg: blCfg}
}

// GetTransferFields - Get all transfer fields in the entire system
func (s *TransferfieldsAPIService) GetTransferFields(limit int64) (interface{}, error) {
	return s.blCfg.GetTransferFields(limit)
}
