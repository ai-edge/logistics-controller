//Package asyncsrv consumes incoming async messages
package asyncsrv

import (
	"ecs/api/generated/models"
	"ecs/internal/rabbitnamings"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type controllerService interface {
	NotifyResourceStatus(models.StackingCraneStatusBase) error
	NotifyCreatedWorkOrderRequest(models.OperationsWorkOrderBase) error
	NotifyRemovedWorkOrderRequest(string) error
	NotifyAddedWorkOrderToOperationsRequest(models.OperationsWorkOrderBase) error
	NotifyDeletedWorkOrderFromOperationsRequest(models.OperationsWorkOrderBase) error
	NotifyWorkItemRequest(models.OperationsWorkOrderItemStatusBase) error
	NotifyStFieldStatus(models.StackingFieldStatus) error
	NotifyTrFieldStatus(models.TransferFieldStatus) error
	NotifyTruckStatus(models.HtStatus) error
}

//Asyncsrv is the async service
type Asyncsrv struct {
	L2StatusCh           <-chan amqp.Delivery
	L4CreatedWorkOrderCh <-chan amqp.Delivery
	L4RemovedWorkOrderCh <-chan amqp.Delivery
	L3StFieldStCh        <-chan amqp.Delivery
	L3TrFieldStCh        <-chan amqp.Delivery
	L3AddedWoCh          <-chan amqp.Delivery
	L3DeletedWoCh        <-chan amqp.Delivery
	L3WorkItemCh         <-chan amqp.Delivery
	L4TruckStatusCh      <-chan amqp.Delivery

	connClosedCh <-chan *amqp.Error
	subCloseCh   <-chan *amqp.Error
	subCancelCh  <-chan string

	stopCh chan bool

	amqpWrap *amqpwrap.AMQPWrapper

	blockID string

	controller controllerService
}

//NewAsyncsrv returns a new async service. Creates exchanges only for consumed messages.
func NewAsyncsrv(blockID string, controller controllerService, amqpWrap *amqpwrap.AMQPWrapper) (*Asyncsrv, error) {
	asyncsrv := Asyncsrv{controller: controller, amqpWrap: amqpWrap, blockID: blockID}
	var err error

	asyncsrv.stopCh = make(chan bool)

	//subscribe to scgw/*/cranestatus
	route := rabbitnamings.L2RouteBase + ".*." + rabbitnamings.CraneStatusRouteKey
	asyncsrv.L2StatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.CraneStatusRouteKey, //queue name contains route key
		rabbitnamings.MQTTExchange,                                //exchange name e.g. "amq.topic" for mqtt
		route)

	if err != nil {
		return nil, err
	}

	//subscribe to operations/*/stackingFieldstatus
	route = rabbitnamings.L3RouteBase + "." + "*" + "." + rabbitnamings.StFieldStatusRouteKey
	asyncsrv.L3StFieldStCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.StFieldStatusRouteKey, //queue name contains route key
		rabbitnamings.L3RouteBase,                                   //exchange name
		route)

	//subscribe to operations/*/transferFieldstatus
	route = rabbitnamings.L3RouteBase + ".*." + rabbitnamings.TrFieldStatusRouteKey
	asyncsrv.L3TrFieldStCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.TrFieldStatusRouteKey, //queue name contains route key
		rabbitnamings.L3RouteBase,                                   //exchange name
		route)

	//subscribe to ht/*/truckstatus
	route = rabbitnamings.HtRouteBase + ".*." + rabbitnamings.TruckStatusRouteKey
	asyncsrv.L4TruckStatusCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.TruckStatusRouteKey, //queue name contains route key
		rabbitnamings.HtRouteBase,                                 //exchange name e.g. "amq.topic" for mqtt
		route)

	//subscribe to tosgw/*/createdworkorder
	route = rabbitnamings.L4RouteBase + ".*." + rabbitnamings.CreatedWorkOrderRouteKey
	asyncsrv.L4CreatedWorkOrderCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.CreatedWorkOrderRouteKey, //queue name contains route key
		rabbitnamings.L4RouteBase, //exchange name e.g. "amq.topic" for mqtt
		route)

	//subscribe to tosgw/*/removedworkorder
	route = rabbitnamings.L4RouteBase + ".*." + rabbitnamings.RemovedWorkOrderRouteKey
	asyncsrv.L4RemovedWorkOrderCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.RemovedWorkOrderRouteKey, //queue name contains route key
		rabbitnamings.L4RouteBase, //exchange name e.g. "amq.topic" for mqtt
		route)
	if err != nil {
		return nil, err
	}

	//subscribe to operations/*/addedworkorder
	route = rabbitnamings.L3RouteBase + ".*." + rabbitnamings.WorkOrderAddedToOperationsRouteKey
	asyncsrv.L3AddedWoCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.WorkOrderAddedToOperationsRouteKey, //queue name contains route key
		rabbitnamings.L3RouteBase, //exchange name e.g. "amq.topic" for mqtt
		route)

	//subscribe to operations/*/deletedworkorder
	route = rabbitnamings.L3RouteBase + ".*." + rabbitnamings.WorkOrderDeletedFromOperationsRouteKey
	asyncsrv.L3DeletedWoCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.WorkOrderDeletedFromOperationsRouteKey, //queue name contains route key
		rabbitnamings.L3RouteBase, //exchange name e.g. "amq.topic" for mqtt
		route)
	if err != nil {
		return nil, err
	}

	//subscribe to operations/*/workorderprogress
	route = rabbitnamings.L3RouteBase + ".*." + rabbitnamings.WorkOrderProgressRouteKey
	asyncsrv.L3WorkItemCh, err = amqpWrap.InitAMQPConsumer(
		os.Getenv("SERVICE_ID")+rabbitnamings.WorkOrderProgressRouteKey, //queue name contains route key
		rabbitnamings.L3RouteBase, //exchange name e.g. "amq.topic" for mqtt
		route)
	if err != nil {
		return nil, err
	}

	asyncsrv.connClosedCh = amqpWrap.GetConnCloseChan()
	asyncsrv.subCloseCh = amqpWrap.GetSubCloseChan()
	asyncsrv.subCancelCh = amqpWrap.GetSubCancelChan()

	return &asyncsrv, nil
}

//DeInit closes connection
func (as *Asyncsrv) DeInit() {
	as.stopCh <- true
}

//Asyncsrv is the main loop for consuming incoming async messages
func (as *Asyncsrv) Asyncsrv() error {

	for {
		select {
		//new scrane status event
		case delivery := <-as.L2StatusCh:
			rstatus := models.StackingCraneStatusBase{}
			err := json.Unmarshal(delivery.Body, &rstatus)
			if err != nil {
				log.WithError(err).Errorln("unmarshal crane status failed for delivery", delivery)
			} else {
				// log.Println("received crane status:", rstatus)
				err = as.controller.NotifyResourceStatus(rstatus)
				if err != nil {
					log.WithError(err).Error("error in NotifyResourceStatus")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//truck status
		case delivery := <-as.L4TruckStatusCh:
			ts := models.HtStatus{}
			err := json.Unmarshal(delivery.Body, &ts)
			if err != nil {
				log.WithError(err).Errorln("unmarshal truck status failed for delivery", delivery)
			} else {
				log.Debugln("received truck status:", ts)
				err = as.controller.NotifyTruckStatus(ts)
				if err != nil {
					log.WithError(err).Error("error in NotifyTruckStatus")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//stacking field status
		case delivery := <-as.L3StFieldStCh:
			stStat := models.StackingFieldStatus{}
			err := json.Unmarshal(delivery.Body, &stStat)
			if err != nil {
				log.WithError(err).Errorln("unmarshal stacking field status failed for delivery", delivery)
			} else {
				log.Debugln("received stacking field status:", stStat)
				err = as.controller.NotifyStFieldStatus(stStat)
				if err != nil {
					log.WithError(err).Error("error in NotifyStFieldStatus")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//transfer field status
		case delivery := <-as.L3TrFieldStCh:
			trStat := models.TransferFieldStatus{}
			err := json.Unmarshal(delivery.Body, &trStat)
			if err != nil {
				log.WithError(err).Errorln("unmarshal transfer field status failed for delivery", delivery)
			} else {
				log.Debugln("received transfer field status:", trStat)
				err = as.controller.NotifyTrFieldStatus(trStat)
				if err != nil {
					log.WithError(err).Error("error in NotifyTrFieldStatus")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//created work order from tosgw
		case delivery := <-as.L4CreatedWorkOrderCh:
			wo := models.OperationsWorkOrderBase{}
			err := json.Unmarshal(delivery.Body, &wo)
			if err != nil {
				log.WithError(err).Errorln("unmarshal work order failed for delivery", delivery)
			} else {
				log.Debugln("received work order:", wo)
				err = as.controller.NotifyCreatedWorkOrderRequest(wo)
				if err != nil {
					log.WithError(err).Error("error in NotifyCreatedWorkOrderRequest")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//removed work order from tosgw
		case delivery := <-as.L4RemovedWorkOrderCh:
			var workOrderName string
			err := json.Unmarshal(delivery.Body, &workOrderName)
			if err != nil {
				log.WithError(err).Errorln("unmarshal work order failed for delivery", delivery)
			} else {
				log.Debugln("deleted work order:", workOrderName)
				err = as.controller.NotifyRemovedWorkOrderRequest(workOrderName)
				if err != nil {
					log.WithError(err).Error("error in NotifyRemovedWorkOrderRequest")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//added work order to operations
		case delivery := <-as.L3AddedWoCh:
			wo := models.OperationsWorkOrderBase{}
			err := json.Unmarshal(delivery.Body, &wo)
			if err != nil {
				log.WithError(err).Errorln("unmarshal work order failed for delivery", delivery)
			} else {
				log.Debugln("received work order:", wo)
				err = as.controller.NotifyAddedWorkOrderToOperationsRequest(wo)
				if err != nil {
					log.WithError(err).Error("error in NotifyAddedWorkOrderToOperationsRequest")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//deleted work order from operations
		case delivery := <-as.L3DeletedWoCh:
			wo := models.OperationsWorkOrderBase{}
			err := json.Unmarshal(delivery.Body, &wo)
			if err != nil {
				log.WithError(err).Errorln("unmarshal work order failed for delivery", delivery)
			} else {
				log.Debugln("received work order:", wo)
				err = as.controller.NotifyDeletedWorkOrderFromOperationsRequest(wo)
				if err != nil {
					log.WithError(err).Error("error in NotifyDeletedWorkOrderFromOperationsRequest")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		//work order progoress from operations
		case delivery := <-as.L3WorkItemCh:
			wis := models.OperationsWorkOrderItemStatusBase{}
			err := json.Unmarshal(delivery.Body, &wis)
			if err != nil {
				log.WithError(err).Errorln("unmarshal work item status failed for delivery", delivery)
			} else {
				log.Debugln("received work item status:", wis)
				err = as.controller.NotifyWorkItemRequest(wis)
				if err != nil {
					log.WithError(err).Error("error in NotifyWorkItemRequest")
				}
			}
			if !amqpwrap.AutoAck {
				delivery.Ack(false)
			}

		case <-as.stopCh:
			log.Warningln("received stop", as.blockID)
			return nil

		case err := <-as.connClosedCh:
			if err != nil {
				log.WithError(err).Errorf("connection error. Shutting down %s", as.blockID)
			} else {
				log.Warningf("connection closed %s. Shutting down", as.blockID)
			}
			panic(fmt.Errorf("connection closed %s", as.blockID))

		case err := <-as.subCloseCh:
			log.WithError(err).Errorf("channel error. Shutting down %s", as.blockID)
			panic(fmt.Errorf("channel closed %s", as.blockID))

		case msg := <-as.subCancelCh:
			log.Errorf("channel canceled. Shutting down %s", msg)
			panic(fmt.Errorf("channel canceled %s", as.blockID))

		}
	}
}
