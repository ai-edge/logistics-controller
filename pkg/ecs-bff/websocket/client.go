package websocket

import (
	"ecs/api/generated/models"
	"encoding/json"
	"strings"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
)

//Client is used when a new client connection is established
type Client struct {
	ID         int32
	Conn       *websocket.Conn
	Pool       *Pool
	Data       *ConfigurationData
	Controller controllerInterface
}

//Message struct that is sent to the client
type Message struct {
	Type string `json:"type"`
	Body string `json:"body"`
}

//ConfigurationData for keeping required data for ui
type ConfigurationData struct {
	Cranes               map[string]models.StackingCrane
	CraneStatuses        map[string]models.StackingCraneStatusBase
	CreatedWorkOrders    map[string]models.OperationsWorkOrderBase
	OperationsWorkOrders map[string]models.OperationsWorkOrderBase
	TruckStatuses        map[string]models.HtStatus
	StackingFields       []models.StackingField
	TransferFields       []models.TransferField
	StFieldStatuses      map[string]models.StackingFieldStatus
	TrFieldStatuses      map[string]models.TransferFieldStatus
	Block                *models.ContainerBlockComposite
	FieldData            models.FieldData
}

type controllerInterface interface {
	CreateDevice(string) string
	DeleteDevice(string) string
	CreateWorkOrder([]byte) error
	PutWorkOrder(wo models.OperationsWorkOrderBase) string
	DeleteCreatedWorkOrder(string) string
	DeleteWorkOrder(string) string
	DeleteContainer(string) string
	AddContainer(models.Container) string
}

//Read will constantly listen in for new messages coming through on this Client's websocket connection.
func (c *Client) Read() {
	defer func() {
		c.Pool.Unregister <- c
		c.Conn.Close()
	}()

	for {
		_, p, err := c.Conn.ReadMessage()
		if err != nil {
			log.WithError(err).Error("error in reading message")
			return
		}
		message := Message{}
		s := string(p)
		sList := strings.Split(s, ";")
		if len(sList) != 2 {
			log.WithError(err).Error("error in message format")
			return
		}
		message.Type = sList[0]
		message.Body = sList[1]

		switch message.Type {
		case "disconnect":
			c.Pool.Unregister <- c
			return
		case "getDevices":
			values := []models.StackingCrane{}
			for _, value := range c.Data.Cranes {
				values = append(values, value)
			}
			err := sendMessage("setDevices", values, c)
			if err != nil {
				log.WithError(err).Error("error in sendMessage setDevices")
			}
		case "getDeviceStatuses":
			values := []models.StackingCraneStatusBase{}
			for _, value := range c.Data.CraneStatuses {
				values = append(values, value)
			}
			err := sendMessage("setDeviceStatuses", values, c)
			if err != nil {
				log.WithError(err).Error("error in sendMessage setDeviceStatuses")
			}
		case "getStackingFields":
			values := []models.StackingField{}
			for _, value := range c.Data.StackingFields {
				values = append(values, value)
			}
			err := sendMessage("setStackingFields", values, c)
			if err != nil {
				log.WithError(err).Error("error in sendMessage setStackingFields")
			}
		case "getTransferFields":
			values := []models.TransferField{}
			for _, value := range c.Data.TransferFields {
				values = append(values, value)
			}
			err := sendMessage("setTransferFields", values, c)
			if err != nil {
				log.WithError(err).Error("error in sendMessage setTransferFields")
			}
		case "getStackingFieldStatuses":
			values := []models.StackingFieldStatus{}
			for _, value := range c.Data.StFieldStatuses {
				if value.ItemCount > 0 {
					values = append(values, value)
				}
			}
			err := sendMessage("setStackingFieldStatuses", values, c)
			if err != nil {
				log.WithError(err).Error("error in sendMessage setStackingFieldStatuses")
			}
		case "getTransferFieldStatuses":
			values := []models.TransferFieldStatus{}
			for _, value := range c.Data.TrFieldStatuses {
				if value.ItemCount > 0 {
					values = append(values, value)
				}
			}
			err := sendMessage("setTransferFieldStatuses", values, c)
			if err != nil {
				log.WithError(err).Error("error in sendMessage setTransferFieldStatuses")
			}
		case "getTruckStatuses":
			values := []models.HtStatus{}
			for _, value := range c.Data.TruckStatuses {
				values = append(values, value)
			}
			err := sendMessage("setTruckStatuses", values, c)
			if err != nil {
				log.WithError(err).Error("error in sendMessage setTruckStatuses")
			}
		case "getWorkOrders":
			values := []models.OperationsWorkOrderBase{}
			for _, value := range c.Data.OperationsWorkOrders {
				values = append(values, value)
			}
			err := sendMessage("setWorkOrders", values, c)
			if err != nil {
				log.WithError(err).Error("error in sendMessage setWorkOrders")
			}
		case "getCreatedWorkOrders":
			values := []models.OperationsWorkOrderBase{}
			for _, value := range c.Data.CreatedWorkOrders {
				values = append(values, value)
			}
			err := sendMessage("setCreatedWorkOrders", values, c)
			if err != nil {
				log.WithError(err).Error("error in sendMessage setCreatedWorkOrders")
			}
		case "getFieldData":
			err := sendMessage("setFieldData", c.Data.FieldData, c)
			if err != nil {
				log.WithError(err).Error("error in sendMessage setFieldData")
			}
		case "deleteWorkOrder":
			c.Controller.DeleteWorkOrder(message.Body)
		case "putWorkOrder":
			wo := models.OperationsWorkOrderBase{}
			err = json.Unmarshal([]byte(message.Body), &wo)
			if err == nil {
				c.Controller.PutWorkOrder(wo)
			}
		case "createWorkOrder":
			log.Debugln(message.Body)
			err = c.Controller.CreateWorkOrder([]byte(message.Body))
			if err != nil {
				err = sendMessage("errorMessage", "Error in creating work order", c)
			} else {
				err = sendMessage("successMessage", "Create Work Order is successful", c)
			}
			if err != nil {
				log.WithError(err).Error("error in sending error message")
			}
		case "deleteCreatedWorkOrder":
			c.Controller.DeleteCreatedWorkOrder(message.Body)
		case "deleteDevice":
			c.Controller.DeleteDevice(message.Body)
		case "deleteContainer":
			c.Controller.DeleteContainer(message.Body)
		case "addContainer":
			container := models.Container{}
			err = json.Unmarshal([]byte(message.Body), &container)
			if err == nil {
				c.Controller.AddContainer(container)
			}
		case "createDevice":
			c.Controller.CreateDevice(message.Body)
		default:
			log.Debugln("unhandled message type ", message.Body)
		}
		log.Debugf("Message Received: %+v\n", message)
	}
}

func sendMessage(messageType string, values interface{}, c *Client) error {
	b, err := json.Marshal(values)
	if err != nil {
		return err
	}

	err = c.Conn.WriteJSON(Message{Type: messageType, Body: string(b)})
	if err != nil {
		return err
	}
	return nil
}
