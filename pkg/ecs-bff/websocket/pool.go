package websocket

import log "github.com/sirupsen/logrus"

//Pool contains all the of the channels we need for concurrent communication
type Pool struct {
	Register   chan *Client
	Unregister chan *Client
	Clients    map[*Client]bool
	Broadcast  chan Message
}

//NewPool is created when a new client connects
func NewPool() *Pool {
	return &Pool{
		Register:   make(chan *Client),
		Unregister: make(chan *Client),
		Clients:    make(map[*Client]bool),
		Broadcast:  make(chan Message),
	}
}

//Start constantly listens for anything that passed to any of the Pool's channel
func (pool *Pool) Start() {
	for {
		select {
		case client := <-pool.Register:
			pool.Clients[client] = true
			log.Debugf("A new client is registered. Client id: %d Size of Connection Pool: %d", client.ID, len(pool.Clients))
			break
		case client := <-pool.Unregister:
			delete(pool.Clients, client)
			log.Debugf("Client is unregistered. Client id: %d Size of Connection Pool: %d", client.ID, len(pool.Clients))
			break
		case message := <-pool.Broadcast:
			log.Debugln("Sending message to all clients in Pool")
			for client := range pool.Clients {
				if err := client.Conn.WriteJSON(message); err != nil {
					log.WithError(err).Error("Error in broadcasting message to the client")
					return
				}
			}
		}
	}
}
