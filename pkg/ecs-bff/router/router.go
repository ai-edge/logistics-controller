package router

import (
	"ecs/pkg/ecs-bff/controller"
	"net/http"

	"github.com/gorilla/mux"
)

// Router is exported and used in main.go
func Router(controller *controller.Controller) *mux.Router {

	router := mux.NewRouter()
	router.Use(commonMiddleware)

	// router.HandleFunc("/devices", controller.GetDeviceList).Methods(http.MethodGet)
	// router.HandleFunc("/device/{id}", controller.GetDevice).Methods(http.MethodGet)
	// router.HandleFunc("/device", controller.CreateDevice).Methods(http.MethodPost)
	// router.HandleFunc("/device/{id}", controller.DeleteDevice).Methods(http.MethodDelete)
	// router.HandleFunc("/devicestatuses", controller.GetDeviceStatusList).Methods(http.MethodGet)
	// router.HandleFunc("/devicestatus/{id}", controller.GetDeviceStatus).Methods(http.MethodGet)
	// router.HandleFunc("/workorders/creator", controller.CreateWorkOrder).Methods(http.MethodPost)
	// router.HandleFunc("/workorders/creator/{id}", controller.GetCreatedWorkOrder).Methods(http.MethodGet)
	// router.HandleFunc("/workorders/delete/{id}", controller.DeleteCreatedWorkOrder).Methods(http.MethodGet)
	// router.HandleFunc("/workorders/creator", controller.GetCreatedWorkOrders).Methods(http.MethodGet)
	// router.HandleFunc("/workorders/{id}", controller.PutWorkOrder).Methods(http.MethodPost)
	// router.HandleFunc("/workorders/{id}", controller.DeleteWorkOrder).Methods(http.MethodGet)
	// router.HandleFunc("/workorders", controller.GetWorkOrders).Methods(http.MethodGet)
	// router.HandleFunc("/workitems", controller.GetWorkItemList).Methods(http.MethodGet)
	// router.HandleFunc("/stackingfields", controller.GetStackingFields).Methods(http.MethodGet)
	// router.HandleFunc("/transferfields", controller.GetTransferFields).Methods(http.MethodGet)
	// router.HandleFunc("/stackingfieldstatuses", controller.GetStackingFieldStatuses).Methods(http.MethodGet)
	// router.HandleFunc("/transferfieldstatuses", controller.GetTransferFieldStatuses).Methods(http.MethodGet)
	// router.HandleFunc("/containers/{id}", controller.DeleteContainer).Methods(http.MethodGet)
	// router.HandleFunc("/containers/{id}", controller.AddContainer).Methods(http.MethodPost)
	// router.HandleFunc("/fielddata", controller.GetFieldData).Methods(http.MethodGet)
	// router.HandleFunc("/ctruckstatuses", controller.GetTruckStatuses).Methods(http.MethodGet)

	return router
}

func commonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
