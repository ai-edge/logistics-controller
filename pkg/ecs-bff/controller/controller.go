package controller

import (
	"bytes"
	"ecs/api/generated/models"
	"ecs/pkg/ecs-bff/asyncsrv"
	"ecs/pkg/ecs-bff/websocket"
	"ecs/tools/amqpwrap"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// Controller context for keeping the URLs, async data and other configuration information.
type Controller struct {
	operationsURL    string
	tosgwURL         string
	deviceManagerURL string
	blockID          string
	client           *http.Client
	as               *asyncsrv.Asyncsrv
	amqpWrap         *amqpwrap.AMQPWrapper
	data             *websocket.ConfigurationData
	pool             *websocket.Pool
}

/****************************************Websocket******************************************/

//ServeWs defines WebSocket endpoint
func ServeWs(client *websocket.Client, pool *websocket.Pool, w http.ResponseWriter, r *http.Request) {
	log.Debugln("WebSocket Endpoint Hit")
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
	}
	client.Conn = conn
	pool.Register <- client
	client.Pool = pool
	client.Read()
}

func (controller *Controller) setupRoutes(data *websocket.ConfigurationData) {
	controller.pool = websocket.NewPool()
	go controller.pool.Start()

	client := &websocket.Client{
		Data:       data,
		Controller: controller,
		ID:         int32(time.Now().Unix()),
	}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		ServeWs(client, controller.pool, w, r)
	})
}

/****************************************Websocket******************************************/

// NewController creates a controller with specific URL's
func NewController(conn *amqp.Connection, operationsURL string, tosgwURL string, deviceManagerURL string, caCertPath string) (*Controller, error) {
	var client = http.DefaultClient
	if caCertPath != "" {
		caCert, err := ioutil.ReadFile(caCertPath)
		if err != nil {
			log.Fatal(err)
		}
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)

		client = &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					RootCAs: caCertPool,
				},
			},
		}
	} else {
		log.Debugf("using TLS termination inside cluster")
	}

	data := &websocket.ConfigurationData{
		Cranes:               make(map[string]models.StackingCrane),
		CraneStatuses:        make(map[string]models.StackingCraneStatusBase),
		CreatedWorkOrders:    make(map[string]models.OperationsWorkOrderBase),
		OperationsWorkOrders: make(map[string]models.OperationsWorkOrderBase),
		TruckStatuses:        make(map[string]models.HtStatus),
		StFieldStatuses:      make(map[string]models.StackingFieldStatus),
		TrFieldStatuses:      make(map[string]models.TransferFieldStatus),
		Block:                nil,
		FieldData: models.FieldData{
			MaxStackingBayCount: 0,
			MaxStackingRowCount: 0,
			MaxTransferBayCount: 0,
			MaxTransferRowCount: 0,
		},
	}

	controller := &Controller{
		operationsURL:    operationsURL,
		tosgwURL:         tosgwURL,
		deviceManagerURL: deviceManagerURL,
		client:           client,
		blockID:          "BL01",
		data:             data,
	}

	//init AMQP wrapper
	wrap, err := amqpwrap.NewAMQPWrapper(conn)
	if err != nil {
		log.WithError(err).Error("Could not create amqp wrapper")
		return nil, fmt.Errorf("Could not create amqp wrapper: %w", err)
	}
	//different channels for publish and subscribe
	err = wrap.InitPubChannel()
	if err != nil {
		log.WithError(err).Error("Init channel failed")
		return nil, fmt.Errorf("Init channel failed: %w", err)
	}
	err = wrap.InitSubChannel()
	if err != nil {
		log.WithError(err).Error("Init channel failed")
		return nil, fmt.Errorf("Init channel failed: %w", err)
	}
	controller.amqpWrap = wrap

	//Init consuming async messages
	as, err := asyncsrv.NewAsyncsrv(controller.blockID, controller, wrap)
	if err != nil {
		log.WithError(err).Error("Create asyncsrv failed")
		return nil, err
	}
	controller.as = as

	go as.Asyncsrv()
	controller.InitializeNotifiedFields()
	controller.setupRoutes(data)

	return controller, err
}

//DeInit close dispatcher
func (controller *Controller) DeInit() {
	controller.as.DeInit()
	controller.amqpWrap.DeInit()
}

//InitializeNotifiedFields gets the initial information from related services
func (controller *Controller) InitializeNotifiedFields() error {
	if controller.InitializeTosgwNotifiedFields() != nil {
		go func() {
			for {
				select {
				case <-time.After(5 * time.Minute): //timeout Handler
					if controller.InitializeTosgwNotifiedFields() == nil {
						break
					}
				}
			}
		}()
	}

	if controller.InitializeOperationsNotifiedFields() != nil {
		go func() {
			for {
				select {
				case <-time.After(5 * time.Minute): //timeout Handler
					if controller.InitializeOperationsNotifiedFields() == nil {
						break
					}
				}
			}
		}()
	}

	return nil
}

//InitializeTosgwNotifiedFields gets the initial information from tosgw
func (controller *Controller) InitializeTosgwNotifiedFields() error {
	return controller.UpdateCreatedWorkOrders()
}

//InitializeOperationsNotifiedFields gets the initial information from operations
func (controller *Controller) InitializeOperationsNotifiedFields() error {
	err := controller.UpdateOperationsWorkOrders()
	if err != nil {
		return fmt.Errorf("Error in UpdateOperationsWorkOrders: %w", err)
	}
	err = controller.UpdateWorkItems()
	if err != nil {
		return fmt.Errorf("Error in UpdateWorkItems: %w", err)
	}
	err = controller.UpdateTruckStatuses()
	if err != nil {
		return fmt.Errorf("Error in UpdateTruckStatuses: %w", err)
	}
	err = controller.UpdateStFieldStatuses()
	if err != nil {
		return fmt.Errorf("Error in UpdateStFieldStatuses: %w", err)
	}
	err = controller.UpdateTrFieldStatuses()
	if err != nil {
		return fmt.Errorf("Error in UpdateTrFieldStatuses: %w", err)
	}
	err = controller.UpdateDevices()
	if err != nil {
		return fmt.Errorf("Error in UpdateDevices: %w", err)
	}
	err = controller.UpdateStackingFields()
	if err != nil {
		return fmt.Errorf("Error in UpdateStackingFields: %w", err)
	}
	err = controller.UpdateTransferFields()
	if err != nil {
		return fmt.Errorf("Error in UpdateTransferFields: %w", err)
	}
	err = controller.UpdateBlock()
	if err != nil {
		return fmt.Errorf("Error in UpdateBlock: %w", err)
	}
	return nil
}

//UpdateCreatedWorkOrders get all created work orders from tosgw
func (controller *Controller) UpdateCreatedWorkOrders() error {
	req, err := http.NewRequest(http.MethodGet, controller.tosgwURL+"/workorders/creator", nil)
	if err != nil {
		log.WithError(err).Error("new request failed")
		return fmt.Errorf("new request failed %w", err)
	}

	req.URL.RawQuery = "limit=100"
	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		log.WithError(err).Error("send request failed")
		return fmt.Errorf("send request failed %w", err)
	}

	wos := []models.OperationsWorkOrderBase{}
	err = json.Unmarshal(body, &wos)

	if err != nil {
		return nil
	}

	controller.data.CreatedWorkOrders = make(map[string]models.OperationsWorkOrderBase)
	for _, wo := range wos {
		controller.data.CreatedWorkOrders[wo.WorkOrderName] = wo
	}

	return nil
}

//UpdateOperationsWorkOrders get all work orders from operations services
func (controller *Controller) UpdateOperationsWorkOrders() error {
	req, err := http.NewRequest(http.MethodGet, controller.operationsURL+"/workorders", nil)
	if err != nil {
		log.WithError(err).Error("new request failed")
		return fmt.Errorf("new request failed %w", err)
	}

	req.URL.RawQuery = "limit=100"
	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		log.WithError(err).Error("send request failed")
		return fmt.Errorf("send request failed %w", err)
	}

	wos := []models.OperationsWorkOrderBase{}
	err = json.Unmarshal(body, &wos)

	if err != nil {
		return nil
	}

	controller.data.OperationsWorkOrders = make(map[string]models.OperationsWorkOrderBase)
	for _, wo := range wos {
		controller.data.OperationsWorkOrders[wo.WorkOrderName] = wo
	}

	return nil
}

//UpdateWorkItems get all work items from operations service
func (controller *Controller) UpdateWorkItems() error {
	req, err := http.NewRequest(http.MethodGet, controller.operationsURL+"/workorderitemstatuses", nil)
	if err != nil {
		log.WithError(err).Error("new request failed")
		return fmt.Errorf("new request failed %w", err)
	}

	req.URL.RawQuery = "limit=100"
	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		log.WithError(err).Error("send request failed")
		return fmt.Errorf("send request failed %w", err)
	}

	wis := []models.OperationsWorkOrderItemStatusBase{}
	err = json.Unmarshal(body, &wis)

	if err != nil {
		return nil
	}

	for _, wi := range wis {
		controller.UpdateWorkOrderWithItemData(wi)
	}

	return nil
}

//UpdateWorkOrderWithItemData updates the status of the workitem inside the workorder
func (controller *Controller) UpdateWorkOrderWithItemData(wi models.OperationsWorkOrderItemStatusBase) {
	for woInx, wo := range controller.data.OperationsWorkOrders {
		if wi.WorkOrderReference == wo.WorkOrderName {
			for wiInx, wowi := range wo.OperationsWorkOrderItems {
				if wi.WorkItemName == wowi.WorkItemName {
					controller.data.OperationsWorkOrders[woInx].OperationsWorkOrderItems[wiInx].ItemStatus = wi.ItemStatus
					controller.broadcastMessage("updateWorkOrder", controller.data.OperationsWorkOrders[woInx])
					break
				}
			}
		}
	}
}

//UpdateTruckStatuses gets all truck statuses from operations service
func (controller *Controller) UpdateTruckStatuses() error {
	req, err := http.NewRequest(http.MethodGet, controller.operationsURL+"/ctruckstatuses", nil)
	if err != nil {
		log.WithError(err).Error("new request failed")
		return fmt.Errorf("new request failed %w", err)
	}

	req.URL.RawQuery = "limit=100"
	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		log.WithError(err).Error("send request failed")
		return fmt.Errorf("send request failed %w", err)
	}

	trStats := []models.HtStatus{}
	err = json.Unmarshal(body, &trStats)

	if err != nil {
		return nil
	}

	controller.data.TruckStatuses = make(map[string]models.HtStatus)
	for _, trStat := range trStats {
		controller.data.TruckStatuses[trStat.HtID] = trStat
	}

	return nil
}

//UpdateStFieldStatuses gets all stacking field statuses from operations service
func (controller *Controller) UpdateStFieldStatuses() error {
	req, err := http.NewRequest(http.MethodGet, controller.operationsURL+"/stackingfieldstatuses", nil)
	if err != nil {
		log.WithError(err).Error("new request failed")
		return fmt.Errorf("new request failed %w", err)
	}

	req.URL.RawQuery = "limit=100"
	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		log.WithError(err).Error("send request failed")
		return fmt.Errorf("send request failed %w", err)
	}

	stFields := []models.StackingFieldStatus{}
	err = json.Unmarshal(body, &stFields)

	if err != nil {
		return nil
	}

	controller.data.StFieldStatuses = make(map[string]models.StackingFieldStatus)
	for _, field := range stFields {
		controller.data.StFieldStatuses[field.FieldNameComposite] = field
	}

	return nil
}

//UpdateTrFieldStatuses gets all transfer field statuses from operations service
func (controller *Controller) UpdateTrFieldStatuses() error {
	req, err := http.NewRequest(http.MethodGet, controller.operationsURL+"/transferfieldstatuses", nil)
	if err != nil {
		log.WithError(err).Error("new request failed")
		return fmt.Errorf("new request failed %w", err)
	}

	req.URL.RawQuery = "limit=100"
	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		log.WithError(err).Error("send request failed")
		return fmt.Errorf("send request failed %w", err)
	}

	trFields := []models.TransferFieldStatus{}
	err = json.Unmarshal(body, &trFields)

	if err != nil {
		return nil
	}

	controller.data.TrFieldStatuses = make(map[string]models.TransferFieldStatus)
	for _, field := range trFields {
		controller.data.TrFieldStatuses[field.FieldNameComposite] = field
	}

	return nil
}

// UpdateDevices fills the crane list in the controller structure
func (controller *Controller) UpdateDevices() error {
	req, err := http.NewRequest(http.MethodGet, controller.operationsURL+"/scranes", nil)
	if err != nil {
		log.WithError(err).Error("new request failed")
		return fmt.Errorf("new request failed %w", err)
	}

	req.URL.RawQuery = "limit=100"
	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		log.WithError(err).Error("send request failed")
		return fmt.Errorf("send request failed %w", err)
	}

	cranes := []models.StackingCrane{}
	err = json.Unmarshal(body, &cranes)

	if err != nil {
		return err
	}

	controller.data.Cranes = make(map[string]models.StackingCrane)
	for _, crane := range cranes {
		controller.data.Cranes[crane.ResourceID] = crane
	}

	return nil
}

// UpdateStackingFields fills the stacking fields list in the controller structure
func (controller *Controller) UpdateStackingFields() error {
	req, err := http.NewRequest(http.MethodGet, controller.operationsURL+"/stackingfields", nil)
	if err != nil {
		log.WithError(err).Error("new request failed")
		return fmt.Errorf("new request failed %w", err)
	}

	req.URL.RawQuery = "limit=100"
	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		log.WithError(err).Error("send request failed")
		return fmt.Errorf("send request failed %w", err)
	}

	err = json.Unmarshal(body, &controller.data.StackingFields)

	for _, field := range controller.data.StackingFields {
		if field.FieldBase.Bay == "001" {
			row, _ := strconv.ParseInt(field.FieldBase.Row, 10, 64)
			if int32(row) > controller.data.FieldData.MaxStackingRowCount {
				controller.data.FieldData.MaxStackingRowCount = int32(row)
			}
		}
		if field.FieldBase.Row == "001" {
			bay, _ := strconv.ParseInt(field.FieldBase.Bay, 10, 64)
			if int32(bay) > controller.data.FieldData.MaxStackingBayCount {
				controller.data.FieldData.MaxStackingBayCount = int32(bay)
			}
		}
	}

	if err != nil {
		return err
	}

	return nil
}

// UpdateTransferFields fills the transfer fields list in the controller structure
func (controller *Controller) UpdateTransferFields() error {
	req, err := http.NewRequest(http.MethodGet, controller.operationsURL+"/transferfields", nil)
	if err != nil {
		log.WithError(err).Error("new request failed")
		return fmt.Errorf("new request failed %w", err)
	}

	req.URL.RawQuery = "limit=100"
	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		log.WithError(err).Error("send request failed")
		return fmt.Errorf("send request failed %w", err)
	}

	err = json.Unmarshal(body, &controller.data.TransferFields)

	if len(controller.data.TransferFields) > 0 {
		for _, field := range controller.data.TransferFields {
			if field.FieldBase.Bay == "001" {
				row, _ := strconv.ParseInt(field.FieldBase.Row, 10, 64)
				if int32(row) > controller.data.FieldData.MaxTransferRowCount {
					controller.data.FieldData.MaxTransferRowCount = int32(row)
				}
			}
			if field.FieldBase.Row == "001" {
				bay, _ := strconv.ParseInt(field.FieldBase.Bay, 10, 64)
				if int32(bay) > controller.data.FieldData.MaxTransferBayCount {
					controller.data.FieldData.MaxTransferBayCount = int32(bay)
				}
			}
		}
	}

	if err != nil {
		return err
	}

	return nil
}

//UpdateBlock fills the block data in the controller structure
func (controller *Controller) UpdateBlock() error {
	req, err := http.NewRequest(http.MethodGet, controller.operationsURL+"/setup/blocks/"+controller.blockID, nil)
	if err != nil {
		log.WithError(err).Error("new request failed")
		return fmt.Errorf("new request failed %w", err)
	}

	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		log.WithError(err).Error("send request failed")
		return fmt.Errorf("send request failed %w", err)
	}

	err = json.Unmarshal(body, &controller.data.Block)
	if err == nil {
		controller.data.FieldData.MaxSATierCount = controller.data.Block.MaxSATierCount
		controller.data.FieldData.MaxTZTierCount = controller.data.Block.MaxTZTierCount
		controller.data.FieldData.StackingAreaNames = controller.data.Block.StackingAreaNames
		controller.data.FieldData.TransferZoneNames = controller.data.Block.TransferZoneNames
	}
	return err
}

// CreateDevice create a single device
func (controller *Controller) CreateDevice(deviceID string) string {
	req, err := http.NewRequest(http.MethodPost, controller.deviceManagerURL+deviceID, nil)
	if err != nil {
		return ""
	}

	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		return ""
	}

	crane := models.StackingCrane{}
	err = json.Unmarshal(body, &crane)
	if err != nil {
		return ""
	}

	controller.broadcastMessage("createdDevice", crane)
	controller.UpdateDevices()
	return crane.ResourceID
}

// DeleteDevice deletes single device
func (controller *Controller) DeleteDevice(deviceID string) string {
	req, err := http.NewRequest(http.MethodDelete, controller.deviceManagerURL+"/scranes/"+deviceID, nil)
	if err != nil {
		return ""
	}

	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		return ""
	}

	crane := models.StackingCrane{}
	err = json.Unmarshal(body, &crane)
	if err != nil {
		return ""
	}
	controller.UpdateDevices()
	return crane.ResourceID
}

// CreateWorkOrder forwards create work order request
func (controller *Controller) CreateWorkOrder(body []byte) error {
	req, err := http.NewRequest(http.MethodPost, controller.tosgwURL+"/workorders/creator", bytes.NewBuffer(body))
	if err != nil {
		log.WithError(err).Error("new request failed")
		return fmt.Errorf("new request failed %w", err)
	}

	body, stCode, err := controller.sendRequest(req)
	if err == nil && stCode != 200 {
		log.WithError(err).Error("send request failed")
		return fmt.Errorf("send request failed %w", err)
	}

	return nil
}

// DeleteCreatedWorkOrder deletes created work order from TOS with the given id
func (controller *Controller) DeleteCreatedWorkOrder(woName string) string {
	req, err := http.NewRequest(http.MethodDelete, controller.tosgwURL+"/workorders/creator/"+woName, nil)
	if err != nil {
		return ""
	}

	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		return ""
	}
	retVal := ""
	err = json.Unmarshal(body, &retVal)
	if err != nil {
		return ""
	}
	return retVal
}

// PutWorkOrder creates or updates a work order
func (controller *Controller) PutWorkOrder(wo models.OperationsWorkOrderBase) string {
	body, err := json.Marshal(wo)
	if err != nil {
		return ""
	}

	req, err := http.NewRequest(http.MethodPut, controller.tosgwURL+"/workorders/"+wo.WorkOrderName, bytes.NewBuffer(body))
	if err != nil {
		return ""
	}

	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		return ""
	}
	retVal := ""
	err = json.Unmarshal(body, &retVal)
	if err != nil {
		return ""
	}
	return retVal
}

// DeleteWorkOrder deletes the selected work order
func (controller *Controller) DeleteWorkOrder(woName string) string {
	req, err := http.NewRequest(http.MethodDelete, controller.operationsURL+"/workorders/"+woName, nil)
	if err != nil {
		return ""
	}

	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		return ""
	}
	retVal := ""
	err = json.Unmarshal(body, &retVal)
	if err != nil {
		return ""
	}
	return retVal
}

// DeleteContainer deletes the selected container
func (controller *Controller) DeleteContainer(containerID string) string {
	req, err := http.NewRequest(http.MethodDelete, controller.operationsURL+"/containers/"+containerID, nil)
	if err != nil {
		return ""
	}

	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		return ""
	}

	retVal := ""
	err = json.Unmarshal(body, &retVal)
	if err != nil {
		return ""
	}
	return retVal
}

// AddContainer adds a container on the given field
func (controller *Controller) AddContainer(container models.Container) string {
	body, err := json.Marshal(container)
	if err != nil {
		return ""
	}
	req, err := http.NewRequest(http.MethodPut, controller.operationsURL+"/containers/"+container.ItemGUID, bytes.NewBuffer(body))
	if err != nil {
		return ""
	}

	body, stCode, err := controller.sendRequest(req)
	if err != nil || stCode != 200 {
		return ""
	}
	retVal := ""
	err = json.Unmarshal(body, &retVal)
	if err != nil {
		return ""
	}
	return retVal
}

func internalServerError(response http.ResponseWriter, statusCode int, err error) {
	response.WriteHeader(statusCode)
	if err != nil {
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
	}
}

func (controller *Controller) sendRequest(req *http.Request) ([]byte, int, error) {
	resp, err := controller.client.Do(req)
	if err != nil {
		return nil, 500, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		log.Debugln(resp)
	}
	if err != nil {
		return nil, resp.StatusCode, err
	}
	return body, resp.StatusCode, err
}

/****************************************Command and Event Handlers******************************************/

//NotifyResourceStatus notifies a newly received crane status
func (controller *Controller) NotifyResourceStatus(st models.StackingCraneStatusBase) error {
	if !compareCranePositions(controller.data.CraneStatuses[st.StackingCraneID].CraneMainAxesPosition, st.CraneMainAxesPosition) {
		controller.broadcastMessage("updateDeviceStatus", st)
	}
	controller.data.CraneStatuses[st.StackingCraneID] = st
	return nil
}

//NotifyStFieldStatus handles a new stacking field status
func (controller *Controller) NotifyStFieldStatus(stField models.StackingFieldStatus) error {
	controller.data.StFieldStatuses[stField.FieldNameComposite] = stField
	broadcastMessage := "updateStackingFieldStatus"
	if stField.FieldStatus == models.SOCCUPIED && stField.ItemCount <= 0 {
		broadcastMessage = "deleteStackingFieldStatus"
	}
	controller.broadcastMessage(broadcastMessage, stField)

	return nil
}

//NotifyTrFieldStatus handles a new transfer field status
func (controller *Controller) NotifyTrFieldStatus(trField models.TransferFieldStatus) error {
	controller.data.TrFieldStatuses[trField.FieldNameComposite] = trField
	broadcastMessage := "updateTransferFieldStatus"
	if trField.FieldStatus == models.TOCCUPIED && trField.ItemCount <= 0 || trField.FieldStatus == models.TFREE {
		broadcastMessage = "deleteTransferFieldStatus"
	}
	controller.broadcastMessage(broadcastMessage, trField)
	return nil
}

//NotifyTruckStatus handles a new truck status
func (controller *Controller) NotifyTruckStatus(ts models.HtStatus) error {
	ts.LastUpdated = time.Now()
	controller.data.TruckStatuses[ts.HtID] = ts
	controller.broadcastMessage("updateTruckStatus", ts)
	return nil
}

//NotifyCreatedWorkOrderRequest handles CreatedWorkOrderRequest
func (controller *Controller) NotifyCreatedWorkOrderRequest(wo models.OperationsWorkOrderBase) error {
	controller.data.CreatedWorkOrders[wo.WorkOrderName] = wo
	controller.broadcastMessage("addedCreatedWorkOrder", wo)
	return nil
}

//NotifyRemovedWorkOrderRequest handles RemovedWorkOrderRequest
func (controller *Controller) NotifyRemovedWorkOrderRequest(workOrderName string) error {
	if _, ok := controller.data.CreatedWorkOrders[workOrderName]; ok {
		delete(controller.data.CreatedWorkOrders, workOrderName)
	}

	controller.broadcastMessage("deletedCreatedWorkOrder", workOrderName)
	return nil
}

//NotifyAddedWorkOrderToOperationsRequest handles new work order request from operations
func (controller *Controller) NotifyAddedWorkOrderToOperationsRequest(wo models.OperationsWorkOrderBase) error {
	controller.data.OperationsWorkOrders[wo.WorkOrderName] = wo
	controller.broadcastMessage("addedWorkOrder", wo)
	return nil
}

//NotifyDeletedWorkOrderFromOperationsRequest handles deleted work order from operations
func (controller *Controller) NotifyDeletedWorkOrderFromOperationsRequest(wo models.OperationsWorkOrderBase) error {
	if _, ok := controller.data.OperationsWorkOrders[wo.WorkOrderName]; ok {
		delete(controller.data.OperationsWorkOrders, wo.WorkOrderName)
	}
	controller.broadcastMessage("deletedWorkOrder", wo.WorkOrderName)
	return nil
}

//NotifyWorkItemRequest handles work item requests
func (controller *Controller) NotifyWorkItemRequest(wis models.OperationsWorkOrderItemStatusBase) error {
	controller.UpdateWorkOrderWithItemData(wis)
	return nil
}

func (controller *Controller) broadcastMessage(messageType string, value interface{}) error {
	if controller.pool == nil || len(controller.pool.Clients) == 0 {
		return nil
	}
	b, err := json.Marshal(value)
	if err != nil {
		return err
	}

	controller.pool.Broadcast <- websocket.Message{Type: messageType, Body: string(b)}

	return nil
}

/****************************************helpers******************************************/
func compareCranePositions(pos1 models.LiftingDevicePosition, pos2 models.LiftingDevicePosition) bool {
	if pos1.GantryPosition != pos2.GantryPosition {
		return false
	}
	if pos1.TrolleyPosition != pos2.TrolleyPosition {
		return false
	}
	if pos1.HoistPosition != pos2.HoistPosition {
		return false
	}
	return true
}
