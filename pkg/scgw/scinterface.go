package scgw

import "ecs/api/generated/models"

//Scrane Simulated or real resource interface
type Scrane interface {
	SetCommand(newJobOrder models.StackingCraneJobOrderBase) error
	GetStatus() models.StackingCraneStatusBase
	UpdateNeighboringStatus(models.StackingCraneStatusBase) error
}
