//Package scgw is the root package for stacking crane gateway app.
package scgw

import (
	"ecs/api/generated/models"
	"ecs/internal/dbservice"
	"ecs/internal/rabbitnamings"
	"ecs/tools/mqttwrap"
	"encoding/json"
	"fmt"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	log "github.com/sirupsen/logrus"
)

//SCGW gateway for stacking crane
type SCGW struct {
	stackingCrane Scrane
	mqConn        *mqttwrap.MQTTConnection
	dbConn        dbservice.DBService
}

//NewSCGW initialize the gateway
func NewSCGW(sc Scrane, statusInterval time.Duration, mqR *mqttwrap.MQTTConnection, dbC dbservice.DBService) *SCGW {
	scGateway := &(SCGW{stackingCrane: sc, mqConn: mqR, dbConn: dbC})
	//start receiving jobs from topic "scgw/{craneid}/jobrequest"
	var qos = byte(1)
	scGateway.mqConn.InitConsumer(
		rabbitnamings.L2RouteBase+"/"+sc.GetStatus().StackingCraneID+"/"+rabbitnamings.JobRequestRouteKey,
		qos,
		scGateway.jobHandler)

	//start receiving status from topic "scgw/+/cranestatus"
	//not durable
	qos = byte(1)
	scGateway.mqConn.InitConsumer(
		rabbitnamings.L2RouteBase+"/+/"+rabbitnamings.CraneStatusRouteKey,
		qos,
		scGateway.statusHandler)

	//start publishing status messages
	go scGateway.startStatusPublish(statusInterval)

	return scGateway
}

//DeInit deinitialize the gateway
func (scGateway *SCGW) DeInit() {

}

//startStatusPublish publishes crane status in a given time interval
func (scGateway *SCGW) startStatusPublish(interval time.Duration) error {
	//Publish sc status to "scgw/{craneid}/cranestatus"
	for {
		var status = models.StackingCraneStatusBase{}
		status = scGateway.stackingCrane.GetStatus()
		bytes, err := json.Marshal(status)
		if err != nil {
			return fmt.Errorf("marshal failed for status :%v", status)
		}
		err = scGateway.mqConn.PublishTo(
			rabbitnamings.L2RouteBase+"/"+status.StackingCraneID+"/"+rabbitnamings.CraneStatusRouteKey,
			1, //qos 1 for persistent messages
			bytes)
		if err != nil {
			log.WithError(err).Fatal("failed to publish sc status.")
			return err
		}
		time.Sleep(interval)
	}
}

//jobHandler consumes new job orders in an idempotent way
func (scGateway *SCGW) jobHandler(client mqtt.Client, msg mqtt.Message) {

	job := models.StackingCraneJobOrderBase{}
	err := json.Unmarshal(msg.Payload(), &job)
	if err != nil {
		log.WithError(err).Errorln("unmarshal job failed, msg", msg)
	}
	log.Debugln("received job:", job)

	err = scGateway.stackingCrane.SetCommand(job)
	if err != nil {
		log.WithError(err).Errorln("failed to set command. Sc status", scGateway.stackingCrane.GetStatus())
		return
	}
	return
}

//statusHandler consumes status events of cranes
func (scGateway *SCGW) statusHandler(client mqtt.Client, msg mqtt.Message) {
	var status = models.StackingCraneStatusBase{}
	err := json.Unmarshal(msg.Payload(), &status)
	if err != nil {
		log.WithError(err).Fatalln("unmarshal status failed, msg", msg)
	}
	//log.Println("received status:", status)
	//if own status
	if scGateway.stackingCrane.GetStatus().StackingCraneID == status.StackingCraneID {
		return
	}
	err = scGateway.stackingCrane.UpdateNeighboringStatus(status)
	if err != nil {
		log.WithError(err).Fatalln("failed to update status", scGateway.stackingCrane.GetStatus())
	}
}
