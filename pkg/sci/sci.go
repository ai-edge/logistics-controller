//Package sci is the root package for scrane installer app.
package sci

import (
	"ecs/pkg/setup/repo/terminal"
	"ecs/tools/kubeclient"
)

//SCI scrane installer
type SCI struct {
	tr  *terminal.Terminal
	kbc *kubeclient.KubeConnection
}

//NewSCI initialize SCI
func NewSCI(tr *terminal.Terminal, kbc *kubeclient.KubeConnection) *SCI {
	sci := &SCI{
		tr:  tr,
		kbc: kbc,
	}
	return sci
}

// GetTerminal returns terminal
func (sci *SCI) GetTerminal() *terminal.Terminal {
	return sci.tr
}

// GetKubeConnection returns kbc
func (sci *SCI) GetKubeConnection() *kubeclient.KubeConnection {
	return sci.kbc
}

//DeInitSCI deinitialize the sci
func (sci *SCI) DeInitSCI() {
	// TODO
}
