package blockcfg

import (
	"ecs/api/generated/models"
	"ecs/internal/dbservice"
	"fmt"
)

//BlockCfg repository for terminal block cfg
type BlockCfg struct {
	BlockID string
	dbNames blockCfgDBNames

	dbs dbservice.DBService
}

type blockCfgDBNames struct {

	//DB names
	blockCfgDB,

	//Collection Names
	stackingFieldsCollectionName,
	transferFieldsCollectionName string
	//queryLimit maximum items to be returned
	queryLimit int64
	//IDField defines the name of the ID field in the DB
	IDField string
}

const (
	compositeLocationNameLength int = 15
)

//NewBlockCfgRepository returns a new repository of a container terminal's block
func NewBlockCfgRepository(dbs dbservice.DBService, blockID string) (*BlockCfg, error) {
	var dbNames blockCfgDBNames

	if dbs == nil {
		return nil, fmt.Errorf("invalid arguments dbs %v", dbs)
	}

	//Block01CfgDB is the name of the block's configuration DB
	dbNames.blockCfgDB = blockID + "-cfg"
	dbNames.stackingFieldsCollectionName = "stackingfields"
	dbNames.transferFieldsCollectionName = "transferfields"

	dbNames.queryLimit = 1000
	dbNames.IDField = "_id"

	blockCfg := BlockCfg{dbs: dbs, BlockID: blockID, dbNames: dbNames}
	return &blockCfg, nil
}

//DeInit closes connection
func (blCfg *BlockCfg) DeInit() {
	blCfg.dbs.DeInit()
}

//GetQueryLimit returns query limit parameter
func (blCfg *BlockCfg) GetQueryLimit() int64 {
	return blCfg.dbNames.queryLimit
}

// GetStackingFields gets all stacking field value objects
func (blCfg *BlockCfg) GetStackingFields(limit int64) ([]models.StackingField, error) {
	var sfields []models.StackingField
	query := map[string]interface{}{}
	err := blCfg.dbs.GetAll(blCfg.dbNames.blockCfgDB, blCfg.dbNames.stackingFieldsCollectionName, query, &sfields, limit)
	return sfields, err
}

// GetTransferFields gets all transfer field value objects
func (blCfg *BlockCfg) GetTransferFields(limit int64) ([]models.TransferField, error) {
	var tfields []models.TransferField
	query := map[string]interface{}{}
	err := blCfg.dbs.GetAll(blCfg.dbNames.blockCfgDB, blCfg.dbNames.transferFieldsCollectionName, query, &tfields, limit)
	return tfields, err
}

/***************************Helpers*****************************/

//GetCompositeName converts given area, bay, row and tier to composite field name
func GetCompositeName(area string, bay string, row string, tier string) (string, error) {
	nameComposite := area + "-" + bay + "-" + row + "-" + tier
	if len(nameComposite) != compositeLocationNameLength {
		return "", fmt.Errorf("invalid name %s", nameComposite)
	}
	return nameComposite, nil
}

//GetCompositeReservationName converts given area, bay, row and tier to composite reservation name
func GetCompositeReservationName(area string, bay string, row string, tier string) (string, error) {
	cname, err := GetCompositeName(area, bay, row, tier)
	if err != nil {
		return "", err
	}
	return "res-" + cname, err
}
