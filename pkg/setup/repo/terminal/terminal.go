package terminal

import (
	"ecs/api/generated/models"
	"ecs/internal/dbservice"
	"fmt"
)

//Terminal repository for terminal
type Terminal struct {
	dbNames terminalDBNames
	dbs     dbservice.DBService
}

type terminalDBNames struct {
	terminalSetupDB,
	OperationsCollectionName,
	TOSGWCollectionName,
	blocksCollectionName,
	resourcesCollectionName,
	ordersSampleDBName,
	createdOrdersCollectionName,
	statusSampleDBName,
	SCGWCollectionName,
	SCSimCollectionName,
	DispCollectionName,
	HtCollectionName,
	SchedulerCollectionName string

	//queryLimit maximum items to be returned
	queryLimit int64
	//IDField defines the name of the ID field in the DB
	IDField string
}

//NewTerminalRepository returns a new repository of a container terminal
func NewTerminalRepository(dbs dbservice.DBService) (*Terminal, error) {
	if dbs == nil {
		return nil, fmt.Errorf("invalid argument dbs %v", dbs)
	}
	var dbNames terminalDBNames

	//terminalSetupDB is the name of the main configuration DB
	dbNames.terminalSetupDB = "terminal-setup"
	dbNames.OperationsCollectionName = "operations"
	dbNames.TOSGWCollectionName = "tosgw"
	dbNames.blocksCollectionName = "blocks"
	dbNames.resourcesCollectionName = "resources"
	dbNames.ordersSampleDBName = "orders-sample"
	dbNames.createdOrdersCollectionName = "createdorders"
	dbNames.statusSampleDBName = "status-sample"
	dbNames.SCGWCollectionName = "scgw"
	dbNames.SCSimCollectionName = "scsim"
	dbNames.DispCollectionName = "dispatchers"
	dbNames.HtCollectionName = "ht"
	dbNames.SchedulerCollectionName = "schedulers"
	dbNames.queryLimit = 1000
	dbNames.IDField = "_id"
	terminal := Terminal{dbs: dbs, dbNames: dbNames}
	return &terminal, nil
}

//DeInit closes connection
func (tr *Terminal) DeInit() {
	tr.dbs.DeInit()
}

//GetQueryLimit returns query limit parameter
func (tr *Terminal) GetQueryLimit() int64 {
	return tr.dbNames.queryLimit
}

//GetTOSGW returns tosgw cfg
func (tr *Terminal) GetTOSGW(serviceid string) (models.Tosgw, error) {
	var ts models.Tosgw
	query := map[string]interface{}{"serviceid": serviceid}
	err := tr.dbs.GetOne(tr.dbNames.terminalSetupDB, tr.dbNames.TOSGWCollectionName, query, &ts)
	return ts, err
}

//GetAllTOSGWServices returns all tosgw services
func (tr *Terminal) GetAllTOSGWServices(limit int64) ([]models.Tosgw, error) {
	var tss []models.Tosgw
	query := map[string]interface{}{}
	err := tr.dbs.GetAll(tr.dbNames.terminalSetupDB, tr.dbNames.TOSGWCollectionName, query, &tss, limit)
	return tss, err
}

//PutGeneratedWorkOrderByResourceID outs a generated work order
func (tr *Terminal) PutGeneratedWorkOrderByResourceID(resourceID string, newWo models.OperationsWorkOrderBase) error {
	err := tr.dbs.InsertOne(tr.dbNames.ordersSampleDBName, tr.dbNames.createdOrdersCollectionName, &newWo)
	return err
}

// GetGeneratedWorkOrderByID returns a work order by order id
func (tr *Terminal) GetGeneratedWorkOrderByID(workorderid string) (models.OperationsWorkOrderBase, error) {
	var workorder models.OperationsWorkOrderBase
	query := map[string]interface{}{"workordername": workorderid}
	err := tr.dbs.GetOne(tr.dbNames.ordersSampleDBName, tr.dbNames.createdOrdersCollectionName, query, &workorder)
	return workorder, err
}

// DeleteGeneratedWorkOrderByID deletes a work order by order id
func (tr *Terminal) DeleteGeneratedWorkOrderByID(workorderid string) error {
	query := map[string]interface{}{"workordername": workorderid}
	err := tr.dbs.DeleteOne(tr.dbNames.ordersSampleDBName, tr.dbNames.createdOrdersCollectionName, query)
	return err
}

// GetGeneratedWorkOrders returns generated work orders
func (tr *Terminal) GetGeneratedWorkOrders(limit int64) ([]models.OperationsWorkOrderBase, error) {
	var workorders []models.OperationsWorkOrderBase
	query := map[string]interface{}{}
	err := tr.dbs.GetAll(tr.dbNames.ordersSampleDBName, tr.dbNames.createdOrdersCollectionName, query, &workorders, limit)
	return workorders, err
}

//GetScranes returns scranes of the entire terminal
func (tr *Terminal) GetScranes(limit int64) ([]models.StackingCrane, error) {
	//get assigned cranes
	var resources []models.StackingCrane
	query := map[string]interface{}{"resourcetype": "RTG"}
	err := tr.dbs.GetAll(tr.dbNames.terminalSetupDB, tr.dbNames.resourcesCollectionName, query, &resources, limit)
	return resources, err
}

//GetScraneByScraneID returns an scrane by id
func (tr *Terminal) GetScraneByScraneID(scraneid string) (models.StackingCrane, error) {
	var scrane models.StackingCrane
	query := map[string]interface{}{
		"$and": []map[string]interface{}{
			map[string]interface{}{"resourcetype": "RTG"},
			map[string]interface{}{"resourceid": scraneid},
		},
	}
	err := tr.dbs.GetOne(tr.dbNames.terminalSetupDB, tr.dbNames.resourcesCollectionName, query, &scrane)
	return scrane, err
}

//GetScranesByBlockID returns scranes of a block
func (tr *Terminal) GetScranesByBlockID(bl string, limit int64) ([]models.StackingCrane, error) {
	var resources []models.StackingCrane
	query := map[string]interface{}{
		"$and": []map[string]interface{}{
			map[string]interface{}{"resourcetype": "RTG"},
			map[string]interface{}{"assignedblock": bl},
		},
	}
	err := tr.dbs.GetAll(tr.dbNames.terminalSetupDB, tr.dbNames.resourcesCollectionName, query, &resources, limit)
	return resources, err
}

// GetAllSCGWServices gets info about all scgw services
func (tr *Terminal) GetAllSCGWServices(limit int64) ([]models.Scgw, error) {
	var scgw []models.Scgw
	query := map[string]interface{}{}
	err := tr.dbs.GetAll(tr.dbNames.terminalSetupDB, tr.dbNames.SCGWCollectionName, query, &scgw, limit)
	return scgw, err
}

// GetSCGWByServiceID gets info about specific scgw service
func (tr *Terminal) GetSCGWByServiceID(serviceid string) (models.Scgw, error) {
	var scgw models.Scgw
	query := map[string]interface{}{"serviceid": serviceid}
	err := tr.dbs.GetOne(tr.dbNames.terminalSetupDB, tr.dbNames.SCGWCollectionName, query, &scgw)
	return scgw, err
}

// PutSCGW puts status info about a specific scgw
func (tr *Terminal) PutSCGW(newScgw models.Scgw) (models.Scgw, error) {
	var scgw models.Scgw
	query := map[string]interface{}{"serviceid": newScgw.ServiceID}
	err := tr.dbs.GetOne(tr.dbNames.terminalSetupDB, tr.dbNames.SCGWCollectionName, query, &scgw)
	if err != nil {
		err = tr.dbs.InsertOne(tr.dbNames.terminalSetupDB, tr.dbNames.SCGWCollectionName, &newScgw)
	} else {
		err = tr.dbs.ReplaceOne(tr.dbNames.terminalSetupDB, tr.dbNames.SCGWCollectionName, query, &newScgw)
	}
	return newScgw, err
}

// DeleteSCGWByServiceID deletes a scgw service
func (tr *Terminal) DeleteSCGWByServiceID(serviceid string) error {
	query := map[string]interface{}{"serviceid": serviceid}
	err := tr.dbs.DeleteOne(tr.dbNames.terminalSetupDB, tr.dbNames.SCGWCollectionName, query)
	return err
}

// GetAllSCSimServices gets info about all scsim services
func (tr *Terminal) GetAllSCSimServices(limit int64) ([]models.ScSim, error) {
	var scsim []models.ScSim
	query := map[string]interface{}{}
	err := tr.dbs.GetAll(tr.dbNames.terminalSetupDB, tr.dbNames.SCSimCollectionName, query, &scsim, limit)
	return scsim, err
}

// GetSCSimByServiceID gets info about specific scsim service
func (tr *Terminal) GetSCSimByServiceID(serviceid string) (models.ScSim, error) {
	var scsim models.ScSim
	query := map[string]interface{}{"serviceid": serviceid}
	err := tr.dbs.GetOne(tr.dbNames.terminalSetupDB, tr.dbNames.SCSimCollectionName, query, &scsim)
	return scsim, err
}

// PutSCSim puts status info about a specific scsim (i.e. creates if it does not exist)
func (tr *Terminal) PutSCSim(newScsim models.ScSim) (models.ScSim, error) {
	var scsim models.ScSim
	query := map[string]interface{}{"serviceid": newScsim.ServiceID}
	err := tr.dbs.GetOne(tr.dbNames.terminalSetupDB, tr.dbNames.SCSimCollectionName, query, &scsim)
	if err != nil {
		err = tr.dbs.InsertOne(tr.dbNames.terminalSetupDB, tr.dbNames.SCSimCollectionName, &newScsim)
	} else {
		err = tr.dbs.ReplaceOne(tr.dbNames.terminalSetupDB, tr.dbNames.SCSimCollectionName, query, &newScsim)
	}
	return newScsim, err
}

// DeleteSCSimByServiceID deletes a scgw service
func (tr *Terminal) DeleteSCSimByServiceID(serviceid string) error {
	query := map[string]interface{}{"serviceid": serviceid}
	err := tr.dbs.DeleteOne(tr.dbNames.terminalSetupDB, tr.dbNames.SCSimCollectionName, query)
	return err
}

// GetOperationsService gets info about an operations service
func (tr *Terminal) GetOperationsService(serviceid string) (models.Operations, error) {
	var operations models.Operations
	query := map[string]interface{}{"serviceid": serviceid}
	err := tr.dbs.GetOne(tr.dbNames.terminalSetupDB, tr.dbNames.OperationsCollectionName, query, &operations)
	return operations, err
}

// GetAllOperationsServices gets info about all operations services
func (tr *Terminal) GetAllOperationsServices(limit int64) ([]models.Operations, error) {
	var operations []models.Operations
	query := map[string]interface{}{}
	err := tr.dbs.GetAll(tr.dbNames.terminalSetupDB, tr.dbNames.OperationsCollectionName, query, &operations, limit)
	return operations, err
}

// GetDispService gets info about a dispatcher services
func (tr *Terminal) GetDispService(serviceid string) (models.Dispatcher, error) {
	var disp models.Dispatcher
	query := map[string]interface{}{"serviceid": serviceid}
	err := tr.dbs.GetOne(tr.dbNames.terminalSetupDB, tr.dbNames.DispCollectionName, query, &disp)
	return disp, err
}

// GetAllDispServices gets info about dispatcher services
func (tr *Terminal) GetAllDispServices(limit int64) ([]models.Dispatcher, error) {
	var disps []models.Dispatcher
	query := map[string]interface{}{}
	err := tr.dbs.GetAll(tr.dbNames.terminalSetupDB, tr.dbNames.DispCollectionName, query, &disps, limit)
	return disps, err
}

// GetAllHtServices gets info about HT services
func (tr *Terminal) GetAllHtServices(limit int64) ([]models.Ht, error) {
	var hts []models.Ht
	query := map[string]interface{}{}
	err := tr.dbs.GetAll(tr.dbNames.terminalSetupDB, tr.dbNames.HtCollectionName, query, &hts, limit)
	return hts, err
}

// GetHtService gets info about the th service
func (tr *Terminal) GetHtService(serviceid string) (models.Ht, error) {
	var ht models.Ht
	query := map[string]interface{}{"serviceid": serviceid}
	err := tr.dbs.GetOne(tr.dbNames.terminalSetupDB, tr.dbNames.HtCollectionName, query, &ht)
	return ht, err
}

// GetSchedulerService gets info about a scheduler service
func (tr *Terminal) GetSchedulerService(serviceid string) (models.Scheduler, error) {
	var sche models.Scheduler
	query := map[string]interface{}{"serviceid": serviceid}
	err := tr.dbs.GetOne(tr.dbNames.terminalSetupDB, tr.dbNames.SchedulerCollectionName, query, &sche)
	return sche, err
}

// GetAllSchedulerServices gets info about scheduler services
func (tr *Terminal) GetAllSchedulerServices(limit int64) ([]models.Scheduler, error) {
	var sches []models.Scheduler
	query := map[string]interface{}{}
	err := tr.dbs.GetAll(tr.dbNames.terminalSetupDB, tr.dbNames.SchedulerCollectionName, query, &sches, limit)
	return sches, err
}

// GetAllBlocks gets block information
func (tr *Terminal) GetAllBlocks(limit int64) ([]models.ContainerBlockComposite, error) {
	var blocks []models.ContainerBlockComposite
	query := map[string]interface{}{}
	err := tr.dbs.GetAll(tr.dbNames.terminalSetupDB, tr.dbNames.blocksCollectionName, query, &blocks, limit)
	return blocks, err
}

// GetBlockWithAreaName gets block information with given area name
func (tr *Terminal) GetBlockWithAreaName(areaName string) (models.ContainerBlockComposite, error) {
	var block models.ContainerBlockComposite
	query := map[string]interface{}{"areaname": areaName}
	err := tr.dbs.GetOne(tr.dbNames.terminalSetupDB, tr.dbNames.blocksCollectionName, query, &block)
	return block, err
}
