```mermaid
graph TD
	A[Scheduler] -->|ActiveSchedules, startingSchedule| B[GenerateInterlockingSchedules]
	B --> C[calculateClaimAreas for activeSchedules]
  C --> F[extractTasks for startingSchedule]
  F --> D[calculateClaimArea for startingSchedule]
  D --> |startingTasks| E[findCollisions]
  E --> |collidingTasks, startingTasks, activeSchedules, startingSchedule| H[insertAvoidanceTasks]
  H --> I[/for each task in startingTasks/]
  I --> J{collisionAvoidanceTask<br/> exists for this task?}
  J --> |yes| K{collision avoidance <br/>added to activeSchedule?}
  J --> |no| M
  K --> |yes| L[create new schedule and insert to schedules]
  K --> |no| M[insert task in startingTasks]
  M --> |until loop finishes| I
  L --> |schedules| N
  M --> |startingSchedule| N[return schedules, startingSchedule]
  
```