```mermaid
classDiagram

abstractPlanningService <|-- TOSGW
abstractPlanningService <|-- ERPGW

abstractOperationsService <|-- Operations
abstractOperationsService <|-- QCCS
abstractOperationsService <|-- fleetManager
abstractOperationsService <|-- personManager
```

```mermaid
classDiagram
abstractOperationsOrder <|-- OperationsWorkOrder
abstractOperationsOrder <|-- QCCSWorkOrder
abstractOperationsOrder <|-- fleetManagerWorkOrder
abstractOperationsOrder <|-- personManagerWorkOrder
```

```mermaid
classDiagram
abstractResourceService <|-- SCGW
abstractResourceService <|-- QCGW
abstractResourceService <|-- AGVGW
abstractResourceService <|-- stradGW
abstractResourceService <|-- personGW
```

```mermaid
classDiagram
abstractResourceOrder <|-- stackingCraneJobOrder
abstractResourceOrder <|-- quayCraneJobOrder
abstractResourceOrder <|-- AGVJobOrder
abstractResourceOrder <|-- stradJobOrder
abstractResourceOrder <|-- personJobOrder
```

```mermaid
classDiagram
abstractResourceStatus <|-- stackingCraneStatus
abstractResourceStatus <|-- quayCraneStatus
abstractResourceStatus <|-- AGVStatus
abstractResourceStatus <|-- stradStatus
abstractResourceStatus <|-- personStatus
```

```mermaid
classDiagram
abstractResource <|-- stackingCrane
abstractResource <|-- quayCrane
abstractResource <|-- cTruck
abstractResource <|-- cAGV
abstractResource <|-- strad
abstractResource <|-- person
```

```mermaid
classDiagram
abstractItem <|-- container
abstractItem <|-- paperRoll
abstractItem <|-- metalCoil
abstractItem <|-- metalSlab

abstractLocation <|-- bayLogicalLocation
abstractLocation <|-- logicalLocation3D
```

```mermaid
classDiagram
abstractField <|-- stackingField
abstractField <|-- transferField


```
