```mermaid








stateDiagram-v2
    [*] --> DISP_IDLE
    DISP_IDLE --> DISP_IDLE :R_IDLE
    DISP_IDLE --> DISP_IDLE : R_MOVING
    DISP_IDLE -->  DISP_ACTIVE : new Dispatch Request
    DISP_IDLE --> DISP_SUSPENDED :R_OFF
    DISP_IDLE --> DISP_SUSPENDED :R_FAULT
    DISP_IDLE --> DISP_SUSPENDED :R_STARTUP

    DISP_ACTIVE --> DISP_SUSPENDED :R_OFF
    DISP_ACTIVE --> DISP_SUSPENDED :R_FAULT
    DISP_ACTIVE --> DISP_SUSPENDED :R_STARTUP
    DISP_ACTIVE --> DISP_IDLE :  dispatch request finished
    
    DISP_SUSPENDED --> DISP_SUSPENDED : R_OFF
    DISP_SUSPENDED --> DISP_SUSPENDED : R_FAULT
    DISP_SUSPENDED --> DISP_SUSPENDED : R_STARTUP
    DISP_SUSPENDED --> DISP_IDLE : R_IDLE

state DISP_IDLE {
[*] --> D_VOID
}

state DISP_ACTIVE {
    [*] --> D_INITIALIZE
    D_INITIALIZE -->  D_INITIALIZE : R_IDLE
    D_INITIALIZE --> D_ABORTED : R_MOVING
    D_INITIALIZE -->  D_READY : safe distance

    D_READY --> D_BLOCKED : if wait task
    D_BLOCKED --> D_BLOCKED :R_IDLE
    D_BLOCKED --> D_ABORTED : R_MOVING
    D_BLOCKED --> D_READY : wait task completed

    D_READY --> D_ENTERED : if resource task <br> send job to resource
    D_ENTERED --> D_ENTERED :R_IDLE
    D_ENTERED  --> D_RUNNING : R_MOVING
    D_RUNNING  --> D_RUNNING : R_MOVING
    D_RUNNING --> D_COMPLETED : R_IDLE with jobnr

    D_RUNNING --> D_ABORTED : R_IDLE with aborted
    D_RUNNING --> D_CANCELED : R_IDLE with canceled

    D_COMPLETED --> D_READY

    D_READY --> D_FINISHED  : if no tasks

    D_FINISHED -->  [*]
    D_ABORTED --> [*] 
    D_CANCELED -->  [*]
}

state DISP_SUSPENDED {
    [*] --> D_FAULT
}

    


```
    

