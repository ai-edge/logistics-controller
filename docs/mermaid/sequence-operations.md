```mermaid
sequenceDiagram
	participant TOSGW
	participant BlocksRepo
	participant Operations
	participant HT
	participant Broker
    participant Scheduler
	participant SchedulesRepo
    participant Dispatcher
	participant SCGW

TOSGW ->> Broker: Publish (OperationsWorkOrderRequest)
Broker -->> Operations: Notify (OperationsWorkOrderRequest)(async)
Operations ->> BlocksRepo: Put (OperationsWorkOrder)
Broker -->> HT: Notify (OperationsWorkOrderRequest)(async)
HT ->> HT: Process WorkOrder and create truck-work iten relation for each item

loop Each WorkOrderItem
    alt TRANSFER IN or OUT
        HT ->> Broker: Publish (htStatus) arriving
        Note right of HT: Each htStatus contains truckid, status
        Broker -->> Scheduler: Notify (htStatus)
	end

	Operations ->> BlocksRepo: Put (WorkOrderItemStatus) as "entered"
	Operations ->> Broker: Publish (OperationsWorkOrderItemStatusEvent)
	Broker -->> TOSGW: Notify (OperationsWorkOrderItemStatusEvent) (async)

	Operations ->> Broker: Publish (ScheduleRequest) for one work item
	Broker -->> Scheduler: Notify (ScheduleRequest)(async)
	Scheduler ->> SchedulesRepo: Put (ScheduleRequest)
	Scheduler ->> Scheduler: Generate DispatchRequests
	Scheduler ->> SchedulesRepo: Put (DispatchRequests)

	Scheduler ->> Broker : Publish (ScheduleStatusEvent) as request queued
	Broker -->> Operations: Notify (ScheduleStatusEvent)(async)
	Operations ->> BlocksRepo: Update WorkOrderItemStatus as "queued"
	Operations ->> Broker: Publish (OperationsWorkOrderItemStatusEvent)
	Broker -->> TOSGW: Notify (OperationsWorkOrderItemStatusEvent) (async)

	loop Each DispatchRequest (until the DispatchRequests are completed)
		alt transfer == IN and moveType == pick OR transfer == OUT and moveType == place
			HT->> Broker : Publish (htStatus) arrived
			Broker -->> Scheduler: Notify (htStatus)
			Scheduler ->> Broker : Publish (FieldStatusUpdate)
			Broker -->> Operations: Notify (FieldStatusUpdate)(async)
		end

        Scheduler ->> Broker : Publish (DispatchRequest)
		Broker -->> Dispatcher: Notify (DispatchRequest)(async)
		Dispatcher ->> Broker: publish (StackingCraneJobOrderBase)

		Broker -->> SCGW: Notify (StackingCraneJobOrderBase) (async)
		Note right of Scheduler: scgw publishes status cyclically
		SCGW ->> Broker: Publish status (stackingCraneStatusEvent)

		Broker -->> Dispatcher: Notify (stackingCraneStatusEvent) (async) as job completed

		Dispatcher ->> Dispatcher: Ack Job on Queue
		Dispatcher ->> Broker: Publish (DispatchStatusEvent) as "completed"

		Broker -->> Scheduler: Notify (DispatchStatusEvent)(async)
		Scheduler ->> SchedulesRepo: Update (DispatchRequest) as "completed"

		alt moveType == place OR moveType == pick
			Note right of Scheduler: Empty crane moves does not update field status
			Scheduler ->> Broker : Publish (ContainerLocationUpdate)
			Broker -->> Operations: Notify (ContainerLocationUpdate)(async)
			Scheduler ->> Broker : Publish (FieldStatusUpdate)
			Broker -->> Operations: Notify (FieldStatusUpdate)(async)
		end

		Broker -->> HT: Notify (DispatchStatus) (async)
		
		alt transfer == IN and moveType == pick OR transfer == OUT and moveType == place
			HT ->> HT: Update (htStatus) removed
			HT ->> Broker: Publish (htStatus)
			Broker -->> Scheduler: Notify (htStatus)(async)
			Scheduler ->> Broker : Publish (FieldStatusUpdate)
			Broker -->> Operations: Notify (FieldStatusUpdate)(async)
		end

	end

	Scheduler ->> SchedulesRepo: Update (ScheduleRequest) as "completed"
	Scheduler ->> Broker : Publish (ScheduleStatusEvent) as ScheduleRequest "completed"
	Broker -->> Operations: Notify (ScheduleStatusEvent)(async)
	Operations ->> BlocksRepo: Update WorkOrderItemStatus as "completed"
	Operations ->> Broker: Publish (OperationsWorkOrderItemStatusEvent)
	Broker -->> TOSGW: Notify (OperationsWorkOrderItemStatusEvent) (async)
end

```
