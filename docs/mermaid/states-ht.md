```mermaid


stateDiagram-v2
    [*] --> HT_ARRIVING
    HT_ARRIVING --> HT_WAITING :timeout, request TZ reservation
    HT_WAITING --> HT_WAITING : reservation failed. Request again.
    HT_WAITING --> HT_ENTERING :TZ reservation confirmed
    HT_WAITING --> HT_FAILED :timeout wait on reservation
    HT_ENTERING --> HT_ARRIVED :timeout
    HT_ARRIVED --> HT_LEAVING :container picked or placed
    HT_ARRIVED --> HT_FAILED :timeout wait on container
    HT_LEAVING --> HT_LEFT :timeout, free TZ

    HT_LEFT --> [*]
    HT_FAILED  --> [*]


    
```
