```mermaid
sequenceDiagram
    participant RFIDReader_Portal
    participant TOSGW
    participant Broker
    participant Operations
    participant SCGW

TOSGW ->> Broker: Publish (OperationsWorkOrderRequest)
Broker -->> Operations: Notify (OperationsWorkOrderRequest)

Operations ->> Broker: Publish (OperationsWorkOrderItemStatusEvent) after starting the workorder
Broker -->> TOSGW: Notify (OperationsWorkOrderItemStatusEvent)

Operations ->> Broker: Publish Pickup Order (stackingCraneJobRequest)
Broker -->> SCGW: Notify Pickup Order (stackingCraneJobRequest)
SCGW ->> Broker: Publish completed pickup (stackingCraneStatusEvent)
Broker -->> Operations: Notify (stackingCraneStatusEvent)

Operations ->> Broker: Publish place order (stackingCraneJobRequest)
Broker -->> SCGW: Notify place order (stackingCraneJobRequest)
SCGW ->> Broker: Publish completed stacking (stackingCraneStatusEvent)
Broker -->> Operations: Notify (stackingCraneStatusEvent)

Operations ->> Broker: Publish completed WorkOrder (CompletedWorkOrder)
Broker -->> TOSGW: Notify (CompletedWorkOrder)

```