```mermaid



stateDiagram-v2
      [*] --> SCHE_READY
    SCHE_READY --> SCHE_READY :DISP_IDLE
    SCHE_READY --> SCHE_READY :DISP_READY
    SCHE_READY --> SCHE_SUSPENDED :D_FAULT
    SCHE_READY --> SCHE_SUSPENDED :DISP_SUSPENDED

    SCHE_SUSPENDED --> SCHE_READY :DISP_IDLE
    SCHE_SUSPENDED --> SCHE_SUSPENDED :DISP_SUSPENDED
    SCHE_SUSPENDED --> SCHE_SUSPENDED :D_FAULT

    state SCHE_READY {
        state SCHEDULE1{
            [*] --> [*]
        }
        --
        state SCHEDULE2{
            [*] --> [*] 
        }
    }

    state SCHE_SUSPENDED {
        [*] --> [*] : abort all executing schedules
    }



```
