```mermaid
sequenceDiagram
    participant RFIDReader_Portal
    participant TOSGW
    participant Broker
    participant Operations
    participant SCGW

TOSGW ->> Broker: Publish (OperationsWorkOrderRequest)
Broker -->> Operations: Notify (OperationsWorkOrderRequest)

Operations ->> Operations: Update internal WorkOrder Queue
RFIDReader_Portal->> Broker: Publish cTruck Information(TBD)
Broker -->> Operations: Notify cTruck Information (TBD)
Broker -->> TOSGW: Notify cTruck Information (TBD)

Operations ->> Operations: Process WorkOrder List based on prioritization and optimization criteria
Operations ->> Broker: Publish (OperationsWorkOrderItemStatusEvent) after starting the workorder
Broker -->> TOSGW: Notify (OperationsWorkOrderItemStatusEvent)


Operations ->> Broker: Publish Gantry Move Order (stackingCraneJobRequest)
Broker -->> SCGW: Notify Gantry Move Order (stackingCraneJobRequest)
SCGW ->> Broker: Publish location cyclically (stackingCraneStatusEvent)
Broker -->> Operations: Notify (stackingCraneStatusEvent)
SCGW ->> Broker: Publish stoppage location (stackingCraneStatusEvent)
SCGW ->> Broker: Publish scanned cTruckID (stackingCraneStatusEvent)
Broker -->> Operations: Notify (stackingCraneStatusEvent)

Operations ->> Broker: Publish Pickup Order (stackingCraneJobRequest)
Broker -->> SCGW: Notify Pickup Order (stackingCraneJobRequest)
SCGW ->> Broker: Publish completed pickup (stackingCraneStatusEvent)
Broker -->> Operations: Notify (stackingCraneStatusEvent)

Operations ->> Broker: Publish place order (stackingCraneJobRequest)
Broker -->> SCGW: Notify place order (stackingCraneJobRequest)
SCGW ->> Broker: Publish completed stacking (stackingCraneStatusEvent)
Broker -->> Operations: Notify (stackingCraneStatusEvent)

Operations ->> Broker: Publish completed WorkOrder (CompletedWorkOrder)
Broker -->> TOSGW: Notify (CompletedWorkOrder)

```