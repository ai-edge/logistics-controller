```mermaid
classDiagram

class Block
<<aggregate>> Block
Block: areaName

class StackingFieldStatus
<<entity>> StackingFieldStatus
class StackingField
<<value>> StackingField
StackingFieldStatus--StackingField
StackingFieldStatus : Status
StackingField : areaName
StackingField : LogicalLocation
StackingField : Dimensions

class TransferFieldStatus
<<entity>> TransferFieldStatus
class TransferField
<<value>> TransferField
TransferFieldStatus--TransferField
TransferFieldStatus : Status
TransferField : areaName
TransferField : LogicalLocation
TransferField : Dimensions

class Container
<<entity>> Container

class ScraneStatus
<<entity>> ScraneStatus
class Scrane
<<value>>Scrane
ScraneStatus--Scrane
ScraneStatus : CraneMainAxesPosition
ScraneStatus : JobOrder
Scrane : NominalSpeeds
Scrane : scraneID
Scrane : assignedBlock

Block *-- StackingFieldStatus
Block*-- TransferFieldStatus

Block*-- Container
Container : UUID
Container : Dimensions
Container : LogicalLocation
Container : Offset

Block*-- ScraneStatus

```
